//
//  GroomlyTests.swift
//  GroomlyTests
//
//  Created by clar3 on 30/04/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import XCTest
@testable import Groomly

class ImpartialCharacter {
    static let shared = ImpartialCharacter()
    private(set) var getClientID: Int?
    func set(id: Int) {
        guard getClientID == nil else { return }
        getClientID = id
    }
}

class NetworkTests: XCTestCase {
    
    var networker: Networker!
    var clientID: Int = 1
    
    func randomPhotos(limit: Int = 30, completion: @escaping ([RandomPhoto]) -> Void) {
        guard let url = URL(string: "https://picsum.photos/v2/list?page=8&limit=\(limit)") else { completion([]); return }
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data else { completion([]); return }
            let jsonDecoder = JSONDecoder()
            do {
                let parsedJSON = try jsonDecoder.decode([RandomPhoto].self, from: data)
                completion(parsedJSON)
            }
            catch {
                print(error)
                completion([])
            }
        }.resume()
    }


    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        var hostURL: String? = Bundle.main.object(forInfoDictionaryKey: "TestServerURL") as? String
        if hostURL == "" { hostURL = nil }
        networker = Networker(hostURL)
        
        let _ = try networker.authorize(user: customerCreds()).wait()
        let resp = try networker.clientList().wait()
        if let out = resp.first?.id {
            ImpartialCharacter.shared.set(id: out)
        }
        clientID = ImpartialCharacter.shared.getClientID ?? 1
    }
    
    func customerCreds() -> LoginRequest {
        return LoginRequest(email: "fake2@gmail.com", password: "strongpword")
    }
    
    func clientCreds() -> LoginRequest {
        return LoginRequest(email: "fake@gmail.com", password: "strongpword")
    }
    
    func fail(_ error: Error,_ message: String = "") {
        if case let NetworkReqError.Detail(data) = error {
            print("REFRESH \(String(data: data, encoding: .utf8)!)")
        }
        XCTFail("\(message) \(error.localizedDescription)")
    }

    func _testLogin() throws {
        do {
            let _ = try networker.authorize(user: customerCreds()).wait()
            print("LOGIN #1")
            let _ = try networker.clientList().wait()
            print("TEST GET")
            XCTAssertNotNil(networker.cachedLogin)
            let loginDetails = networker.cachedLogin!
            sleep(5)
            print("ATTEMPT REGET")
            let _ = try networker.clientList().wait()
            print("TESTING ACCESS")
            XCTAssertNotNil(networker.cachedLogin)
            XCTAssertNotEqual(loginDetails.access, networker.cachedLogin!.access)
            sleep(6)
            print("TESTING REFRESH")
            let _ = try networker.clientList().wait()
            XCTAssertNotNil(networker.cachedLogin)
            XCTAssertNotEqual(loginDetails.refresh, networker.cachedLogin!.refresh)
        } catch {
            XCTFail("LOGIN FAILED: \(error.localizedDescription)")
        }
    }
    
    func _testLogout() throws {
        do {
            let _ = try networker.authorize(user: customerCreds()).wait()
            XCTAssertNotNil(networker.cachedLogin)
            let prevCreds = networker.cachedLogin!
            let _ = try networker.logout().wait()
            XCTAssertNil(networker.cachedLogin)
            XCTAssertNil(networker.loginReq)
            sleep(6) // wait for access token to invalidate
            networker.cachedLogin = prevCreds
            let _ = try networker.clientList().wait()
            XCTFail("SAME CREDENTIALS WORKED AFTER LOGGING OUT")
        } catch NetworkReqError.Authorization {
            print("SUCCESFULLY LOGGET OUT: FAILED TO USE SAME CREDENTIALS")
        } catch {
            XCTFail("TEST FAILED: \(error.localizedDescription)")
        }
    }
    
    func testLoginLogout() throws {
        try _testLogin()
        try _testLogout()
    }

    // ALL FOLLOWING API TESTS ARE JUST TO CHECK THAT THE CODABLE MODELS ARE CORRECT!
    func testGetServices() throws {
        let _ = try networker.authorize(user: customerCreds()).wait()
        let resp = try networker.getServices(for: clientID).wait()
        print(resp)
    }
    func testClientList() throws {
        let _ = try networker.authorize(user: customerCreds()).wait()
        let _ = try networker.clientList().wait()
    }
    func testCreateUser() throws {
        let user = CustomerModel(
            email: "boom@gmail.com",
            password: "strongpword",
            primary_address: AddressNetModel(
                address: "33 Loyds Close",
                city: "Abingdon",
                postal_code: "OX141XR"
            ),
            first_name: "Harry",
            last_name: "Smith",
            phone_number: "+447493327774"
        )
        let resp = try networker.create(customer: user).wait()
        print(resp)
    }
    func testGetBooking() throws {
        let _ = try networker.authorize(user: customerCreds()).wait()
        let _ = try networker.get(booking: GetModelRequest(id: 1)).wait()
    }
    func testDeleteBooking() throws {
        let _ = try networker.authorize(user: customerCreds()).wait()
        let resp = try networker.delete(booking: GetModelRequest(id: 2)).wait()
        print(resp)
    }
    func testSaveClientProfile() throws {
        do {
            let _ = try networker.authorize(user: clientCreds()).wait()
            let services = try networker.getServices().wait().services.map { cat -> ServicePriceModel in
                let str = "01:10:00"
                let date = DateFormatter.netTime.date(from: str)!
                return ServicePriceModel(id: cat.key, price: 10.0, duration: date)
            }
            let profile = ClientProfileUpdate(description: "HELLO!", matrix: services)
            let resp = try networker.saveClient(profile: profile).wait()
            print(resp)
            let _ = try networker.authorize(user: customerCreds()).wait()
            let tworesp = try networker.getClient(profile: GetClientProfileRequest(clientID: self.clientID)).wait()
            print(tworesp)
        } catch {
            fail(error)
        }
    }
    func testAddToPortfolio() throws {
        do {
            let _ = try networker.authorize(user: clientCreds()).wait()
            let _ = try networker.getProfile().wait()
            print("GOT PROFILE")
            let promise = networker.worker.eventLoop.newPromise(Void.self)
            randomPhotos(limit: 1) { (photos) in
                guard
                    let photo = photos.first,
                    let image = URL(string: "\(photo.download_url).jpg"),
                    let data = try? Data(contentsOf: image)
                else { promise.fail(error: NetworkReqError.InvalidParams); return }
                let req = PortfolioPostRequest(
                    caption: "Bye", image: data, type: "jpg"
                )
                print("ATTEMPTING TO UPLOAD")
                self.networker.portfolio(post: req) { progress in
                    print("progress: \(progress.fractionCompleted)")
                }.do { _ in
                    promise.succeed()
                }.catch {
                    promise.fail(error: $0)
                }
            }
            let _ = try promise.futureResult.wait()
        } catch {
            fail(error)
        }
    }
    func testGetUserDiary() throws {
        let _ = try networker.authorize(user: customerCreds()).wait()
        let _ = try networker.getUserDiary(for: GetUserDiary(month: 2, year: 2020)).wait()
    }
    func testClientAvailability() throws {
        guard   let start = Calendar.autoupdatingCurrent.date(byAdding: .hour, value: 6, to: Date.tomorrow()),
                let end = Calendar.autoupdatingCurrent.date(byAdding: .hour, value: 2, to: start)
        else { XCTFail(); return }
        do {
            let _ = try networker.authorize(user: clientCreds()).wait()
            let blocks = [
                AvailableBlockModel(start: start, end: end)
            ]
            let avail = UpdateClientAvailability(date: start, blocks: blocks)
            let resp = try networker.saveClient(availability: avail).wait()
            print(resp)
            let _ = try networker.authorize(user: customerCreds()).wait()
            let tworesp = try networker.getClient(availability: GetClientAvailability(
                clientID: self.clientID,
                date: start
            )).wait()
            print(tworesp)
        } catch {
            fail(error)
        }
    }
    
    func testGetAddresses() throws {
        let _ = try networker.authorize(user: customerCreds()).wait()
        let addresses = try networker.getAddresses().wait()
        XCTAssertTrue(addresses.count == 1)
    }
    func _testAddAddress() throws -> AddressModel {
        let _ = try networker.authorize(user: customerCreds()).wait()
        let address = AddressModel(
            addressOne: "Unit C, Crondall Place",
            addressTwo: "Coxbridge Business Park",
            city: "Farnham",
            postcode: "GU10 5EH"
        )
        let model = try networker.create(address: address).wait()
        XCTAssertTrue(model.count == 2)
        XCTAssertTrue(model[1].addressOne == "Unit C, Crondall Place")
        return model[1]
    }
    func testAddAddress() throws {
        let _ = try _testAddAddress()
    }
    func testSelectAddress() throws {
        let model = try _testAddAddress()
        let addresses = try networker.getAddresses().wait()
        let previous = addresses.first { $0.id != model.id }
        let _ = try networker.select(address: previous!)
    }
    func testDeleteAddress() throws {
        let _ = try networker.authorize(user: customerCreds()).wait()
        let addresses = try networker.getAddresses().wait()
        let _ = try networker.delete(address: addresses.first!)
    }
}

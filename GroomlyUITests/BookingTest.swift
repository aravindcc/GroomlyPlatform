//
//  BookingTest.swift
//  BookingTest
//
//  Created by Ravi on 07/08/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import XCTest

class BookingTest: XCTestCase {
    
    let customerEmail = "customer@gmail.com"
    let clientEmail = "client@gmail.com"
    let userPassword = "strongpword"
    let matchDescription = "Corporis asperiores ducimus mollitia voluptatibus. Quo fugit repellat ad dolorem soluta."

    override func setUpWithError() throws {
        continueAfterFailure = false
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func _login(customer: Bool, zoomToBooking: Int? = nil) -> XCUIApplication {
        let app = XCUIApplication()
        app.launchArguments += ["UI-Testing"]
        if let url = Bundle(for: BookingTest.self).object(forInfoDictionaryKey: "TestServerURL") as? String {
            app.launchEnvironment["TestServerURL"] = url
        }
        if let date = Bundle(for: BookingTest.self).object(forInfoDictionaryKey: "ClampDate") as? String, date != "" {
            print("RECEIVED DATE")
            app.launchEnvironment["ClampDate"] = date
        }
        if let zoomToBooking = zoomToBooking {
            app.launchArguments += ["UI-Testing-Booking"]
            print("LOADING BOOKING ID \(zoomToBooking)")
            app.launchEnvironment["BookingID"] = "\(zoomToBooking)"
        }
        app.launch()
        app.login(customer: customer)
        sleep(1)
        return app
    }
    
    func _client() -> XCUIApplication {
        let app = _login(customer: true)
        let scrollViewsQuery = app.scrollViews
        scrollViewsQuery.children(matching: .other).element(boundBy: 0).images.firstMatch.tap()
        
        app.cells.matching(identifier: "ProCellID").element.waitForExistence(timeout: 5)
        let count = app.cells.matching(identifier: "ProCellID").count
        var found = false
        for i in 0..<count {
            let cell = app.cells.matching(identifier: "ProCellID").element(boundBy: i)
            cell.tap()
            sleep(1)
            if app.staticTexts["proviewdesc"].label == matchDescription {
                found = true
                break
            }
            app.images["proviewimage"].gentleSwipe(.Down, adjustment: 2)
        }
        XCTAssert(found)
        app.otherElements["BookButton"].tap()
        return app
    }
    
    func _services() -> XCUIApplication {
        let app = _client()
        app.collectionViews["priceListCV"].children(matching: .any).element(boundBy: 3).tap(wait: 5)
        app.otherElements["confirmNotification"].staticTexts["ok"].tap(wait: 4)
        let note = app.otherElements["notification"]
        XCTAssert(note.waitForExistence(timeout: 2))
        note.tap()
        let next = app.staticTexts["choose time"]
        XCTAssert(next.waitForExistence(timeout: 4))
        XCTAssert(!app.staticTexts["No Services Selected"].waitForExistence(timeout: 1))
        next.tap()
        return app
    }
    
    func _booking(card: StripeTestCards) -> XCUIApplication {
        let app = _services()
        app.staticTexts[" pay"].tap(wait: 5)
        card.addCard(app: app)
        app.otherElements["rvcprocess"].staticTexts["pay"].tap(wait: 3)
        app.otherElements["confirmNotification"].staticTexts["ok"].tap(wait: 4)
        if !card.authRequired {
            XCTAssert(app.otherElements["notification"].waitForExistence(timeout: 5))
        }
        sleep(6)
        return app
    }

    func testNormalBooking() throws {
        let app = _booking(card: .normal)
        XCTAssert(app.otherElements["homePageView"].waitForExistence(timeout: 10))
    }
    
    func testAuthBooking() throws {
        let app = _booking(card: .auth)
        XCTAssert(!app.staticTexts[" pay"].waitForExistence(timeout: 3))
        app.otherElements["confirmNotification"].staticTexts["ok"].tap(wait: 4)
        app.performSequence(asCustomer: true, start: .RESOLVING, action: nil, end: .REQUESTED, interimBlock: {
            StripeTestCards.authCard(app: app)
        }, withLogin: false)
    }
    
    func testAuthPreBooking() throws {
        let app = _booking(card: .allAuth)
        XCTAssert(!app.staticTexts[" pay"].waitForExistence(timeout: 3))
    }
    
    func testAuthLaterFailPreBooking() throws {
        let app = _booking(card: .authFails)
        XCTAssert(!app.staticTexts[" pay"].waitForExistence(timeout: 3))
    }
    
    func testAuthLaterFailBooking() throws {
        let app = _booking(card: .authFails)
        XCTAssert(!app.staticTexts[" pay"].waitForExistence(timeout: 3))
        app.otherElements["confirmNotification"].staticTexts["ok"].tap(wait: 4)
        app.performSequence(asCustomer: true, start: .RESOLVING, action: nil, end: .RESOLVING, interimBlock: {
            StripeTestCards.authCard(app: app)
            XCTAssert(app.otherElements["rvcprocess"].waitForExistence(timeout: 10))
            app.otherElements["rvcprocess"].gentleSwipe(.Down, adjustment: 1.0)
        }, withLogin: false)
    }
    
    func testBookingCancel() throws {
        let app = _booking(card: .normal)
        XCTAssert(!app.staticTexts[" pay"].waitForExistence(timeout: 3))
        app.terminate()
        let app2 = _login(customer: true, zoomToBooking: 1)
        app2.performSequence(
            asCustomer: true,
            start: .REQUESTED,
            action: .customerCancel,
            end: .CANCELLED,
            checkCharged: false,
            withLogin: false
        )
    }

}

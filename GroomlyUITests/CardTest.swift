//
//  CardTest.swift
//  GroomlyUITests
//
//  Created by Ravi on 19/08/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import XCTest

extension XCUIElement {
    func tap(wait: Double, fail: Bool = true) {
        if fail {
            XCTAssert(waitForExistence(timeout: wait))
            tap()
        } else if waitForExistence(timeout: wait) {
            tap()
        }
    }
}

enum StripeTestCards: String, CaseIterable {
    case normal = "4242424242424242"
    case auth = "4000002500003155"
    case allAuth = "4000002760003184"
    case authFails = "4000008260003178"
    
    var last: String {
        String(rawValue.suffix(4))
    }
    var authRequired: Bool {
        self != .normal
    }
    
    func addCard(app: XCUIApplication) {
        app.otherElements["rvcprocess"].staticTexts["new"].tap(wait: 2)
        app.textFields["4242424242424242"].enter(text: rawValue, wait: 2)
        app.textFields["MM/YY"].enter(text: "1030")
        app.textFields["CVC"].enter(text: "434")
        app.buttons["Done"].tap()
        app.staticTexts["Add"].tap(wait: 1.5)
        if authRequired {
            StripeTestCards.authCard(app: app)
            XCTAssert(app.otherElements["rvcprocess"].waitForExistence(timeout: 10))
        }
        XCTAssert(app.otherElements["notification"].staticTexts["Added payment method"].waitForExistence(timeout: 5))
        XCTAssert(app.otherElements["rvcprocess"].staticTexts["Visa Ending In \(last)"].waitForExistence(timeout: 2))
    }
    
    static func authCard(app: XCUIApplication) {
        let webViewsQuery = app.webViews.element(boundBy: 0)
        XCTAssert(webViewsQuery.waitForExistence(timeout: 10))
        webViewsQuery.buttons["COMPLETE AUTHENTICATION"].tap(wait: 10)
        app.buttons["Close"].tap(wait: 5)
    }
}

class CardTest: XCTestCase {
    override func setUpWithError() throws {
        continueAfterFailure = false
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func setup(withAPIKey: Bool = false) -> XCUIApplication {
        let app = XCUIApplication()
        app.launchArguments += ["UI-Testing"]
        if let url = Bundle(for: CardTest.self).object(forInfoDictionaryKey: "TestServerURL") as? String, url != "" {
            app.launchEnvironment["TestServerURL"] = url
        }
        app.launch()
        return app
    }
    
    func testAddStandardCard() {
        let app = setup()
        app.login(customer: true, overrideEmail: variable("clientSignupEmail"))
        app.buttons["profileCustomerButton"].tap(wait: 5)
        app.otherElements["embeddedCardView"].staticTexts["edit"].tap()
        StripeTestCards.normal.addCard(app: app)
    }
}

//
//  CalendarTest.swift
//  GroomlyUITests
//
//  Created by Ravi on 08/08/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import XCTest

enum BookingState: Int, Codable, CustomStringConvertible {
    case REQUESTED = 1
    case CONFIRMED = 2
    case COMPLETED = 3
    case RESOLVING = 4
    case CANCELLED = 5
    case PAID = 6
    case REFUNDED = 7
    
    var description: String {
        switch  self {
        case .CANCELLED: return "cancelled"
        case .COMPLETED: return "completed"
        case .CONFIRMED: return "confirmed"
        case .REQUESTED: return "requested"
        case .RESOLVING: return "resolving"
        case .PAID: return "paid"
        case .REFUNDED: return "refunded"
        }
    }
}

enum BookingActions {
    case customerCancel
    case clientCancel
    case accept
    case resolveRequest
    case changeTime
    case resolveTime
    case resolvePayment
    case review
    case report
    case confirm
    case clientReport
    
    var rawValue: String {
        switch self {
        case .customerCancel: return "cancel"
        case .clientCancel: return "cancel"
        case .accept: return "accept"
        case .resolveRequest: return "unavailable"
        case .changeTime: return "change time"
        case .resolveTime: return "submit time"
        case .review: return "review"
        case .report: return "report"
        case .confirm: return "confirm"
        case .clientReport: return "report"
        case .resolvePayment: return "finish payment"
        }
    }
}

class TestConstants {
    static let customerEmail = "customer@gmail.com"
    static let clientEmail = "client@gmail.com"
    static let userPassword = "strongpword"
    static let matchDescription = "Corporis asperiores ducimus mollitia voluptatibus. Quo fugit repellat ad dolorem soluta."
}

extension XCUIApplication {
    func performSequence(
        asCustomer customer: Bool,
        start startState: BookingState,
        action actionString: BookingActions? = nil,
        end endState: BookingState? = nil,
        checkCharged: Bool? = nil,
        interimBlock: (() -> Void)? = nil,
        withLogin: Bool = true
    ) {
        if withLogin {
            login(customer: customer)
        }
        
        let state = otherElements["bookingState"]
        XCTAssert(state.waitForExistence(timeout: 8))
        XCTAssert(state.staticTexts.firstMatch.label == startState.description)
        
        if let actionString = actionString {
            staticTexts[actionString.rawValue].tap()
            
            let notification = otherElements["confirmNotification"]
            if notification.waitForExistence(timeout: 2) {
                notification.staticTexts["ok"].tap()
            }
        }
        if let interimBlock = interimBlock {
            interimBlock()
        } else {
            sleep(5)
        }
        if let endState = endState {
            XCTAssert(state.waitForExistence(timeout: 10))
            XCTAssert(state.staticTexts.firstMatch.label == endState.description)
        }
        if let checkCharged = checkCharged {
            let label = staticTexts["stateExplanation"].label
            let charge = checkCharged == label.contains("charged")
            XCTAssert(label.contains("cancelled") && charge)
        }
        logout()
    }
    func logout() {
        buttons["settingsButton"].tap()
        XCTAssert(staticTexts["Logout"].waitForExistence(timeout: 1))
        staticTexts["Logout"].tap()
    }
    func login(customer: Bool, overrideEmail: String? = nil, overridePassword: String? = nil) {
        let email = textFields["Email*"]
        let password = secureTextFields["Password*"]
        email.tap()
        email.typeText(overrideEmail ?? (customer ? TestConstants.customerEmail : TestConstants.clientEmail))
        password.tap()
        password.typeText(overridePassword ?? TestConstants.userPassword)
        buttons["Done"].tap()
        otherElements["LoginButton"].tap()
    }
}

class CalendarTest: XCTestCase {
    

    override func setUpWithError() throws {
        continueAfterFailure = false
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func setup() -> XCUIApplication {
        let app = XCUIApplication()
        app.launchArguments += ["UI-Testing"]
        app.launchArguments += ["UI-Testing-Booking"]
        if let url = Bundle(for: CalendarTest.self).object(forInfoDictionaryKey: "TestServerURL") as? String {
            app.launchEnvironment["TestServerURL"] = url
        }
        if let id = Bundle(for: CalendarTest.self).object(forInfoDictionaryKey: "BookingID") as? String {
            print("LOADING BOOKING ID \(id)")
            app.launchEnvironment["BookingID"] = id
        }
        app.launch()
        return app
    }
    
    
    
    func testConfirmBooking() {
        let app = setup()
        app.performSequence(asCustomer: true, start: .REQUESTED)
        app.performSequence(asCustomer: false, start: .REQUESTED, action: .accept, end: .CONFIRMED)
    }
    
    func testCancelBooking() {
        let app = setup()
        app.performSequence(asCustomer: true, start: .REQUESTED, action: .customerCancel, end: .CANCELLED, checkCharged: false)
    }
    
    func testResolveCycle() {
        let app = setup()
        app.performSequence(asCustomer: false, start: .REQUESTED, action: .resolveRequest, end: .RESOLVING)
        app.performSequence(asCustomer: true, start: .RESOLVING, action: .resolveTime, end: .REQUESTED)
        app.performSequence(asCustomer: false, start: .REQUESTED, action: .clientCancel, end: .CANCELLED, checkCharged: false)
        app.performSequence(asCustomer: true, start: .CANCELLED, checkCharged: false)
    }
    
    func testResolveCycleWithConfirmCancelClient() {
        let app = setup()
        app.performSequence(asCustomer: false, start: .REQUESTED, action: .resolveRequest, end: .RESOLVING)
        app.performSequence(asCustomer: true, start: .RESOLVING, action: .resolveTime, end: .REQUESTED)
        app.performSequence(asCustomer: false, start: .REQUESTED, action: .accept, end: .CONFIRMED)
        app.performSequence(asCustomer: false, start: .CONFIRMED, action: .clientCancel, end: .CANCELLED, checkCharged: true)
        app.performSequence(asCustomer: true, start: .CANCELLED, checkCharged: false)
    }
    
    func testResolveCycleWithConfirmCancelCustomer() {
        let app = setup()
        app.performSequence(asCustomer: false, start: .REQUESTED, action: .resolveRequest, end: .RESOLVING)
        app.performSequence(asCustomer: true, start: .RESOLVING, action: .resolveTime, end: .REQUESTED)
        app.performSequence(asCustomer: false, start: .REQUESTED, action: .accept, end: .CONFIRMED)
        app.performSequence(asCustomer: true, start: .CONFIRMED, action: .customerCancel, end: .CANCELLED, checkCharged: true)
        app.performSequence(asCustomer: false, start: .CANCELLED, checkCharged: false)
    }
    
    func testResolveCycleWithConfirmNoSwitch() {
        let app = setup()
        app.performSequence(asCustomer: false, start: .REQUESTED, action: .accept, end: .CONFIRMED)
        app.performSequence(asCustomer: false, start: .CONFIRMED, action: .resolveRequest, end: .RESOLVING)
        app.performSequence(asCustomer: true, start: .RESOLVING, action: .resolveTime, end: .REQUESTED)
        app.performSequence(asCustomer: false, start: .REQUESTED, action: .clientCancel, end: .CANCELLED, checkCharged: true)
        app.performSequence(asCustomer: true, start: .CANCELLED, checkCharged: false)
    }
    
    func testCompletedToPaid() {
        let app = setup()
        app.performSequence(asCustomer: false, start: .COMPLETED, action: .confirm, end: .PAID)
        app.performSequence(asCustomer: true, start: .PAID)
    }
}

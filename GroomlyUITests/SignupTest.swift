//
//  SignupTest.swift
//  GroomlyUITests
//
//  Created by Ravi on 19/08/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import XCTest

extension XCUIElementQuery {
    func by(havingLabel label: String, usePlaceHolder: Bool = false, block: @escaping (XCUIElement) -> Void) -> XCUIFormSection {
        XCUIFormSection(element: {
            self.by(havingLabel: label, usePlaceHolder: usePlaceHolder, timeout: $0)
        }, onFind: block)
    }
    
    func by(havingLabel label: String, usePlaceHolder: Bool = false, timeout: Int) -> XCUIElement? {
        for _ in 0..<3 { // retries
            for i in 0..<self.count {
                let elem = self.element(boundBy: i)
                let value = usePlaceHolder ? elem.placeholderValue : elem.label
                if value?.lowercased().contains(label.lowercased()) ?? false {
                    return elem
                }
            }
            usleep(1000000 * UInt32(timeout / 3))
        }
        return nil
    }
}

class XCUIForm {
    let sections: [XCUIFormSection]
    init(sections: [XCUIFormSection]) {
        self.sections = sections
    }
    
    func begin(at: Int) {
        let section = sections[at]
        if let object = section.elementFinder(4) {
            section.onFind(object)
        }
        sleep(3)
        quickScan(from: at + 1)
    }
    
    func quickScan(from index: Int = 0) {
        guard index < sections.count else { return }
        for (i, section) in sections[index...].enumerated() {
            let time = i == 0 ? 6 : 2
            if section.elementFinder(time) != nil {
                begin(at: i + index)
            }
        }
    }
}

class XCUIFormSection {
    let onFind: (XCUIElement) -> Void
    let elementFinder: (Int) -> XCUIElement?
    init(element: @escaping (Int) -> XCUIElement?, onFind: @escaping (XCUIElement) -> Void) {
        self.onFind = onFind
        self.elementFinder = element
    }
}

extension XCUIElement {
    func enter(text: String, wait: Double? = nil) {
        if let wait = wait {
            XCTAssert(waitForExistence(timeout: wait))
        }
        tap()
        typeText(text)
    }
    
    func section(_ block: @escaping (XCUIElement) -> Void) -> XCUIFormSection {
        XCUIFormSection(element: {
            self.waitForExistence(timeout: Double($0)) ? self : nil
        }, onFind: block)
    }
    
    func hackType(_ text: String) {
        for x in text.capitalized {
            keys[String(x)].tap()
        }
    }
    
    fileprivate func enter(text: String, forField field: FieldNum) {
        let field = field.isSecure ? secureTextFields[field.display] : textFields[field.display]
        field.tap()
        field.tap()
        XCTAssert(keyboards.firstMatch.waitForExistence(timeout: 5))
        field.enter(text: text)
        buttons["Done"].tap()
    }
}

fileprivate enum FormState: CaseIterable {
    case name, address, password

    var fieldSet: [FieldNum] {
        switch self {
        case .name: return [.email, .first_name, .last_name, .phone_number, .company]
        case .address: return [.addressOne, .addressTwo, .city, .postal_code]
        case .password: return [.password, .confirmPassword]
        }
    }
    
    var leftButtonTitle: String {
        switch self {
        case .address, .name: return "Next"
        case .password: return "Submit"
        }
    }
}
fileprivate enum FieldNum: String, CaseIterable {
    case email = "Email"
    case first_name = "First Name"
    case last_name = "Last Name"
    case phone_number = "Phone Number"
    case company = "Company"
    case addressOne = "Address"
    case addressTwo = "Address 2"
    case city = "City"
    case postal_code = "Postcode"
    case password = "Password"
    case confirmPassword = "Confirm Password"
    
    var isSecure: Bool {
        self == .password || self == .confirmPassword
    }
    var isNotRequired: Bool {
        self == .addressTwo || self == .company
    }
    var display: String {
        "\(rawValue)\(isNotRequired ? "" : "*")"
    }
}

extension XCTestCase {
    func variable(_ key: String) -> String? {
        if let url = Bundle(for: BookingTest.self).object(forInfoDictionaryKey: key) as? String, url != "" {
            return url
        }
        return nil
    }
}


class SignupTest: XCTestCase {
    override func setUpWithError() throws {
        continueAfterFailure = false
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    
    
    func setup(withAPIKey: Bool = false) -> XCUIApplication {
        let app = XCUIApplication()
        app.launchArguments += ["UI-Testing"]
        if let url = variable("TestServerURL") {
            app.launchEnvironment["TestServerURL"] = url
        }
        if withAPIKey, let url = variable("apiKey") {
            app.launchEnvironment["apiKey"] = url
        }
        app.launch()
        return app
    }
    
    func testSignupCustomer() {
        let app = setup()
        app.staticTexts["Sign Up"].tap()
        let inputData: [FieldNum: String] = [
            .email: "test@new.com",
            .first_name: "hello",
            .last_name: "omg",
            .phone_number: "+447493327774",
            .addressOne: "33 Loyd Close",
            .city: "Abingdon",
            .postal_code: "OX14 1XR",
            .password: "wrap132Q!",
            .confirmPassword: "wrap132Q!"
        ]
        for section in FormState.allCases {
            for field in section.fieldSet {
                if let data = inputData[field] {
                    app.enter(text: data, forField: field)
                }
            }
            app.staticTexts[section.leftButtonTitle].tap()
        }
        
        XCTAssert(app.otherElements["homePageView"].waitForExistence(timeout: 10))
    }
    
    func testSignupClient() {
        let app = setup(withAPIKey: true)
        
        if variable("OnlyBoard") != nil {
            app.login(customer: false, overrideEmail: variable("clientSignupEmail"))
            _testSetupConnectedAccount(app: app)
            return
        }
        
        
        let givenClientEmail = variable("clientSignupEmail")
        let inputData: [FieldNum: String] = [
            .email: givenClientEmail ?? "barber-test@client.com",
            .first_name: "hello",
            .last_name: "omg",
            .company: "cc",
            .phone_number: "+447493327774",
            .addressOne: "33 Loyd Close",
            .city: "Abingdon",
            .postal_code: "OX14 1XR",
            .password: "wrap132Q!",
            .confirmPassword: "wrap132Q!"
        ]
        for section in FormState.allCases {
            for field in section.fieldSet {
                if let data = inputData[field] {
                    app.enter(text: data, forField: field)
                }
            }
            app.staticTexts[section.leftButtonTitle].tap()
        }
        
        _testSetupConnectedAccount(app: app)
    }
    
    
    func _testSetupConnectedAccount(app: XCUIApplication) {
        let webViewsQuery = app.webViews.element(boundBy: 0)
        XCTAssert(webViewsQuery.waitForExistence(timeout: 15))
        let phone_section = webViewsQuery.buttons["​ the test phone number"].section {
            $0.tap()
            app.buttons["Done"].tap(wait: 5, fail: false)
            webViewsQuery.buttons["​ Continue"].tap()
        }
        
        let test_code_section = webViewsQuery.buttons.by(havingLabel: "use test code") {
            $0.tap()
        }

        let addr_section = webViewsQuery.textFields["MM"].section { (mm) in
            let dd = webViewsQuery.textFields["DD"]
            XCTAssert(dd.waitForExistence(timeout: 2))
            let chosen = dd.frame.minX < mm.frame.minX ? dd : mm
            chosen.tap()
            chosen.tap()
            chosen.enter(text: "10")
            app.typeText("10")
            app.typeText("2000")
            webViewsQuery.tap()
            webViewsQuery.textFields["Address line 1"].enter(text: "33 Loyd Close")
            webViewsQuery.textFields["Address line 2"].enter(text: "")
            webViewsQuery.textFields["Town or City"].enter(text: "Abingdon")
            webViewsQuery.textFields["Postal code"].enter(text: "OX14 1XR")
            app.buttons["Done"].tap()
            webViewsQuery.swipeUp()
            webViewsQuery.buttons["​ Continue"].tap()
        }
        
        // identify proof
        let identity_proof = webViewsQuery.buttons.by(havingLabel: "test document") {
            $0.tap()
        }

        // address proof
        let addr_proof = webViewsQuery.buttons.by(havingLabel: "test document") {
            $0.tap()
        }
        
        
        let industry = webViewsQuery.buttons["Please select your industry…"].section { (select) in
            select.tap()
            UIPasteboard.general.string = "salons"
            usleep(200000)
            webViewsQuery.otherElements["Search…"].press(forDuration: 0.3)
            webViewsQuery.otherElements["Search…"].tap()
            app.menuItems["Paste"].tap()
            webViewsQuery.otherElements["Salons or barbers"].tap()
            webViewsQuery.buttons["​ Continue"].tap()
        }
        
        let bank = webViewsQuery.buttons.by(havingLabel: "test account") {
            $0.tap()
        }
        
        let confirm = webViewsQuery.buttons.by(havingLabel: "Done") {
            webViewsQuery.swipeUp()
            $0.tap()
        }
        
        let form = XCUIForm(sections: [
            phone_section, test_code_section, addr_section, identity_proof, addr_proof, industry, bank, confirm
        ])
        form.quickScan()
        XCTAssert(app.staticTexts["Profile"].waitForExistence(timeout: 10))
    }
}

import UIKit
import Neon
import DateToolsSwift

protocol PassThroughScrollViewDelegate: AnyObject {
    var pendingEvent: EventView? { get }
    func tappedToClearEdit(withClearNext flag: Bool)
}

extension Date {
    func within(seconds: Double, of other: Date) -> Bool {
        abs(timeIntervalSince1970 - other.timeIntervalSince1970) < seconds
    }
}

class PassThroughView: UIView {
    
    weak var passDelegate: PassThroughScrollViewDelegate?
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        guard passDelegate?.pendingEvent == nil else {
            for v in subviews {
                if let vv = v.hitTest(convert(point, to: v), with: event) {
                    return vv
                }
            }
            passDelegate?.tappedToClearEdit(withClearNext: true)
            return super.hitTest(point, with: event) //absorb touch
        }
        return nil
    }
}

public protocol TimelinePagerViewDelegate: AnyObject {
    func timelinePagerDidSelectEventView(_ eventView: EventView)
    func timelinePagerDidLongPressEventView(_ eventView: EventView)
    func timelinePagerDidTapToEndEdit(event: EventDescriptor?)
    func timelinePager(timelinePager: TimelinePagerView, didTapTimelineAt date: Date)
    func timelinePagerDidBeginDragging(timelinePager: TimelinePagerView)
    func timelinePager(timelinePager: TimelinePagerView, willMoveTo date: Date)
    func timelinePager(timelinePager: TimelinePagerView, didMoveTo  date: Date)
    func timelinePager(timelinePager: TimelinePagerView, didLongPressTimelineAt date: Date)
    func timelinePager(timelinePager: TimelinePagerView, didEditDelete desc: EventDescriptor)
    
    // Editing
    func timelinePager(timelinePager: TimelinePagerView, didUpdate event: EventDescriptor)
}

public final class TimelinePagerView: UIView, UIGestureRecognizerDelegate, UIScrollViewDelegate, DayViewStateUpdating, UIPageViewControllerDataSource, UIPageViewControllerDelegate, TimelineViewDelegate, PassThroughScrollViewDelegate {
    
    private var isSingleTimeline = false
    public weak var dataSource: EventDataSource?
    public weak var delegate: TimelinePagerViewDelegate?
    
    public private(set) var calendar: Calendar = Calendar.autoupdatingCurrent
    
    public var timelineScrollOffset: CGPoint {
        // Any view is fine as they are all synchronized
        let offset = (currentTimeline)?.container.contentOffset
        return offset ?? CGPoint()
    }
    
    private var currentTimeline: TimelineContainerController? {
        return pagingViewController.viewControllers?.first as? TimelineContainerController
    }
    
    public var autoScrollToFirstEvent = false
    
    private var mimicContentView = PassThroughView()
    private var pagingViewController = UIPageViewController(transitionStyle: .scroll,
                                                            navigationOrientation: .horizontal,
                                                            options: nil)
    private var style = TimelineStyle()
    
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer,
                                  shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        if otherGestureRecognizer.view is EventResizeHandleView {
            return false
        }
        return true
    }
    
    
    private var wasEndEditingTap = false
    @objc internal func tappedToClearEdit(withClearNext flag: Bool = false) {
        wasEndEditingTap = flag
        delegate?.timelinePagerDidTapToEndEdit(event: pendingEvent?.descriptor)
    }
    
    public weak var state: DayViewState? {
        willSet(newValue) {
            state?.unsubscribe(client: self)
        }
        didSet {
            state?.subscribe(client: self)
        }
    }
    
    public init(calendar: Calendar, withSingle isSingle: Bool = false) {
        self.calendar = calendar
        self.isSingleTimeline = isSingle
        super.init(frame: .zero)
        configure()
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configure()
    }
    
    private func configure() {
        let vc = configureTimelineController(date: Date())
        pagingViewController.setViewControllers([vc], direction: .forward, animated: false, completion: nil)
        
        if !isSingleTimeline {
            pagingViewController.dataSource = self
            pagingViewController.delegate = self
        }
        addSubview(pagingViewController.view!)
        
        mimicContentView.passDelegate = self
        addSubview(mimicContentView)
        
        clipsToBounds = true
    }
    
    public func updateStyle(_ newStyle: TimelineStyle) {
        style = newStyle
        pagingViewController.viewControllers?.forEach({ (timelineContainer) in
            if let controller = timelineContainer as? TimelineContainerController {
                self.updateStyleOfTimelineContainer(controller: controller)
            }
        })
        pagingViewController.view.backgroundColor = style.backgroundColor
    }
    
    private func updateStyleOfTimelineContainer(controller: TimelineContainerController) {
        let container = controller.container
        let timeline = controller.timeline
        timeline.updateStyle(style)
        container.backgroundColor = style.backgroundColor
    }
    
    public func timelinePanGestureRequire(toFail gesture: UIGestureRecognizer) {
        for controller in pagingViewController.viewControllers ?? [] {
            if let controller = controller as? TimelineContainerController {
                let container = controller.container
                container.panGestureRecognizer.require(toFail: gesture)
            }
        }
    }
    
    public func scrollTo(hour24: Float, animated: Bool = true) {
        // Any view is fine as they are all synchronized
        if let controller = currentTimeline {
            controller.container.scrollTo(hour24: hour24, animated: animated)
        }
    }
    
    private func configureTimelineController(date: Date) -> TimelineContainerController {
        let controller = TimelineContainerController()
        updateStyleOfTimelineContainer(controller: controller)
        let timeline = controller.timeline
        timeline.longPressGestureRecognizer.addTarget(self, action: #selector(timelineDidLongPress(_:)))
        timeline.delegate = self
        timeline.calendar = calendar
        timeline.date = date.dateOnly(calendar: calendar)
        controller.container.delegate = self
        updateTimeline(timeline)
        return controller
    }
    
    private var initialContentOffset = CGPoint.zero
    public func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        initialContentOffset = scrollView.contentOffset
    }
    
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset
        mimicContentView.frame.origin = CGPoint(x: offset.x * -1, y: offset.y * -1)
    }
    
    public func reloadData() {
        pagingViewController.children.forEach({ (controller) in
            if let controller = controller as? TimelineContainerController {
                self.updateTimeline(controller.timeline)
            }
        })
    }
    
    private func timelineFor(date: Date) -> TimelineView? {
        for controller in pagingViewController.children {
            guard let controller = controller as? TimelineContainerController else {
                continue
            }
            let other = controller.timeline.date.dateOnly(calendar: calendar)
            if date.dateOnly(calendar: calendar) == other.dateOnly(calendar: calendar) {
                return controller.timeline
            }
        }
        return nil
    }
    
    public func findEventView(for descriptor: EventDescriptor) -> EventView? {
        guard let timeline = currentTimeline?.timeline else { return nil }
        let startY = timeline.dateToY(descriptor.startDate)
        let point = CGPoint(x: timeline.frame.width / 2, y: startY)
        return timeline.findEventView(at: point)
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        pagingViewController.view.fillSuperview()
        if let controller = currentTimeline {
            mimicContentView.frame.size = controller.container.contentSize
            mimicContentView.frame.origin = CGPoint(x: controller.container.contentOffset.x * -1, y: controller.container.contentOffset.y * -1)
        }
    }
    
    private func updateTimeline(_ timeline: TimelineView) {
        guard let dataSource = dataSource else { return }
        let date = timeline.date.dateOnly(calendar: calendar)
        let events = dataSource.eventsForDate(date)
        let day = TimePeriod(beginning: date,
                             chunk: TimeChunk.dateComponents(days: 1))
        let validEvents = isSingleTimeline ? events : events.filter {$0.datePeriod.overlaps(with: day)}
        timeline.isSingle = isSingleTimeline
        timeline.layoutAttributes = validEvents.map(EventLayoutAttributes.init)
    }
    
    public func scrollToFirstEventIfNeeded() {
        if autoScrollToFirstEvent {
            if let controller = currentTimeline {
                controller.container.scrollToFirstEvent()
            }
        }
    }
    
    // Event creation prototype
    var pendingEvent: EventView?
    
    /// Tag of the last used resize handle
    private var resizeHandleTag: Int?
    
    /// Creates an EventView and places it on the Timeline
    /// - Parameter event: the EventDescriptor based on which an EventView will be placed on the Timeline
    /// - Parameter animated: if true, CalendarKit animates event creation
    public func create(event: EventDescriptor, animated: Bool) {
        let eventView = event.renderClass.init()
        eventView.updateWithDescriptor(event: event)
        mimicContentView.addSubview(eventView)
        // layout algo
        if let currentTimeline = currentTimeline {
            
            let tapGestureRecog = UITapGestureRecognizer(target: self, action: #selector(tappedToClearEdit))
            eventView.addGestureRecognizer(tapGestureRecog)
            
            let swipe = eventView.tapGestureRecognizer
            swipe.cancelsTouchesInView = true
            swipe.addTarget(self, action: #selector(handleEditSwipe(_:)))
            
            let panGestureRecognizer = eventView.panGestureRecognizer
            panGestureRecognizer.addTarget(self, action: #selector(handlePanGesture(_:)))
            panGestureRecognizer.require(toFail: swipe)
            
            for handle in eventView.eventResizeHandles {
                let panGestureRecognizer = handle.panGestureRecognizer
                panGestureRecognizer.addTarget(self, action: #selector(handleResizeHandlePanGesture(_:)))
                panGestureRecognizer.cancelsTouchesInView = true
            }
            
            
            
            let timeline = currentTimeline.timeline
            // algo needs to be extracted to a separate object
            let yStart = timeline.dateToY(event.startDate)
            let yEnd = timeline.dateToY(event.endDate)
            
            let newRect = CGRect(x: timeline.leftTextInset,
                                 y: yStart,
                                 width: timeline.calendarWidth,
                                 height: yEnd - yStart)
            eventView.frame = newRect
            
            if animated {
                eventView.animateCreation()
            }
        }
        pendingEvent = eventView
        accentDateForPendingEvent()
    }
    
    /// Puts timeline in the editing mode and highlights a single event as being edited.
    /// - Parameter event: the `EventDescriptor` to be edited. An editable copy of the `EventDescriptor` is created by calling `makeEditable()` method on the passed value
    /// - Parameter animated: if true, CalendarKit animates beginning of the editing
    public func beginEditing(event: EventDescriptor, animated: Bool = false) {
        if pendingEvent == nil {
            let edited = event.makeEditable()
            create(event: edited, animated: animated)
        }
    }
    
    private var prevOffset: CGPoint = .zero
    private var originalPosition: CGPoint?
    @objc func handlePanGesture(_ sender: UIPanGestureRecognizer) {
        
        if let pendingEvent = pendingEvent {
            let newCoord = sender.translation(in: pendingEvent)
            if sender.state == .began {
                prevOffset = newCoord
            }
            
            let diff = CGPoint(x: newCoord.x - prevOffset.x, y: newCoord.y - prevOffset.y)
            pendingEvent.frame.origin.x += diff.x
            pendingEvent.frame.origin.y += diff.y
            
            prevOffset = newCoord
            accentDateForPendingEvent()
        }
        
        if sender.state == .ended {
            commitEditing()
        }
        
    }
    
    @objc private func handleEditSwipe(_ sender: UITapGestureRecognizer) {
        if sender.state == .ended, let desc = pendingEvent?.descriptor {
            endEventEditing(false)
            delegate?.timelinePager(timelinePager: self, didEditDelete: desc)
        }
    }
    
    @objc func handleResizeHandlePanGesture(_ sender: UIPanGestureRecognizer) {
        if let pendingEvent = pendingEvent {
            let newCoord = sender.translation(in: pendingEvent)
            if sender.state == .began {
                prevOffset = newCoord
                originalPosition = sender.location(in: pendingEvent)
            }
            guard let tag = sender.view?.tag else {
                return
            }
            resizeHandleTag = tag
            
            let diff = CGPoint(x: newCoord.x - prevOffset.x,
                               y: newCoord.y - prevOffset.y)
            var suggestedEventFrame = pendingEvent.frame
            
            if tag == 0 { // Top handle
                suggestedEventFrame.origin.y += diff.y
                suggestedEventFrame.size.height -= diff.y
            } else { // Bottom handle
                suggestedEventFrame.size.height += diff.y
            }
            let minimumMinutesEventDurationWhileEditing = CGFloat(style.minimumEventDurationInMinutesWhileEditing)
            let minimumEventHeight = minimumMinutesEventDurationWhileEditing * style.verticalDiff / 60
            let suggestedEventHeight = suggestedEventFrame.size.height
            
            if suggestedEventHeight > minimumEventHeight {
                pendingEvent.frame = suggestedEventFrame
                prevOffset = newCoord
                accentDateForPendingEvent(eventHeight: tag == 0 ? 0 : suggestedEventHeight)
            }
            
            let vel = sender.velocity(in: pendingEvent).y
            if vel > 0 && (originalPosition!.y - newCoord.y) > 0  {
                keepMimicContentInView(doMax: true, doMin: false)
            }
            
            if vel < 0 && (originalPosition!.y - newCoord.y) < 0  {
                keepMimicContentInView(doMax: false, doMin: true)
            }
        }
        
        if sender.state == .ended {
            originalPosition = nil
            commitEditing()
        }
    }
    
    private func keepMimicContentInView(doMax: Bool = true, doMin: Bool = true) {
        guard let pendingEvent = pendingEvent, let timeline = currentTimeline?.container else { return }
        let contentOffset = timeline.contentOffset
        let contentSize = timeline.contentSize
        let miny = pendingEvent.frame.origin.y - contentOffset.y
        let maxy = pendingEvent.frame.maxY - contentOffset.y
        let fullHeight = bounds.size.height
        let padding: CGFloat = 50
        var yToScroll: CGFloat?
        let upperlimit = fullHeight - padding - 30
        if (maxy > upperlimit) && doMax {
            yToScroll = maxy - upperlimit
        } else if (miny < (padding)) && doMin {
            yToScroll = miny - padding
        }
        
        if var yToScroll = yToScroll {
            yToScroll += contentOffset.y
            yToScroll = min(yToScroll, contentSize.height - fullHeight)
            yToScroll = max(yToScroll, 0)
            timeline.setContentOffset(CGPoint(x: contentOffset.x, y: yToScroll), animated: true)
        }
    }
    
    private func accentDateForPendingEvent(eventHeight: CGFloat = 0) {
        if let currentTimeline = currentTimeline {
            let timeline = currentTimeline.timeline
            let converted = pendingEvent?.frame.origin ?? .zero
            let date = timeline.yToDate(converted.y + eventHeight)
            timeline.accentedDate = date
            timeline.setNeedsDisplay()
        }
    }
    
    
    private func commitEditing(_ final: Bool = false) {
        if let currentTimeline = currentTimeline {
            let timeline = currentTimeline.timeline
            timeline.accentedDate = nil
            timeline.setNeedsDisplay()
            
            // TODO: Animate cancellation
            
            if let editedEventView = pendingEvent,
                let descriptor = editedEventView.descriptor {
                update(descriptor: descriptor, with: editedEventView)
                
                let clmped: (Date) -> Date = {
                    let other = self.resizeHandleTag == 1 ? descriptor.startDate : descriptor.endDate
                    let start = other.dateOnly(calendar: self.calendar)
                    return $0.clamped(to: start...start.advanced(by: 86399))
                }
                let ytd = timeline.yToDate(editedEventView.frame.origin.y)
                let snapped = clmped(timeline.snappingBehavior.nearestDate(to: ytd))
                let x = timeline.leftTextInset
                
                var eventFrame = editedEventView.frame
                eventFrame.origin.x = x
                eventFrame.origin.y = timeline.dateToY(snapped)
                
                if resizeHandleTag == 1 {
                    let bottomHandleYTD = timeline.yToDate(editedEventView.frame.origin.y + editedEventView.frame.size.height)
                    let bottomHandleSnappedDate = clmped(timeline.snappingBehavior.nearestDate(to: bottomHandleYTD))
                    eventFrame.size.height = timeline.dateToY(bottomHandleSnappedDate) - timeline.dateToY(snapped)
                }
                
                func animateEventSnap() {
                    editedEventView.frame = eventFrame
                }
                
                func completionHandler(_ completion: Bool) {
                    update(descriptor: descriptor, with: editedEventView)
                    if final {
                        delegate?.timelinePager(timelinePager: self, didUpdate: descriptor)
                    }
                }
                
                UIView.animate(withDuration: 0.3,
                               delay: 0,
                               usingSpringWithDamping: 0.6,
                               initialSpringVelocity: 5,
                               options: [],
                               animations: animateEventSnap,
                               completion: completionHandler(_:))
            }
            
            resizeHandleTag = nil
            prevOffset = .zero
        }
    }
    
    /// Ends editing mode
    public func endEventEditing(_ commit: Bool = true) {
        if commit {
            commitEditing(true)
        }
        prevOffset = .zero
        pendingEvent?.eventResizeHandles.forEach {$0.panGestureRecognizer.removeTarget(self, action: nil)}
        pendingEvent?.removeFromSuperview()
        pendingEvent = nil
    }
    
    @objc private func timelineDidLongPress(_ sender: UILongPressGestureRecognizer) {
        if sender.state == .ended {
            commitEditing()
        }
    }
    
    private func update(descriptor: EventDescriptor, with eventView: EventView) {
        if let currentTimeline = currentTimeline {
            let timeline = currentTimeline.timeline
            let eventFrame = eventView.frame
            let beginningY = eventFrame.minY
            let endY = eventFrame.maxY
            let beginning = timeline.yToDate(beginningY)
            let end = timeline.yToDate(endY)
            descriptor.startDate = beginning
            descriptor.endDate = end
        }
    }
    
    // MARK: DayViewStateUpdating
    
    public func move(from oldDate: Date, to newDate: Date) {
        let oldDate = oldDate.dateOnly(calendar: calendar)
        let newDate = newDate.dateOnly(calendar: calendar)
        let newController = configureTimelineController(date: newDate)
        
        delegate?.timelinePager(timelinePager: self, willMoveTo: newDate)
        
        func completionHandler(_ completion: Bool) {
            pagingViewController.viewControllers?.first?.view.setNeedsLayout()
            scrollToFirstEventIfNeeded()
            delegate?.timelinePager(timelinePager: self, didMoveTo: newDate)
        }
        
        if newDate.isEarlier(than: oldDate) {
            pagingViewController.setViewControllers([newController], direction: .reverse, animated: true, completion: completionHandler(_:))
        } else if newDate.isLater(than: oldDate) {
            pagingViewController.setViewControllers([newController], direction: .forward, animated: true, completion: completionHandler(_:))
        }
    }
    
    // MARK: UIPageViewControllerDataSource
    
    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if isSingleTimeline { return nil }
        guard let containerController = viewController as? TimelineContainerController  else {return nil}
        let previousDate = containerController.timeline.date.add(TimeChunk.dateComponents(days: -1), calendar: calendar)
        let vc = configureTimelineController(date: previousDate)
        let offset = (pageViewController.viewControllers?.first as? TimelineContainerController)?.container.contentOffset
        vc.pendingContentOffset = offset
        return vc
    }
    
    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if isSingleTimeline { return nil }
        guard let containerController = viewController as? TimelineContainerController  else {return nil}
        let nextDate = containerController.timeline.date.add(TimeChunk.dateComponents(days: 1), calendar: calendar)
        let vc = configureTimelineController(date: nextDate)
        let offset = (pageViewController.viewControllers?.first as? TimelineContainerController)?.container.contentOffset
        vc.pendingContentOffset = offset
        return vc
    }
    
    // MARK: UIPageViewControllerDelegate
    
    public func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        guard completed else {return}
        if let timelineContainerController = pageViewController.viewControllers?.first as? TimelineContainerController {
            let selectedDate = timelineContainerController.timeline.date
            delegate?.timelinePager(timelinePager: self, willMoveTo: selectedDate)
            state?.client(client: self, didMoveTo: selectedDate)
            scrollToFirstEventIfNeeded()
            delegate?.timelinePager(timelinePager: self, didMoveTo: selectedDate)
        }
    }
    
    public func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        delegate?.timelinePagerDidBeginDragging(timelinePager: self)
    }
    
    // MARK: TimelineViewDelegate
    
    public func timelineView(_ timelineView: TimelineView, didTapAt date: Date) {
        guard !wasEndEditingTap else { wasEndEditingTap = false; return }
        delegate?.timelinePager(timelinePager: self, didTapTimelineAt: date)
    }
    
    public func timelineView(_ timelineView: TimelineView, didLongPressAt date: Date) {
        delegate?.timelinePager(timelinePager: self, didLongPressTimelineAt: date)
    }
    
    public func timelineView(_ timelineView: TimelineView, didTap event: EventView) {
        guard !wasEndEditingTap else { wasEndEditingTap = false; return }
        delegate?.timelinePagerDidSelectEventView(event)
    }
    
    public func timelineView(_ timelineView: TimelineView, didLongPress event: EventView) {
        delegate?.timelinePagerDidLongPressEventView(event)
    }
}

import UIKit
import Neon

struct HashableType<T> : Hashable {
    static func == (lhs: HashableType, rhs: HashableType) -> Bool {
        return lhs.base == rhs.base
    }
    
    let base: T.Type
    
    init(_ base: T.Type) {
        self.base = base
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(ObjectIdentifier(base))
    }
}

open class EventView: UIView {
    
    static var hashable: HashableType<EventView> { return HashableType(self) }
    
    public var descriptor: EventDescriptor?
    public var color = UIColor.lightGray
    
    public var contentHeight: CGFloat {
        return textView.height
    }
    
    public lazy var textView: UITextView = {
        let view = UITextView()
        view.isUserInteractionEnabled = false
        view.backgroundColor = .clear
        view.isScrollEnabled = false
        return view
    }()
    
    /// Resize Handle views showing up when editing the event.
    /// The top handle has a tag of `0` and the bottom has a tag of `1`
    public lazy var eventResizeHandles = [EventResizeHandleView(), EventResizeHandleView()]
    public lazy var deleteEventHandle = { EventCrossButtonView() }()
    
    
    public var panGestureRecognizer = UIPanGestureRecognizer()
    public var tapGestureRecognizer = UITapGestureRecognizer()
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configure()
    }
    
    private func configure() {
        clipsToBounds = false
        color = tintColor
        addSubview(textView)
        addGestureRecognizer(panGestureRecognizer)
        
        for (idx, handle) in eventResizeHandles.enumerated() {
            handle.tag = idx
            handle.isHidden = true
            addSubview(handle)
        }
        deleteEventHandle.isHidden = true
        deleteEventHandle.addGestureRecognizer(tapGestureRecognizer)
        addSubview(deleteEventHandle)
    }
    
    open func updateWithDescriptor(event: EventDescriptor) {
        isUserInteractionEnabled = event.selectable
        layer.zPosition = CGFloat(event.zPosition)
        if let attributedText = event.attributedText {
            textView.attributedText = attributedText
        } else {
            textView.text = event.text
            textView.textColor = event.textColor
            textView.font = event.font
        }
        descriptor = event
        backgroundColor = event.backgroundColor
        color = event.color
        let applyHandle: (EventHandleView) -> Void = {
            $0.borderColor = event.color
            $0.isHidden = !event.resizeable || event.editedEvent == nil
        }
        eventResizeHandles.forEach(applyHandle)
        applyHandle(deleteEventHandle)
        deleteEventHandle.isHidden = event.editedEvent == nil
        drawsShadow = event.editedEvent != nil
        setNeedsDisplay()
        setNeedsLayout()
    }
    
    public func animateCreation() {
        transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        func scaleAnimation() {
            transform = .identity
        }
        UIView.animate(withDuration: 0.2,
                       delay: 0,
                       usingSpringWithDamping: 0.2,
                       initialSpringVelocity: 10,
                       options: [],
                       animations: scaleAnimation,
                       completion: nil)
    }
    
    /**
     Custom implementation of the hitTest method is needed for the tap gesture recognizers
     located in the ResizeHandleView to work.
     Since the ResizeHandleView could be outside of the EventView's bounds, the touches to the ResizeHandleView
     are ignored.
     In the custom implementation the method is recursively invoked for all of the subviews,
     regardless of their position in relation to the Timeline's bounds.
     */
    public override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        for resizeHandle in eventResizeHandles + [deleteEventHandle] {
            if let subSubView = resizeHandle.hitTest(convert(point, to: resizeHandle), with: event) {
                return subSubView
            }
        }
        return super.hitTest(point, with: event)
    }
    
    private var drawsShadow = false
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        let first = eventResizeHandles.first
        let last = eventResizeHandles.last
        let radius: CGFloat = 40
        
        textView.fillSuperview(left: radius / 4, right: 0, top: 0, bottom: 0)
        let yPad: CGFloat =  -radius / 2
        first?.anchorInCorner(.topRight,
                              xPad: layoutMargins.right * 2,
                              yPad: yPad,
                              width: radius,
                              height: radius)
        last?.anchorInCorner(.bottomLeft,
                             xPad: layoutMargins.left * 2,
                             yPad: yPad,
                             width: radius,
                             height: radius)
        deleteEventHandle.anchorToEdge(.left, padding: -10, width: 25, height: 25)
        if drawsShadow {
            applySketchShadow(alpha: 0.13,
                              blur: 10)
        }
    }
    
    private func applySketchShadow(
        color: UIColor = .black,
        alpha: Float = 0.5,
        x: CGFloat = 0,
        y: CGFloat = 2,
        blur: CGFloat = 4,
        spread: CGFloat = 0) {
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = alpha
        layer.shadowOffset = CGSize(width: x, height: y)
        layer.shadowRadius = blur / 2.0
        if spread == 0 {
            layer.shadowPath = nil
        } else {
            let dx = -spread
            let rect = bounds.insetBy(dx: dx, dy: dx)
            layer.shadowPath = UIBezierPath(rect: rect).cgPath
        }
    }
}

import Foundation

 public protocol EventDescriptor: AnyObject {
  var startDate: Date {get set}
  var endDate: Date {get set}
  var isAllDay: Bool {get}
  var text: String {get}
  var attributedText: NSAttributedString? {get}
  var font: UIFont {get}
  var color: UIColor {get}
  var textColor: UIColor {get}
  var backgroundColor: UIColor {get}
  var editedEvent: EventDescriptor? {get set}
  var zPosition: Int {get set}
  var selectable: Bool {get set}
  var resizeable: Bool {get set}
  func makeEditable() -> Self
  func commitEditing()
  
  var renderClass: EventView.Type { get }
  var renderInit: () -> EventView { get }
}

public func ==(lhs: EventDescriptor, rhs: EventDescriptor) -> Bool {
  guard type(of: lhs) == type(of: rhs) else { return false }
  return    ((lhs.startDate == rhs.startDate &&
    lhs.endDate == rhs.endDate) ||
    lhs.isAllDay == rhs.isAllDay)  &&
    lhs.text == rhs.text
}

public func !=(lhs: EventDescriptor, rhs: EventDescriptor) -> Bool {
  return   !(lhs == rhs)
}

//
//  EventCrossButtonView.swift
//  CalendarKit
//
//  Created by Ravi on 04/08/2020.
//

import Foundation
import Neon

public final class EventCrossButtonView: UIView, EventHandleView {
    public lazy var panGestureRecognizer = UIPanGestureRecognizer()
    public lazy var crossView: UIView = {
        let label = UIImageView()
        label.image = UIImage(
            systemName: "delete.left.fill",
            withConfiguration:
            UIImage.SymbolConfiguration.init(
                font: .systemFont(ofSize: 18, weight: .semibold)
            )
        )
        label.contentMode = .scaleAspectFit
        label.tintColor = .white
        self.addSubview(label)
        return label
    }()
    public lazy var dotView: UIView = {
        let v = UIView()
        self.addSubview(v)
        return v
    }()
    
    public var borderColor: UIColor? {
        get {
            return dotView.backgroundColor
        }
        set(value) {
            dotView.backgroundColor = value
        }
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        dotView.layer.cornerRadius = bounds.height / 2
        dotView.frame = bounds
        crossView.anchorInCenter(width: 20, height: 20)
    }
    
    private func configure() {
        dotView.clipsToBounds = true
        backgroundColor = .clear
    }
}

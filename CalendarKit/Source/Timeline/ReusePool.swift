import UIKit

final class ReusePool {
  private var storage: [EventView]
  private var creator: () -> EventView

  init(_ creator: @escaping () -> EventView) {
    storage = [EventView]()
    self.creator = creator
  }

  func enqueue(views: [EventView]) {
    views.forEach {$0.frame = .zero}
    storage.append(contentsOf: views)
  }
  
  func enqueue(_ view: EventView) {
    view.frame = .zero
    storage.append(view)
  }

  func dequeue() -> EventView {
    guard !storage.isEmpty else { return creator() }
    return storage.removeLast()
  }
}

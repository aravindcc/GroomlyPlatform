import Foundation
import DateToolsSwift

/**
 A bridge between CalendarKit and underlying date processing library.
 Allows using any third-party library for the date processing,
 without exposing it to the client.
 */
public extension EventDescriptor {
  var datePeriod: TimePeriod {
    return TimePeriod(beginning: startDate, end: endDate)
  }
}

public extension EventDescriptor {
  func overlaps(with event: EventDescriptor) -> Bool {
    return  zPosition == event.zPosition &&
            datePeriod.overlaps(with: event.datePeriod)
  }
}

//
//  FeedBack.swift
//  Groomly
//
//  Created by Ravi on 20/07/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import SwiftEntryKit
import Async

struct NotificationDetails {
    let title: String
    let desc: String
    let imageName: String?
    
    internal init(title: String, desc: String = "", imageName: String? = nil) {
        self.title = title
        self.desc = desc
        self.imageName = imageName
    }
}

enum FeedbackType {
    case message
    case progress
    
    fileprivate var attributes: EKAttributes {
        switch self {
        case .message: return Feedback.getFloatAttributes()
        case .progress: return Feedback.getSimpleAttributes()
        }
    }
}

class Feedback {
    static var displayMode: EKAttributes.DisplayMode {
        return (ColorScheme.normal.bg.isLight() ?? true) ? .light : .dark
    }
    static var statusBar: EKAttributes.StatusBar {
        return (ColorScheme.normal.bg.isLight() ?? true) ? .dark : .light
    }
    
    static fileprivate func getSimpleAttributes() -> EKAttributes {
        var attributes: EKAttributes = getFloatAttributes()
        attributes.displayDuration = .infinity
        return attributes
    }
    
    static fileprivate func getFloatAttributes(_ initial: EKAttributes = .topFloat) -> EKAttributes {
        var attributes: EKAttributes = initial
        attributes.displayMode = displayMode
        attributes.hapticFeedbackType = .success
        attributes.entryBackground = .gradient(
            gradient: .init(
                colors: [EKColor(ColorScheme.normal.highlightColorA),
                         EKColor(ColorScheme.normal.highlightColorB)],
                startPoint: .zero,
                endPoint: CGPoint(x: 1, y: 1)
            )
        )
        attributes.popBehavior = .animated(
            animation: .init(
                translate: .init(duration: 0.3),
                scale: .init(from: 1, to: 0.7, duration: 0.7)
            )
        )
        attributes.shadow = .active(
            with: .init(
                color: .black,
                opacity: 0.5,
                radius: 10
            )
        )
        attributes.statusBar = statusBar
        attributes.scroll = .enabled(
            swipeable: true,
            pullbackAnimation: .easeOut
        )
        attributes.positionConstraints.maxSize = .init(
            width: .constant(value: UIScreen.main.bounds.width), // min edge
            height: .intrinsic
        )
        return attributes
    }
    
    static fileprivate func getCenterFloat() -> EKAttributes {
        var attributes = getFloatAttributes(.centerFloat)
        attributes.entryBackground = .color(color: EKColor(ColorScheme.normal.bg))
        attributes.shadow = .active(
            with: .init(
                color: .black,
                opacity: 0.7,
                radius: 20
            )
        )
        attributes.screenBackground = .color(color: EKColor(UIColor.black.withAlphaComponent(0.5)))
        attributes.scroll = .disabled
        attributes.entryInteraction = .init(defaultAction: .absorbTouches, customTapActions: [])
        attributes.screenInteraction = .init(defaultAction: .absorbTouches, customTapActions: [])
        attributes.displayDuration = .infinity
        return attributes
    }
    
    static func showNotification(
        message: NotificationDetails,
        type: FeedbackType = .message,
        tapped: (Completion)? = nil
    ) {
        Thread.onMain {
            showNotificationOnMain(message: message, type: type, tapped: tapped)
        }
    }
    
    static func showConfirmation(
        popup: NotificationDetails,
        successTitle: String,
        failureTitle: String?,
        completion: @escaping (Bool) -> Void
    ) {
        Thread.onMain {
            showConfirmationOnMain(popup: popup, successTitle: successTitle, failureTitle: failureTitle, completion: completion)
        }
    }
    
    static fileprivate func showConfirmationOnMain(
        popup: NotificationDetails,
        successTitle: String,
        failureTitle: String?,
        completion: @escaping (Bool) -> Void
    ) {
        let contentView = APConfirmationView(message: popup, textColor: ColorScheme.normal.primaryTextColor, successTitle: successTitle, failureTitle: failureTitle, completion: completion)
        contentView.accessibilityIdentifier = "confirmNotification"
        var attribs = getCenterFloat()
        attribs.precedence = .override(priority: .max, dropEnqueuedEntries: false)
        SwiftEntryKit.display(entry: contentView, using: attribs)
    }
    
    static func showRating(
        title: String,
        desc: String = "",
        completion: @escaping (Double?) -> Void
    ) {
        Thread.onMain {
            let details = NotificationDetails(
                title: title,
                desc: desc
            )
            let contentView = APRatingView(message: details, textColor: ColorScheme.normal.primaryTextColor, completion: completion)
            contentView.accessibilityIdentifier = "confirmNotification"
            var attribs = getCenterFloat()
            attribs.precedence = .override(priority: .max, dropEnqueuedEntries: false)
            SwiftEntryKit.display(entry: contentView, using: attribs)
        }
    }
    
    static func askForInput(
        title: String,
        desc: String = "",
        placeholder: String? = nil,
        completion: @escaping (String?) -> Void
    ) {
        Thread.onMain {
            let details = NotificationDetails(
                title: title,
                desc: desc
            )
            let contentView = APTextInputView(message: details, placeholder: placeholder, textColor: ColorScheme.normal.primaryTextColor, completion: completion)
            contentView.accessibilityIdentifier = "confirmNotification"
            var attribs = getCenterFloat()
            attribs.precedence = .override(priority: .max, dropEnqueuedEntries: false)
            SwiftEntryKit.display(entry: contentView, using: attribs)
        }
    }
    
    static func showCentral(view: UIView) {
        SwiftEntryKit.display(entry: view, using: getCenterFloat())
    }
    
    static fileprivate func showNotificationOnMain(
        message: NotificationDetails,
        type: FeedbackType = .message,
        tapped: (Completion)? = nil
    ) {
        var attributes = type.attributes
        let textColor = EKColor(.white)
        
        if case .progress = type {
            let style = EKProperty.LabelStyle(
                font: .systemFont(ofSize: 16, weight: .semibold),
                color: textColor,
                alignment: .center,
                displayMode: displayMode
            )
            let labelContent = EKProperty.LabelContent(
                text: message.title,
                style: style
            )
            let contentView = APProcessingNoteMessageView(
                with: labelContent,
                activityIndicatorColor: textColor.light
            )
            attributes.entryInteraction = .init(defaultAction: .absorbTouches, customTapActions: [])
            attributes.screenInteraction = .init(defaultAction: .absorbTouches, customTapActions: [])
            contentView.accessibilityIdentifier = "notification"
            SwiftEntryKit.display(entry: contentView, using: attributes)
            return
        }
        
        if let tapped = tapped {
            attributes.entryInteraction = .init(defaultAction: .dismissEntry, customTapActions: [tapped])
        }
        let title = EKProperty.LabelContent(
            text: message.title,
            style: .init(
                font: UIFont.systemFont(ofSize: 16, weight: .semibold),
                color: textColor,
                displayMode: displayMode
            ),
            accessibilityIdentifier: "title"
        )
        let description = EKProperty.LabelContent(
            text: message.desc,
            style: .init(
                font: UIFont.systemFont(ofSize: 14, weight: .regular),
                color: textColor,
                displayMode: displayMode
            ),
            accessibilityIdentifier: "description"
        )
        var image: EKProperty.ImageContent?
        if  let imageName = message.imageName,
            let img = UIImage(named: imageName) ?? UIImage(systemName: imageName) {
            image = EKProperty.ImageContent(
                image: img.withRenderingMode(.alwaysTemplate),
                displayMode: displayMode,
                size: CGSize(width: 35, height: 35),
                tint: textColor,
                accessibilityIdentifier: "thumbnail"
            )
        }
        let simpleMessage = EKSimpleMessage(
            image: image,
            title: title,
            description: description
        )
        let notificationMessage = EKNotificationMessage(simpleMessage: simpleMessage)
        let contentView = EKNotificationMessageView(with: notificationMessage)
        contentView.accessibilityIdentifier = "notification"
        SwiftEntryKit.display(entry: contentView, using: attributes)
    }
}

//
//  FeedbackViews.swift
//  Groomly
//
//  Created by Ravi on 06/08/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import SwiftEntryKit
import Async
import PinLayout
import FlexLayout
import Cosmos

class APConfirmationView: UIView {
    let container = UIView()
    init(
        message: NotificationDetails,
        textColor: UIColor,
        successTitle: String,
        failureTitle: String?,
        completion inputCompletion: @escaping (Bool) -> Void,
        hasExtra extraView: UIView? = nil
    ) {
        var bs = UIScreen.main.bounds
        bs.size.width *= 0.8
        super.init(frame: bs)
        
        let completion = { (x: Bool) -> Void in
            SwiftEntryKit.dismiss(.displayed)
            inputCompletion(x)
        }
        container.flex.alignItems(.center).define { (flex) in
            if
                let imageName = message.imageName,
                let img = UIImage(named: imageName) ?? UIImage(systemName: imageName) {
                let imageView = UIImageView()
                imageView.tintColor = textColor
                imageView.image = img.withTintColor(textColor)
                flex.addItem(imageView).height(25).aspectRatio(of: imageView)
            }
            let titleLabel = UILabel(central: 18, weight: .bold)
            titleLabel.singleCentralLine()
            titleLabel.textColor = textColor
            titleLabel.text = message.title
            flex.addItem(titleLabel).width(100%).marginTop(10).marginBottom(20)
            
            if message.desc != "" {
                let descLabel = UILabel(central: 15, weight: .regular)
                descLabel.textColor = textColor
                descLabel.text = message.desc
                descLabel.numberOfLines = 0
                // max column width
                let maxWidth = "0".calculateSize(
                    ofFontSize: 15,
                    weight: .regular,
                    inFrame: .infinite,
                    andInsets: .zero,
                    repeating: 28
                ).width
                flex.addItem(descLabel).maxWidth(maxWidth).marginBottom(25)
            }
            
            if let extraView = extraView {
                flex.addItem(extraView).marginBottom(25)
            }
            
            flex.addItem()
                .marginHorizontal(20)
                .justifyContent(.spaceAround)
                .height(35)
                .width(100%)
                .direction(.row).define { (flex) in
                    let successButton = FlexButton()
                    successButton.title = successTitle
                    successButton.tapped = { _ in completion(true) }
                    
                    if let failureTitle = failureTitle {
                        let failureButton = FlexButton()
                        failureButton.title = failureTitle
                        failureButton.tapped = { _ in completion(false) }
                        flex.addItem(failureButton).width(failureButton.wrappedWidth)
                    }
                    flex.addItem(successButton).width(successButton.wrappedWidth)
            }
        }
        addSubview(container)
        container.frame = CGRect(origin: .zero, size: CGSize(width: frame.width, height: 0))
        container.flex.layout(mode: .adjustHeight)
        
        container.set(.height, of: container.frame.height)
        container.set(.width, of: container.frame.width)
        container.layoutToSuperview(axis: .horizontally, offset: 8, priority: .must)
        container.layoutToSuperview(axis: .vertically, offset: 20, priority: .must)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension CosmosView {
    static func normal(sized size: CGFloat) -> CosmosView {
        let s = CosmosView()
        s.settings.starSize = size
        s.settings.totalStars = 5
        s.settings.fillMode = .half
        s.settings.minTouchRating = 0
        s.settings.textColor = ColorScheme.normal.highlightColorA
        s.settings.emptyBorderColor = ColorScheme.normal.highlightColorA
        s.settings.filledBorderColor = ColorScheme.normal.highlightColorA
        s.settings.updateOnTouch = false
        return s
    }
    static func normal() -> CosmosView {
        return normal(sized: 35)
    }
}

class APRatingView: APConfirmationView {
    let stars = CosmosView.normal()
    init(
        message: NotificationDetails,
        textColor: UIColor,
        completion inputCompletion: @escaping (Double?) -> Void
    ) {
        stars.settings.updateOnTouch = true
        let completion = { [weak stars] (_ x: Bool) in
            if x, let stars = stars {
                inputCompletion(stars.rating)
            } else {
                inputCompletion(nil)
            }
        }
        super.init(
            message: message,
            textColor: textColor,
            successTitle: "ok",
            failureTitle: "cancel",
            completion: completion,
            hasExtra: stars
        )
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class APTextInputView: APConfirmationView {
    let stars = UITextField()
    init(
        message: NotificationDetails,
        placeholder: String? = nil,
        textColor: UIColor,
        completion inputCompletion: @escaping (String?) -> Void
    ) {
        stars.placeholder = placeholder
        stars.schemedTextColor(.highlightColorA)
        stars.textAlignment = .center
        let completion = { [weak stars] (_ x: Bool) in
            if x, let stars = stars {
                inputCompletion(stars.text)
            } else {
                inputCompletion(nil)
            }
        }
        super.init(
            message: message,
            textColor: textColor,
            successTitle: "ok",
            failureTitle: "cancel",
            completion: completion,
            hasExtra: stars
        )
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class APProcessingNoteMessageView: UIView {
    
    // MARK: Props
    private var activityIndicatorView: UIActivityIndicatorView!
    private var noteMessageView: EKNoteMessageView!
    private let contentView = UIView()
    var accessoryView: UIView!
    
    /** Activity indication can be turned off / on */
    public var isProcessing: Bool = true {
        didSet {
            if isProcessing {
                activityIndicatorView.startAnimating()
            } else {
                activityIndicatorView.stopAnimating()
            }
        }
    }
    
    // MARK: Setup
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(
        with content: EKProperty.LabelContent,
        activityIndicatorColor: UIColor
    ) {
        super.init(frame: UIScreen.main.bounds)
        setup(with: content, activityIndicatorColor: activityIndicatorColor)
    }
    
    func setup(with content: EKProperty.LabelContent,
               activityIndicatorColor: UIColor,
               setProcessing: Bool = true) {
        activityIndicatorView = UIActivityIndicatorView()
        activityIndicatorView.style = .medium
        activityIndicatorView.color = activityIndicatorColor
        isProcessing = setProcessing
        accessoryView = activityIndicatorView
        
        clipsToBounds = true
        
        addSubview(contentView)
        contentView.layoutToSuperview(.centerX, .top, .bottom)
        contentView.layoutToSuperview(.bottom, relation: .greaterThanOrEqual, offset: 8)
        contentView.layoutToSuperview(.top, relation: .lessThanOrEqual, offset: -8)
        
        noteMessageView = EKNoteMessageView(with: content)
        noteMessageView.horizontalOffset = 8
        noteMessageView.verticalOffset = 15
        contentView.addSubview(noteMessageView)
        noteMessageView.layoutToSuperview(.top, .bottom, .trailing)
        
        contentView.addSubview(accessoryView)
        accessoryView.layoutToSuperview(.leading, .centerY)
        accessoryView.layout(.trailing, to: .leading, of: noteMessageView)
    }
}

class APProgressFloat: UIView {
    let container = UIView()
    var bars: [(Flex, UILabel)] = []
    
    func set(bar: Int, to progress: Double) {
        guard bar < bars.count else { return }
        bars[bar].0.width(Int(progress * 100)%)
        bars[bar].1.text = "\(Int(progress * 100))%"
        container.flex.layout()
    }
    
    init(bars: Int) {
        var bs = UIScreen.main.bounds
        bs.size.width *= 0.8
        super.init(frame: bs)
        
        let padY: CGFloat = 10
        container.flex.marginTop(padY).define { (flex) in
            for _ in 0..<bars {
                flex.addItem().direction(.row).alignItems(.center).height(30).marginBottom(padY).define({ (flex) in
                    let f = UIView()
                    f.schemedBG(.highlightColorA)
                    let label = UILabel(right: 15, weight: .bold)
                    label.text = "000%"
                    label.singleCentralLine()
                    let ff = flex.addItem(f).height(5).shrink(1)
                    flex.addItem(label).alignSelf(.end)
                        .shrink(0)
                        .width(label.intrinsicContentSize.width)
                        .height(100%)
                        .marginRight(5)
                    label.text = ""
                    self.bars.append((ff, label))
                    label.schemedTextColor(.primaryTextColor)
                })
            }
        }
        
        addSubview(container)
        container.frame = CGRect(origin: .zero, size: CGSize(width: frame.width, height: 0))
        container.flex.layout(mode: .adjustHeight)
        
        container.set(.height, of: container.frame.height)
        container.set(.width, of: container.frame.width)
        container.layoutToSuperview(axis: .horizontally, offset: 0, priority: .must)
        container.layoutToSuperview(axis: .vertically, offset: 20, priority: .must)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//
//  Message.swift
//  Groomly
//
//  Created by Ravi on 06/08/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import SwiftEntryKit
import Async

enum ProcessType: String {
    case loadingLocation = "Loading Location..."
    case loadingAppointments = "Loading appointments..."
}

fileprivate class PassiveProcessTracker {
    static let shared = PassiveProcessTracker()
    private var runningProcess: [ProcessType: String] = [:]
    
    func add(process: ProcessType) {
        runningProcess[process] = process.rawValue
    }
    
    func remove(process: ProcessType) {
        let _ = runningProcess.removeValue(forKey: process)
    }
    
    func exists(process: ProcessType) -> Bool {
        runningProcess[process] != nil
    }
    
    func pullForward(process: ProcessType) {
        if let message = runningProcess[process] {
            DispatchQueue.main.async {
                UIApplication.shared.windows.first!
                    .rootViewController?
                    .view.isUserInteractionEnabled = false
            }
            Feedback.showNotification(message: NotificationDetails(title: message), type: .progress)
        }
    }
}

class Message {
    
    static func notify<T>(
        process: Future<T>?,
        loadingMessage: ProcessType,
        successMessage: String?,
        errorMessage: String?,
        inForeground: Bool = true,
        silentSuccess: Bool = true,
        throwNilPromise: Bool = false,
        onCompletion: @escaping (Result<T, Error>) -> Void
    ) {
        guard !PassiveProcessTracker.shared.exists(process: loadingMessage) else {
            if inForeground {
                PassiveProcessTracker.shared.pullForward(process: loadingMessage)
            }
            return
        }
        PassiveProcessTracker.shared.add(process: loadingMessage)
        Message.notify(
            process: process,
            loadingMessage: inForeground ? loadingMessage.rawValue : nil,
            successMessage: successMessage,
            errorMessage: errorMessage == nil ? nil : { _ in errorMessage! },
            silentSuccess: silentSuccess,
            throwNilPromise: throwNilPromise
        ) { object in
            PassiveProcessTracker.shared.remove(process: loadingMessage)
            onCompletion(object)
        }
    }

    
    static func curriedNotify<T>(
        loadingMessage: String?,
        successMessage: String?,
        errorMessage: String?,
        silentSuccess: Bool = true,
        throwNilPromise: Bool = false,
        onCompletion: @escaping (Result<T, Error>) -> Void
    ) -> (Future<T>) -> Void {
        return { process in
            notify(
                process: process,
                loadingMessage:
                loadingMessage,
                successMessage: successMessage,
                errorMessage: errorMessage,
                silentSuccess: silentSuccess,
                throwNilPromise: throwNilPromise,
                onCompletion: onCompletion
            )
        }
    }
    
    static func notify<T>(
        process: Future<T>?,
        loadingMessage: String?,
        successMessage: String?,
        errorMessage: String?,
        silentSuccess: Bool = true,
        throwNilPromise: Bool = false,
        onCompletion: @escaping (Result<T, Error>) -> Void
    ) {
        Message.notify(
            process: process,
            loadingMessage: loadingMessage,
            successMessage: successMessage,
            errorMessage: errorMessage == nil ? nil : { _ in errorMessage! },
            silentSuccess: silentSuccess,
            throwNilPromise: throwNilPromise,
            onCompletion: onCompletion
        )
    }
    
    static func notify<T>(
        process: Future<T>?,
        loadingMessage: String?,
        successMessage: String?,
        errorMessage: ((Error) -> String)?,
        silentSuccess: Bool = true,
        throwNilPromise: Bool = false,
        onCompletion: @escaping (Result<T, Error>) -> Void
    ) {
        Message.notify(
            process: process,
            message: loadingMessage == nil ? nil : NotificationDetails(title: loadingMessage!),
            onSuccessMessage: successMessage == nil ? nil : NotificationDetails(
                title: successMessage!,
                imageName: "checkmark"
            ),
            onFailureMessage: errorMessage == nil ? nil : { error in
                NotificationDetails(
                    title: errorMessage!(error),
                    imageName: "exclamationmark.triangle.fill"
                )
            },
            silentSuccess: silentSuccess,
            throwNilPromise: throwNilPromise,
            onCompletion: onCompletion
        )
    }
    
    static func notify<T>(
        process: Future<T>?,
        message: NotificationDetails?,
        onSuccessMessage: NotificationDetails?,
        onFailureMessage: ((Error) -> NotificationDetails)?,
        silentSuccess: Bool = true,
        throwNilPromise: Bool = false,
        onCompletion: @escaping (Result<T, Error>) -> Void
    ) {
        let displayMessage = {
            (message: NotificationDetails?, type: FeedbackType) -> Void in
            if let message = message {
                Feedback.showNotification(message: message, type: type)
            }
        }
        
        guard let process = process else {
            if throwNilPromise {
                displayMessage(onFailureMessage?(ActionError.invalid), .message)
                onCompletion(.failure(ActionError.invalid))
            }
            return
        }
        
        let sema = DispatchSemaphore(value: 0)
        DispatchQueue.global(qos: .background).async {
            let result = sema.wait(timeout: .now() + 0.8)
            if case .timedOut = result {
                displayMessage(message, .progress)
            }
        }
        
        if message != nil {
            DispatchQueue.main.async {
                UIApplication.shared.windows.first!
                    .rootViewController?
                    .view.isUserInteractionEnabled = false
            }
        }
        
        let complete = { (_ result: Result<T, Error>) -> Void in
            sema.signal()
            DispatchQueue.main.async {
                UIApplication.shared.windows.first!
                    .rootViewController?
                    .view.isUserInteractionEnabled = true
                let wasDisplaying = SwiftEntryKit.isCurrentlyDisplaying
                switch result {
                case .success(let res):
                    SwiftEntryKit.dismiss(.displayed)
                    if !silentSuccess || wasDisplaying {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
                            displayMessage(onSuccessMessage, .message)
                        }
                    }
                    onCompletion(.success(res))
                case .failure(let error):
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
                        displayMessage(onFailureMessage?(error), .message)
                    }
                    onCompletion(.failure(error))
                }
            }
        }
        
        process.do { result in
            complete(.success(result))
        }.catch { error in
            complete(.failure(error))
        }
    }
    
    static func showConfirmation(title: String, description: String = "", cancelExists: Bool = true, completion: @escaping (Bool) -> Void) {
        Feedback.showConfirmation(
            popup: NotificationDetails(
                title: title,
                desc: description
            ),
            successTitle: "ok",
            failureTitle: cancelExists ? "cancel" : nil,
            completion: completion
        )
    }
}

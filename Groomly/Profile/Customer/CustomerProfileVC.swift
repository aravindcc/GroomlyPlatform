//
//  ProfileVC.swift
//  Groomly
//
//  Created by clar3 on 13/05/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import PinLayout
import FlexLayout
import Cosmos

class ShareAccProfView: UIView {
    let containerView = UIView()
    init(processor: SubProcessVC) {
        super.init(frame: .zero)
        
        let titleLabel = UILabel(left: 25, weight: .semibold)
        titleLabel.text = Networker.shared.session?.email
        titleLabel.adjustsFontSizeToFitWidth = true
        titleLabel.schemedTextColor(.primaryTextColor)
        
        let stars = CosmosView.normal()
        stars.rating = 0
        let numReviewLabel = UILabel(left: 17, weight: .ultraLight)
        numReviewLabel.singleCentralLine()
        numReviewLabel.schemedTextColor(.primaryTextColor, alpha: 0.8)
                           
        let addressVC = AddressVC(viewType: .embedded)
        let addressBlock = BlockView()
        addressBlock.buttonTitle = "edit"
        addressBlock.title = "Locations"
        addressBlock.tapped { [weak processor] _ in
            processor?.show(process: AddressVC(viewType: .edit))
        }
        processor.add(addressVC, block: addressBlock)
        
        addSchemedView(containerView)
        containerView.flex.define { (flex) in
            flex.addItem(titleLabel).marginVertical(10).width(95%)
            flex.addItem().height(35).direction(.row).width(100%).alignItems(.stretch).marginBottom(15).define { (flex) in
                flex.addItem(stars).marginRight(10)
                flex.addItem(numReviewLabel).grow(1)
            }
            flex.addItem(addressBlock).grow(1).marginBottom(10)
        }
        
        if let num = Networker.shared.session?.score {
            numReviewLabel.text = "\(num.num) review\(num.num == 1 ? "" : "s")"
            stars.rating = Double(num.score)
        } else {
            numReviewLabel.text = "no reviews"
            stars.rating = 0
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        containerView.pin.all()
        containerView.flex.layout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class CustomerProfileVC: SubProcessVC {
    let containerView = UIView()
    let contentWrap = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        contentWrap.schemedBG()
        contentWrap.layer.cornerRadius = 20
        contentWrap.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        view.addSchemedView(contentWrap)
        contentWrap.addSchemedView(containerView)
        
        view.schemedBG()
        
        let cardVC = CardViewer(viewType: .embedded)
        let cardBlock = BlockView()
        cardBlock.accessibilityIdentifier = "embeddedCardView"
        cardBlock.buttonTitle = "edit"
        cardBlock.title = "Payment"
        cardBlock.tapped { [weak self] _ in
            self?.show(process: CardViewer(viewType: .edit))
        }
        add(cardVC, block: cardBlock)
        
        containerView.flex.define { (flex) in
            flex.addItem(ShareAccProfView(processor: self)).minHeight(250).width(100%)
            flex.addItem(cardBlock).grow(1).marginVertical(10)
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        contentWrap.pin.top(safeAreaInsets).horizontally().bottom(view.pin.safeArea)
        containerView.pin.all(20)
        containerView.flex.layout()
    }
}

//
//  NewCardVC.swift
//  Groomly
//
//  Created by clar3 on 14/05/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import Stripe
import PinLayout
import FlexLayout

extension NewCardVC: STPPaymentCardTextFieldDelegate {

    func paymentCardTextFieldDidChange(_ textField: STPPaymentCardTextField) {
        updateDoneButton()
    }
    
    func paymentCardTextFieldDidBeginEditingCVC(_ textField: STPPaymentCardTextField) {
        var isAmex = false
        if let card = textField.cardNumber {
            isAmex = STPCardValidator.brand(forNumber: card) == STPCardBrand.amex
        }
        let newImage: UIImage? = isAmex ? cardAmexBackImage : cardBackImage
        let transition: UIView.AnimationOptions = isAmex ? .transitionCrossDissolve : .transitionFlipFromRight
        UIView.transition(with: imageView, duration: 0.2, options: transition, animations: {
            self.imageView.image = newImage
        }, completion: nil)
    }
    
    func paymentCardTextFieldDidEndEditingCVC(_ textField: STPPaymentCardTextField) {
        var isAmex = false
        if let card = textField.cardNumber {
            isAmex = STPCardValidator.brand(forNumber: card) == STPCardBrand.amex
        }
        let transition: UIView.AnimationOptions = isAmex ? .transitionCrossDissolve : .transitionFlipFromLeft
        UIView.transition(with: imageView, duration: 0.2, options: transition, animations: {
            self.imageView.image = self.cardFrontImage
        }, completion: nil)
    }
}

class NewCardVC: APListNewController<STPPaymentMethod> {
    
    let inputField = STPPaymentCardTextField()
    let imageView = UIImageView()
    let containerView = UIView()
    var addButton: FlexButton!
    
    let cardFrontImage = UIImage(named: "stp_card_form_front")
    let cardBackImage = UIImage(named: "stp_card_form_back")
    let cardAmexBackImage = UIImage(named: "stp_card_form_amex_cvc")

    override func viewDidLoad() {
        super.viewDidLoad()
        
        preferredContentSize = CGSize(width: .zero, height: 400)

        view.colorScheme = .normal
        view.schemedBG()
        view.addSchemedView(containerView)
        
        inputField.postalCodeEntryEnabled = false
        inputField.textColor = view.colorScheme!.primaryTextColor
        inputField.font = UIFont.systemFont(ofSize: 15, weight: .bold)
        inputField.delegate = self
        
        imageView.image = cardFrontImage
        imageView.schemedTintColor(.highlightColorC)
        
        addButton = FlexButton()
        addButton.enabled = false
        addButton.title = "Add"
        addButton.preset = true
        addButton.tapped = {
            [weak self] _ in
            if
                let self = self,
                let backend = self.apiDataSource as? PaymentBackend,
                let context = self.navigationController {
                backend.add(field: self.inputField, presenter: context)
            }
            self?.navigationController?.popViewController(animated: true)
        }
        
        containerView.flex.alignItems(.center).marginHorizontal(20).define { (flex) in
            flex.addItem(imageView).width(75%).aspectRatio(of: imageView).marginVertical(10)
            flex.addItem(inputField).alignSelf(.stretch).height(40).marginHorizontal(5).marginVertical(30) 
            flex.addItem(addButton).width(addButton.wrappedWidth).alignSelf(.center).height(40).marginTop(20)
        }
    }
    
    func updateDoneButton() {
        addButton.enabled = inputField.isValid
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        containerView.pin.all(view.pin.safeArea)
        containerView.flex.layout()
    }
}


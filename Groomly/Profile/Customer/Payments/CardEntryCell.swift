//
//  File.swift
//  Groomly
//
//  Created by clar3 on 14/05/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import IGListKit
import PinLayout
import FlexLayout
import Stripe


class CardEntryCell: APListEntryCell {
    
    private var leftIcon = UIImageView()
    private var titleLabel = UILabel(left: 15, weight: .semibold)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        titleLabel.singleCentralLine()
        infoView.flex.direction(.row).alignItems(.stretch).define { (flex) in
            flex.addItem(leftIcon).alignSelf(.center).aspectRatio(of: leftIcon)
            flex.addItem(titleLabel).grow(1).marginHorizontal(10)
        }
    }
    
    func primaryColorForPaymentOption(withSelected selected: Bool) -> UIColor? {
        return selected ? colorScheme!.highlightColorB : colorScheme!.primaryTextColor.withAlphaComponent(0.8)
    }
    
    func buildAttributedString(
        withBrand brand: STPCardBrand,
        last4: String?,
        selected: Bool
    ) -> NSAttributedString? {
        let brandString = STPCard.string(from: brand)
        let label = String(format: "%@ Ending In %@", brandString, last4 ?? "")
        let primaryColor = selected ? colorScheme!.highlightColorB : colorScheme!.primaryTextColor
        let secondaryColor = primaryColor.withAlphaComponent(0.6)
        let emphasisFont = UIFont.systemFont(ofSize: 15, weight: .bold)

        let attributes: [NSAttributedString.Key: Any] = [
                NSAttributedString.Key.foregroundColor: secondaryColor,
                NSAttributedString.Key.font: titleLabel.font!
        ]

        let attributedString = NSMutableAttributedString(string: label, attributes: attributes)
        attributedString.addAttribute(.foregroundColor, value: primaryColor, range: (label as NSString).range(of: brandString))
        if let last4 = last4, let rng = label.range(of: last4) {
            let start = label.distance(from: label.startIndex, to: rng.lowerBound)
            let length = label.distance(from: rng.lowerBound, to: rng.upperBound)
            let range = NSMakeRange(start, length)
            attributedString.addAttribute(.foregroundColor, value: primaryColor, range: range)
            attributedString.addAttribute(.font, value: emphasisFont, range: range)
        }
        if let rng = label.range(of: brandString) {
            let start = label.distance(from: label.startIndex, to: rng.lowerBound)
            let length = label.distance(from: rng.lowerBound, to: rng.upperBound)
            let range = NSMakeRange(start, length)
            attributedString.addAttribute(.font, value: emphasisFont, range: range)
        }
        return attributedString
    }
    
    func buildAttributedString(with card: STPCard?, selected: Bool) -> NSAttributedString? {
        guard let card = card else { return nil }
        return buildAttributedString(
            withBrand: card.brand,
            last4: card.last4,
            selected: selected)
    }

    func buildAttributedString(withCardSource card: STPSource?, selected: Bool) -> NSAttributedString? {
        guard let details = card?.cardDetails else { return nil }
        return buildAttributedString(
            withBrand: details.brand,
            last4: details.last4,
            selected: selected)
    }

    func buildAttributedString(withCardPaymentMethod paymentMethod: STPPaymentMethod?, selected: Bool) -> NSAttributedString? {
        guard let details = paymentMethod?.card else { return nil }
        return buildAttributedString(
            withBrand: details.brand,
            last4: details.last4,
            selected: selected)
    }
    
    func buildAttributedString(withCardPaymentMethodParams paymentMethodParams: STPPaymentMethodParams?, selected: Bool) -> NSAttributedString? {
        guard let details = paymentMethodParams?.card?.number else { return nil }
        let brand = STPCardValidator.brand(forNumber: details)
        return buildAttributedString(
            withBrand: brand,
            last4: paymentMethodParams?.card?.last4,
            selected: selected)
    }
    
    func buildAttributedString(forPaymentOption paymentOption: STPPaymentOption, selected: Bool) -> NSAttributedString? {
        if (paymentOption is STPCard) {
            return buildAttributedString(with: paymentOption as? STPCard, selected: selected)
        } else if (paymentOption is STPSource) {
            let source = paymentOption as? STPSource
            if source?.type == STPSourceType.card && source?.cardDetails != nil {
                return buildAttributedString(withCardSource: source, selected: selected)
            }
        } else if (paymentOption is STPPaymentMethod) {
            let paymentMethod = paymentOption as? STPPaymentMethod
            if paymentMethod?.type == STPPaymentMethodType.card && paymentMethod?.card != nil {
                return buildAttributedString(withCardPaymentMethod: paymentMethod, selected: selected)
            }
        } else if (paymentOption is STPApplePayPaymentOption) {
            if let primaryColor = primaryColorForPaymentOption(withSelected: selected) {
                return NSAttributedString(string: "Apple Pay", attributes: [
                    NSAttributedString.Key.foregroundColor: primaryColor
                ])
            }
        } else if (paymentOption is STPPaymentMethodParams) {
            let paymentMethodParams = paymentOption as? STPPaymentMethodParams
            if paymentMethodParams?.type == STPPaymentMethodType.card && paymentMethodParams?.card != nil {
                return buildAttributedString(withCardPaymentMethodParams: paymentMethodParams, selected: selected)
            }
        }
        return nil
    }
    
    override func bindViewModel(_ viewModel: Any) {
        super.bindViewModel(viewModel)
        guard let info = viewModel as? ListInfoModel, let paymentOption = info.model as? STPPaymentOption else {
            leftIcon.image = nil
            titleLabel.text = nil
            return
        }
        leftIcon.image = paymentOption.templateImage
        leftIcon.tintColor = primaryColorForPaymentOption(withSelected: info.selected)
        titleLabel.attributedText = buildAttributedString(forPaymentOption: paymentOption, selected: info.selected)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


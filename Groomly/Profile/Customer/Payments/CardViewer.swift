//
//  CardViewer.swift
//  Groomly
//
//  Created by clar3 on 14/05/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import Stripe
import Async
import IGListKit

extension STPPaymentMethod: ListDiffable {
    public func diffIdentifier() -> NSObjectProtocol {
        return self.stripeId as NSObjectProtocol
    }
    
    public func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        if self === object { return true }
        guard let object = object as? STPPaymentMethod else { return false }
        return  stripeId == object.stripeId
    }
}

extension Array where Element: ListDiffable {
    func equals(_ rhs: [Element]) -> Bool {
        guard count == rhs.count else { return false }
        for i in 0..<count {
            if !self[i].isEqual(toDiffableObject: rhs[i]) {
                return false
            }
        }
        return true
    }
}

class CardViewer: APListController<STPPaymentMethod, CardEntryCell> {
    override var rowHeight: CGFloat { 40.0 }
    override var notSelectedString: String {
        "Please choose a payment method."
    }
    init(viewType: APListModeReduced) {
        switch viewType {
        case .edit:
            super.init(viewType: .edit(onNew: NewCardVC()))
        case .embedded:
            super.init(viewType: .embedded)
        case .full(let onSelect):
            super.init(viewType: .full(doneTitle: "pay", onSelect: onSelect, onNew: NewCardVC()))
        case .selection(let onSelect):
            super.init(viewType: .selection(doneTitle: "pay", onSelect: onSelect))
        }

        apiDataSource = PaymentBackend.shared
        preferredContentSize = CGSize(width: .zero, height: 380)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

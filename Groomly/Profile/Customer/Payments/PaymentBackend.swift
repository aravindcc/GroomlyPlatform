//
//  PaymentBackend.swift
//  Groomly
//
//  Created by Ravi on 15/08/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import Stripe
import Alamofire
import Async
import SwiftEntryKit

fileprivate struct RemoveIntentRequest: Codable {
    let payment_method: String
    let replacement_method: String?
}

fileprivate struct IntentResponse: Codable {
    let secret: String
    let name: String
}

fileprivate struct RemoveIntentResponse: Codable {
    enum RemoveIntentState: String, Codable {
        case ok, action
    }
    let state: RemoveIntentState
}

fileprivate struct SelectIntentMessage: Codable {
    let selected: String?
    static let blank = SelectIntentMessage(selected: nil)
}

fileprivate extension Networker {
    func intent() -> Future<IntentResponse> {
        send("/payment/intent", .get, blank)
    }
    
    func remove(method: RemoveIntentRequest) -> Future<RemoveIntentResponse> {
        send("/payment/intent", .post, method)
    }
    
    func selected(method: SelectIntentMessage) -> Future<SelectIntentMessage> {
        send("/payment/intent", .patch, method)
    }
    
    func secret(for bookingID: Int) -> Future<IntentResponse> {
        send("/payment/epheremal", .get, GetModelRequest(id: bookingID))
    }
    
    func verify(booking bookingID: Int) -> Future<BlankResponse> {
        send("/payment/epheremal", .patch, GetModelRequest(id: bookingID))
    }
}

class PaymentBackend: ListBackend<STPPaymentMethod>, STPCustomerEphemeralKeyProvider, STPAuthenticationContext {
    func authenticationPresentingViewController() -> UIViewController {
        DispatchQueue.main.async {
            SwiftEntryKit.dismiss(.displayed)
        }
        return requestedController ?? UIApplication.shared.windows.first!.rootViewController!
    }
    
    static let shared = PaymentBackend()
    private var context: STPCustomerContext?
    private var worker: Worker!
    private weak var requestedController: UIViewController?

    override init() {
        super.init()
        let eventLoopGroup = MultiThreadedEventLoopGroup(numberOfThreads: 1)
        let eventLoop = eventLoopGroup.next()
        self.worker = eventLoop
    }
    func setupCustomer() {
        context = STPCustomerContext(keyProvider: self)
    }
    
    func createCustomerKey(withAPIVersion apiVersion: String, completion: @escaping STPJSONResponseCompletionBlock) {
        guard
            let token = Networker.shared.cachedLogin?.token,
            let url = Networker.shared.path("/payment/epheremal")
        else {
            completion(nil, NetworkReqError.InvalidParams)
            return
        }

        let parameters: [String: Any] = ["api_version": apiVersion]
        let headers = HTTPHeaders(["Authorization": token])

        AF.request(url, method: .post, parameters: parameters, headers: headers).responseJSON { (response) in
            if case let .success(data) = response.result, let json = data as? [AnyHashable: Any] {
                completion(json, nil)
                super.load()
            } else {
                completion(nil, NetworkReqError.ResponseDecodeError)
            }
        }
    }
    
    fileprivate func completeIntent(_ setupIntentParams: STPSetupIntentConfirmParams) -> Future<String?> {
        let promise = APIClient.api.worker.eventLoop.newPromise(String?.self)
        let paymentHandler = STPPaymentHandler.shared()
        
        DispatchQueue.main.async {
            paymentHandler.confirmSetupIntent(setupIntentParams, with: self) { status, setupIntent, error in
                if case .succeeded = status, let paymentID = setupIntent?.paymentMethodID {
                    promise.succeed(result: paymentID)
                }
                else if let error = error {
                    promise.fail(error: ActionError.detail(message: error))
                } else {
                    promise.fail(error: ActionError.cancelled)
                }
            }
        }
        return promise.futureResult
    }
    
    func add(field: STPPaymentCardTextField, presenter: UIViewController) {
        self.requestedController = presenter
        let promise = APIClient.api.intent().flatMap { (response) in
            let cardParams = field.cardParams
            let billingDetails = STPPaymentMethodBillingDetails()
            billingDetails.name = response.name
            if let selected = AddressBackend.shared.selected {
                let addr = STPPaymentMethodAddress()
                addr.line1 = selected.addressOne
                addr.line2 = selected.addressTwo
                addr.city = selected.city
                addr.postalCode = selected.postcode
                billingDetails.address = addr
            }
            let paymentMethodParams = STPPaymentMethodParams(card: cardParams, billingDetails: billingDetails, metadata: nil)
            let setupIntentParams = STPSetupIntentConfirmParams(clientSecret: response.secret)
            setupIntentParams.paymentMethodParams = paymentMethodParams
            return self.completeIntent(setupIntentParams)
        }.flatMap { out in
            APIClient.api.selected(method:
                SelectIntentMessage(selected: out)
            )
        }.flatMap { _ in
            self.retrievePaymentMethods()
        }
        
        run(
            process: promise,
            loadingMessage: "Adding payment method...",
            successMessage: "Added payment method",
            errorMessage: {
                if let error = $0 as? ActionError, case let .detail(message) = error {
                    return message.localizedDescription
                }
                return "Error adding payment method"
            },
            silentSuccess: false
        )
    }
    
    fileprivate func remove(method: STPPaymentMethod, withSelected selected: String?) -> Future<RemoveIntentResponse> {
        APIClient.api.remove(method: RemoveIntentRequest(
            payment_method: method.stripeId,
            replacement_method: selected
        ))
    }
    
    func retrieveLastPaymentMethod() -> Future<String?> {
        guard let context = context else { return worker.at(once: nil) }
        let promise = worker.eventLoop.newPromise(String?.self)
        context.retrieveCustomer { (customer, error) in
            guard error == nil, let paymentID = customer?.defaultSource?.stripeID else {
                promise.fail(error: error ?? NetworkReqError.Misc)
                return
            }
            promise.succeed(result: paymentID)
        }
        return promise.futureResult
    }
    
    private var selectedMethod: String?
    func retrievePaymentMethods() -> Future<[STPPaymentMethod]> {
        let promise = worker.eventLoop.newPromise([STPPaymentMethod].self)
        if let context = context {
            context.clearCache()
            context.listPaymentMethodsForCustomer { (methods, error) in
                guard error == nil, let methods = methods else {
                    promise.fail(error: error ?? NetworkReqError.Misc)
                    return
                }
                APIClient.api.selected(method: .blank).map { (past) in
                    if let past = past.selected {
                        self.selectedMethod = past
                    }
                }.always {
                    promise.succeed(result: methods)
                }
            }
        }
        else {
            promise.fail(error: NetworkReqError.Authorization)
        }
        return promise.futureResult
    }


    override func remove(model: STPPaymentMethod) {
        let onUsage = { (action: Future<Void>) in
            let promi = action.flatMap { _ in
                self.retrievePaymentMethods()
            }
            self.run(
                process: promi,
                loadingMessage: "Removing payment method...",
                successMessage: "Removed payment method",
                errorMessage: "Payment method could not be deleted because it is currently in use."
            )
        }
        
        let secondRound = { () -> Future<Void> in
            let newOne = self.worker.eventLoop.newPromise(Void.self)
            SwiftEntryKit.dismiss(.displayed)
            Message.showConfirmation(
                title: "Payment Method in Use.",
                description: Talk.it.paymentMethodInUse
            ) { (chosen) in
                if chosen, let selected = self.selectedMethod {
                    let final = self.remove(method: model, withSelected: selected).map { (chained) in
                        if case .action = chained.state {
                            throw NetworkReqError.Misc
                        }
                    }
                    Message.notify(
                        process: newOne.futureResult,
                        loadingMessage: "Removing payment method...",
                        successMessage: nil,
                        errorMessage: String?.none
                    ) { _ in }
                    final.cascade(promise: newOne)
                    return
                }
                newOne.fail(error: ActionError.cancelled)
            }
            return newOne.futureResult
        }
        
        let promi = remove(method: model, withSelected: nil).flatMap { cb -> Future<Void> in
            if case .action = cb.state {
                if self.selectedMethod != nil {
                    return secondRound()
                }
                throw NetworkReqError.InvalidParams
            }
            return self.worker.at(once: ())
        }
        
        onUsage(promi)
    }

    override func select(model: STPPaymentMethod) {
        let promi = APIClient.api.selected(method:
            SelectIntentMessage(selected: model.stripeId)
        ).flatMap { _ in
            self.retrievePaymentMethods()
        }
        run(
            process: promi,
            loadingMessage: "Selecting method as primary...",
            successMessage: nil,
            errorMessage: "Error selecting payment method"
        )
    }
    
    override func refreshList() {
        run(
            process: retrievePaymentMethods(),
            loadingMessage: nil,
            successMessage: nil,
            errorMessage: "Error loading your payment information."
        )
    }
    
    override func getSelected() -> Int {
        if
            let method = selectedMethod,
            let index = cachedList.firstIndex(where: { $0.stripeId == method }) {
            return index
        }
        return 0
    }
    
    func authenticate(booking bookingID: Int, showingRemedyOn: SubProcessVC? = nil) -> Future<Void> {
        requestedController = showingRemedyOn
        let promise = worker.eventLoop.newPromise(Void.self)
        APIClient.api.secret(for: bookingID).do { (intent) in
            self.startRecoveryFlow(forBooking: bookingID, clientSecret: intent.secret, cascadingTo: promise, showingRemedyOn: showingRemedyOn)
        }.catch {
            promise.fail(error: $0)
        }
        return promise.futureResult
    }
    
    private func submit(forBooking id: Int, payment params: STPPaymentIntentParams, cascadingTo promise: EventLoopPromise<Void>) {
        let submit = { (message: String) in
            DispatchQueue.main.async {
                SwiftEntryKit.dismiss(.displayed)
                Feedback.showNotification(message: NotificationDetails(
                    title: "Couldn't complete",
                    desc: message,
                    imageName: "exclamationmark.triangle.fill"
                ))
            }
        }
        let paymentHandler = STPPaymentHandler.shared()
        paymentHandler.confirmPayment(withParams: params, authenticationContext: self) { (status, paymentIntent, error) in
            switch (status) {
            case .failed:
                self.attemptRevive(
                    message: error?.localizedDescription,
                    booking: id,
                    intent: params,
                    atVC: self.requestedController as? SubProcessVC,
                    cascading: promise
                )
                break
            case .canceled:
                promise.fail(error: ActionError.invalid)
                break
            case .succeeded:
                APIClient.api.verify(booking: id).map({ _ in }).catch({ (_) in
                    Feedback.showNotification(message: NotificationDetails(
                        title: "Couldn't complete payment",
                        desc: "Server error - please try again later",
                        imageName: "exclamationmark.triangle.fill"
                    ))
                }).cascade(promise: promise)
                break
            @unknown default:
                promise.fail(error: NetworkReqError.Misc)
                submit("error processing details")
                break
            }
        }
    }
    
    func startRecoveryFlow(forBooking id: Int, clientSecret: String, cascadingTo promise: EventLoopPromise<Void>, showingRemedyOn: SubProcessVC? = nil) {
        // Retrieve the PaymentIntent
        STPAPIClient.shared.retrievePaymentIntent(withClientSecret: clientSecret) { (paymentIntent, error) in
            guard
                error == nil,
                let current_status = paymentIntent?.status else {
                // Handle error (e.g. allow your customer to retry)
                promise.fail(error: NetworkReqError.Misc)
                return
            }

            let paymentIntentParams = STPPaymentIntentParams(clientSecret: clientSecret)
            if current_status == .requiresAction {
                // Payment failed because authentication is required, reuse the PaymentMethod
                paymentIntentParams.paymentMethodId = paymentIntent?.paymentMethodId
                self.submit(forBooking: id, payment: paymentIntentParams, cascadingTo: promise)
            } else if let lastPaymentError = paymentIntent?.lastPaymentError {
                self.attemptRevive(message: lastPaymentError.message, booking: id, intent: paymentIntentParams, atVC: showingRemedyOn, cascading: promise)
            } else {
                APIClient.api.verify(booking: id).map({ _ in }).catch({ (_) in
                    Feedback.showNotification(message: NotificationDetails(
                        title: "Couldn't complete payment",
                        desc: "Server error - please try again later",
                        imageName: "exclamationmark.triangle.fill"
                    ))
                }).cascade(promise: promise)
            }

        }
    }
    
    var revivingBool = false
    private func attemptRevive(message: String?, booking id: Int, intent: STPPaymentIntentParams, atVC: SubProcessVC?, cascading promise: EventLoopPromise<Void>) {
        if let message = message {
            DispatchQueue.main.async {
                SwiftEntryKit.dismiss(.displayed)
                Feedback.showNotification(message: NotificationDetails(
                    title: "Couldn't complete payment",
                    desc: message,
                    imageName: "exclamationmark.triangle.fill"
                ))
            }
        }
        if let atVC = atVC {
            revivingBool = false
            atVC.show(process: CardViewer(viewType:
                .full(onSelect: {
                    self.revivingBool = true
                    intent.paymentMethodId = PaymentBackend.shared.selectedMethod
                    self.submit(forBooking: id, payment: intent, cascadingTo: promise)
                })
            ))  {
                if !self.revivingBool {
                    promise.fail(error: ActionError.cancelled)
                }
            }
        } else {
            promise.fail(error: ActionError.cancelled)
        }
    }
}


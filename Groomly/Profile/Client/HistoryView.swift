//
//  HistoryView.swift
//  Groomly
//
//  Created by Ravi on 22/08/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import IGListKit
import PinLayout
import FlexLayout

fileprivate let innerBlockPadding: CGFloat = 10

final class HistoryEntryCell: UICollectionViewCell, ListBindable {
    let container = UIView()
    let status = UIImageView()
    let nameLabel = UILabel(left: 16, weight: .medium)
    let timeLabel = UILabel(left: 15, weight: .regular)
    let costLabel = UILabel(right: 17, weight: .bold)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        status.schemedTintColor(.highlightColorB)
        nameLabel.schemedTextColor(.primaryTextColor)
        timeLabel.schemedTextColor(.primaryTextColor, alpha: 0.7)
        costLabel.singleCentralLine()
        costLabel.schemedTextColor(.primaryTextColor, alpha: 0.9)
        container.flex.direction(.row).alignItems(.stretch).define { (flex) in
            flex.addItem().width(30)
                .justifyContent(.center)
                .alignContent(.center)
                .marginRight(10).marginLeft(innerBlockPadding).define { (flex) in
                    flex.addItem(status)
            }
            flex.addItem().grow(1).shrink(1)
                .alignItems(.stretch)
                .alignContent(.center)
                .justifyContent(.center).define { (flex) in
                flex.addItem(nameLabel).height(25)
                flex.addItem(timeLabel).height(25).marginTop(5)
            }.marginVertical(10)
            flex.addItem(costLabel).marginLeft(10).marginRight(innerBlockPadding)
        }

        addSubview(container)
    }
    
    required init?(coder: NSCoder) {
        super.init(frame: .zero)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        container.pin.vertically(10).horizontally()
        container.flex.layout()
    }
    
    func bindViewModel(_ viewModel: Any) {
        guard let model = viewModel as? PaymentHistoryModel else { return }
        timeLabel.text = model.timeString
        costLabel.text = model.amount == 0 ? "-" : model.amount.sterling
        nameLabel.text = model.name
        let config = UIImage.SymbolConfiguration(pointSize: 20, weight: .bold)
        let icon = UIImage(systemName: model.status.icon, withConfiguration: config)
        status.image = icon
        
        status.flex.size(status.intrinsicContentSize)
        costLabel.flex.width(costLabel.intrinsicContentSize.width)
        layoutSubviews()
    }
}

final class HistoryPriceBar: UIView {
    private let container = UIView()
    private let totalLabel = UILabel(left: 16, weight: .bold)
    private let cancellationLabel = UILabel(left: 16, weight: .medium)
    
    init() {
        super.init(frame: .zero)
        
        addTargetPinClosure { (pin) in
            pin.all()
        }
        
        totalLabel.schemedTextColor(.primaryTextColor)
        cancellationLabel.schemedTextColor(.primaryTextColor)
        totalLabel.singleCentralLine()
        cancellationLabel.singleCentralLine()
        container.flex.direction(.row).alignItems(.stretch).define { (flex) in
            flex.addItem(cancellationLabel).grow(1).shrink(1)
            flex.addItem(totalLabel).width(10)
        }
        addSubview(container)
    }
    
    required init?(coder: NSCoder) {
        super.init(frame: .zero)
    }
    
    func load(response: HistoryResponse) {
        totalLabel.text = "Total: \(response.total.sterling)"
        cancellationLabel.text = "Pending: \(response.pending.sterling)"
        totalLabel.flex.width(totalLabel.intrinsicContentSize.width)
        layoutSubviews()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        container.pin.bottom().top(10).left().right(innerBlockPadding)
        container.flex.layout()
        cancellationLabel.pin.all()
    }
}
 
final class HistoryVC: UIViewController, ListAdapterDataSource {
    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        historyItems.sorted(by: { $0.startTime! > $1.startTime! })
    }
    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        TableSectionController<PaymentHistoryModel, HistoryEntryCell>(cellHeight: 75) {
            [unowned self] in
            if let mainvc = self.tabBarController as? MainTabVC {
                mainvc.process(bookingID: $0.id)
            }
        }
    }
    func emptyView(for listAdapter: ListAdapter) -> UIView? { nil }
    
    lazy var cv : UICollectionView = {
        let cv = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        cv.accessibilityIdentifier = "historyViewVC"
        return cv
    }()
    lazy var adapter: ListAdapter = {
        return ListAdapter(updater: ListAdapterUpdater(), viewController: nil)
    }()
    
    var historyItems: [PaymentHistoryModel] = [] {
        didSet {
            hasLoaded = true
            adapter.reloadData()
        }
    }
    private lazy var loadingIndicator: UIActivityIndicatorView = {
        let progressView = UIActivityIndicatorView(style: .medium)
        progressView.color = self.view.colorScheme!.highlightColorA
        progressView.tag = 88
        progressView.startAnimating()
        self.view.addSubview(progressView)
        return progressView
    }()
    var hasLoaded: Bool = true {
        didSet {
            let hasLoaded = self.hasLoaded
            UIView.animate(withDuration: 0.1) {
                self.loadingIndicator.isHidden = hasLoaded
                self.cv.isHidden = !hasLoaded
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(cv)
        cv.schemedBG()
        adapter.collectionView = cv
        adapter.dataSource = self
        hasLoaded = false
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        view.pin.all()
        loadingIndicator.pin.center()
        cv.pin.all()
    }
}

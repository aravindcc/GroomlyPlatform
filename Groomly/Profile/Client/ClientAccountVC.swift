//
//  ClientProfileVC.swift
//  Groomly
//
//  Created by clar3 on 13/05/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import PinLayout
import FlexLayout


enum HistoryFrames: String, CaseIterable {
    case week, month, all
    
    var value: Int? {
        switch self {
        case .all: return nil
        case .month: return 30
        case .week: return 7
        }
    }
}

class ClientAccountVC: SubProcessVC {
    let contentWrap = UIView()
    let containerView = UIView()
    let historyVC = HistoryVC()
    let totalBar = HistoryPriceBar()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        contentWrap.schemedBG()
        contentWrap.layer.cornerRadius = 20
        contentWrap.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        view.addSchemedView(contentWrap)
        contentWrap.addSchemedView(containerView)
        
        view.schemedBG()
        
        let historyFrameButton = OptionButton(cases: HistoryFrames.self)
        historyFrameButton.didSelectOption = { [weak self] in
            if let frame = HistoryFrames(rawValue: $0) {
                self?.loadHistory(days: frame.value)
            }
        }
        let cardBlock = BlockView(button: historyFrameButton)
        let dashboard = FlatButton()
        dashboard.defaultPx = 10
        dashboard.title = "payout"
        dashboard.tapped = { [weak self] _ in
            self?.present(APWebView(request: try? APIClient.api.getStripeDashboard()), animated: true)
        }
        cardBlock.mainButton = dashboard
        cardBlock.title = "History"
        cardBlock.showingBar = true
        cardBlock.set(bar: totalBar)
        add(historyVC, block: cardBlock)
        
        containerView.flex.define { (flex) in
            flex.addItem(ShareAccProfView(processor: self)).height(215).width(100%)
            flex.addItem(cardBlock).grow(1).marginVertical(10)
        }
        
        loadHistory(days: HistoryFrames(rawValue: historyFrameButton.selectedOption)?.value)
    }
    
    func loadHistory(days: Int? = nil) {
        historyVC.hasLoaded = false
        Message.notify(
            process: APIClient.api.getClientHistory(range: HistoryRequest(days: days)),
            loadingMessage: nil,
            successMessage: nil,
            errorMessage: "Failed to load payment history."
        ) { [weak self] (res) in
            if case .success(let response) = res {
                self?.historyVC.historyItems = response.payments
                self?.totalBar.load(response: response)
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        contentWrap.pin.top(safeAreaInsets).horizontally().bottom(view.pin.safeArea)
        containerView.pin.all(20)
        containerView.flex.layout()
    }
}

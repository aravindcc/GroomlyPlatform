//
//  APWebView.swift
//  Groomly
//
//  Created by Ravi on 21/08/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import WebKit
import PinLayout
import FlexLayout

extension FlexButton {
    convenience init(_ title: String?,_ icon: String?, px: CGFloat = 0) {
        self.init()
        self.title = title
        self.icon = icon
        self.defaultPx = px
    }
}

class APWebView: FullScreenVC {
    let wkView = WKWebView()
    let backButton = FlexButton(nil, "arrow.left", px: 10)
    let forwardButton = FlexButton(nil, "arrow.right", px: 10)
    let closeButton = FlexButton(nil, "xmark.circle.fill", px: 10)
    let containerView = UIView()
    let progressView: UIView = {
        let v = UIView()
        v.schemedBG(.highlightColorC)
        return v
    }()
    var showingControls = true {
        didSet {
            UIView.animate(withDuration: 0.5) {
                self.forwardButton.enabled = self.wkView.canGoForward && self.showingControls
                self.backButton.enabled = self.wkView.canGoBack && self.showingControls
                self.closeButton.enabled = self.showingControls
                self.viewDidLayoutSubviews()
            }
        }
    }
    
    let request: URLRequest?
    var showsNavigation = true
    init(request: URLRequest?, showsNavigation: Bool = true) {
        self.request = request
        self.showsNavigation = showsNavigation
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        request = nil
        super.init(nibName: nil, bundle: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(wkView)
        if let request = request {
            wkView.load(request)
        } else {
            Feedback.showNotification(message: Talk.it.authError)
        }
        
        wkView.addObserver(self, forKeyPath: #keyPath(WKWebView.canGoBack), options: .new, context: nil)
        wkView.addObserver(self, forKeyPath: #keyPath(WKWebView.canGoForward), options: .new, context: nil)
        wkView.addObserver(self, forKeyPath: #keyPath(WKWebView.estimatedProgress), options: .new, context: nil)

        
        containerView.schemedBG()
        containerView.flex.direction(.row).alignItems(.stretch).define { (flex) in
            let item = flex.addItem(closeButton).width(closeButton.wrappedWidth)
            if showsNavigation {
                item.marginRight(30)
                flex.addItem(backButton).width(backButton.wrappedWidth).marginHorizontal(30)
                flex.addItem(forwardButton).width(forwardButton.wrappedWidth)
            } else {
                item.marginHorizontal(10)
            }
        }
        
        closeButton.tapped = { [unowned self] _ in
            self.dismiss(animated: true)
        }
        backButton.tapped = { [unowned self] _ in
            self.wkView.goBack()
        }
        forwardButton.tapped = { [unowned self] _ in
            self.wkView.goForward()
        }
        view.addSubview(containerView)
        view.addSubview(progressView)
        
        self.backButton.enabled = wkView.canGoBack
        self.forwardButton.enabled = wkView.canGoForward
        
        if showsNavigation {
            let tapgesture = UITapGestureRecognizer(target: self, action: #selector(handleTap))
            containerView.addGestureRecognizer(tapgesture)
        }
    }
    
    @objc private func handleTap() {
        showingControls = !showingControls
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if let _ = object as? WKWebView {
            if keyPath == #keyPath(WKWebView.canGoBack) {
                self.backButton.enabled = wkView.canGoBack
            } else if keyPath == #keyPath(WKWebView.canGoForward) {
                self.forwardButton.enabled = wkView.canGoForward
            } else if keyPath == #keyPath(WKWebView.estimatedProgress) {
                UIView.animate(withDuration: 0.1) {
                    let percent = (self.wkView.estimatedProgress * 100)%
                    self.progressView.pin.top(self.view.pin.safeArea).left().width(percent).height(5)
                }
            }
        }
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let percent = (wkView.estimatedProgress * 100)%
        progressView.pin.top(view.pin.safeArea).left().width(percent).height(5)
        wkView.pin.horizontally().bottom().top(view.pin.safeArea)
        containerView.pin.bottom(view.pin.safeArea).marginBottom(20).height(60)
        containerView.flex.paddingVertical(15).paddingHorizontal(20).layout(mode: .adjustWidth)
        
        if showingControls {
            containerView.pin.left(5%)
        } else {
            containerView.pin.left(-containerView.frame.width * 0.75)
        }
        containerView.layer.cornerRadius = containerView.frame.height / 2
    }
    
    override var prefersStatusBarHidden: Bool { false }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return (ColorScheme.normal.bg.isLight() ?? true) ? .darkContent : .lightContent
    }
}

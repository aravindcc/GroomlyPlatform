//
//  VerticalStepper.swift
//  Groomly
//
//  Created by Ravi on 10/08/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import FlexLayout
import PinLayout

class VerticalStepper: UIView, UITextFieldDelegate {
    
    class DummyField: UITextField{
        override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
            return false
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        animatingUnderline = true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        animatingUnderline = false
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        false
    }
    
    var onChange: ((Int) -> Void)? {
        didSet {
            let input = VerticalStepperInputView()
            input.onChange = onChange
            field.inputView = input
        }
    }
    var enabled: Bool {
        get { field.isUserInteractionEnabled }
        set {
            field.isUserInteractionEnabled = newValue
        }
    }
    
    private let animationTime: Double = 0.5
    private var animatingUnderline: Bool = false {
        didSet {
            blinkTimer?.invalidate()
            self.underlineFlex.view?.alpha = 0
            if animatingUnderline {
                blinkTimer = .scheduledTimer(
                    timeInterval: animationTime,
                    target: self,
                    selector: #selector(blink),
                    userInfo: nil,
                    repeats: true
                )
            }
        }
    }
    private var blinkTimer: Timer?
    private var blinkToggle: Bool = false
    @objc func blink() {
        blinkToggle = !blinkToggle
        UIView.animate(withDuration: animationTime) {
            self.underlineFlex.view?.alpha = self.blinkToggle ? 1 : 0
        }
    }
    var underlineFlex: Flex!
    let container = UIView()
    let field = DummyField()
    var labelFlex: Flex!
    var currentValue: Int = 0
    override init(frame: CGRect) {
        super.init(frame: frame)
        field.textAlignment = .center
        field.backgroundColor = .clear
        field.contentVerticalAlignment = .center
        field.delegate = self
        field.tintColor = .clear
        field.inputView = VerticalStepperInputView()
        field.defaultTextAttributes.updateValue(1.1, forKey: .kern)
        container.flex
            .justifyContent(.center)
            .define { flex in
            labelFlex = flex.addItem(field).marginTop(5)
            underlineFlex = flex.addItem().height(3)
        }
        underlineFlex.view?.schemedBG(.highlightColorC)
        underlineFlex.view?.alpha = 0
        addSubview(container)
        
        display(seconds: 0)
    }
    
    func display(seconds: Int) {
        currentValue = seconds
        let (h, m) = (seconds / 3600, (seconds % 3600) / 60)
        let string = NSMutableAttributedString(string: "")
        let altColor = colorScheme!.primaryTextColor.withAlphaComponent(0.5)
        string.append(NSAttributedString(string: "\(h)", attributes: [.foregroundColor: colorScheme!.primaryTextColor]))
        string.append(NSAttributedString(string: "h ", attributes: [.foregroundColor: altColor]))
        string.append(NSAttributedString(string: String(format: "%02d", m), attributes: [.foregroundColor: colorScheme!.primaryTextColor]))
        string.append(NSAttributedString(string: "m", attributes: [.foregroundColor: altColor]))
        field.attributedText = string
        let w = field.intrinsicContentSize.width * 1.15
        labelFlex.width(w)
        underlineFlex.width(w)
        layoutSubviews()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        container.pin.all()
        container.flex.layout()
    }
    
    required init?(coder: NSCoder) {
        super.init(frame: .zero)
    }
}

fileprivate class VerticalStepperInputView: UIView {

    class StepButton: FlatButton {
        override func commonInit() {
            super.commonInit()
            defaultPx = 0
        }
        
        override func setTitleTextColor(key: ColorKey) {
            titleLabel.schemedTextColor(.highlightColorA) { [weak self] in
                self?.refreshText()
            }
        }
        
        override func draw(_ rect: CGRect) {
            layer.borderWidth = 0
            layer.borderColor = nil
            normal(draw: rect)
        }
    }
    
    var onChange: ((Int) -> Void)?
    private let container = UIView()
    private let upButton = StepButton()
    private let downButton = StepButton()
    private var buttonContainer: Flex!
    private var labelFlex: Flex!
    override init(frame: CGRect) {
        super.init(frame: CGRect(x: 0, y: 0, width: 10, height: 200))
        upButton.icon = "plus.circle"
        downButton.icon = "minus.circle"
        upButton.fontSize = 35
        downButton.fontSize = 35
        
        upButton.tapped = { [unowned self] _ in self.tapped(true) }
        downButton.tapped = { [unowned self] _ in self.tapped(false) }
        
        let produceLabel = { (text: String) -> UILabel in
            let lbl = UILabel(left: 20, weight: .heavy, text: text)
            lbl.textColor = .black
            lbl.singleCentralLine()
            lbl.font = .italicSystemFont(ofSize: 25)
            return lbl
        }
        
        container.flex.direction(.row).alignItems(.center).justifyContent(.center).define { (flex) in
            buttonContainer = flex.addItem().alignItems(.center).justifyContent(.center).define { (flex) in
                flex.addItem().direction(.row).alignItems(.center).define { (flex) in
                    flex.addItem(upButton).size(upButton.innerSize).marginBottom(8)
                    flex.addItem(produceLabel("+ 15min")).marginLeft(20)
                }
                flex.addItem().direction(.row).alignItems(.center).define { (flex) in
                    flex.addItem(downButton).size(upButton.innerSize).marginBottom(8)
                    flex.addItem(produceLabel("- 15min")).marginLeft(20)
                }
            }
        }
        addSubview(container)
        backgroundColor = .clear
    }
    
    func tapped(_ isUp: Bool) {
        onChange?(isUp ? 1 : -1)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        container.pin.top().bottom(pin.safeArea).width(50%).center()
        if buttonContainer.view?.frame.width ?? 0 == 0 {
            buttonContainer.height(100%).layout(mode: .adjustWidth)
            if let max = buttonContainer.view?.subviews.map({ $0.frame.width }).max() {
                buttonContainer.width(max)
            }
        }
        container.flex.layout()
    }

    required init?(coder: NSCoder) {
        super.init(frame: .zero)
    }

}

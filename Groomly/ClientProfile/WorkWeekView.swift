//
//  WorkWeekView.swift
//  Groomly
//
//  Created by Ravi on 21/08/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import PinLayout
import FlexLayout


extension UILabel {
    var maxHeight: CGFloat {
        let maxSize = CGSize(width: frame.size.width, height: CGFloat(Float.infinity))
        let textSize = attributedText?.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, context: nil)
        return textSize?.height ?? frame.size.height
    }
    
    func setLineSpacing(lineSpacing: CGFloat = 0.0, lineHeightMultiple: CGFloat = 0.0, alignment: NSTextAlignment = .left) {
        guard let string = attributedText else { return }
        let mutable = NSMutableAttributedString(attributedString: string)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = lineSpacing
        paragraphStyle.lineHeightMultiple = lineHeightMultiple
        paragraphStyle.alignment = alignment
        mutable.addAttribute(.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, string.length))
        self.attributedText = mutable
    }
}

fileprivate class DayEntryView: UIView {
    var title: String? {
        didSet {
            titleLabel.text = title
            titleLabel.flex.size(titleLabel.intrinsicContentSize)
            lay()
        }
    }
    var detail: NSAttributedString? {
        didSet {
            self.detailLabel.attributedText = self.detail
            self.detailLabel.setLineSpacing(lineSpacing: 1.0, lineHeightMultiple: 1.2, alignment: .right)
            self.lay()
        }
    }
    var callback: Completion?
    
   
    private let container = UIView()
    private let sep = UIView()
    private let titleLabel = UILabel(left: 15, weight: .regular)
    private let detailLabel = UILabel()
    
    init() {
        super.init(frame: .zero)
        detailLabel.numberOfLines = 0
        detailLabel.schemedTextColor(.primaryTextColor)
        titleLabel.singleCentralLine()
        
        self.schemedBG()
        sep.schemedBG(.highlightColorA)
        self.layer.cornerRadius = 15
        self.layer.masksToBounds = true
        
        container.flex.paddingLeft(15).paddingRight(10).paddingVertical(15).direction(.row)
            .alignItems(.center)
            .define { (flex) in
                flex.addItem(titleLabel).grow(0).shrink(0).marginRight(10)
                flex.addItem(detailLabel).grow(1).shrink(1)
        }

        addSchemedView(container)
        addSchemedView(sep)
        
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTap)))
    }
    
    @objc private func handleTap() {
        callback?()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        sep.pin.horizontally(-5).height(5).bottom()
    }
    
    
    private func lay() {
        detailLabel.flex.height(detailLabel.maxHeight)
        titleLabel.flex.height(titleLabel.maxHeight)
    }
    
    // ignore
    required init?(coder: NSCoder) {
        super.init(frame: .zero)
    }
}

class WorkWeekView: UIView, DefaultAvailabilityHandler {
    
    private let weekdaySymoblsNet = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
    let scroll = UIScrollView()
    let container = UIView()
    private var entries = [DayEntryView]()
    private var cachedResponse: [Int: [AvailableBlockModel]] = [:] {
        didSet {
            
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(frame: .zero)
        commonInit()
    }
    
    private func commonInit() {
        container.flex.alignItems(.stretch)
        scroll.showsVerticalScrollIndicator = false
        addSchemedView(scroll)
        scroll.addSchemedView(container)
        loadData()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        scroll.pin.all()
        container.pin.all().marginTop(10)
        container.flex.layout(mode: .adjustHeight)
        scroll.contentSize.height = container.bounds.size.height + 30
    }
    
    
    private func loadEntries() {
        let extra = cachedResponse.count - entries.count
        if extra < 0 {
            for _ in 0..<abs(extra) {
                entries.popLast()?.removeFromSuperview()
            }
        } else if extra > 0 {
            for _ in 0..<extra {
                let view = DayEntryView()
                container.flex.addItem(view).marginTop(20)
                entries.append(view)
            }
        }
        
        for (key, val) in cachedResponse {
            let entry = entries[key]
            entry.title = weekdaySymoblsNet[key]
            entry.detail = WorkWeekView.getComplementPeriods(from: val).attributedTimeDisplayString
            entry.callback = { [weak self] in
                self?.showUpdateVC(for: key)
            }
        }
        
        layoutSubviews()
        setNeedsLayout()
        setNeedsDisplay()
    }
    
    weak var dailyVC: DailyVC?
    var defaultResponse = DefaultAvailability(day: 0, blocks: [])
    func onSync(response: DefaultAvailabilityResponse) {
        cachedResponse = WorkWeekView.transform(response)
        loadEntries()
        dailyVC?.dismiss(animated: true)
    }
    
    private func showUpdateVC(for key: Int) {
        guard let response = cachedResponse[key] else { return }
        defaultResponse = DefaultAvailability(day: key, blocks: response)
        let dailyTempVC = DailyVC(forDefault: self)
        dailyTempVC.dayState.move(to: Date())
        
        window?.backgroundColor = AppColors.orange
        parentViewController?.present(dailyTempVC, animated: true)
        dailyVC = dailyTempVC
    }
    
    static func getComplementPeriods(from dates: [AvailableBlockModel]) -> [AvailableBlockModel] {
        let current = Date().startOfDay
        var start = current
        var periods = [AvailableBlockModel]()
        for date in dates.sorted(by: { $0.start < $1.start }) {
            guard
                let starting = current.setTo(time: date.start),
                let ending = current.setTo(time: date.end)
            else { continue }
            if start != starting {
                periods.append(AvailableBlockModel(
                    start: start,
                    end: starting
                ))
            }
            start = ending
        }
        
        if start != Date().endOfDay {
            periods.append(AvailableBlockModel(
                start: start,
                end: Date().endOfDay
            ))
        }
        return periods
    }
    
    fileprivate static let transform = { (response: DefaultAvailabilityResponse) -> [Int: [AvailableBlockModel]] in
        var out = [Int: [AvailableBlockModel]]()
        for day in response.blocks {
            out[day.day] = day.blocks
        }
        return out
    }
    
    private func loadData() {
        let promi = APIClient.api.getWorkWeek().map(WorkWeekView.transform)
        Message.notify(
            process: promi,
            loadingMessage: "Getting schedule...",
            successMessage: nil,
            errorMessage: "Error fetching work schedule."
        ) { [weak self] (result) in
            guard let self = self else { return }
            if case .success(let data) = result {
                self.cachedResponse = data
                self.loadEntries()
            }
        }
    }
}

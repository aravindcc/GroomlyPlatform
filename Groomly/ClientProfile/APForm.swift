//
//  APForm.swift
//  Groomly
//
//  Created by Ravi on 09/08/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import PinLayout
import FlexLayout

fileprivate let paddingX: CGFloat = 10
fileprivate let shadowType: UIView.NeumorphicCase = .cusp

enum TextFormElementType {
    case short, long, option, custom
}

struct TextFormElementConfig<Option: RawRepresentable> where Option.RawValue == String {

    internal init(_ option: Option, placeholder: String = "", characterLimit: Int? = nil, type: TextFormElementType, customView: UIView? = nil, icon: String? = nil, nestView: UIView? = nil) {
        self.option = option
        self.placeholder = placeholder
        self.characterLimit = characterLimit
        self.type = type
        self.customView = customView
        self.icon = icon
        self.nestView = nestView
    }
    
    let option: Option
    let placeholder: String
    let characterLimit: Int?
    let type: TextFormElementType
    let customView: UIView?
    let icon: String?
    let nestView: UIView?
    
    fileprivate var rawElemClass: TextFormElement.Type {
        switch type {
        case .short:
            return APShortText.self
        case .long:
            return APLongText.self
        case .option:
            return APOptionText.self
        case .custom:
            return APCustomView.self
        }
    }
    
    fileprivate func renderClass() -> TextFormElement? {
        let font = UIFont.systemFont(ofSize: 13, weight: .medium)
        return rawElemClass.init(
            title: option.rawValue,
            placeholder: placeholder,
            font: font,
            characterLimit: characterLimit,
            customView: customView,
            icon: icon
        )
    }
}

class APForm<Option: RawRepresentable & Hashable>: UIView where Option.RawValue == String {
    let content = UIView()
    private var elements: [TextFormElement] = []
    
    init(_ configs: [TextFormElementConfig<Option>], builder: @escaping ButtonBuilder, headers: UIView?..., showUpdateInHeader: Bool = false, completion: (([Option: String?]) -> Void)? = nil) {
        super.init(frame: .zero)
        
        let internalBuilder = { () -> FlatButton in
            let button = builder()
            button.tapped = { [unowned self] _ in
                var out = [Option: String]()
                for elem in self.elements {
                    if let option = Option(rawValue: elem.title) {
                        out[option] = elem.value
                    }
                }
                completion?(out)
            }
            button.flex.width(button.wrappedWidth).height(35).marginTop(30)
                .marginBottom(20)
                .alignSelf(.center)
            return button
        }
        
        content.flex.alignItems(.stretch).define { (flex) in
            for (i, header) in headers.enumerated() {
                if let header = header {
                    if header.tag == 69 {
                        flex.addItem(header)
                    } else {
                        flex.addItem(header).size(header.frame.size).marginTop(i == 0 ? 15 : 10).marginBottom(5).alignSelf(.center)
                    }
                }
            }
            if showUpdateInHeader {
                flex.addItem(internalBuilder())
            }
            for (i, config) in configs.enumerated() {
                var atFlex: Flex?
                var addedSubv = false
                if let subv = config.nestView {
                    if !nestViews.contains(subv) {
                        flex.addItem(subv)
                        addedSubv = true
                    }
                    nestViews.append(subv)
                    atFlex = subv.flex
                }
                
                if let text = config.renderClass() {
                    if atFlex == nil {
                        atFlex = flex.addItem(text)
                    } else {
                        atFlex?.view?.flex.addItem(text).grow(1).marginLeft(addedSubv ? 0 : 15)
                    }
                    elements.append(text)
                }
                
                if i + 1 == configs.count && showUpdateInHeader {
                    atFlex?.marginBottom(30)
                }
            }

            if !showUpdateInHeader {
                flex.addItem(internalBuilder())
            }
        }
        content.addTargetPinClosure { [weak self] (_) in
            UIView.animate(withDuration: 0.2) {
                self?.layout()
            }
        }
        
        addSchemedView(content)
    }
    
    fileprivate var nestViews = [UIView]()
    fileprivate var elementSubvs: [TextFormElement] {
        return (
            content.subviews.compactMap({ $0 as? TextFormElement }) +
            nestViews.map({$0.subviews.compactMap({ $0 as? TextFormElement })}).flatMap({ $0 })
        )
    }
    func load(_ values: [Option: String?]) {
        for elem in elementSubvs {
            elem.value = values[Option(rawValue: elem.title)!] ?? nil
        }
    }
    func load(options: [String], for key: Option) {
        for s in elementSubvs {
            if let elem = s as? APOptionText, Option(rawValue: elem.title) == key {
                elem.items = options
            }
        }
    }
    
    func layout() {
        content.pin.all()
        content.flex.layout(mode: .adjustHeight)
        frame.size.height = content.frame.maxY
        (superview as? UIScrollView)?.contentSize.height = frame.size.height
    }
    
    func recalculate() {
        elements.forEach({
            if let custom = $0 as? APCustomView {
                custom.layoutSubviews()
            }
        })
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layout()
    }
}

fileprivate protocol TextFormElement: UIView {
    init(title: String, placeholder: String, font: UIFont, characterLimit: Int?, customView: UIView?, icon: String?)
    var value: String? {get set}
    var title: String {get}
    var hideTitle: Bool {get set}
    func layoutContainer()
}

class APTextFormElement: UIView, TextFormElement {
    private let labelView = UILabel()
    private let imageView = UIImageView()
    private let container = UIView()
    private let shouldCenter: Bool
    let textContainer = UIView()
    let title: String
    
    var value: String?
    var hideTitle: Bool = false {
        didSet {
            layout()
        }
    }
    
    required init(title: String, placeholder: String, font: UIFont, characterLimit: Int?, customView: UIView?, icon: String?) {
        self.title = title
        self.shouldCenter = customView?.intrinsicContentSize.width ?? 0 == 0
        super.init(frame: .zero)
        
        labelView.font = font.withSize(font.pointSize + 2)
        labelView.schemedTextColor(.primaryTextColor)
        labelView.text = title
        
        container.backgroundColor = .clear
        container.schemedBorderColor(.secondaryTextColor, alpha: 0.5)
        container.borderWidth = 2
        container.layer.masksToBounds = true
        container.layer.cornerRadius = 10
        
        if let icon = icon, let image = UIImage(systemName: icon) {
            imageView.image = image
            imageView.contentMode = .scaleAspectFit
            imageView.schemedTintColor(.secondaryTextColor, alpha: 0.5)
        }

        addSchemedView(labelView)
        addSchemedView(container)
        container.addSchemedView(imageView)
        container.addSchemedView(textContainer)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func layoutContainer() {}
    
    func layout() {
        if hideTitle {
            labelView.pin.top().left().size(0)
        } else {
            labelView.pin.top(15).horizontally().sizeToFit(.width)
        }

        container.pin.below(of: labelView).marginTop(10).horizontally()
        if imageView.image != nil {
            imageView.pin.left(10).width(20).height(20).vCenter().marginRight(10)
            textContainer.pin.right(of: imageView).marginLeft(10).right(paddingX)
        } else {
            imageView.pin.height(20).width(paddingX)
            textContainer.pin.right(of: imageView).right(paddingX)
        }
        layoutContainer()
        textContainer.pin.wrapContent(padding: .zero)
        container.pin.wrapContent(padding:
            UIEdgeInsets(top: 13, left: 10, bottom: 13, right: 10)
        )
        imageView.pin.vCenter()
        textContainer.pin.vCenter()
        let h = container.frame.maxY + 5
        if abs(h - frame.size.height) > 2 {
            flex.height(h)
            superview?.executePin()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layout()
    }
}

final class APCustomView: APTextFormElement {
    var customView: UIView?
    required init(title: String, placeholder: String, font: UIFont, characterLimit: Int?, customView: UIView?, icon: String?) {
        self.customView = customView
        super.init(title: title, placeholder: placeholder, font: font, characterLimit: characterLimit, customView: customView, icon: icon)
        if let customView = customView {
            textContainer.addSchemedView(customView)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutContainer() {
        if let customView = customView {
            if customView.intrinsicContentSize.width > 0 {
                customView.pin.left().top().width(customView.intrinsicContentSize.width).height(customView.intrinsicContentSize.height)
            } else {
                customView.pin.horizontally().top().height(customView.intrinsicContentSize.height)
            }
        }
    }
}

final class APShortText: APTextFormElement {
    private let textView = UITextField()
    override var value: String? {
        get {
            textView.text
        }
        set {
            textView.text = newValue
        }
    }
    
    required init(title: String, placeholder: String, font: UIFont, characterLimit: Int?, customView: UIView?, icon: String?) {
        super.init(title: title, placeholder: placeholder, font: font, characterLimit: characterLimit, customView: customView, icon: icon)
        textView.font = font
        textView.placeholder = placeholder
        textView.schemedTextColor(.primaryTextColor)
        textView.backgroundColor = .clear
        textContainer.addSchemedView(textView)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutContainer() {
        textView.pin.horizontally().top().sizeToFit(.width)
    }
}

final class APOptionText: APTextFormElement, UIPickerViewDelegate, UIPickerViewDataSource {
    private let textView = UITextField()
    private let pickerView = UIPickerView()
    private let font: UIFont
    private let rowHeight: CGFloat
    private let placeholder: String
    var _value: String?
    var items: [String] = [] {
        didSet {
            pickerView.reloadComponent(0)
            respondToVal()
        }
    }
    override var value: String? {
        get {
            textView.text
        }
        set {
            textView.text = newValue
        }
    }
    private func respondToVal() {
        if let _value = _value, let index = items.firstIndex(of: _value) {
            pickerView.selectRow(index + 1, inComponent: 0, animated: true)
        } else {
            pickerView.selectRow(0, inComponent: 0, animated: true)
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let label = (view as? UILabel) ?? UILabel()
        label.font = font
        label.schemedTextColor(.primaryTextColor)
        label.alpha = row == 0 ? 0.5 : 1
        label.pin.all()
        label.textAlignment = .center
        label.singleCentralLine()
        label.text = row == 0 ? placeholder : items[row - 1]
        return label
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        rowHeight
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        _value = row == 0 ? nil : items[row - 1]
        textView.text = _value
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return items.count + 1
    }
    
    required init(title: String, placeholder: String, font: UIFont, characterLimit: Int?, customView: UIView?, icon: String?) {
        self.font = font
        self.placeholder = placeholder
        
        let lbl = UILabel()
        lbl.attributedText = NSAttributedString(string: "0")
        lbl.font = font
        rowHeight = lbl.maxHeight + 10
        
        super.init(title: title, placeholder: placeholder, font: font, characterLimit: characterLimit, customView: customView, icon: icon)
        pickerView.delegate = self
        pickerView.backgroundColor = .clear
        
        textView.font = font
        textView.placeholder = placeholder
        textView.schemedTextColor(.primaryTextColor)
        textView.backgroundColor = .clear
        textView.inputView = pickerView
        textContainer.addSchemedView(textView)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutContainer() {
        textView.pin.horizontally().top().sizeToFit(.width)
    }
}

final class APLongText: APTextFormElement, UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        displayCharacterCount()
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let current = textView.text.count + (text.count - range.length)
        return current <= maxCharacterCount
    }
    
    private func displayCharacterCount() {
        placeholderView.isHidden = textView.text.count > 0
        let current = maxCharacterCount - textView.text.count
        characterLabel.text = "\(current) characters left"
        layout()
    }
    
    private let maxCharacterCount: Int
    private let textView = UITextView()
    private let characterLabel = UILabel(left: 10, weight: .regular)
    private let placeholderView = UILabel()
    
    override var value: String? {
        get {
            textView.text
        }
        set {
            textView.text = newValue
            displayCharacterCount()
        }
    }
    
    required init(title: String, placeholder: String, font: UIFont, characterLimit: Int?, customView: UIView?, icon: String?) {
        self.maxCharacterCount = characterLimit ?? 500
        super.init(title: title, placeholder: placeholder, font: font, characterLimit: characterLimit, customView: customView, icon: icon)
        
        placeholderView.schemedTextColor(.primaryTextColor, alpha: 0.15)
        placeholderView.text = placeholder
        
        placeholderView.font = font
        textView.font = font
        textView.schemedTextColor(.primaryTextColor)
        textView.delegate = self
        textView.sizeToFit()
        textView.isScrollEnabled = false
        textView.backgroundColor = .clear
        
        characterLabel.text = "\(maxCharacterCount) characters left"
        characterLabel.schemedTextColor(.secondaryTextColor)

        textContainer.addSchemedView(textView)
        textContainer.addSchemedView(placeholderView)
        addSchemedView(characterLabel)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutContainer() {
        textView.pin.horizontally().top().sizeToFit(.width)
        placeholderView.pin.horizontally()
            .top(textView.textContainerInset.top)
            .marginLeft(3)
            .sizeToFit(.width)
    }
}

//
//  PriceInput.swift
//  Groomly
//
//  Created by Ravi on 10/08/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import FlexLayout
import PinLayout

class PriceInput: UIView, UITextFieldDelegate {
    let container = UIView()
    let maxLength: CGFloat
    let dp: Int
    let maxText: String
    let maxValue: Float
    let field = UITextField()
    let preLabel = UILabel(central: 12, weight: .medium)
    var fieldFlex: Flex!
    
    var value: Float? {
        get {
            field.text == nil ? nil : Float(field.text!)
        } set {
            field.text = newValue == nil ? nil : String(format: "%.\(dp)f", newValue!)
        }
    }
    var enabled: Bool {
        get { field.isUserInteractionEnabled }
        set {
            field.isUserInteractionEnabled = newValue
        }
    }
    var onChange: (() -> Void)?
    
    override var intrinsicContentSize: CGSize {
        get {
            return CGSize(
                width: 5 + preLabel.intrinsicContentSize.width + maxLength * 1.2,
                height: 5 + max(field.intrinsicContentSize.height, preLabel.intrinsicContentSize.height)
            )
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var set = CharacterSet.decimalDigits
        if let text = textField.text, text.count > 0, !text.contains(".") {
            set = set.union(CharacterSet(charactersIn: "."))
        }
        guard string == "" || string.rangeOfCharacter(from: set) != nil else { return false }
        
        if let text = textField.text,
            let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
        
            if updatedText == "" { return true }
            
            if updatedText.count > maxText.count { return false }
            
            guard let val = Float(updatedText) else { return false }
            if val > maxValue {
                return false
            }
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        onChange?()
    }
    
    init(frame: CGRect, isPre: Bool = true, prefixText: String = "£", maxText: String = "£000.00", maxValue: Float = 200, dp: Int = 2) {
        self.dp = dp
        self.maxText = maxText
        self.maxLength = UILabel(left: 15, weight: .heavy).maxWidth(maxText)
        self.maxValue = maxValue
        super.init(frame: frame)
        field.textAlignment = .left
        field.placeholder = " - "
        field.backgroundColor = .clear
        field.delegate = self
        field.keyboardType = .decimalPad
        field.font = .systemFont(ofSize: 15, weight: .bold)
       // field.addTarget(self, action: #selector(textViewDidChange(_:)), for: .editingChanged)
        field.schemedTextColor(.primaryTextColor)
        field.contentVerticalAlignment = .center
        preLabel.schemedTextColor(.primaryTextColor, alpha: 0.5)
        preLabel.text = prefixText
        preLabel.singleCentralLine()
        
        container.flex.direction(.row).alignItems(.stretch).define { (flex) in
            if isPre {
                flex.addItem(preLabel).grow(0).shrink(0).marginRight(5).marginTop(4)
                fieldFlex = flex.addItem(field).grow(1).marginTop(3)
            } else {
                fieldFlex = flex.addItem(field).grow(1).marginTop(2)
                flex.addItem(preLabel).grow(0).shrink(0).marginTop(1)
            }
        }
        
        addSubview(container)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        container.pin.all()
        container.flex.layout()
    }
    
    required init?(coder: NSCoder) {
        maxLength = 0
        maxValue = 0
        dp = 0
        maxText = "0"
        super.init(frame: .zero)
    }
}

//
//  PriceEditor.swift
//  Groomly
//
//  Created by Ravi on 10/08/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import IGListKit
import PinLayout
import FlexLayout

extension Array {
    public func toDictionary<Key: Hashable>(with selectKey: (Element) -> Key) -> [Key:[Element]] {
        var dict = [Key:[Element]]()
        for element in self {
            if dict[selectKey(element)] == nil {
               dict[selectKey(element)] = []
            }
            dict[selectKey(element)]?.append(element)
        }
        return dict
    }
}

class EditableServicePrice: ListDiffable {
    public func diffIdentifier() -> NSObjectProtocol {
        return "\(self.service.name)-\(self.service.category)" as NSObjectProtocol
    }
    
    public func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        if self === object { return true }
        guard let object = object as? EditableServicePrice else { return false }
        return  service.name == object.service.name &&
                price == object.price &&
                duration == object.duration
    }
    
    let service: ServiceModel
    var price: Float?
    var duration: Int?
    
    init(service: ServiceModel, price: Float? = nil, duration: Int? = nil) {
        self.service = service
        self.price = price
        self.duration = duration
    }
}

class ListableCategory: ListDiffable, Hashable {
    static func == (lhs: ListableCategory, rhs: ListableCategory) -> Bool {
        lhs.category == rhs.category
    }
    func hash(into hasher: inout Hasher) {
        hasher.combine(category)
    }
    
    public func diffIdentifier() -> NSObjectProtocol {
        return self.category as NSObjectProtocol
    }
    
    public func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        if self === object { return true }
        guard let object = object as? ListableCategory else { return false }
        return services.equals(object.services)
    }
    
    let category: String
    var services: [EditableServicePrice]
    
    init(category: String, services: [EditableServicePrice]) {
        self.category = category
        self.services = services
    }
}

class PriceEditorCategoryHeader: UICollectionViewCell {
    private let container = UIView()
    
    private let titleLabel = MaskedLabel(central: 16, weight: .bold)
    private let thinLine = UIView()
    private let button = SimpleButton()
    
    var clear = false {
        didSet {
            container.backgroundColor = clear ? UIColor.clear : ColorScheme.normal.bg
            thinLine.backgroundColor = clear ? UIColor.clear : ColorScheme.normal.bg
            layoutSubviews()
        }
    }
    var title: String? {
        get { titleLabel.text }
        set {
            titleLabel.text = newValue
            layoutSubviews()
        }
    }
    var tapped: Completion? {
        didSet {
            if tapped != nil {
                container.flex.addItem(button)
            }
        }
    }
    
    class SimpleButton: FlatButton {
        override func commonInit() {
            super.commonInit()
            defaultPx = 10
            backgroundColor = .clear
        }
        
        override func draw(_ rect: CGRect) {
            layer.borderWidth = 0
            layer.borderColor = nil
            layer.cornerRadius = frame.height / 2
            normal(draw: rect)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        colorScheme = .alt
        container.schemedBG()
        thinLine.schemedBG()
        titleLabel.addBGradient()
        titleLabel.singleCentralLine()
        
        button.icon = "plus.circle"
        button.flex.width(button.innerSize.width).marginHorizontal(10).alignSelf(.stretch)
        button.tapped = { [unowned self] _ in self.tapped?()}
        
        container.flex.direction(.row).paddingHorizontal(20).alignItems(.center).define { (flex) in
            flex.addItem(titleLabel)
        }
        
        addSchemedView(thinLine)
        addSchemedView(container)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        thinLine.pin.horizontally().height(3).vCenter()
        titleLabel.flex.width(titleLabel.intrinsicContentSize.width)
        container.pin.height(100%)
        container.flex.layout(mode: .adjustWidth)
        container.pin.vCenter().left(clear ? 0% : 10%)
        container.layer.cornerRadius = container.frame.height / 2
    }
    
    required init?(coder: NSCoder) {
        super.init(frame: .zero)
    }
}

fileprivate class PriceEditorSectionController: ListSectionController, ListSupplementaryViewSource {
    
    var addService: ((ListableCategory?) -> Void)?
    var didSelect: ((ServiceModel) -> Void)?
    
    func supportedElementKinds() -> [String] {
        [UICollectionView.elementKindSectionHeader]
    }
    
    func viewForSupplementaryElement(ofKind elementKind: String, at index: Int) -> UICollectionReusableView {
        guard let view = collectionContext?.dequeueReusableSupplementaryView(
            ofKind: UICollectionView.elementKindSectionHeader,
            for: self,
            class: PriceEditorCategoryHeader.self,
            at: index
        ) as? PriceEditorCategoryHeader
        else { return UICollectionViewCell() }
        view.title = (object as? ListableCategory)?.category
        view.tapped = { [unowned self] in
            self.addService?(object as? ListableCategory)
        }
        return view
    }
    
    func sizeForSupplementaryView(ofKind elementKind: String, at index: Int) -> CGSize {
        return CGSize(width: collectionContext!.containerSize.width, height: 35)
    }
    

    private let rowHeight: CGFloat = 70
    private var object: Any? = nil
    
    override init() {
        super.init()
        supplementaryViewSource = self
    }
    
    
    override func numberOfItems() -> Int {
        if let count = (object as? ListableCategory)?.services.count, count > 0 {
            return count
        }
        return 1
    }
    
    override func sizeForItem(at index: Int) -> CGSize {
        guard let width = collectionContext?.containerSize.width else { return .zero }
        return CGSize(width: width, height: rowHeight)
    }

    override func cellForItem(at index: Int) -> UICollectionViewCell {
        if
            let topBoy = object as? ListableCategory,
            topBoy.services.count > 0,
            let cell = schemedReusableCell(of: PriceEditorCell.self, for: self, at: index) as? PriceEditorCell {
            cell.display(model: topBoy.services[index])
            return cell
        }
        if let cell = schemedReusableCell(of: PlaceholderEditorCell.self, for: self, at: index) as? PlaceholderEditorCell {
            cell.text = "add a service to this category"
            return cell
        }
        return UICollectionViewCell()
    }
    
    override func didSelectItem(at index: Int) {
        if let object = object as? ListableCategory, index < object.services.count {
            didSelect?(object.services[index].service)
        }
    }

    override func didUpdate(to object: Any) {
        if let object = object as? ListableCategory {
            self.object = object
        }
    }
    
    override func canMoveItem(at index: Int) -> Bool {
        return (object as? ListableCategory)?.services.count ?? 0 > 0
    }
    
    override func moveObject(from sourceIndex: Int, to destinationIndex: Int) {
        if let list = object as? ListableCategory {
            list.services.swapAt(sourceIndex, destinationIndex)
        }
    }
}

class PriceEditor: SubProcessVC, ListAdapterDataSource, ListAdapterMoveDelegate, ListScrollDelegate {
    
    func listAdapter(_ listAdapter: ListAdapter, didScroll sectionController: ListSectionController) {
        categories.select(
            listAdapter.visibleSectionControllers().map { $0.section }.min()
        )
    }
    func listAdapter(_ listAdapter: ListAdapter, willBeginDragging sectionController: ListSectionController) {}
    func listAdapter(_ listAdapter: ListAdapter, didEndDragging sectionController: ListSectionController, willDecelerate decelerate: Bool) {}
    
    func listAdapter(_ listAdapter: ListAdapter, move object: Any, from previousObjects: [Any], to objects: [Any]) {
        guard let objects = objects as? [ListableCategory] else { return }
        items = objects
    }
    
    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        if items.count == 0 { helpLabel.isHidden = true }
        return items
    }
    
    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        let controller = PriceEditorSectionController()
        controller.addService = { [unowned self] in self.addService(to: $0)}
        controller.didSelect = { [unowned self] in self.edit(service: $0)}
        helpLabel.isHidden = false
        return controller
    }
    
    func emptyView(for listAdapter: ListAdapter) -> UIView? {
        let v = UILabel()
        v.text = "Get started by adding a category."
        v.schemedTextColor(.primaryTextColor)
        v.textAlignment = .center
        v.frame.size.height = v.maxHeight
        return v
    }
    
    private let categories = TabScrollView()
    private var items: [ListableCategory] = []
    lazy var cv : UICollectionView = {
        let layout = ListCollectionViewLayout(stickyHeaders: false, topContentInset: 0, stretchToEdge: true)
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.accessibilityIdentifier = "priceEditorVC"
        return cv
    }()
    lazy var adapter: ListAdapter = {
        return ListAdapter(updater: ListAdapterUpdater(), viewController: self)
    }()
    
    var processUpdateReqeust: ((ClientProfileUpdate, ClientServiceList) -> Void)?
    var hideSegmented: Completion?
    
    private let contentWrap = UIView()
    private let container = UIView()
    private let button: FlatButton
    private var helpLabel: UILabel = UILabel(central: 13, weight: .medium)
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongGesture(gesture:)))
        cv.addGestureRecognizer(longPressGesture)
        adapter.collectionView = cv
        adapter.dataSource = self
        adapter.moveDelegate = self
        button.tapped = { [unowned self] _ in self.extractMatrix() }
    
        let addbutton = FlatButton()
        addbutton.title = "category"
        addbutton.icon = "plus"
        addbutton.tapped = { [unowned self] _ in self.addCategory() }
        
        helpLabel.singleCentralLine()
        helpLabel.numberOfLines = 0
        helpLabel.schemedTextColor(.primaryTextColor)
        helpLabel.text = "tap a service to edit"
        
        categories.didSelect = { [unowned self] index in
            categories.select(index)
            if self.cv.contentSize.height > self.cv.frame.height {
                self.adapter.scroll(
                    to: self.items[index],
                    supplementaryKinds: [UICollectionView.elementKindSectionHeader],
                    scrollDirection: .vertical,
                    scrollPosition: .top,
                    animated: true
                )
            }
        }
        
        container.flex.alignItems(. stretch).define { (flex) in
            flex.addItem(cv).grow(1).width(100%).marginBottom(5)
            flex.addItem(helpLabel).marginBottom(20)
            flex.addItem()
                .height(35)
                .marginTop(5)
                .alignItems(.stretch)
                .direction(.row)
                .justifyContent(.spaceAround)
                .define({ (flex) in
                flex.addItem(button).width(button.wrappedWidth)
                flex.addItem(addbutton).width(addbutton.wrappedWidth)
            })
        }
        
        contentWrap.isUserInteractionEnabled = true
        contentWrap.schemedBG(.lightShadow)
        contentWrap.layer.cornerRadius = 20
        contentWrap.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        contentWrap.addSchemedView(container)
    
        categories.showsHorizontalScrollIndicator = false
        view.addSchemedView(categories)
        view.addSchemedView(contentWrap)
        cv.backgroundColor = .clear
    }
    
    init(button: ButtonBuilder) {
        self.button = button()
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        button = FlexButton()
        button.title = "save"
        super.init(nibName: nil, bundle: nil)
    }
    
    @objc func handleLongGesture(gesture: UILongPressGestureRecognizer) {
        switch gesture.state {
        case .began:
            let touchLocation = gesture.location(in: cv)
            guard let selectedIndexPath = cv.indexPathForItem(at: touchLocation) else {
                break
            }
            cv.beginInteractiveMovementForItem(at: selectedIndexPath)
        case .changed:
            if let view = gesture.view {
                let position = gesture.location(in: view)
                cv.updateInteractiveMovementTargetPosition(position)
            }
        case .ended:
            cv.endInteractiveMovement()
        default:
            cv.cancelInteractiveMovement()
        }
    }

    func on(profile: ClientProfileModel) {
        for entry in profile.matrix {
            for item in self.items {
                if let index = item.services.firstIndex(where: { $0.service.key == entry.id }) {
                    let current = item.services[index]
                    item.services[index] = EditableServicePrice(
                        service: current.service,
                        price: entry.price,
                        duration: entry.approxTime
                    )
                }
            }
        }
        self.adapter.performUpdates(animated: true)
    }
    
    static func transform(inputServices: [ServiceModel]) -> [ListableCategory] {
        let services = inputServices.map({ (model) in
            EditableServicePrice(service: model)
        })
        let mapped = services.toDictionary(with: { $0.service.category })
        return mapped.keys.map({ListableCategory(
            category: $0,
            services: mapped[$0]!.sorted(by: { $0.service.order < $1.service.order })
        )})
    }
    
    func load(_ inputServices: [ServiceModel]) {
        items = PriceEditor.transform(inputServices: inputServices)
        categories.set(items: items.map { $0.category })
        view.setNeedsDisplay()
        view.setNeedsLayout()
    }
    
    func addCategory() {
        hideSegmented?()
        Feedback.askForInput(
            title: "Add a Category",
            desc: "This will add a section to your service list.",
            placeholder: "choose a name..."
        ) { [weak self] (text) in
            if let self = self, let text = text, text.trimmingCharacters(in: .whitespaces) != "" {
                self.items.append(ListableCategory(category: text, services: []))
                self.categories.set(items: self.items.map { $0.category })
                self.categories.didSelect?(self.items.count - 1)
                self.adapter.reloadData()
                self.view.setNeedsLayout()
            }
        }
    }
    
    func edit(service: ServiceModel) {
        hideSegmented?()
        
        if service.key == "" {
            Feedback.showNotification(
                message: NotificationDetails(
                    title: "Please save list.",
                    desc: "Before editing the service and adding your images, please save your service."
                )
            )
            return
        }
        show(
            process: ServiceEditor(
                key: service.key,
                category: service.category,
                update: { [unowned self] in
                    self.extractMatrix(replacing: $0)
                },
                delete: { [unowned self] in
                    self.extractMatrix(deleting: $0)
                }
            )
        )
    }
    
    func addService(to category: ListableCategory?) {
        guard let category = category else { return }
        hideSegmented?()
        Feedback.askForInput(
            title: "Add a Service",
            desc: "This will add a service to the \(category.category) category.",
            placeholder: "choose a name..."
        ) { [weak self] (text) in
            if let self = self, let text = text, text.trimmingCharacters(in: .whitespaces) != "", let index = self.items.firstIndex(where: { $0.isEqual(toDiffableObject: category) }) {
                self.items[index].services.append(
                    EditableServicePrice(
                        service: ServiceModel(
                            name: text,
                            category: category.category
                        )
                    )
                )
                self.adapter.reloadData()
            }
        }
    }
    
    func extractMatrix(replacing: ServiceModel? = nil, deleting: String? = nil) {
        let messedUp = {
            Feedback.showNotification(message:
                NotificationDetails(title: "Please make sure you've filled out the duration and price.")
            )
        }
        do {
            let services = items.map({ $0.services }).flatMap { $0 }
            let list = ClientServiceList(
                services: services.compactMap {
                    if deleting == $0.service.key {
                        return nil
                    } else if replacing?.key == $0.service.key {
                        return replacing!
                    }
                    return $0.service
                }
            )
            let matrix: [ServicePriceModel] = try services.compactMap {
                guard let seconds = $0.duration, let duration = seconds.approxTime, let value = $0.price else {
                    if ($0.duration ?? 0) > 0 {
                        messedUp()
                        throw ActionError.invalid
                    }
                    return nil
                }
                if value <= 0 || seconds <= 0 {
                    messedUp()
                    throw ActionError.invalid
                }
                let key = $0.service.key == "" ? $0.diffIdentifier() as! String : $0.service.key
                return ServicePriceModel(id: key, price: value, duration: duration)
            }
            let update = ClientProfileUpdate(matrix: matrix)
            processUpdateReqeust?(update, list)
        }
        catch { () }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        categories.pin.horizontally(20).height(40).top()
        categories.flex.layout()
        categories.contentSize.width = categories.subviews.last!.frame.maxX
        contentWrap.pin.horizontally().top(38).bottom(view.pin.safeArea)
        container.pin.horizontally(20).top(20).bottom(20)
        container.flex.layout()
    }
}

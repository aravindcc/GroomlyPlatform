//
//  PortfolioView.swift
//  Groomly
//
//  Created by Ravi on 11/08/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import PinLayout
import FlexLayout
import Kingfisher

class PortfolioView: UIView {
    
    class SimpleButton: FlatButton {
        override func commonInit() {
            super.commonInit()
            defaultPx = 20
            schemedBG(.lightShadow, alpha: 0.8)
        }
        
        override func draw(_ rect: CGRect) {
            layer.borderWidth = 0
            layer.borderColor = nil
            layer.cornerRadius = frame.height / 2
            normal(draw: rect)
        }
        
        override func handleClick() {
            makeAction()
        }
    }
    
    fileprivate enum InternalActions: Int {
        case select
        case add
        case cancel
        case delete
        
        var config: (String, String) {
            switch  self {
            case .add: return ("plus", "add")
            case .cancel: return ("xmark", "cancel")
            case .delete: return ("trash", "delete")
            case .select: return ("pencil.and.outline", "select")
            }
        }
        
        var pair: InternalActions {
            switch  self {
            case .add: return .delete
            case .cancel: return .select
            case .delete: return .add
            case .select: return .cancel
            }
        }
        
        var color: ColorKey {
            switch  self {
            case .add: return .highlightColorA
            case .cancel: return .highlightColorB
            case .delete: return .highlightColorA
            case .select: return .highlightColorA
            }
        }
    }
    
    
    var popoverEditor = true
    var showingEditing = false {
        didSet {
            for i in buttonContainer.subviews {
                guard let button = i as? FlatButton else { continue }
                var action = InternalActions(rawValue: button.tag)!
                if showingEditing {
                    action = action.pair
                }
                let (_, title) = action.config
                button.title = title
                //button.icon = icon
                button.setTitleTextColor(key: action.color)
                button.flex.width(button.wrappedWidth)
                
                if action == .select {
                    button.isHidden = gallery.items.count == 0
                    if gallery.items.count == 0 {
                        button.flex.width(0)
                        buttonContainer.flex.justifyContent(.center)
                    } else {
                        buttonContainer.flex.justifyContent(.spaceAround)
                    }
                }
            }
            buttonContainer.flex.layout()
        }
    }
    
    override var intrinsicContentSize: CGSize {
        get {
            let columnCount = ceil(CGFloat(gallery.items.count) / 3)
            let height = gallery.cv.frame.width / 3
            return CGSize(
                width: bounds.width,
                height: height * columnCount + buttonContainer.height + 20
            )
        }
    }
    
    let gallery = APImageGrid<PortfolioImage>()
    let buttonContainer = UIView()
    var processAction: ((PortfolioActions, [PortfolioImage]) -> Void)?
    var showGallery: Completion?
    private weak var poped: PortfolioPopup?
    
    private func processInternal(action: InternalActions) {
        let action = showingEditing ? action.pair : action
        switch action {
        case .add: self.showGallery?()
        case .cancel:
            showingEditing = !showingEditing
            gallery.isEditing = false
        case .delete:
            let model: [PortfolioImage] = gallery.selectedMap.compactMap({
                $0.value ? (
                    gallery.items[$0.key.row]
                ) : nil
            })
            processAction?(.delete, model)
        case .select:
            showingEditing = !showingEditing
            gallery.isEditing = true
        }
    }
    
    init() {
        super.init(frame: .zero)
        gallery.transitionClosure = { [unowned self] (model, ind, transition) in
            if self.popoverEditor {
                let poped = PortfolioPopup(model, transition: transition)
                poped.processAction = self.processAction
                self.poped = poped
                return poped
            } else {
                self.parentViewController?.push(vc: PortfolioPopup(model, transition: transition))
                return nil
            }
        }
        
        buttonContainer.flex.direction(.row).alignItems(.stretch).justifyContent(.spaceAround).define { (flex) in
            for action in [InternalActions.select, InternalActions.add] {
                let button = SimpleButton()
                button.tag = action.rawValue
                button.tapped = { [unowned self] _ in self.processInternal(action: action) }
                flex.addItem(button)
            }
            showingEditing = false
        }
        addSchemedView(gallery.view)
        addSchemedView(buttonContainer)
    }
    
    required init?(coder: NSCoder) {
        super.init(frame: .zero)
    }
    
    func load(images: [PortfolioImage]) {
        gallery.set(items: images)
        showingEditing = false
        gallery.isEditing = false
        if let image = images.first(where: { $0.id == poped?.model?.id }) {
            poped?.load(image)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        buttonContainer.pin.horizontally().bottom().marginBottom(5).height(35)
        buttonContainer.flex.layout()
        gallery.view.pin.horizontally().top(20).above(of: buttonContainer).marginBottom(15)
    }
}

enum PortfolioActions: String, CaseIterable {
    case delete = "trash"
    case update = "square.and.pencil"
}

fileprivate class PortfolioPopup: FullScreenVC, TransitionController {
    private var profileImageV: UIImageView!
    private let containerView = UIView()
    private let contentView = UIView()
    private let longText: APLongText
    private var hasCrisped: Bool = false
    
    private(set) var model: PortfolioImage?
    var processAction: ((PortfolioActions, [PortfolioImage]) -> Void)?
    var heroID: String? {
        get {
            return profileImageV.hero.id
        }
        set (val) {
            profileImageV.hero.id = val
            if let val = val {
                containerView.hero.modifiers = [.source(heroID: val), .spring(stiffness: 250, damping: 25)]
            }
        }
    }
    
    private func process(action: PortfolioActions) {
        if let model = model {
            if action == .delete {
                handleClick()
            }
            if let processAction = processAction {
                let textVal = longText.value ?? model.caption
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
                    processAction(action, [PortfolioImage(
                        imageString: model.imageString,
                        caption: textVal,
                        id: model.id,
                        width: model.width,
                        height: model.height
                    )])
                }
            }
        }
    }
    
    private func createContent() {
        contentView.flex.define { (flex) in
            flex.addItem(longText).marginHorizontal(5%)
            longText.hideTitle = true
            flex.addItem()
                .direction(.row)
                .width(100%)
                .height(35)
                .marginVertical(30)
                .alignItems(.stretch)
                .justifyContent(.center).define { (flex) in
                    let all: [PortfolioActions] = [.delete, .update]
                    for (i, option) in all.enumerated() {
                        let button = FlexButton()
                        button.icon = option.rawValue
                        button.tapped = { [unowned self] _ in
                            self.process(action: option)
                        }
                        button.setTitleTextColor(key: i == all.count - 1 ? .highlightColorA : .highlightColorB)
                        flex.addItem(button).width(button.wrappedWidth).marginHorizontal(40)
                    }
            }
        }
        contentView.addTargetPinClosure { [unowned self] (_) in
            self.viewDidLayoutSubviews()
        }
    }
    
    func load(_ model: PortfolioImage) {
        self.model = model
        longText.value = model.caption
    }
    
    init(_ model: PortfolioImage, transition: UIImage?) {
        longText = APLongText(
            title: "caption",
            placeholder: "add a caption to display...",
            font: .systemFont(ofSize: 15, weight: .medium),
            characterLimit: 500,
            customView: nil,
            icon: nil
        )
        
        // use lower resolution transistion image.
        profileImageV = APCollectionCell.configureBG()
        profileImageV.image = transition
        profileImageV.isUserInteractionEnabled = true
        profileImageV.layer.cornerRadius = 10
        profileImageV.accessibilityIdentifier = "popoverImage"
        
        super.init(nibName: nil, bundle: nil)
        
        modalPresentationStyle = .overCurrentContext
        view.schemedBG(.bg)
        
        // apply content view with model
        containerView.backgroundColor = .clear
        contentView.backgroundColor = .clear
        createContent()
        load(model)
        
        containerView.addSchemedView(profileImageV)
        containerView.addSchemedView(contentView)
        containerView.clipsToBounds = true
        
        containerView.hero.modifiers = [.spring(stiffness: 250, damping: 25)]
        profileImageV.hero.modifiers = [.forceNonFade, .spring(stiffness: 250, damping: 25)]
        contentView.hero.modifiers = [.forceNonFade, .forceAnimate, .spring(stiffness: 250, damping: 25)]
        view.hero.modifiers = [.fade]
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleClick))
        view.addGestureRecognizer(tap)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSchemedView(containerView)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc private func handleClick() {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        containerView.pin.all()
        if profileImageV.image != nil {
            profileImageV.pin.horizontally(10).aspectRatio().vCenter(-15%)
        } else {
            profileImageV.pin.horizontally().top().aspectRatio(1)
        }
        contentView.pin.horizontally().below(of: profileImageV).marginTop(30)
        contentView.flex.layout(mode: .adjustHeight)
        
        if !hasCrisped, profileImageV.frame.size != .zero {
            hasCrisped = true
            // attempt to high res image
            if let image = model?.image {
                let processor = DownsamplingImageProcessorScaled(size: profileImageV.frame.size)
                KingfisherManager.shared.retrieveImage(with: image, options: [.processor(processor)]) { result in
                    switch result {
                    case .success(let value):
                        DispatchQueue.main.async {
                            self.profileImageV.image = value.image
                            self.view.layoutSubviews()
                        }
                    case .failure(let error):
                        Log(error) // The error happens
                    }
                }
            }
        }
    }
    
    deinit {
        Log("POP DEINIT")
    }
}

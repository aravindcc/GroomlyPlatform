//
//  ServiceEditor.swift
//  Groomly
//
//  Created by Ravi on 22/10/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import PinLayout
import FlexLayout
import Async
import YPImagePicker

fileprivate enum InfoFields: String {
    case name = "name"
    case description = "description"
    case gallery = "gallery"
}

class ServiceEditor: UIViewController {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        IQKeyboardManager.shared.previousNextDisplayMode = .alwaysShow
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        IQKeyboardManager.shared.previousNextDisplayMode = .alwaysHide
    }
    
    private let containerView = IQPreviousNextView()
    private var form: APForm<InfoFields>!
    private var portfolio: PortfolioView!
    private let key: String
    private let category: String
    private let update: ((ServiceModel) -> Void)?
    private let delete: ((String) -> Void)?
    
    init(key: String, category: String, update: ((ServiceModel) -> Void)?, delete: ((String) -> Void)?) {
        self.key = key
        self.category = category
        self.update = update
        self.delete = delete
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        preferredContentSize.height = .infinity
        view.colorScheme = .normal
        view.schemedBG()
        
        portfolio = PortfolioView()
        portfolio.popoverEditor = false
        portfolio.showGallery = { [unowned self] in self.showGallery() }
        portfolio.processAction = { [unowned self] in self.process(action: $0, models: $1) }
        add(portfolio.gallery, add: false)
        
        form = APForm([
            TextFormElementConfig(InfoFields.name, placeholder: "name of the service...", characterLimit: nil, type: .short),
            TextFormElementConfig(InfoFields.description, placeholder: "short description (optional)...", characterLimit: 500, type: .long),
            TextFormElementConfig(InfoFields.gallery, type: .custom, customView: portfolio)
        ], builder: ProfileEditorVC.buildButton) { [unowned self] in
            self.processUpdate(request: $0)
        }
        
        let label = UILabel(central: 20, weight: .bold)
        label.schemedTextColor(.highlightColorA)
        label.text = "Edit Service"
        
        let button = FlexButton()
        button.icon = "trash"
        button.tapped = { [unowned self] _ in
            Message.showConfirmation(title: "Delete Service?") { [unowned self] in
                if $0 {
                    self.delete?(self.key)
                    self.dismiss(animated: true)
                }
            }
            
        }
        
        containerView.flex.define { (flex) in
            flex.addItem(label).alignSelf(.center)
            flex.addItem(button).width(button.wrappedWidth).height(35).alignSelf(.center).marginTop(15)
            flex.addItem(form).grow(1).alignSelf(.stretch)
        }
        view.addSchemedView(containerView)
        
        refreshProfile()
    }
    
    private func load(data: ServiceDataResponse) {
        DispatchQueue.main.async {
            self.portfolio.load(images: data.portfolio)
            self.form.load([
                .name: data.name,
                .description: data.description,
            ])
            self.form.recalculate()
            self.form.layout()
        }
    }
    
    private func processUpdate(request: [InfoFields: String?]) {
        guard let name = request[.name] as? String else {
            Feedback.showNotification(message: Talk.it.paramError)
            return
        }
        let model = ServiceModel(name: name, description: request[.description] ?? nil, key: key, category: category)
        update?(model)
    }
    
    private func refreshProfile() {
        APIClient.api.detail(service: key).do { [weak self] in
            self?.load(data: $0)
        }.catch { error in
            Feedback.showNotification(
                message: Talk.it.serverError
            )
        }
    }
    
    private func process(action: PortfolioActions, models: [PortfolioImage]) {
        switch action {
        case .delete:
            let performDelete = { [weak self] in
                let delete = models.map {
                    APIClient.api.deleteServicePorfolio(image: GetModelRequest(id: $0.id))
                }
                let promi = Future.andAll(delete, eventLoop: APIClient.api.worker.eventLoop)
                Message.notify(
                    process: promi,
                    loadingMessage: "Deleting portfolio image...",
                    successMessage: "Deleted.",
                    errorMessage: "Error occured while deleting.",
                    silentSuccess:  false
                ) { [weak self] (_) in
                    self?.refreshProfile()
                }
            }
            let s = models.count > 1 ? "s" : ""
            Message.showConfirmation(title: "Delete Image\(s)?", description: "Please confirm you want to delete the images.") {
                if $0 { performDelete() }
            }
        case .update:
            guard let model = models.first else { return }
            let update = ServicePortfolioPostRequest(key: key, caption: model.caption, image: nil, type: nil, id: model.id)
            Message.notify(
                process: APIClient.api.servicePortfolio(post: update),
                loadingMessage: "Updating caption...",
                successMessage: "Updated.",
                errorMessage: "Error occured while updating.",
                silentSuccess:  false
            ) { [weak self] (result) in
                self?.refreshProfile()
            }
        }
    }
    
    private func showGallery(maxNumberOfItems: Int = 10, forProfile: Bool = false) {
        ProcessNVC.jellyLock = true
        let key = self.key
        var config = YPImagePickerConfiguration()
        config.showsVideoTrimmer = false
        config.library.maxNumberOfItems = forProfile ? 1 : maxNumberOfItems
        config.showsPhotoFilters = false
        config.screens = [.library]
        config.library.mediaType = .photo
        config.wordings.warningMaxItemsLimit = "The limit is %d photos"
        config.wordings.done = "Upload"
        config.gallery.hidesRemoveButton = false
        let picker = YPImagePicker(configuration: config)
        picker.didFinishPicking { [unowned picker] items, cancelled in
            defer {
                picker.dismiss(animated: true, completion: nil)
                ProcessNVC.jellyLock = false
            }
            guard !cancelled else { return }
            let photos: [ServicePortfolioPostRequest] = items.compactMap {
                if case .photo(let photo) = $0 {
                    let data = photo.image.jpegData(compressionQuality: 1)
                    return ServicePortfolioPostRequest(key: key, image: data, type: "jpeg")
                }
                return nil
            }
            let loader = APProgressFloat(bars: 10)
            Feedback.showCentral(view: loader)
            let promises = photos.enumerated().map { (i, elem) in
                APIClient.api.servicePortfolio(post: elem, progress: {
                    [unowned loader] in
                    loader.set(bar: i, to: $0.fractionCompleted)
                    }
                )
            }
            DispatchQueue.global(qos: .background).asyncAfter(deadline: .now() + 0.5) { [weak self] in
                guard let self = self else { return }
                Message.notify(
                    process: Future.andAll(promises, eventLoop: APIClient.api.worker.eventLoop),
                    loadingMessage: nil,
                    successMessage: "Uploaded.",
                    errorMessage: "Error uploading gallery.",
                    silentSuccess:  false
                ) { [weak self] (_) in
                    self?.refreshProfile()
                }
            }
        }
        present(picker, animated: true, completion: nil)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        containerView.pin.all(safeAreaInsets).marginHorizontal(20)
        containerView.flex.layout()
    }
}

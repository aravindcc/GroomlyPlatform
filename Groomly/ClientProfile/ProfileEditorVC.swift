//
//  ProfileEditorVC.swift
//  Groomly
//
//  Created by Ravi on 09/08/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import PinLayout
import FlexLayout
import Async
import YPImagePicker
import Instructions
import Kingfisher

enum ProfileOptions: String, CaseIterable {
    case detail = "info"
    case services = "services"
    case portfolio = "gallery"
    case schedule = "times"
}

fileprivate enum InfoFields: String {
    case firstName = "First Name"
    case lastName = "Last Name"
    case group = "Discipline"
    case radius = "Service Radius"
    case company = "Company Name"
    case description = "About"
}

fileprivate class SimpleVC: UIViewController {
    
    private let contentWrap = UIScrollView()
    let name: String
    let container: UIView
    let padding: CGFloat
    
    init(name: String, container: UIView, padding: CGFloat = 20, hasBg: Bool = true, bgColorCode: ColorKey = .bg) {
        self.name = name
        self.container = container
        self.padding = padding
        if hasBg { self.contentWrap.schemedBG(bgColorCode) }
        super.init(nibName: nil, bundle: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.colorScheme = .normal
        
        contentWrap.layer.cornerRadius = 20
        contentWrap.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        contentWrap.maskToBounds = true
        contentWrap.showsVerticalScrollIndicator = false
        contentWrap.showsHorizontalScrollIndicator = false
        container.isUserInteractionEnabled = true
        view.addSchemedView(contentWrap)
        contentWrap.addSchemedView(container)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        contentWrap.pin.top(safeAreaInsets).horizontally().bottom(view.pin.safeArea)
        container.pin.top().bottom(padding).horizontally(padding)
        container.flex.layout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

typealias ButtonBuilder = () -> FlatButton
class ProfileEditorVC: UIViewController {
    
    private let homePage = ClientHomePage()
    fileprivate let titleLabel = MaskedLabel(left: 22, weight: .semibold, color: .black)
    fileprivate let workWeek = WorkWeekView()
    fileprivate var form: APForm<InfoFields>!
    fileprivate var mileageInput: PriceInput!
    fileprivate var priceMatrix: PriceEditor!
    fileprivate var portfolio: PortfolioView!
    fileprivate let photo = UIImageView()
    private let imgF: CGFloat = 0.65
    fileprivate var tempProfileImage: UIImage?
    
    static var buildButton: ButtonBuilder = {
        let button = FlatButton()
        button.title = "update"
        button.icon = "arrow.up.doc.fill"
        return button
    }
    
    private func getVCs() -> [ProfileOptions: SimpleVC] {
        var out = [ProfileOptions: SimpleVC]()
        for option in ProfileOptions.allCases {
            switch option {
            case .detail: out[.detail] = SimpleVC(name: "Profile", container: form, bgColorCode: .lightShadow)
            case .services: out[.services] = SimpleVC(name: "Services", container: priceMatrix.view, padding: 0, hasBg: false)
            case .portfolio: out[.portfolio] = SimpleVC(name: "Portfolio", container: portfolio)
            case .schedule: out[.schedule] = SimpleVC(name: "Schedule", container: workWeek, bgColorCode: .lightShadow)
            }
        }
        return out
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.colorScheme = .normal
        
        
        
        portfolio = PortfolioView()
        portfolio.processAction = { [unowned self] in self.process(action: $0, models: $1) }
        
        let w = UIScreen.main.bounds.width * imgF
        self.photo.frame = CGRect(origin: .zero, size: CGSize(width: w, height: w))
        self.photo.layer.cornerRadius = w / 2
        
        photo.schemedBG(.lightShadow)
        photo.schemedBG(alpha: 0.8)
        photo.maskToBounds = true
        photo.contentMode = .scaleAspectFill
        photo.clipsToBounds = true
        
        let button = FLTButton()
        button.shouldDescend = -1
        button.icon = "camera.fill.badge.ellipsis"
        button.fontSize = 30
        button.defaultPx = 10
        button.tag = 69
        button.schemedBG(.highlightColorA)
        button.setTitleTextColor(key: .lightShadow)
        button.flex.width(button.wrappedWidth)
            .height(button.wrappedWidth)
            .marginTop(-button.wrappedWidth*1.5)
            .alignSelf(.end)
            .marginRight(6%)
            .marginBottom(30)
        
        mileageInput = PriceInput(frame: .zero, isPre: false, prefixText: "miles", maxText: "00.0", maxValue: 80, dp: 1)
        let first_form_row = UIView()
        first_form_row.flex.direction(.row).alignItems(.stretch)
        form = APForm([
            TextFormElementConfig(InfoFields.firstName, placeholder: "your first name...", characterLimit: nil, type: .short, icon: "person", nestView: first_form_row),
            TextFormElementConfig(InfoFields.lastName, placeholder: "your last name...", characterLimit: nil, type: .short, nestView: first_form_row),
            TextFormElementConfig(InfoFields.group, placeholder: "tap to change...", characterLimit: nil, type: .option, icon: "bag"),
            TextFormElementConfig(InfoFields.radius, placeholder: "15", characterLimit: nil, type: .custom, customView: mileageInput, icon: "location"),
            TextFormElementConfig(InfoFields.company, placeholder: "your company name...", characterLimit: nil, type: .short, icon: "briefcase"),
            TextFormElementConfig(InfoFields.description, placeholder: "tell your customer about you...", characterLimit: 500, type: .long, icon: "pencil")
        ], builder: ProfileEditorVC.buildButton, headers: photo, button) { [unowned self] in
            let update = self.detail(fields: $0)
            self.processUpdate(request: update)
        }
        
        priceMatrix = PriceEditor(button: ProfileEditorVC.buildButton)
        priceMatrix.processUpdateReqeust = { [unowned self] (update, list) in self.processServiceUpdate(list, update)
        }
        add(priceMatrix, add: false)
        
        let vcs = getVCs()
        vcs[.portfolio]?.add(portfolio.gallery, add: false)
        portfolio.showGallery = { [unowned self] in self.showGallery(vc: vcs[.portfolio]) }
        button.tapped = { [unowned self] _ in
            self.showGallery(vc: vcs[.detail], maxNumberOfItems: 1, forProfile: true)
        }
        
        homePage.didSelect = { [unowned self] in
            guard let vc = vcs[$0] else { return }
            self.push(to: vc.name, from: "", using: vc)
        }
        view.addSchemedView(homePage)
        view.schemedBG()
        
        loadAll()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Onboarder.check.showOnboarding(for: .profileEditor, atVC: self)
    }
    
    private func process(action: PortfolioActions, models: [PortfolioImage]) {
        switch action {
        case .delete:
            let performDelete = { [weak self] in
                let delete = models.map {
                    APIClient.api.deletePorfolio(image: GetModelRequest(id: $0.id))
                }
                let promi = Future.andAll(delete, eventLoop: APIClient.api.worker.eventLoop)
                Message.notify(
                    process: promi,
                    loadingMessage: "Deleting portfolio image...",
                    successMessage: "Deleted.",
                    errorMessage: "Error occured while deleting.",
                    silentSuccess:  false
                ) { [weak self] (_) in
                    self?.refreshProfile()
                }
            }
            let s = models.count > 1 ? "s" : ""
            Message.showConfirmation(title: "Delete Image\(s)?", description: "Please confirm you want to delete the images.") {
                if $0 { performDelete() }
            }
        case .update:
            guard let model = models.first else { return }
            let update = PortfolioPostRequest(caption: model.caption, image: nil, type: nil, id: model.id)
            Message.notify(
                process: APIClient.api.portfolio(post: update),
                loadingMessage: "Updating caption...",
                successMessage: "Updated.",
                errorMessage: "Error occured while updating.",
                silentSuccess:  false
            ) { [weak self] (result) in
                switch result {
                case .success(let model):
                    self?.load(profile: model)
                case .failure(_):
                    self?.refreshProfile()
                }
            }
        }
    }
    
    func refreshProfile() {
        APIClient.api.getProfile().do { [weak self] in
            self?.load(profile: $0)
        }.catch { error in
            Feedback.showNotification(
                message: Talk.it.serverError
            )
        }
    }
    
    func loadImage(_ url: URL?) {
        guard let url = url else { return }
        let s = UIScreen.main.bounds.width * imgF
        let processor = DownsamplingImageProcessorScaled(size: CGSize(width: s, height: s))
        KingfisherManager.shared.retrieveImage(with: url, options: [.processor(processor)]) { result in
            switch result {
            case .success(let value):
                DispatchQueue.main.async {
                    self.photo.layer.masksToBounds = true
                    self.photo.image = value.image
                    self.view.setNeedsLayout()
                }
            case .failure(let error):
                Log(error) // The error happens
            }
        }
    }
    
    func load(profile: ClientProfileModel, groups: [ServiceGroup]? = nil, services: [ServiceModel]? = nil) {
        DispatchQueue.main.async {
            self.mileageInput.value = profile.mileage
            self.loadImage(profile.image)
            if let services = services {
                self.priceMatrix.load(services)
            }
            self.priceMatrix.on(profile: profile)
            self.form.load(self.toDetailFields(profile))
            if let groups = groups {
                self.form.load(options: groups.map { $0.name }, for: .group)
            }
            self.portfolio.load(images: profile.portfolio ?? [])
        }
    }
    
    private func processServiceUpdate(_ list: ClientServiceList,_ update: ClientProfileUpdate) {
        let promi: Future<(ClientProfileModel, ClientServiceList)> = APIClient.api.save(services: list).flatMap({ model in
            let findService = { (key: String) in
                model.services.first {
                    $0.key == key || EditableServicePrice(service: $0).diffIdentifier() as? String == key
                }?.key
            }
            var transform = update
            transform.matrix = (transform.matrix ?? []).compactMap {
                guard let newKey = findService($0.id) else { return nil }
                return ServicePriceModel(id: newKey, price: $0.price, duration: $0.duration)
            }
            return APIClient.api.saveClient(profile: transform).map { ($0, model) }
        })
        Message.notify(
            process: promi,
            loadingMessage: "Saving services...",
            successMessage: "Saved.",
            errorMessage: "Error saving profile, pleaase check entries.",
            silentSuccess:  false
        ) { [weak self] (result) in
            switch result {
            case .success(let (model, list)):
                self?.load(profile: model, services: list.services)
            case .failure(_):
                self?.refreshProfile()
            }
        }
    }
    
    func processUpdate(request: ClientProfileUpdate) {
        Message.notify(
            process: APIClient.api.saveClient(profile: request),
            loadingMessage: "Saving profile...",
            successMessage: "Saved.",
            errorMessage: "Error saving profile, pleaase check entries.",
            silentSuccess:  false
        ) { [weak self] (result) in
            switch result {
            case .success(let model):
                self?.load(profile: model)
            case .failure(_):
                self?.refreshProfile()
            }
        }
    }
    
    private func toDetailFields(_ profile: ClientProfileModel) -> [InfoFields: String?] {
        return [
            .firstName: profile.first_name,
            .lastName: profile.last_name,
            .group: profile.group,
            .company: profile.company,
            .description: profile.description
        ]
    }
    private func detail(fields fs: [InfoFields: String?]) -> ClientProfileUpdate {
        let cempty = { (x: String?) -> String? in x == nil || x == "" ? nil : x }
        return ClientProfileUpdate(
            description: cempty(fs[.description] ?? nil),
            first_name: cempty(fs[.firstName] ?? nil),
            last_name: cempty(fs[.lastName] ?? nil),
            company: cempty(fs[.company] ?? nil),
            group: cempty(fs[.group] ?? nil),
            mileage: mileageInput.value
        )
    }
    
    func loadAll() {
        Message.notify(
            process: map(
                APIClient.api.getProfile(),
                APIClient.api.getGroups(),
                APIClient.api.getServices()
            ) { ($0, $1, $2) },
            loadingMessage: "Loading Profile...",
            successMessage: "Loaded Profile",
            errorMessage: "Error Loading Profile") { [weak self] (result) in
                if case .success(let (profile, groups, data)) = result {
                    guard let self = self else { return }
                    self.load(profile: profile, groups: groups)
                    self.priceMatrix.load(data.services)
                }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        homePage.pin.top(safeAreaInsets).marginTop(5).horizontally().bottom(view.pin.safeArea)
    }
    
    private func showGallery(vc: UIViewController?, maxNumberOfItems: Int = 10, forProfile: Bool = false) {
        guard let vc = vc else { return }
        var config = YPImagePickerConfiguration()
        config.showsVideoTrimmer = false
        config.library.maxNumberOfItems = forProfile ? 1 : maxNumberOfItems
        config.showsPhotoFilters = false
        config.screens = [.library]
        config.library.mediaType = .photo
        config.wordings.warningMaxItemsLimit = "The limit is %d photos"
        config.wordings.done = "Upload"
        config.gallery.hidesRemoveButton = false
        let picker = YPImagePicker(configuration: config)
        picker.didFinishPicking { [unowned picker] items, cancelled in
            defer {
                picker.dismiss(animated: true, completion: nil)
            }
            guard !cancelled else { return }
            let photos: [PortfolioPostRequest] = items.compactMap {
                if case .photo(let photo) = $0 {
                    let data = photo.image.jpegData(compressionQuality: 1)
                    return PortfolioPostRequest(caption: "", image: data, type: "jpeg", is_profile: forProfile)
                }
                return nil
            }
            let loader = APProgressFloat(bars: 10)
            Feedback.showCentral(view: loader)
            let promises = photos.enumerated().map { (i, elem) in
                APIClient.api.portfolio(post: elem, progress: {
                    [unowned loader] in
                    loader.set(bar: i, to: $0.fractionCompleted)
                    }
                )
            }
            DispatchQueue.global(qos: .background).asyncAfter(deadline: .now() + 0.5) { [weak self] in
                guard let self = self else { return }
                Message.notify(
                    process: Future.andAll(promises, eventLoop: APIClient.api.worker.eventLoop),
                    loadingMessage: nil,
                    successMessage: "Uploaded.",
                    errorMessage: "Error uploading gallery.",
                    silentSuccess:  false
                ) { [weak self] (_) in
                    self?.refreshProfile()
                }
            }
        }
        vc.present(picker, animated: true, completion: nil)
    }
}

extension ProfileEditorVC: OnboardingExperience {
    func setup() {
        //
    }
    
    var messages: [(String, UIView, CoachMarkArrowOrientation)] {
//        return [
//            (Talk.it.formHelp, form, .bottom),
//            (Talk.it.priceHelp, container.barView, .top),
//            (Talk.it.portfolioHelp, container.barView, .top),
//            (Talk.it.timesHelp, container.barView, .top),
//        ]
        return []
    }
    
    func willTransition(to index: Int) {
//        let chosen = ProfileOptions.allCases[index]
//        options.select(option: chosen.rawValue)
    }
}

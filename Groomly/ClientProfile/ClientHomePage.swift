///Users/ravi/Desktop/Work/Groomly/GroomlyPlatform/Groomly/ClientProfile/ProfileEditorVC.swift
//  ClientHomePage.swift
//  Groomly
//
//  Created by Ravi on 21/08/2021.
//  Copyright © 2021 Ashla. All rights reserved.
//

import UIKit
import PinLayout
import FlexLayout

import UIKit
import PinLayout
import FlexLayout
import Kingfisher

class ClientHomePage: UIView {
    let content =  UIView()
    let decorative = UIView()
    let scroll = UIScrollView()
    var didSelect: ((ProfileOptions) -> Void)?
    init() {
        super.init(frame: .zero)
        decorative.schemedBG(.lightShadow)
        content.flex.wrap(.wrap).alignContent(.center).justifyContent(.center).direction(.row)
        scroll.addSchemedView(content)
        addSchemedView(scroll)
        addSchemedView(decorative)
        scroll.isUserInteractionEnabled = true
        content.isUserInteractionEnabled = true
        accessibilityIdentifier = "homePageView"
        
        content.clear()
        
        content.flex.addItem(DescriptionCell(
            title: "Profile",
            buttonTitle: "My Profile",
            description: "Find your recent bookings\nand future bookings all\nin one",
            imagePath: "/static/images/shop.jpg",
            { [unowned self] in
                self.didSelect?(ProfileOptions.detail)
            }
        ))
        content.flex.addItem(DescriptionCell(
            title: "Services",
            buttonTitle: "My Services",
            description: "Tell us about the services\nthat you offer",
            imagePath: "/static/images/stock.jpg",
            { [unowned self] in
                self.didSelect?(ProfileOptions.services)
            }
        ))
        content.flex.addItem(DescriptionCell(
            title: "Portfolio",
            buttonTitle: "My Portfolio",
            description: "Show off your recent work\nto your potential clients.",
            imagePath: "/static/images/portfolio.jpg",
            { [unowned self] in
                self.didSelect?(ProfileOptions.portfolio)
            }
        ))
        content.flex.addItem(DescriptionCell(
            title: "Schedule",
            buttonTitle: "My Schedule",
            description: "Edit your normal schedule\nand working hours.",
            imagePath: "/static/images/hours.jpg",
            { [unowned self] in
                self.didSelect?(ProfileOptions.schedule)
            }
        ))
    }
    
    override func layoutSubviews() {
        decorative.pin.top(-6).width(70).height(6).hCenter()
        scroll.pin.all()
        content.pin.horizontally()
        
        if content.subviews.count == 0 {
            return
        }
            
        for v in content.subviews {
            if let v = v as? DescriptionCell {
                v.flex.width(scroll.frame.width).height(v.lblHeight(in: scroll.frame.width)).grow(0).shrink(0)
            }
        }
        
        
        
        content.flex.layout(mode: .adjustHeight)
        let reachedH = content.bounds.size.height
        scroll.contentSize.height = reachedH
        if reachedH < frame.height {
            scroll.pin.height(reachedH).vCenter()
        } else {
            scroll.pin.vertically()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

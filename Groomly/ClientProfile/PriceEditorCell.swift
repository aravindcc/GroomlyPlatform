//
//  PriceEditorCell.swift
//  Groomly
//
//  Created by Ravi on 10/08/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import FlexLayout
import PinLayout
import IGListKit

extension UICollectionView {
    override func rescheme(disregardOwn: Bool = true) {
        super.rescheme(disregardOwn: disregardOwn)
        reloadData()
    }
}


final class PlaceholderEditorCell: APCalendarCell {

    fileprivate let label: UILabel = {
        return UILabel(left: 15, weight: .regular)
    }()
    
    var text: String? {
        get { label.text }
        set { label.text = newValue }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        label.schemedTextColor(.primaryTextColor, alpha: 0.5)
        label.singleCentralLine()
        contentView.addSchemedView(label)
        backgroundView = nil
        backgroundColor = UIColor.clear
        contentView.backgroundColor = UIColor.clear
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        label.pin.all().marginHorizontal(10)
    }

    override var isHighlighted: Bool {
        didSet {
            contentView.backgroundColor = UIColor.clear
        }
    }
}


class PriceEditorCell: UICollectionViewCell {

    let stepper = VerticalStepper()
    let input = PriceInput(frame: .zero)
    let label = UILabel(left: 13, weight: .regular)
    let container = UIView()
    let sep = UIView()
    
    var enabled: Bool {
        get { stepper.enabled }
        set {
            stepper.enabled = newValue
            input.enabled = newValue
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        label.singleCentralLine()
        label.numberOfLines = 0
        label.schemedTextColor(.primaryTextColor)
        
        container.schemedBG()
        sep.schemedBG(.highlightColorA)
        container.layer.cornerRadius = 15
        container.layer.masksToBounds = true
        
        container.flex.paddingLeft(15).paddingRight(10).paddingVertical(10).direction(.row).alignItems(.center).define { (flex) in
            flex.addItem().grow(1).height(100%).define { (flex) in
                flex.view?.addSubview(label)
            }
            flex.addItem(input).width(20%).marginHorizontal(10).grow(0).shrink(0)
            flex.addItem(stepper).width(20%).marginRight(5).grow(0).shrink(0)
        }
        addSubview(container)
        container.addSubview(sep)
        
        input.onChange = { [unowned self] in
            self.sendUpdate()
        }
        stepper.onChange = { [unowned self] in
            self.stepper.display(seconds: self.stepper.currentValue + 15 * 60 * $0)
            self.sendUpdate()
        }
    }
    
    func sendUpdate() {
        guard let model = model else { return }
        model.price = input.value
        model.duration = stepper.currentValue
    }
    
    private var model: EditableServicePrice?
    func display(model: EditableServicePrice) {
        self.model = model
        stepper.display(seconds: model.duration ?? 0)
        label.text = model.service.name
        input.value = model.price
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        container.pin.vertically(10).horizontally()
        container.flex.layout()
        label.pin.all()
        sep.pin.horizontally(-5).height(5).bottom()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

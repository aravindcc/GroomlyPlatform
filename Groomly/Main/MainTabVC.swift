//
//  MainTabVC.swift
//  Learning
//
//  Created by clar3 on 20/03/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import UserNotifications
import PinLayout
import FlexLayout
import Instructions

enum ViewType: String {
    case customerHome = "Home"
    case profileClient = "Account"
    case profileCustomer = "Profile"
    case calendar = "Calendar"
    case configure = "Configure"
    case settings = "Settings"
    case profileEditor = "About"
}

class MainTabVC: APTabController {

    private(set) var indexMap: [ViewType: Int] = [:]
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let views: [(ViewType, String, UIViewController)] = [
            (.profileEditor, "house", APIClient.isClient ? ProfileEditorVC() :  HomeCoordinater.shared.start()),
            (.calendar, "calendar", CalendarVC()),
            (
                APIClient.isClient ? .profileClient : .profileCustomer,
                "person.crop.circle",
                APIClient.isClient ? ClientAccountVC() : CustomerProfileVC()
            ),
            (.settings, "gear", SettingsVC())
        ]
        
//        #if DEBUG
//        views.append((.configure, "slider.horizontal.3", ConfigureVC()))
//        #endif
        
        indexMap = [:]
        let resp = views.enumerated().map { (index, value) in
            create(index, controller: value.0, image: value.1, vc: value.2)
        }
        viewControllers = resp.map { $0.0 }
        setTabBar(items: resp.map { $0.1 })
        
        #if DEBUG
        if AppDelegate.uiTestingBooking {
            let id = ProcessInfo.processInfo.environment["BookingID"] ?? "1"
            process(bookingID: Int(id) ?? 1)
        }
        #else
        if let notify = (UIApplication.shared.delegate as? AppDelegate)?.savedNotification {
            process(notification: notify)
            (UIApplication.shared.delegate as? AppDelegate)?.savedNotification = nil
        }
        #endif
    }
    
    private func create(
        _ index: Int,
        controller title: ViewType,
        image: String,
        vc: UIViewController
    ) -> (UINavigationController, APTabBarItem) {
        indexMap[title] = index
        vc.setStandardNav()
        let outvc = MainNVC(rootViewController: vc)
        outvc.additionalSafeAreaInsets = UIEdgeInsets(top: 0, left: 0, bottom: APTabBar.extraHeight, right: 0)
        vc.view.colorScheme = .normal
        vc.view.schemedBG(.highlightColorA)
        let img = UIImage(named: image) ?? UIImage(systemName: image)
        let item = APTabBarItem(icon: img, title: title.rawValue)
        if title == .settings {
            item.accessibilityIdentifier = "settingsButton"
        } else if title == .profileCustomer {
            item.accessibilityIdentifier = "profileCustomerButton"
        }
        return (outvc, item)
    }
    
    func process(notification: UNNotification) {
        let info = notification.request.content.userInfo
        guard let id = info["booking_id"] as? Int else { return }
        process(bookingID: id)
    }
    
    func process(bookingID id: Int, action: BookingActions? = nil) {
        guard
            let selectedIndex = self.indexMap[.calendar],
            let nvc = (viewControllers ?? [])[selectedIndex] as? MainNVC,
            let calendar = nvc.viewControllers.first as? CalendarVC
        else { return }
        switchTo(tab: selectedIndex)
        calendar.open(booking: id, action: action)
    }
    func switchTo(view: ViewType) {
        guard
            let selectedIndex = self.indexMap[view]
        else { return }
        switchTo(tab: selectedIndex)
    }
}

//
//  HomeCoordinater.swift
//  Groomly
//
//  Created by Ravi on 18/07/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import Async

extension UIViewController {
    func moveTo(_ viewControllerToPresent: UIViewController, direc: CATransitionSubtype) {
        let window = self.view.window!
        let transition = CATransition()
        transition.duration = 0.25
        transition.type = .push
        transition.subtype = direc
        window.layer.add(transition, forKey: kCATransition)
        window.rootViewController = viewControllerToPresent
    }
}

enum OrderEvent {
    case confirmedGroup
    case confirmedServices
    case selectedClient
    case confirmedCheckout 
    case confirmedPayment
    case paymentSuccess
    case paymentCancelled
    case paymentFailed
    case cancelledCheckout
    case cancelledServices
    case cancelledClients
}

enum OrderState {
    case homepage
    case services
    case clients
    case checkout
    case payment
    case processing
}

class HomeCoordinater {
    static let shared = HomeCoordinater()
    
    let orderFSM = StateMachine<OrderState, OrderEvent>(initialState: .services)
    
    init() {
        let ts: [(OrderState, OrderState, OrderEvent, ExecutionBlock?)] = [
            (.homepage, .clients, .confirmedGroup, nil),
            (.clients, .homepage, .cancelledClients, nil),
            (.clients, .services, .selectedClient, nil),
            (.services, .checkout, .confirmedServices, { [weak self] in self?.confirmedServices() }),
            (.checkout, .payment, .confirmedCheckout, nil),
            (.payment, .processing, .confirmedPayment, { [weak self] in self?.confirmedPayment() }),
            (.processing, .clients, .paymentSuccess, { [weak self] in self?.onPaymentSuccess() }),
            (.processing, .checkout, .paymentFailed, nil),
            (.checkout, .services, .cancelledCheckout, nil),
            (.services, .clients, .cancelledServices, nil),
            (.payment, .checkout, .paymentCancelled, nil),
        ]
        for (from, to, trans, post) in ts {
            orderFSM.add(transition:
                Transition(with: trans, from: from, to: to, preBlock: nil, postBlock: post)
            )
        }
    }
    
    func tearDown() {
        exploreVC = nil
        shopVC = nil
        checkoutVC = nil
        order = ServiceOrderModel()
    }
    
    func start() -> UIViewController {
        fetchClients()
        return HomePageVC()
    }
    
    
    // Home Management
    private var exploreVC: ExploreVC?
    private var shopVC: ShopVC?
    private var homePageVC: HomePageVC?
    private var checkoutVC: CheckoutVC?
    private var savedClients: [ClientStubResponse] = []
    private var order: ServiceOrderModel = ServiceOrderModel()
    private var selectedGroup: String?
    private var savedServiceList: ClientServiceList?
    private var savedListOfClient: Int?
    private var savedDate: Date?
    
    func registerEntry(_ vc: HomePageVC) {
        homePageVC = vc
        populateShop() // could potentially have already fetched services ...
        AddressBackend.shared.subscribe { [weak self] () -> Bool in
            guard let self = self else { return false }
            self.refreshClients()
            return true
        }
    }
    
    func select(group: String) {
        selectedGroup = group
        orderFSM.process(event: .confirmedGroup)
        shopVC = shopVC ?? ShopVC()
        self.homePageVC?.push(to: group, from: Talk.it.groupVCName, using: shopVC!)
        populateShop()
    }
    
    private func fetchServices() {
        guard let clientID = order.client?.id else { return }
        if clientID == savedListOfClient {
            populateExplore()
            return
        }
        Message.notify(
            process: APIClient.api.getServices(for: clientID),
            loadingMessage: "Loading services...",
            successMessage: nil,
            errorMessage: "There was an error loading the services available."
        ) { [weak self] (result) in
            switch result {
            case .success(let model):
                // load client service list in explore VC
                self?.savedServiceList = model
                self?.savedListOfClient = clientID
                self?.populateExplore()
            case .failure(let error):
                Log("FAILED FOR REASON \(error)")
            }
        }
    }
    
    private func fetchClients() {
        let _ = APIClient.api.clientList().do { [weak self]
            clients in
            self?.clientLocationCache = AddressBackend.shared.selected?.formatted_address
            self?.savedClients = clients
            self?.populateShop()
        }
        // don't worry if error
    }
    
    private func populateExplore() {
        refreshCart()
        exploreVC?.load(client: order.client, list: savedServiceList)
    }
    
    private func populateShop() {
        let grp = selectedGroup
        DispatchQueue.main.async {
            self.shopVC?.loadData(clients: self.savedClients.filter({ grp == nil || $0.profile?.group == grp! }))
            self.shopVC?.view.isUserInteractionEnabled = true
        }
    }
    
    private func showCart(silent: Bool = false) {
        DispatchQueue.main.async {
            self.exploreVC?.showCart(silent: silent)
        }
    }
    
    // select time
    func select(time: Date) {
        order.time = time
        refreshCart()
    }
    
    func load(timeVC: TimeVC) -> Bool {
        guard let client = order.client else { return false }
        timeVC.client = client.id
        return true
    }
    
    private func confirmedServices() {
    
        let onTime = { [weak self] in
            guard let self = self else { return }
            Message.notify(
                process: APIClient.api.getFee(for: GetModelRequest.blank),
                loadingMessage: "confirming...",
                successMessage: nil,
                errorMessage: "Error loading checkout."
            ) { [weak self] (result) in
                guard let self = self else { return }
                switch result {
                case .success(let model):
                    self.order.location = AddressBackend.shared.selected?.formatted_address
                    self.order.extraFees = model.possible
                    self.checkoutVC = CheckoutVC()
                    self.refreshCart()
                    self.exploreVC?.push(to: "Checkout", from: Talk.it.serviceVCName, using: self.checkoutVC!)
                case .failure(let error):
                    Log("ERROR \(error)")
                    if case NetworkReqError.Detail(let detail) = error {
                        Log(String(data: detail, encoding: .utf8) ?? "NONE")
                    }
                }
            }
        }
        
        #if DEBUG
        if AppDelegate.isUITestingEnabled {
            onTime()
            return
        }
        #endif
        
        let vc = TimeVC()
        vc.setDurationOfEvent = order.items.reduce(0, { $0 + ($1.duration ?? 0) }) / 60
        if load(timeVC: vc) {
            vc.completion = { [unowned self] entry in
                defer {
                    vc.dismiss(animated: true)
                }
                guard let entry = entry else {
                    self.orderFSM.process(event: .cancelledCheckout)
                    return
                }
                self.select(time: entry.start)
                Log("chosen appointment from \(entry.start) till \(entry.end)")
                onTime()
            }
            self.exploreVC?.present(vc, animated: true)
        }
        
    }
    
    func select(client: ClientStubResponse) {
        exploreVC = exploreVC ?? ExploreVC()
        orderFSM.process(event: .selectedClient)
        if order.client != client {
            order = ServiceOrderModel()
            order.client = client
        }
        fetchServices()
        self.shopVC?.push(to: Talk.it.serviceVCName, from: Talk.it.shopVCName, using: exploreVC!)
    }
    
    func potentialTotal(for profile: ClientProfileModel) -> Float {
        order.items.map { profile.price(for: $0.service) ?? 0 }.reduce(0, +)
    }
    
    // add service
    func add(service: ServiceModel) {
        order.items.append(
            ServiceItemOrderModel(
                service: service,
                value: order.client?.profile?.price(for: service),
                duration: order.client?.profile?.duration(for: service)
            )
        )
        refreshCart()
        Feedback.showNotification(
            message: NotificationDetails(
                title: "Success",
                desc: "\(service.name) has been added to cart. Tap to view.",
                imageName: "checkmark"
            ),
            tapped: { self.showCart() }
        )
    }
    
    // remove service
    func remove(service: ServiceModel) {
        for (i, item) in order.items.enumerated() {
            if item.service === service {
                order.items.remove(at: i)
                refreshCart()
                break
            }
        }
    }
    
    private var clientLocationCache: String?
    func refreshClients() {
        let silently = shopVC?.viewIfLoaded?.window == nil
        let chosen = AddressBackend.shared.selected?.formatted_address
        self.order.location = chosen
        guard clientLocationCache != chosen else { return }
        shopVC?.view.isUserInteractionEnabled = false
        Message.notify(
            process: APIClient.api.clientList(),
            loadingMessage: .loadingLocation,
            successMessage: nil,
            errorMessage: "There was an error loading the new location.",
            inForeground: !silently,
            silentSuccess: false
        ) { [weak self] (result) in
            switch result {
            case .success(let model):
                self?.clientLocationCache = AddressBackend.shared.selected?.formatted_address
                self?.savedClients = model
                self?.populateShop()
            case .failure(let error):
                Log("FAILED FOR REASON \(error)")
            }
        }
    }
    
    // refresh cart
    private func refreshCart() {
        guard orderFSM.currentState != .clients else { return }
        
        // copy model
        let newOrder = order.copy()
        
        if orderFSM.currentState == .checkout {
            checkoutVC?.basket.model = newOrder
        }
        else if orderFSM.currentState == .services {
            // assign model to cart view
            self.exploreVC?.orderVC.reload(newOrder)
            
            // show cart if already shown
            showCart(silent: true)
        }
    }
    
    private func confirmedPayment() {
        Message.showConfirmation(
            title: "Pending Charge",
            description: Talk.it.pendingChargeDesc
        ) {
            if $0 { self._confirmedPayment() }
        }
    }
    
    private func _confirmedPayment() {
        Message.notify(
            process: try? APIClient.api.create(booking: order),
            loadingMessage: "Requesting appointment...",
            successMessage: "Requested Appointment",
            errorMessage: { error in
                #if DEBUG
                if case NetworkReqError.Detail(let message) = error,
                    let str = String(bytes: message, encoding: .utf8)?.lowercased() {
                    if str.contains("Payment error:") {
                        return str.replacingOccurrences(of: "Payment error:", with: "")
                    }
                    else if str.contains("invalid") {
                        if str.contains("time") {
                            if self.order.time! < Date() {
                                return "Appointment needs to be in the future"
                            }
                            return "That time is unavailable."
                        } else if str.contains("location") {
                            return "That location is unavailable."
                        }
                    }
                    return "\(str)"
                }
                #endif
                return "There was an error booking the appointment."
            },
            silentSuccess: false,
            throwNilPromise: true
        ) { [weak self] (result) in
            switch result {
            case .success(let model):
                if model.status == .RESOLVING, model.reject_reason == .PAYMENT,
                    let rootvc = UIApplication.shared.windows.first?.rootViewController as? MainTabVC {
                    Message.showConfirmation(
                        title: "Authentication required",
                        description: Talk.it.authRequiredMessage,
                        cancelExists: false
                    ) { (_) in
                        DispatchQueue.main.async {
                            rootvc.process(bookingID: model.id, action: .resolvePayment)
                        }
                    }
                }
                self?.orderFSM.process(event: .paymentSuccess)
            case .failure(_):
                self?.orderFSM.process(event: .paymentFailed)
            }
        }
    }
    
    private func onPaymentSuccess() {
        order = ServiceOrderModel()
        order.location = AddressBackend.shared.selected?.formatted_address
        checkoutVC?.navigationController?.popToRootViewController(animated: true)
        refreshCart()
    }
    
}

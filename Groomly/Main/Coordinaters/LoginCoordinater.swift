//
//  LoginCoordinater.swift
//  Groomly
//
//  Created by Ravi on 05/08/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import Async
import Stripe

class LoginCoordinater {
    static func start() -> UIViewController {
        // use keychain and get saved password
        if  let savedEmail = UserDefaults.standard.string(forKey: "email"),
            let savedPassword: String = KeyChain.load(key: savedEmail) {
            let request = LoginRequest(email: savedEmail, password: savedPassword)
            do {
                try Networker.shared.authorize(user: request).wait()
                return interimLaunch()
            } catch {
                return LoginVC()
            }
        }
        return LoginVC()
    }
    
    static func onLogin(_ atVC: UIViewController) {
        guard   let details = Networker.shared.loginReq,
                let password = details.password.data(using: .utf8)
        else {
            preconditionFailure("No Login Request Made.")
        }
        let email = details.email
        UserDefaults.standard.set(email, forKey: "email")
        let _ = KeyChain.save(key: email, data: password)
        DispatchQueue.main.async {
            atVC.moveTo(interimLaunch(), direc: .fromRight)
        }
    }
    
    static fileprivate func launch() -> UIViewController {
        AddressBackend.shared.load()
        if !APIClient.isClient {
            PaymentBackend.shared.load()
        }
        return MainTabVC()
    }
    
    static func launch(atVC: UIViewController) {
        DispatchQueue.main.async {
            atVC.moveTo(launch(), direc: .fromRight)
        }
    }

    static fileprivate func processStripe(_ atVC: UIViewController) {
        APIClient.api.getStripeSetup().do { [unowned atVC] model in
            Stripe.setDefaultPublishableKey(model.key)
            APIClient.api.session = model
            if !APIClient.isClient {
                PaymentBackend.shared.setupCustomer()
            }
            launch(atVC: atVC)
        }.catch { [unowned atVC] (_) in
            DispatchQueue.main.async {
                if APIClient.isClient, let request = try? APIClient.api.getStripeAuth() {
                    atVC.moveTo(PayeeVC(request: request), direc: .fromRight)
                    return
                }
                Feedback.showNotification(message: Talk.it.serverError)
            }
        }
    }

    static func interimLaunch() -> UIViewController {
        let atVC = LoadingVC()
        processStripe(atVC)
        return atVC
    }
    
    static func clearLoginDetails() {
        if let savedEmail = UserDefaults.standard.string(forKey: "email") {
            KeyChain.clear(key: savedEmail)
            UserDefaults.standard.removeObject(forKey: "email")
        }
    }
    
    static func clearSession(_ atVC: UIViewController) {
        let _ = Networker.shared.logout().always {
            clearLoginDetails()
        }
        
        AddressBackend.shared.clear()
        PaymentBackend.shared.clear()
        HomeCoordinater.shared.tearDown()
        let rootVC = atVC.tabBarController ?? atVC.navigationController ?? atVC
        DispatchQueue.main.async {
            rootVC.moveTo(LoginVC(), direc: .fromLeft)
        }
    }
}

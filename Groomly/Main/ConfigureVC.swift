//
//  ConfigureVC.swift
//  Groomly
//
//  Created by clar3 on 05/05/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//
#if DEBUG
import UIKit
import PinLayout
import FlexLayout


class ConfigureVC: UIViewController {
    let schemeChooser = UISegmentedControl(items: SchemeCase.allCases.map({ $0.rawValue }))
    let propertyChooser = UIPickerView()
    let schemeViewer: SelectableColorGrid!
    let colorGrid = SelectableColorGrid(colorSet: FlatColors.allCases.map({ UIColor($0.rawValue) }))
    let colorView = UIButton()
    let container = UIView()
    var currentChosenColor: UIColor = .black {
        didSet {
            colorView.backgroundColor = currentChosenColor
            showColorScheme()
        }
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        let cases = ColorKey.allCases
        let width = 100 / CGFloat(cases.count)
        schemeViewer = SelectableColorGrid(colorSet: cases.map { _ in .black }, width: width%)
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        schemeViewer.choseColor = { [weak self] color in
            guard let self = self else { return }
            if let selected = self.schemeViewer.colorSet.enumerated().filter({ $0.1 == color }).first?.offset {
                self.propertyChooser.selectRow(selected, inComponent: 0, animated: true)
            }
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.schemedBG()
        schemeChooser.addTarget(self, action: #selector(readCurrentColor), for: .valueChanged)
        propertyChooser.dataSource = self
        propertyChooser.delegate = self
        propertyChooser.backgroundColor = .white
        colorView.addTarget(self, action: #selector(updateColor), for: .touchUpInside)
        colorView.backgroundColor = currentChosenColor
        colorView.layer.borderColor = UIColor.white.cgColor
        colorView.layer.borderWidth = 2
        
        colorGrid.choseColor = self.chose
        
        view.addSchemedView(container)
        container.flex.alignItems(.stretch).define { (flex) in
            flex.addItem(schemeChooser)
            flex.addItem(propertyChooser).height(120).marginVertical(10)
            flex.addItem(schemeViewer)
            flex.addItem(colorGrid).grow(1).shrink(1)
            flex.addItem().marginVertical(10).display(.flex).direction(.row).alignItems(.stretch).define { (flex) in
                flex.view?.backgroundColor = .white
                let lbl = UILabel(left: 20, weight: .bold, color: .black, text: "Choose Color")
                lbl.baselineAdjustment = .alignCenters
                flex.addItem(lbl).marginRight(20)
                flex.addItem(colorView).grow(1).shrink(1)
            }
            flex.addItem()
                .direction(.row)
                .alignItems(.stretch)
                .justifyContent(.spaceAround)
                .height(35)
                .marginTop(15)
                .marginBottom(20)
                .define { (flex) in
                let options = ["Copy"]
                let actions: [ButtonCompletion] = [{ _ in let _ = ColorConfigurator.serialiseCurrent() }, ]
                for i in 0..<options.count {
                    let but = FlexButton()
                    but.title = options[i]
                    but.preset = true
                    but.tapped = actions[i]
                    flex.addItem(but).width(but.wrappedWidth)
                }
            }
        }
        
        schemeChooser.selectedSegmentIndex = 0
        propertyChooser.selectRow(0, inComponent: 0, animated: false)
        readCurrentColor()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        container.pin.bottom(view.pin.safeArea).top(view.pin.safeArea + 10).horizontally(20)
        container.flex.layout()
    }
    
    @objc func updateColor() {
        let alert = UIAlertController(style: .actionSheet)
        alert.addColorPicker(color: currentChosenColor) { color in
            self.chose(color: color)
        }
        alert.addAction(title: "Done", style: .cancel)
        alert.show()
    }
    
    @objc func chose(color: UIColor) {
        let property = ColorKey.allCases[propertyChooser.selectedRow(inComponent: 0)]
        let scheme: SchemeCase = schemeChooser.selectedSegmentIndex == 0 ? .normal : .alt
        ColorConfigurator.set(color: color, forScheme: scheme, andProperty: property)
        ColorConfigurator.shared.notify(changeOfproperty: property, forScheme: scheme, withColor: color)
        currentChosenColor = color
    }
    
    @objc func readCurrentColor() {
        let scheme: SchemeCase = schemeChooser.selectedSegmentIndex == 0 ? .normal : .alt
        let property = ColorKey.allCases[propertyChooser.selectedRow(inComponent: 0)]
        let color: UIColor? = ColorConfigurator.getColor(scheme: scheme, property: property)
        if let color = color {
            currentChosenColor = color
            colorGrid.select(color: currentChosenColor)
        }
    }
    
    func showColorScheme() {
        let scheme: SchemeCase = schemeChooser.selectedSegmentIndex == 0 ? .normal : .alt
        for (i, property) in ColorKey.allCases.enumerated() {
            if let color = ColorConfigurator.getColor(scheme: scheme, property: property) {
                schemeViewer.colorSet[i] = color
            }
            schemeViewer.reColor()
            schemeViewer.select(color: currentChosenColor)
        }
    }
}

extension ConfigureVC: UIPickerViewDataSource, UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        readCurrentColor()
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return ColorKey.allCases.count
    }
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {

        var pickerLabel = view as? UILabel
        if (pickerLabel == nil) {
            pickerLabel = UILabel()
            pickerLabel?.font = UIFont(name: "Montserrat", size: 16)
            pickerLabel?.textColor = .black
            pickerLabel?.textAlignment = .center
        }

        let property = ColorKey.allCases[row]
        pickerLabel?.text = row < ColorKey.allCases.count ? property.rawValue : nil
        return pickerLabel!
    }
}

class SelectableColorGrid: UIView {
    
    var colorSet: [UIColor]
    var choseColor: ((UIColor) -> Void)?
    func select(color: UIColor) {
        let selected = colorSet.enumerated().filter({ $0.1 == color }).first?.offset
        for v in (container.subviews.first?.subviews ?? []) {
            v.borderWidth = 2.0
            v.borderColor = v.tag == selected ? .white : .clear
            v.setNeedsDisplay()
        }
    }
    
    private let container = UIScrollView()
    
    init(colorSet: [UIColor], width: FPercent = 10%) {
        self.colorSet = colorSet
        super.init(frame: .zero)
        
        container.flex.addItem().direction(.row).wrap(.wrap).define { (flex) in
            for (i, color) in colorSet.enumerated() {
                let button = UIButton()
                button.backgroundColor = color
                button.tag = i
                button.addTarget(self, action: #selector(userChoseColor(_:)), for: .touchUpInside)
                flex.addItem(button).aspectRatio(1).width(width)
            }
        }
        
        addSchemedView(container)
    }
    
    func reColor() {
        container.subviews.first?.subviews.enumerated().forEach { (i, view) in
            view.tag = i
            view.backgroundColor = colorSet[i]
        }
    }
    
    @objc func userChoseColor(_ sender: UIButton) {
        let c = colorSet[sender.tag]
        choseColor?(c)
        select(color: c)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        container.pin.all()
        if let sub = container.subviews.first {
            sub.flex.layout()
            container.contentSize = sub.frame.size
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
#endif

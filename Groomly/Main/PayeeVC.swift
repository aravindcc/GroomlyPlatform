//
//  PayeeVC.swift
//  Groomly
//
//  Created by Ravi on 15/08/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import PinLayout
import WebKit

class PayeeVC: UIViewController, WKNavigationDelegate {
    fileprivate lazy var webview: WKWebView = {
        let v = WKWebView()
        v.navigationDelegate = self
        self.view.addSubview(v)
        return v
    }()
    
    func loadWebPage(url: URL)  {
        var customRequest = URLRequest(url: url)
        let _ = try? Networker.shared.confirmClientToken().wait()
        if let token = Networker.shared.cachedLogin?.token {
            customRequest.setValue(token, forHTTPHeaderField: "Authorization")
        }
        webview.load(customRequest)
    }

    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping
    (WKNavigationActionPolicy) -> Void) {
        if navigationAction.request.httpMethod != "GET" || navigationAction.request.value(forHTTPHeaderField: "Authorization") != nil || (navigationAction.request.url?.absoluteString.contains("stripe") ?? false) {
            // not a GET or already a custom request - continue
            decisionHandler(.allow)
            return
        }
        decisionHandler(.cancel)
        loadWebPage(url: navigationAction.request.url!)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        if let url = webView.url?.absoluteString, url.contains("payment/oauth"), url.contains("code") {
            moveTo(LoginCoordinater.interimLaunch(), direc: .fromRight)
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        webview.pin.all()
    }
    
    init(request: URLRequest) {
        super.init(nibName: nil, bundle: nil)
        webview.load(request)
    }
    
    required init?(coder: NSCoder) {
        super.init(nibName: nil, bundle: nil)
    }
}

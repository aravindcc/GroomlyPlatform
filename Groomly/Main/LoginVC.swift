//
//  LoginVC.swift
//  Groomly
//
//  Created by Ravi on 18/07/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import PinLayout
import FlexLayout
import Async

extension Flex {
    func addItem(_ view: UIView,_ tag: Int) -> Flex {
        if let already = self.view?.viewWithTag(tag) {
            if let view = view as? UILabel, let already = already as? UILabel {
                already.text = view.text
            }
            return already.flex
        } else {
            view.tag = tag
            return addItem(view)
        }
    }
}

extension UIView {
    func reflex(animate: Bool = true) {
        let action = { self.flex.layout() }
        if animate { UIView.animate(withDuration: 0.1, animations: action) }
        else { action() }
    }
}

@propertyWrapper
class ClearableProperty<Value> where Value: Equatable {
    
    var display: (() -> Void)?
    var trigger: Bool = true
    private let defaultValue: Value
    private let clearTime: Double
    private var timer: Timer? = nil
    
    var wrappedValue: Value {
        didSet {
            if wrappedValue == defaultValue || !trigger { trigger = true; return }
            DispatchQueue.main.async {
                self.timer?.invalidate()
                self.timer = Timer.scheduledTimer(withTimeInterval: self.clearTime, repeats: false) { _ in
                    self.wrappedValue = self.defaultValue
                    self.display?()
                }
                self.display?()
            }
        }
    }

    internal init(wrappedValue: Value? = nil, defaultValue: Value, clearTime: Double) {
        self.defaultValue = defaultValue
        self.clearTime = clearTime
        self.wrappedValue = wrappedValue ?? defaultValue
    }
}

fileprivate class Checklist: UIView {
    enum Reqs: String, CaseIterable, Hashable {
        case length = "At least 8 characters long"
        case symbol = "At least one symbol (e.g. !,@,£)"
        case number = "At least one number"
        case capital = "At least one capital letter"
    }
    
    let v = UIView()
    private var imgReqMap: [Reqs: UIImageView] = [:]
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let mxM: CGFloat = Reqs.allCases.map {
            let l = UILabel(left: 14, weight: .regular)
            l.text = $0.rawValue
            return l.intrinsicContentSize.width
        }.reduce(0.0, max)
        for req in Reqs.allCases {
            let l = UILabel(left: 14, weight: .regular)
            l.text = req.rawValue
            l.schemedTextColor(.primaryTextColor, alpha: 0.8)
            let imgv = UIImageView(image: UIImage(systemName: "xmark.circle"))
            imgv.schemedTintColor(.secondaryTextColor)
            imgReqMap[req] = imgv
            v.flex.addItem().direction(.row).justifyContent(.center).alignItems(.center).define { (flex) in
                flex.addItem(imgv).width(15).aspectRatio(of: imgv).marginRight(25)
                flex.addItem(l).width(mxM)
            }
        }
        v.flex.justifyContent(.center).alignItems(.stretch)
        addSubview(v)
    }
    
    var hasPassed: Bool = false
    
    private func process(req: Reqs, asPassed passed: Bool, anding acc: inout Bool) {
        imgReqMap[req]?.reschemer.removeAll()
        imgReqMap[req]?.image = UIImage(systemName: passed ? "checkmark.circle": "xmark.circle")
        imgReqMap[req]?.schemedTintColor(passed ? .highlightColorA : .secondaryTextColor)
        imgReqMap[req]?.rescheme()
        acc = acc && passed
    }
    
    func process(password: String) -> Bool {
        var out = true
        
        process(req: .length, asPassed: password.count >= 8, anding: &out)
        
        let capitalLetterRegEx  = ".*[A-Z]+.*"
        let texttest = NSPredicate(format:"SELF MATCHES %@", capitalLetterRegEx)
        process(req: .capital, asPassed: texttest.evaluate(with: password), anding: &out)

        let numberRegEx  = ".*[0-9]+.*"
        let texttest1 = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
        process(req: .number, asPassed: texttest1.evaluate(with: password), anding: &out)

        let specialCharacterRegEx  = ".*[!&^%$#@()/_*+-]+.*"
        let texttest2 = NSPredicate(format:"SELF MATCHES %@", specialCharacterRegEx)
        process(req: .symbol, asPassed: texttest2.evaluate(with: password), anding: &out)

        return out
    }
    
    override func layoutSubviews() {
        v.pin.all()
        v.flex.layout(mode: .adjustHeight)
    }
    
    required init?(coder: NSCoder) {
        super.init(frame: .zero)
    }
}

fileprivate class TermsButton: UIView {
    var termTapped: Completion? { didSet { terms.tapped = termTapped } }
    var privTapped: Completion? { didSet { priv.tapped = privTapped } }
    private let container = UIView()
    private let terms = TextButton(color: .highlightColorC, text: "terms and conditions")
    private let priv = TextButton(color: .highlightColorA, text: " and privacy policy")
    init() {
        super.init(frame: .zero)
        let left = UILabel(left: 14, weight: .semibold)
        left.schemedTextColor(.primaryTextColor)
        left.text = "By signing up you are agreeing to the"
        container.flex.alignItems(.center).justifyContent(.center).define { (flex) in
            flex.addItem(left).marginBottom(10)
            flex.addItem()
                .direction(.row)
                .width(100%)
                .height(priv.intrinsicContentSize.height)
                .justifyContent(.center)
                .alignItems(.center)
                .define
            { (flex) in
                flex.addItem(terms)
                flex.addItem(priv)
            }
        }
        
        addSubview(container)
        isUserInteractionEnabled = true
        container.isUserInteractionEnabled = true
    }
    
    required init?(coder: NSCoder) {
        super.init(frame: .zero)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        container.pin.all()
        container.flex.layout()
    }
}

class LoginVC: UIViewController {
    let container = IQPreviousNextView()
    let extender = UIScrollView()
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        IQKeyboardManager.shared.previousNextDisplayMode = .alwaysShow
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        IQKeyboardManager.shared.previousNextDisplayMode = .alwaysHide
    }
    
    @ClearableProperty(defaultValue: nil, clearTime: 5)
    private var message: Message?
    private var errorMessage: String? {
        get {
            message?.text
        }
        set {
            if let newValue = newValue {
                message = Message(newValue, .error)
            }
        }
    }
    private var errorLabel: UILabel?
    private var titleLabel: UILabel?
    private var clientLabel: UILabel?
    private let termButton = TermsButton()
    private let passChecker = Checklist()
    private var fieldContainer: Flex?
    private var fieldMap: [FieldNum: Flex] = [:]
    private var storedMessages: [FormState: String] = [:]
    private var loginButton: FlatButton!
    private var signupButton: FlatButton!
    private var forgotButton: FlatButton!
    
    private struct Message: Equatable {
        init(_ text: String,_ type: LoginVC.Message.MessageType) {
            self.type = type
            self.text = text
        }
        
        enum MessageType {
            case ok, error
            var color: UIColor {
                .ok == self ? AppColors.green : AppColors.red
            }
        }
        let type: MessageType
        let text: String
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        _message.display = { [weak self] in
            self?.showMessage()
        }
        
        view.schemedBG(.highlightColorA)
        
        view.addSubview(container)
        extender.showsVerticalScrollIndicator = false
        extender.showsHorizontalScrollIndicator = false
        extender.layer.masksToBounds = false
        defineLoginFlex()
        
        termButton.termTapped = { [weak self] in
            if let self = self {
                LoginVC.showTerms(atVC: self)
            }
        }
        termButton.privTapped = { [weak self] in
            if let self = self {
                LoginVC.showTerms(privacy: true, atVC: self)
            }
        }
    }

    private func showMessage(animate: Bool = true) {
        let process = {
            let toHeight: CGFloat = self.message == nil ? 0 : 20
            if self.errorLabel?.frame.height != toHeight {
                self.errorLabel?.flex.height(toHeight)
                if animate {
                    self.refresh()
                }
            }
            self.errorLabel?.textColor = self.message?.type.color
            self.errorLabel?.text = self.message?.text
        }
        
        if animate {
            DispatchQueue.main.async(execute: process)
        } else {
            process()
        }
    }
    
    private func view(_ view: UIView, shouldShow showF: Bool, height: CGFloat, top: CGFloat, bottom: CGFloat) {
        view.isHidden = !showF
        view.flex
            .height(showF ? height : 0)
            .marginTop(showF ? top : 0)
            .marginBottom(showF ? bottom : 0)
    }
    private var currentFormState: FormState = .login {
        didSet {
            self.storedMessages.removeValue(forKey: oldValue)
            self._message.trigger = false
            if let new = self.storedMessages[currentFormState] {
                self._message.wrappedValue = Message(new, .error)
            } else {
                self._message.wrappedValue = nil
            }
            self.showMessage(animate: false)
            for field in FieldNum.allCases {
                guard let flex = fieldMap[field] else { continue }
                if currentFormState.fieldSet.contains(field) {
                    flex.height(45).marginVertical(15)
                    flex.view?.subviews.first?.isHidden = false
                } else {
                    flex.height(0).marginVertical(0)
                    flex.view?.subviews.first?.isHidden = true
                }
                if let text = flex.view?.subviews.first as? SoftTextField {
                    if field.isSecure {
                        text.field.text = ""
                    }
                    text.field.textContentType = field.contentType(for: currentFormState)
                    text.field.keyboardType = field.keyboardType
                }
            }
            
            let _ = passChecker.process(password: "")
            loginButton.title = currentFormState.leftButtonTitle
            signupButton.title = currentFormState.rightButtonTitle
            forgotButton.title = "Forgotten Password?"
            
            view(forgotButton, shouldShow: currentFormState == .login, height: 30, top: 40, bottom: 20)
            view(termButton, shouldShow: currentFormState == .password, height: 40, top: 25, bottom: 10)
            view(passChecker, shouldShow: currentFormState == .password, height: passChecker.v.frame.maxY, top: 25, bottom: 10)
            
            titleLabel?.text = currentFormState.mainTitle
            titleLabel?.flex.height(currentFormState.mainTitle == nil ? 0 : 25)
            
            loginButton.flex.width(loginButton.wrappedWidth)
            signupButton.flex.width(signupButton.wrappedWidth)
            forgotButton.flex.width(forgotButton.wrappedWidth)
        }
    }
    
    enum FormState: CaseIterable {
        case forgot, login, name, address, password
        
        fileprivate var fieldSet: [FieldNum] {
            switch self {
            case .login: return [.email, .password]
            case .name:
                if APIClient.signupClient {
                    return [.email, .first_name, .last_name, .phone_number, .company]
                } else {
                    return [.email, .first_name, .last_name, .phone_number]
                }
                    
            case .address: return [.addressOne, .addressTwo, .city, .postal_code]
            case .password: return [.password, .confirmPassword]
            case .forgot: return [.email]
            }
        }
        
        fileprivate var leftButtonTitle: String {
            switch self {
            case .address, .name: return "Next"
            case .login: return "Login"
            case .password, .forgot: return "Submit"
            }
        }
        
        fileprivate var rightButtonTitle: String {
            switch self {
            case .login: return "Sign Up"
            default: return "Back"
            }
        }
        
        fileprivate var mainTitle: String? {
            switch self {
            case .address:
                let ad = APIClient.signupClient ? "Business " : ""
                return "Sign up - \(ad)Address"
            case .name: return "Sign up"
            case .password: return "Sign up - Password"
            case .login: return "Login"
            case .forgot: return "Reset Password"
            }
        }
        
        fileprivate static let signupStates: [FormState] = [.name, .address, .password]
        
    }
    
    fileprivate enum FieldNum: String, CaseIterable {
        case email = "Email"
        case first_name = "First Name"
        case last_name = "Last Name"
        case phone_number = "Phone Number"
        case company = "Company"
        case addressOne = "Address"
        case addressTwo = "Address 2"
        case city = "City"
        case postal_code = "Postcode"
        case password = "Password"
        case confirmPassword = "Confirm Password"
        
        var isSecure: Bool {
            self == .password || self == .confirmPassword
        }
        var isNotRequired: Bool {
            self == .addressTwo || self == .company
        }
        var display: String {
            "\(rawValue)\(isNotRequired ? "" : "*")"
        }
        
        func contentType(for state: FormState) -> UITextContentType {
            switch self {
            case .email: return .username
            case .password: return state == .login ? .password : .newPassword
            case .confirmPassword: return .newPassword
            case .first_name: return .givenName
            case .last_name: return .familyName
            case .phone_number: return .telephoneNumber
            case .company: return .organizationName
            case .addressOne: return .streetAddressLine1
            case .addressTwo: return .streetAddressLine2
            case .city: return .addressCity
            case .postal_code: return .postalCode
            }
        }
        
        var keyboardType: UIKeyboardType {
            switch self {
            case .email: return .emailAddress
            case .phone_number: return .numberPad
            default: return .default
            }
        }
    }
    
    private func refresh() {
        fieldContainer?.layout(mode: .adjustHeight)
        UIView.animate(withDuration: 0.4) {
            self.viewDidLayoutSubviews()
        }
    }
    
    private func value(for field: FieldNum) -> String? {
        (fieldMap[field]?.view?.subviews.first as? SoftTextField)?.field.text
    }
    
    private func field(for field: FieldNum) -> UITextField? {
        (fieldMap[field]?.view?.subviews.first as? SoftTextField)?.field
    }
    
    private func processAction(isLeft: Bool) -> Future<Void>? {
        let states = FormState.allCases
        let i = states.firstIndex(of: currentFormState)!
        if .login == currentFormState || .forgot == currentFormState {
            if isLeft {
                return currentFormState == .forgot ? self.resetRequest() : self.login()
            }
            self.storedMessages = [:]
            currentFormState = states[i + 1]
        } else if !isLeft {
            currentFormState = states[i - 1]
        } else if i == states.count - 1 {
            return self.signUp()
        } else {
            currentFormState = states[i + 1]
        }
        
        refresh()
        return nil
    }
    
    private func defineLoginFlex() {
        loginButton = FlexButton()
        loginButton.preset = true
        loginButton.action = { [unowned self] _ in
            self.processAction(isLeft: true)
        }
        loginButton.accessibilityIdentifier = "LoginButton"
        
        signupButton = FlexButton()
        signupButton.preset = true
        signupButton.setTitleTextColor(key: .highlightColorB)
        signupButton.action = { [unowned self] _ in
            self.processAction(isLeft: false)
        }
        
        forgotButton = FlatButton()
        forgotButton.fontSize = 12
        forgotButton.tapped = { [unowned self] _ in
            self.currentFormState = .forgot
            self.refresh()
        }
        
        let v = UIView()
        v.schemedBG()
        v.layer.cornerRadius = 20
        v.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        v.layer.masksToBounds = true
        
        container.flex.alignItems(.center).define { (flex) in
            // logo
            if let img = UIImage(named: "logo")?.withTintColor(ColorScheme.normal.lightShadow) {
                let logoView = UIImageView()
                logoView.image =  img
                logoView.contentMode = .scaleAspectFill
                
                ColorConfigurator.shared.register(interestFor: .normal, andProperty: .lightShadow) {
                    [weak logoView] color in
                    guard let logoView = logoView else { return false }
                    logoView.image = logoView.image?.withTintColor(color)
                    return true
                }
                clientLabel = UILabel(left: 50, weight: .medium, wantsMonster: true)
                clientLabel?.schemedTextColor(.lightShadow)
                flex.addItem()
                    .marginHorizontal(5%)
                    .direction(.row)
                    .alignItems(.center)
                    .justifyContent(.center)
                    .width(100%)
                    .height(20%)
                    .marginVertical(20)
                    .grow(0).shrink(0)
                    .define { (flex) in
                        flex.addItem(logoView).height(80).aspectRatio(of: logoView)
                        flex.addItem(clientLabel!)
                }
            }
            
            flex.addItem(v).grow(1).alignSelf(.stretch).alignItems(.center).padding(10).define { flex in
                flex.addItem(extender).width(90%).alignItems(.center).grow(1).define { flex in
                    errorLabel = UILabel(central: 15, weight: .bold, color: AppColors.red)
                    titleLabel = UILabel(central: 17, weight: .heavy); titleLabel?.schemedTextColor(.highlightColorB)
                    
                    flex.addItem(errorLabel!).width(100%).height(0).marginVertical(10)
                    flex.addItem(titleLabel!).width(100%).height(0).marginVertical(10)
                
                    fieldContainer = flex.addItem().width(100%).define { (flex) in
                        for config in FieldNum.allCases {
                            let field = SoftTextField(title: config.display)
                            if config == .password {
                                field.onChangeCB = { [weak self] in
                                    let _ = self?.passChecker.process(password: $0)
                                }
                            }
                            field.field.autocapitalizationType = .none
                            field.field.isSecureTextEntry = config.isSecure
                            fieldMap[config] = flex.addItem().height(45).width(100%).marginVertical(15).define({ (flex) in
                                flex.addItem(field).height(45).width(100%)
                            })
                        }
                    }
                    flex.addItem(passChecker).width(100%)
                    flex.addItem(termButton).width(100%)
                    flex.addItem(UIView())
                        .direction(.row)
                        .justifyContent(.spaceAround)
                        .width(100%)
                        .height(40)
                        .alignItems(.stretch)
                        .marginTop(35)
                        .define({ (flex) in
                        flex.addItem(signupButton).width(signupButton.wrappedWidth)
                        flex.addItem(loginButton).width(loginButton.wrappedWidth)
                    })
                    flex.addItem(forgotButton)
                }
            }
        }
        currentFormState = Networker.shared.apiKey == nil ? .login : .name
    }
    private func resetRequest() -> Future<Void>? {
        guard let email = value(for: .email), email != ""
        else { errorMessage = "Please enter email"; return nil; }
        let comp = Networker.shared.resetPasswordRequest(email: email)
        comp.do { (_) in
            self.message = Message("Sent Password Reset Email", .ok)
            DispatchQueue.main.async {
                self.currentFormState = .login
            }
        }.catch { _ in
            self.errorMessage = "Failed to Send Password Reset Email"
        }
        return comp.map { _ in () }
    }
    
    private func login() -> Future<Void>? {
        guard let email = value(for: .email), email != ""
        else { errorMessage = "Please enter email"; return nil; }
        
        guard let passField = field(for: .password), let password = value(for: .password), password != ""
        else { errorMessage = "Please enter password"; return nil; }
        
        let request = LoginRequest(email: email, password: password)
        let comp = Networker.shared.authorize(user: request)
        comp.do { () in
            LoginCoordinater.onLogin(self)
        }.catch { (error) in
            if  case NetworkReqError.Authorization = error {
                self.errorMessage = "Wrong email/password"
                DispatchQueue.main.async {
                    passField.text = ""
                }
            } else {
                self.errorMessage = "Login Error. Try Again later."
            }
        }.always {
            DispatchQueue.main.async {
                self.view.isUserInteractionEnabled = true
            }
        }
        
        self.view.isUserInteractionEnabled = false
        return comp
    }
    
    private func processStoredMessages() {
        DispatchQueue.main.async {
            for state in FormState.allCases {
                if self.storedMessages[state] != nil {
                    if self.currentFormState != state {
                        self.currentFormState = state
                    } else {
                        self._message.trigger = false
                        self._message.wrappedValue = Message(self.storedMessages[state]!, .error)
                        self.showMessage(animate: false)
                    }
                    self.refresh()
                    break
                }
            }
        }
    }
    
    private func signUp() -> Future<Void>? {
        storedMessages = [:]
        defer { processStoredMessages() }
        
        var vals = [FieldNum: String]()
        FieldNum.allCases.forEach { (field) in
            if let value = value(for: field), value != "" {
                vals[field] = value
            }
        }
        
        let gotSet = Set(vals.keys)
        for state in FormState.signupStates {
            let fs = Set(state.fieldSet.filter { !$0.isNotRequired })
            if !fs.isSubset(of: gotSet) {
                storedMessages[state] = "Please fill in required fields (*)."
            }
        }
        if storedMessages.count > 0 { return nil }

        guard vals[.password]! == vals[.confirmPassword]! else {
            storedMessages[.password] = "Passwords confirmation does not match"
            return nil
        }
        
        guard passChecker.process(password: vals[.password]!) else {
            storedMessages[.password] = "Password is too simple."
            return nil
        }
        
        var address = vals[.addressOne]!
        if let two = vals[.addressTwo] { address = "\(address), \(two)"}
        
        let request = CustomerModel(
            email: vals[.email]!,
            password: vals[.password]!,
            primary_address: AddressNetModel(
                address: address,
                city: vals[.city]!,
                postal_code: vals[.postal_code]!
            ),
            first_name: vals[.first_name]!,
            last_name: vals[.last_name]!,
            phone_number: vals[.phone_number]!,
            company: vals[.company]
        )
        let comp = Networker.shared.create(customer: request)
        comp.flatMap { (_) -> Future<Void>in
            let loginRequest = LoginRequest(
                email: vals[.email]!,
                password: vals[.password]!
            )
            return Networker.shared.authorize(user: loginRequest)
        }.do { (_) in
            LoginCoordinater.onLogin(self)
        }.catch { (error) in
            if  case NetworkReqError.Detail(let data) = error,
                let resp = String(data: data, encoding: .utf8) {
                if resp.contains("primary_address") {
                    self.storedMessages[.address] = "Please enter a valid address"
                }
                
                if resp.contains("email") {
                    self.storedMessages[.name] = "That email is not available"
                }
                
                if resp.contains("phone_number") {
                    if let prev = self.storedMessages[.name] {
                        self.storedMessages[.name] = "\(prev), and please enter a valid phone number."
                    } else {
                        self.storedMessages[.name] = "Please enter a valid phone number"
                    }
                }
                
                
                if resp.contains("password") {
                    self.storedMessages[.password] = "Make sure password is at least 8 characters"
                }
                self.processStoredMessages()
            } else if case NetworkReqError.Authorization = error {
                self.storedMessages[.login] = "Your Stylist Signup Link has expired."
                Networker.shared.apiKey = nil
                self.processStoredMessages()
            } else {
                self.errorMessage = "Signup Error. Try Again later."
            }
            DispatchQueue.main.async {
                self.field(for: .password)?.text = ""
                self.field(for: .confirmPassword)?.text = ""
            }
        }.always {
            DispatchQueue.main.async {
                self.view.isUserInteractionEnabled = true
            }
        }
        
        self.view.isUserInteractionEnabled = false
        return comp.map { _ in () }
    }
    
    static func showTerms(privacy: Bool = false, overrideCustomer: Bool = false, atVC: UIViewController) {
        let color: ColorScheme = ColorScheme.grey
        let shouldShift = !overrideCustomer && APIClient.isClient
        let params = TermsRequest(
            type: privacy ? .privacy : (shouldShift ? .client : .customer),
            bg_color: color.bg.hexString,
            text_color: color.primaryTextColor.hexString,
            bold_color: color.highlightColorB.hexString,
            link_color: color.highlightColorA.hexString
        )
        atVC.present(
            APWebView(request: Networker.shared.path("terms/", params), showsNavigation: false),
            animated: true
        )
    }
    func move(to state: FormState) {
        currentFormState = state
        refresh()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        if let clientLabel = clientLabel {
            clientLabel.text = APIClient.signupClient ? "stylist" : ""
            clientLabel.flex.size(clientLabel.intrinsicContentSize)
            clientLabel.flex.marginLeft(APIClient.signupClient ? 35 : 0)
        }
        
        container.pin.top(view.pin.safeArea.top).bottom(-view.pin.safeArea.bottom).horizontally()
        container.flex.layout()
        extender.contentSize.height = max(extender.bounds.height, (extender.subviews.last?.frame.maxY ?? 0) + 40)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return (ColorScheme.normal.bg.isLight() ?? true) ? .lightContent : .darkContent
    }
}

//
//  Onboarder.swift
//  Groomly
//
//  Created by Ravi on 23/08/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import Instructions


class Onboarder: CoachMarksControllerDataSource, CoachMarksControllerDelegate {
    
    static let check = Onboarder()
    
    weak var questionVC: OnboardingExperience?
    var showingType: ViewType?
    var controller: CoachMarksController
    
    init() {
        controller = CoachMarksController()
        controller.dataSource = self
        controller.delegate = self
    }
    
    func coachMarksController(_ coachMarksController: CoachMarksController, coachMarkViewsAt index: Int, madeFrom coachMark: CoachMark) -> (bodyView: (UIView & CoachMarkBodyView), arrowView: (UIView & CoachMarkArrowView)?) {
        let coachViews = controller.helper.makeDefaultCoachViews(
            withArrow: true,
            arrowOrientation: coachMark.arrowOrientation
        )
        questionVC?.willTransition(to: index)
        let message = questionVC?.messages[index].0
        coachViews.bodyView.hintLabel.text = message
        coachViews.bodyView.nextLabel.text = "Ok"
        return (bodyView: coachViews.bodyView, arrowView: coachViews.arrowView)
    }
    
    func coachMarksController(_ coachMarksController: CoachMarksController, willShow coachMark: inout CoachMark, beforeChanging change: ConfigurationChange, at index: Int) {
        let orientation = questionVC?.messages[index].2 ?? .bottom
        coachMark.arrowOrientation = orientation
    }
    
    func coachMarksController(_ coachMarksController: CoachMarksController, coachMarkAt index: Int) -> CoachMark {
        guard let questionVC = questionVC else { return CoachMark() }
        let view = questionVC.messages[index].1
        let mark = controller.helper.makeCoachMark(for: view)
        return mark
    }
    
    func numberOfCoachMarks(for coachMarksController: CoachMarksController) -> Int {
        return questionVC == nil ? 0 : questionVC!.messages.count
    }
    
    func showOnboarding(for type: ViewType, atVC: OnboardingExperience) {
        #if DEBUG
        if AppDelegate.isUITestingEnabled {
            return
        }
        #endif
        guard !UserDefaults.standard.bool(forKey: type.rawValue), APIClient.isClient else { return }
        showingType = type
        questionVC = atVC
        questionVC?.setup()
        controller.overlay.backgroundColor = ColorScheme.normal.bg.withAlphaComponent(0.4)
        controller.stop(immediately: true)
        controller.overlay.isUserInteractionEnabled = true
        controller.start(in: .window(over: atVC))
    }
    
    func coachMarksController(_ coachMarksController: CoachMarksController, didEndShowingBySkipping skipped: Bool) {
        #if DEBUG
        if AppDelegate.isUITestingEnabled {
            return
        }
        #endif
        guard let type = showingType else { return }
        UserDefaults.standard.set(true, forKey: type.rawValue)
    }
}

protocol OnboardingExperience: UIViewController {
    func setup()
    var messages: [(String, UIView, CoachMarkArrowOrientation)] { get }
    func willTransition(to index: Int)
}

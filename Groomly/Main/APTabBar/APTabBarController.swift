//
//  APTabBarController.swift
//  Groomly
//
//  Created by Ravi on 18/07/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import PinLayout
import FlexLayout

class APTabController: UITabBarController {
    var apTabBar = APTabBar()
    
    override var tabBar: UITabBar {
        return apTabBar
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = ColorScheme.alt.highlightColorA
        view.colorScheme = .normal
        
        ColorConfigurator.shared.register(interestFor: .alt, andProperty: .lightShadow) { [weak self] (color) -> Bool in
            guard let self = self else { return false }
            self.selectedColor = color
            self.normalColor = color.withAlphaComponent(0.5)
            return true
        }
    }
    
    var selectedColor = ColorScheme.alt.lightShadow
    var normalColor = ColorScheme.alt.lightShadow.withAlphaComponent(0.5)


    func setTabBar(items: [APTabBarItem]) {
        guard items.count > 0 else { return }
        apTabBar.set(items)
        apTabBar.tintColor = normalColor
        apTabBar.tab_items.first?.color = selectedColor
        view.addSchemedView(apTabBar)
        for i in 0 ..< items.count {
            let item = items[i]
            item.tag = i
            item.addTarget(self, action: #selector(switchTab), for: .touchUpInside)
            ColorConfigurator.shared.register(interestFor: .alt, andProperty: .lightShadow) { [weak item, weak self] color -> Bool in
                guard let self = self, let item = item else { return false}
                if item.tag == self.selectedIndex {
                    item.color = color
                } else {
                    item.color = color.withAlphaComponent(0.5)
                }
                return true
            }
        }
    }

    @objc func switchTab(button: UIButton) {
        switchTo(tab: button.tag)
    }
    
    func switchTo(tab: Int) {
        apTabBar.tab_items[selectedIndex].color = normalColor
        apTabBar.tab_items[tab].color = selectedColor
        selectedIndex = tab
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        apTabBar.pin.horizontally().bottom().height(APTabBar.tabHeight + view.pin.safeArea.bottom)
    }
}


//
//  APTabBarItem.swift
//  Groomly
//
//  Created by Ravi on 18/07/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import PinLayout
import FlexLayout

class APTabBarItem: UIButton {
    private let iconImageView = UIImageView()
    
    var color: UIColor = AppColors.grey {
         didSet {
             iconImageView.tintColor = color
         }
     }
    
    override var intrinsicContentSize: CGSize {
        return iconImageView.image?.size ?? .zero
    }

    convenience init(icon: UIImage?, title: String) {
        self.init()
        iconImageView.image = icon
        addSchemedView(iconImageView)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        iconImageView.pin.height(22).aspectRatio().center()
    }
}


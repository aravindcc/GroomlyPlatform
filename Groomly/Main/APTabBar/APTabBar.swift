//
//  APTabBar.swift
//  Groomly
//
//  Created by Ravi on 18/07/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import PinLayout
import FlexLayout

class APTabBar: UITabBar {
    var tab_items = [APTabBarItem]()
    var flexed = false
    var containerView = UIView()
    
    static let tabHeight: CGFloat = 58.0
    static var extraHeight: CGFloat {
        tabHeight - 49.0
    }
    
    
    override var tintColor: UIColor! {
        didSet {
            for item in tab_items {
                item.color = tintColor
            }
        }
    }
    
    func set(_ items: [APTabBarItem]) {
        guard tab_items.count == 0 else { return }
        shadowColor = nil
        backgroundColor = ColorScheme.alt.highlightColorA
        barTintColor = ColorScheme.alt.highlightColorA
        ColorConfigurator.shared.register(interestFor: .alt, andProperty: .highlightColorA) {
            [weak self] color in
            guard let self = self else { return false }
            self.barTintColor = color
            self.backgroundColor = color
            self.superview?.backgroundColor = color
            self.layoutSubviews()
            return true
        }
        tab_items = items
        flex.addItem(containerView)
            .direction(.row)
            .justifyContent(.spaceAround)
            .alignItems(.center)
            .define { (flex) in
            for tab in tab_items {
                flex.addItem(tab).size(APTabBar.tabHeight)
            }
        }
    }


    override func layoutSubviews() {
        super.layoutSubviews()
        containerView.pin.horizontally().top().bottom(pin.safeArea)
        containerView.flex.layout()
    }
}

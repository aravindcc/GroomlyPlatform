//
//  SettingsVC.swift
//  Groomly
//
//  Created by Ravi on 23/08/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import PinLayout
import FlexLayout


class SettingsVC: UIViewController {
    let contentWrap = UIView()
    let container = UIView()
    let switchView = UISwitch()
    let switchLbl = UILabel()
    
    private func showMode(isLight: Bool) {
        ColorScheme.normal = (isLight ? ColorScheme.grey : .dark).copy()
        ColorScheme.alt = (isLight ? ColorScheme.white : .black).copy()
        ColorConfigurator.shared.refresh()
        switchView.isOn = !isLight
    }
    
    @objc private func handleToggle() {
        showMode(isLight: !switchView.isOn)
    }
    
    
    private func addLine(to flex: Flex, text: String, extraView: UIView?) {
        flex.addItem()
            .alignItems(.center)
            .direction(.row)
            .height(45)
            .marginHorizontal(5%)
            .marginBottom(15)
            .define { (flex) in
            let lbl = UILabel(left: 18, weight: .bold)
            lbl.schemedTextColor(.primaryTextColor)
            lbl.text = text
            lbl.singleCentralLine()
            flex.addItem(lbl).height(100%).grow(1).shrink(1)
            if let extraView = extraView {
                flex.addItem(extraView)
                if let but = extraView as? FlatButton {
                    extraView.flex.width(but.wrappedWidth).height(30)
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.schemedBG()
        
        contentWrap.schemedBG()
        contentWrap.layer.cornerRadius = 20
        contentWrap.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        view.addSchemedView(contentWrap)
        contentWrap.addSchemedView(container)
        
        showMode(isLight: ColorScheme.grey.bg == ColorScheme.normal.bg)
        switchView.addTarget(self, action: #selector(handleToggle), for: .valueChanged)
        container.flex.alignItems(.stretch).marginTop(15).define { (flex) in
            addLine(to: flex, text: "Dark Mode", extraView: switchView)
            
            let custTermbut = FlatButton()
            custTermbut.title = "view"
            custTermbut.preset = true
            custTermbut.tapped = { [weak self] _ in
                if let self = self {
                    LoginVC.showTerms(
                        overrideCustomer: true,
                        atVC: self
                    )
                }
            }
            addLine(to: flex, text: APIClient.isClient ? "Customer T&Cs" : "T&Cs", extraView: custTermbut)
            
            if APIClient.isClient {
                let termBut = FlatButton()
                termBut.title = "view"
                termBut.preset = true
                termBut.tapped = { [weak self] _ in
                    if let self = self {
                        LoginVC.showTerms(atVC: self)
                    }
                }
                addLine(to: flex, text: "T&Cs", extraView: termBut)
            }
            
            
            let but = FlexButton()
            but.title = "Logout"
            but.preset = true
            but.tapped = { [weak self] _ in
                if let self = self {
                    LoginCoordinater.clearSession(self)
                }
            }
            flex.addItem()
                .height(30)
                .marginBottom(15)
                .direction(.row)
                .alignItems(.stretch)
                .justifyContent(.center)
                .define({ (flex) in
                flex.addItem(but).width(but.wrappedWidth)
            })
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        contentWrap.pin.top(safeAreaInsets).horizontally().bottom(view.pin.safeArea)
        container.pin.all(20)
        container.flex.layout()
    }
}

//
//  SceneDelegate.swift
//  Learning
//
//  Created by clar3 on 16/03/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
    
    private func process(_ key: String) {
        Networker.shared.apiKey = key
        guard let vc = window?.rootViewController else { return }
        if APIClient.isLoggedIn{
            Message.showConfirmation(
                title: "Logout",
                description: "Please confirm logout to go to Stylist Sign Up page."
            ) {
                if $0 {
                    DispatchQueue.main.async {
                        LoginCoordinater.clearSession(vc)
                    }
                }
            }
        } else if let vc = vc as? LoginVC {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                vc.move(to: .name)
            }
        }
    }
    
    func scene(_ scene: UIScene, continue userActivity: NSUserActivity) {
        // Get URL components from the incoming user activity
        guard let incomingURL = userActivity.webpageURL,
            let components = NSURLComponents(url: incomingURL, resolvingAgainstBaseURL: true)
        else {
            return
        }

        // Check for specific URL components that you need
        guard
            let path = components.path, path.contains("signup"),
            let key = components.queryItems?.first(where: { $0.name == "key" })?.value
        else {
            return
        }
        process(key)
    }

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let scene = scene as? UIWindowScene else { return }
        
        if let activity = connectionOptions.userActivities.first {
            self.scene(scene, continue: activity)
        }
        
        #if DEBUG
        if AppDelegate.isUITestingEnabled, let key = ProcessInfo.processInfo.environment["apiKey"]  {
            Networker.shared.apiKey = key
        }
        #endif
        let window = UIWindow(windowScene: scene)
        window.rootViewController = LoginCoordinater.start()
        self.window = window
        window.schemedBG()
        window.makeKeyAndVisible()
    }

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }


}


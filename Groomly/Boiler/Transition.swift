//
//  Transition.swift
//  Groomly
//
//  Created by Ravi on 31/07/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import Foundation

enum TransitionResult {
    case success
    case failure
}

typealias ExecutionBlock = (() -> Void)
typealias TransitionBlock = ((TransitionResult) -> Void)

struct Transition<State, Event> {
    
    let event: Event
    let source: State
    let destination: State
    let preBlock: ExecutionBlock?
    let postBlock: ExecutionBlock?
    
    init(with event: Event,
                from: State,
                to: State,
                preBlock: ExecutionBlock? = nil,
                postBlock: ExecutionBlock? = nil) {
        self.event = event
        self.source = from
        self.destination = to
        self.preBlock = preBlock
        self.postBlock = postBlock
    }
    
    func executePreBlock() {
        preBlock?()
    }
    
    func executePostBlock() {
        postBlock?()
    }
}

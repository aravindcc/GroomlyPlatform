//
//  Utilities.swift
//  Learning
//
//  Created by clar3 on 16/03/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import Foundation
import UIKit
import Hero
import IGListKit
import Async

typealias ButtonCompletion = (FlatButton) -> Void
typealias ButtonAction = (FlatButton) -> Future<Void>?
typealias Completion = () -> Void

class APSubController: UIViewController {}


extension Array {
    mutating func popFirst(_ k: Int) -> Array<Element> {
        guard k < count else { return [] }
        let first = self.prefix(upTo: k)
        self.removeFirst(k)
        return Array(first)
    }
    
    mutating func pop(from k: Int) -> Array<Element> {
        guard k < count else { return [] }
        let last = self.suffix(from: k)
        self.removeLast(count - k)
        return Array(last)
    }
}

infix operator %%
func %%(lhs: Int, rhs: Int) -> Int {
    guard rhs != 0 else { return 0 }
    precondition(rhs > 0, "modulus must be positive")
    let r = lhs % rhs
    return r >= 0 ? r : r + rhs
}

extension CGPoint {
    static func -(lhs: CGPoint, rhs: CGPoint) -> CGPoint {
        return CGPoint(x: lhs.x - rhs.x, y: lhs.y - rhs.y)
    }
    
    static func +(lhs: CGPoint, rhs: CGPoint) -> CGPoint {
        return CGPoint(x: lhs.x + rhs.x, y: lhs.y + rhs.y)
    }
    
    static func +=( lhs: inout CGPoint, rhs: CGPoint) {
        lhs = lhs + rhs
    }
}

extension Comparable {
    mutating func clamp(from: Self, to: Self){
        self = max(from, self)
        self = min(to, self)
    }
    func clamped(from: Self, to: Self) -> Self {
        return max(from, min(to, self))
    }
}

extension UIView {
    static func image(with view: UIView) -> UIImage? {
        func sortSubviews(_ view: UIView) -> Void {
            let orderedLayers = view.subviews.sorted(by: {
                $0.layer.zPosition < $1.layer.zPosition
            })
            for layer in orderedLayers {
                view.bringSubviewToFront(layer)
                sortSubviews(layer)
            }
        }
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, 0.0)
        defer {
            UIGraphicsEndImageContext()
        }
        if let ct = UIGraphicsGetCurrentContext() {
            ct.beginTransparencyLayer(auxiliaryInfo: nil)
            sortSubviews(view)
            view.layer.render(in: ct)
            ct.endTransparencyLayer()
        }
        return UIGraphicsGetImageFromCurrentImageContext()
    }
    
    func clear() {
        for s in subviews {
            s.removeFromSuperview()
        }
    }
}

extension CAGradientLayer {

    enum Point {
        case topLeft
        case centerLeft
        case bottomLeft
        case topCenter
        case center
        case bottomCenter
        case topRight
        case centerRight
        case bottomRight

        var point: CGPoint {
            switch self {
            case .topLeft:
                return CGPoint(x: 0, y: 0)
            case .centerLeft:
                return CGPoint(x: 0, y: 0.5)
            case .bottomLeft:
                return CGPoint(x: 0, y: 1.0)
            case .topCenter:
                return CGPoint(x: 0.5, y: 0)
            case .center:
                return CGPoint(x: 0.5, y: 0.5)
            case .bottomCenter:
                return CGPoint(x: 0.5, y: 1.0)
            case .topRight:
                return CGPoint(x: 1.0, y: 0.0)
            case .centerRight:
                return CGPoint(x: 1.0, y: 0.5)
            case .bottomRight:
                return CGPoint(x: 1.0, y: 1.0)
            }
        }
    }

    convenience init(start: Point, end: Point, colors: [CGColor], type: CAGradientLayerType) {
        self.init()
        self.startPoint = start.point
        self.endPoint = end.point
        self.colors = colors
        self.locations = (0..<colors.count).map(NSNumber.init)
        self.type = type
    }
}

extension UILabel {
    convenience init(central size: CGFloat, weight: UIFont.Weight, color: UIColor? = nil, text input : String? = nil, wantsMonster: Bool = true) {
        self.init()
        font =  wantsMonster ? UIFont.monsterFont(size: size, weight: weight) : UIFont.systemFont(ofSize: size, weight: weight)
        textAlignment = .center
        if let color = color {
            textColor = color
        }
        numberOfLines = 2
        adjustsFontSizeToFitWidth = true
        backgroundColor = .clear
        text = input
    }
    convenience init(left size: CGFloat, weight: UIFont.Weight, color: UIColor? = nil, text input : String? = nil, wantsMonster: Bool = false) {
        self.init(central: size, weight: weight, color: color, text: input, wantsMonster: wantsMonster)
        textAlignment = .left
    }
    convenience init(right size: CGFloat, weight: UIFont.Weight, color: UIColor? = nil, text input : String? = nil, wantsMonster: Bool = false) {
        self.init(central: size, weight: weight, color: color, text: input, wantsMonster: wantsMonster)
        textAlignment = .right
    }
}

extension UIFont {
    static func monsterFont(size: CGFloat, weight: UIFont.Weight) -> UIFont {
        switch weight {
        case .black:
            return UIFont(name: "Montserrat-Black", size: size)!
        case .bold:
            return UIFont(name: "Montserrat-Bold", size: size)!
        case .heavy:
            return UIFont(name: "Montserrat-ExtraBold", size: size)!
        case .light:
            return UIFont(name: "Montserrat-Thin", size: size)!
        case .medium:
            return UIFont(name: "Montserrat-Medium", size: size)!
        case .semibold:
            return UIFont(name: "Montserrat-SemiBold", size: size)!
        case .thin:
            return UIFont(name: "Montserrat-Thin", size: size)!
        case .ultraLight:
            return UIFont(name: "Montserrat-ExtraLight", size: size)!
        default:
            return UIFont(name: "Montserrat-Regular", size: size)!
        }
    }
}

extension UIView {
   func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}

class FullScreenVC: UIViewController {
    var vcWillAppear = false
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        modalPresentationStyle = .fullScreen
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return .slide
    }

    override var prefersStatusBarHidden: Bool {
        return vcWillAppear
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        vcWillAppear = true
        UIView.animate(withDuration: 0.5) {
            self.setNeedsStatusBarAppearanceUpdate()
        }
    }
}

extension UIViewController {
    
    func present(hero vc: UIViewController, completion: Completion? = nil) {
        vc.hero.isEnabled = true
        Hero.shared.isUserInteractionEnabled = true
        present(vc, animated: true, completion: completion)
    }
    
    func add(_ child: UIViewController, anchor: UIView? = nil, add: Bool = true) {
        addChild(child)
        if add {
            (anchor ?? view).addSchemedView(child.view)
        }
        child.didMove(toParent: self)
    }

    func remove() {
        // Just to be safe, we check that this view controller
        // is actually added to a parent before removing it.
        guard parent != nil else {
            return
        }

        willMove(toParent: nil)
        view.removeFromSuperview()
        removeFromParent()
    }
}


extension UIControl {
    
    /// Typealias for UIControl closure.
    public typealias UIControlTargetClosure = (UIControl) -> ()
    
    private class UIControlClosureWrapper: NSObject {
        let closure: UIControlTargetClosure
        init(_ closure: @escaping UIControlTargetClosure) {
            self.closure = closure
        }
    }
    
    private struct AssociatedKeys {
        static var targetClosure = "targetClosure"
    }
    
    private var targetClosure: UIControlTargetClosure? {
        get {
            guard let closureWrapper = objc_getAssociatedObject(self, &AssociatedKeys.targetClosure) as? UIControlClosureWrapper else { return nil }
            return closureWrapper.closure
        }
        set(newValue) {
            guard let newValue = newValue else { return }
            objc_setAssociatedObject(self, &AssociatedKeys.targetClosure, UIControlClosureWrapper(newValue), objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    @objc func closureAction() {
        guard let targetClosure = targetClosure else { return }
        targetClosure(self)
    }
    
    public func addAction(for event: UIControl.Event, closure: @escaping UIControlTargetClosure) {
        targetClosure = closure
        addTarget(self, action: #selector(UIControl.closureAction), for: event)
    }
    
}

//
//  RandomPhoto.swift
//  Groomly
//
//  Created by Ravi on 23/08/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import Foundation
import IGListKit

class RandomPhoto: DisplayImageModel, Codable, ListDiffable {
    init(id: String, author: String, width: Float, height: Float, url: String, download_url: String) {
        self.id = id
        self.author = author
        self.width = width
        self.height = height
        self.url = url
        self.download_url = download_url
    }
    
    let id: String
    let author: String
    let width: Float
    let height: Float
    let url: String
    let download_url: String
    var description: String {
        return author
    }
    
    var image : URL? {
        return getURL()
    }
    
    func getURL() -> URL? {
        return URL(string: download_url)
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        return id as NSString
    }

    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard let object = object as? RandomPhoto else { return false }
        return self.id == object.id
    }
    
    static func get(limit: Int = 30, completion: @escaping ([RandomPhoto]) -> Void) {
        guard let url = URL(string: "https://picsum.photos/v2/list?page=\((1...5).randomElement() ?? 0)&limit=\(limit)") else { completion([]); return }
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data else { completion([]); return }
            let jsonDecoder = JSONDecoder()
            do {
                let parsedJSON = try jsonDecoder.decode([RandomPhoto].self, from: data)
                completion(parsedJSON)
            }
            catch {
                Log(error)
                completion([])
            }
        }.resume()
    }
    
}


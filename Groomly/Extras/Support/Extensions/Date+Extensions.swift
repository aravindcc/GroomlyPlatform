//
//  Date+Extensions.swift
//  Groomly
//
//  Created by Ravi on 04/08/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import Foundation

extension Date {
    
    var calendar: Calendar {
        return Calendar.autoupdatingCurrent
    }
    
    func setTo(time: Date) -> Date? {
        return calendar.date(bySettingHour: time.hour, minute: time.minute, second: time.second, of: self)
    }
    
    var ordinalString: String {
        let daySuffix: String = {
            let calendar = Calendar.current
            let dayOfMonth = calendar.component(.day, from: self)
            switch dayOfMonth {
            case 1, 21, 31: return "st"
            case 2, 22: return "nd"
            case 3, 23: return "rd"
            default: return "th"
            }
        }()
        let df = DateFormatter()
        df.dateFormat = "d'\(daySuffix)' EEEE,MMMM yyyy"
        return df.string(from: self)
    }
    
    static func today() -> Date {
        return Calendar.autoupdatingCurrent.date(bySettingHour: 0, minute: 0, second: 0, of: Date())!
    }

    static func tomorrow() -> Date {
        return Calendar.autoupdatingCurrent.date(byAdding: .day, value: 1, to: today())!
    }
    
    func next(_ weekday: Weekday, considerToday: Bool = false) -> Date {
        return get(.next,
               weekday,
               considerToday: considerToday)
    }
    
    func isWeekday(_ weekDay: Weekday) -> Bool {
        let dayName = weekDay.rawValue
        let weekdaysName = getWeekDaysInEnglish().map { $0.lowercased() }
        assert(weekdaysName.contains(dayName), "weekday symbol should be in form \(weekdaysName)")
        let searchWeekdayIndex = weekdaysName.firstIndex(of: dayName)! + 1
        let components = calendar.dateComponents([.weekday], from: self)
        return searchWeekdayIndex == components.weekday
    }

    func previous(_ weekday: Weekday, considerToday: Bool = false) -> Date {
        return get(.previous,
               weekday,
               considerToday: considerToday)
    }

    func get(_ direction: SearchDirection,
       _ weekDay: Weekday,
       considerToday consider: Bool = false) -> Date {
        
        let dayName = weekDay.rawValue
        let weekdaysName = getWeekDaysInEnglish().map { $0.lowercased() }
        assert(weekdaysName.contains(dayName), "weekday symbol should be in form \(weekdaysName)")
        let searchWeekdayIndex = weekdaysName.firstIndex(of: dayName)! + 1
        let calendar = Calendar(identifier: .gregorian)

        if consider && calendar.component(.weekday, from: self) == searchWeekdayIndex {
            return self
        }

        var nextDateComponent = calendar.dateComponents([.hour, .minute, .second], from: self)
        nextDateComponent.weekday = searchWeekdayIndex

        let date = calendar.nextDate(after: self,
                                     matching: nextDateComponent,
                                     matchingPolicy: .nextTime,
                                     direction: direction.calendarSearchDirection)

        return date!
    }

    func getWeekDaysInEnglish() -> [String] {
        var calendar = Calendar(identifier: .gregorian)
        calendar.locale = Locale(identifier: "en_US_POSIX")
        return calendar.weekdaySymbols
    }

    enum Weekday: String {
        case monday, tuesday, wednesday, thursday, friday, saturday, sunday
    }

    enum SearchDirection {
        case next
        case previous

        var calendarSearchDirection: Calendar.SearchDirection {
            switch self {
            case .next:
                return .forward
            case .previous:
                return .backward
            }
        }
    }

    static func dates(from fromDate: Date, to toDate: Date) -> [Date] {
        var dates: [Date] = []
        var date = fromDate

        while date <= toDate {
            dates.append(date)
            guard let newDate = Calendar.autoupdatingCurrent.date(byAdding: .day, value: 1, to: date) else { break }
            date = newDate
        }
        return dates
    }
    
    func startOfMonth() -> Date {
        return calendar.date(from: calendar.dateComponents([.year, .month], from: calendar.startOfDay(for: self)))!
    }

    func endOfMonth() -> Date {
        return calendar.date(byAdding: DateComponents(month: 1, day: -1), to: self.startOfMonth())!
    }
    
    func sameDay(as comp: Date) -> Bool {
        let day = calendar.component(.day, from: self) == calendar.component(.day, from: comp)
        let month = calendar.component(.month, from: self) == calendar.component(.month, from: comp)
        let year = calendar.component(.year, from: self) == calendar.component(.year, from: comp)
        return day && month && year
    }
    
    static func dateOf(month: String, in year: Int) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        dateFormatter.dateFormat = "LLLLyyyy"
        return dateFormatter.date(from:"\(month)\(year)")
    }
    
    var startOfDay: Date {
        return calendar.startOfDay(for: self)
    }

    var endOfDay: Date {
        var components = DateComponents()
        components.day = 1
        components.second = -1
        return calendar.date(byAdding: components, to: startOfDay)!
    }
    
    /// Date by adding multiples of calendar component.
    ///
    /// - Parameters:
    ///   - component: component type.
    ///   - value: multiples of compnenets to add.
    /// - Returns: original date + multiples of compnenet added.
    func adding(_ component: Calendar.Component, value: Int) -> Date {
        switch component {
        case .second:
            return calendar.date(byAdding: .second, value: value, to: self) ?? self
            
        case .minute:
            return calendar.date(byAdding: .minute, value: value, to: self) ?? self
            
        case .hour:
            return calendar.date(byAdding: .hour, value: value, to: self) ?? self
            
        case .day:
            return calendar.date(byAdding: .day, value: value, to: self) ?? self
            
        case .weekOfYear, .weekOfMonth:
            return calendar.date(byAdding: .day, value: value * 7, to: self) ?? self
            
        case .month:
            return calendar.date(byAdding: .month, value: value, to: self) ?? self
            
        case .year:
            return calendar.date(byAdding: .year, value: value, to: self) ?? self
            
        default:
            return self
        }
    }
    
    
    /// Nearest quarter to date.
    var nearestFifteenMinutes: Date {
        let interval = 15
        let nextDiff = interval - calendar.component(.minute, from: self) % interval
        if nextDiff != interval {
            if nextDiff > 6 {
                return calendar.date(
                    byAdding: .minute,
                    value: nextDiff - interval,
                    to: self
                ) ?? Date()
            }
            return calendar.date(byAdding: .minute, value: nextDiff, to: self) ?? Date()
        }
        return self
    }
}

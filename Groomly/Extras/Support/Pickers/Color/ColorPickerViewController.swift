import UIKit

extension UIAlertController {
    
    /// Add a Color Picker
    ///
    /// - Parameters:
    ///   - color: input color
    ///   - action: for selected color
    
    func addColorPicker(color: UIColor = .black, selection: ColorPickerViewController.Selection?) {
        let selection: ColorPickerViewController.Selection? = selection
        var color: UIColor = color
        
        let buttonSelection = UIAlertAction(title: "Select", style: .default) { action in
            selection?(color)
        }
        buttonSelection.isEnabled = true
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "ColorPicker") as? ColorPickerViewController else { return }
        set(vc: vc)
        
        set(title: color.hexString, font: .systemFont(ofSize: 17), color: color)
        vc.set(color: color) { new in
            color = new
            self.set(title: color.hexString, font: .systemFont(ofSize: 17), color: color)
        }
        addAction(buttonSelection)
    }
}

class ColorPickerViewController: UIViewController {
    
    public typealias Selection = (UIColor) -> Swift.Void
    
    fileprivate var selection: Selection?
    
    @IBOutlet weak var colorView: UIView!
    
    @IBOutlet weak var redSlider: GradientSlider!
    @IBOutlet weak var greenSlider: GradientSlider!
    @IBOutlet weak var blueSlider: GradientSlider!
    
    @IBOutlet weak var mainStackView: UIStackView!
    
    public var color: UIColor {
        return UIColor(red: red, green: green, blue: blue)
    }
    
    public var red: Int = 0
    public var green: Int = 0
    public var blue: Int = 0
    
    fileprivate var preferredHeight: CGFloat = 0
    
    func set(color: UIColor, selection: Selection?) {
        let components = color.rgbComponents
        
        red = components.red
        green = components.green
        blue = components.blue
   
        updateColorView()
        
        self.selection = selection
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Log("preferredHeight = \(preferredHeight)")
        
        redSlider.actionBlock = { [unowned self] slider, newValue in
            CATransaction.begin()
            CATransaction.setValue(true, forKey: kCATransactionDisableActions)
            self.red = Int(newValue * 255)
            self.updateColorView()
            CATransaction.commit()
        }
        
        greenSlider.actionBlock = { [unowned self] slider, newValue in
            CATransaction.begin()
            CATransaction.setValue(true, forKey: kCATransactionDisableActions)
            self.green = Int(newValue * 255)
            self.updateColorView()
            CATransaction.commit()
        }
        
        blueSlider.actionBlock = { [unowned self] slider, newValue in
            CATransaction.begin()
            CATransaction.setValue(true, forKey: kCATransactionDisableActions)
            self.blue = Int(newValue * 255)
            self.updateColorView()
            CATransaction.commit()
        }
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        preferredHeight = mainStackView.frame.maxY
    }
    
    func updateColorView() {
        
        redSlider.minColor = UIColor(red: 0, green: 0, blue: 0)
        redSlider.maxColor = UIColor(red: 255, green: 0, blue: 0)
        greenSlider.minColor = UIColor(red: 0, green: 0, blue: 0)
        greenSlider.maxColor = UIColor(red: 0, green: 255, blue: 0)
        blueSlider.minColor = UIColor(red: 0, green: 0, blue: 0)
        blueSlider.maxColor = UIColor(red: 0, green: 0, blue: 255)
        redSlider.value = CGFloat(red) / 255.0
        greenSlider.value = CGFloat(green) / 255.0
        blueSlider.value = CGFloat(blue) / 255.0
        
        colorView.backgroundColor = color
        selection?(color)
        Log("set color = \(color.hexString)")
    }
}


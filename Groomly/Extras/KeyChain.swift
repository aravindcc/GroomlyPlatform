//
//  KeyChain.swift
//  Groomly
//
//  Created by Ravi on 18/07/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import Foundation
import Security

class KeyChain {

    class func save(key: String, password: String) -> OSStatus {
        if let data = password.data(using: .utf8) {
            return save(key: key, data: data)
        } else {
            return errSecParam
        }
    }
    
    class func load(key: String) -> String? {
        if let data: Data = load(key: key) {
            return String(data: data, encoding: .utf8)
        }
        return nil
    }
    
    class func save(key: String, data: Data) -> OSStatus {
        let query = [
            kSecClass as String       : kSecClassGenericPassword as String,
            kSecAttrAccount as String : key,
            kSecValueData as String   : data ] as [String : Any]

        SecItemDelete(query as CFDictionary)

        return SecItemAdd(query as CFDictionary, nil)
    }

    class func load(key: String) -> Data? {
        let query = [
            kSecClass as String       : kSecClassGenericPassword,
            kSecAttrAccount as String : key,
            kSecReturnData as String  : kCFBooleanTrue!,
            kSecMatchLimit as String  : kSecMatchLimitOne ] as [String : Any]

        var dataTypeRef: AnyObject? = nil

        let status: OSStatus = SecItemCopyMatching(query as CFDictionary, &dataTypeRef)

        if status == noErr {
            return dataTypeRef as! Data?
        } else {
            return nil
        }
    }
    
    class func clear(key: String) {
        let query = [
            kSecClass as String       : kSecClassGenericPassword as String,
            kSecAttrAccount as String : key ] as [String : Any]
        SecItemDelete(query as CFDictionary)
    }
}

//
//  LoadingVC.swift
//  Groomly
//
//  Created by Ravi on 15/08/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit

class LoadingVC: UIViewController {
    private lazy var loadingIndicator: UIActivityIndicatorView = {
        let progressView = UIActivityIndicatorView(style: .medium)
        progressView.color = self.view.colorScheme!.primaryTextColor
        progressView.tag = 88
        progressView.startAnimating()
        self.view.addSubview(progressView)
        progressView.pin.center()
        return progressView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.schemedBG(.highlightColorA)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        loadingIndicator.pin.center()
    }
}

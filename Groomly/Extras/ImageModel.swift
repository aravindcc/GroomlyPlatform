//
//  ImageModel.swift
//  Learning
//
//  Created by clar3 on 20/03/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import Foundation

protocol ImageModel {
    var image: URL? { get }
}

protocol DisplayImageModel: ImageModel {
    var width: Float { get }
    var height: Float { get }
    var description: String { get }
}


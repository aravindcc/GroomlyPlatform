//
//  Talk.swift
//  Groomly
//
//  Created by Ravi on 23/08/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import Foundation

final class Talk {
    static let it = Talk()
    
    let serverError = NotificationDetails(
        title: "Error",
        desc: "Couldn't get data from server.",
        imageName: "exclamationmark.triangle.fill"
    )
    let paramError = NotificationDetails(
        title: "Error",
        desc: "Please fill in the details correctly.",
        imageName: "exclamationmark.triangle.fill"
    )
    let authError = NotificationDetails(
        title: "Unauthorized",
        desc: "Seems like you aren't logged in. Please logout and try again.",
        imageName: "exclamationmark.triangle.fill"
    )
    let shopVCName = "Professionals"
    let serviceVCName = "Services"
    let groupVCName = "Shops"
    let pendingChargeDesc = "A payment will be authorised from your account upto 7 days before the appointment but payment will not be taken until the appointment is completed"
    let authRequiredMessage = "To complete the booking payment authorisation is required."
    let paymentMethodInUse = "Card is being used for another appointment. Would you like to use your currently selected card instead, and delete this one?"
    // Booking actions have a lot strings in them as well...
    
    let formHelp = "Edit base info for your profile here."
    let priceHelp = "Add and edit the services you offer here."
    let portfolioHelp = "Upload images to your porfolio here."
    let timesHelp = "Tell your customers your availability. You can change your availability for a specific day in the calendar."
    let calendarHelp = "This is where you can view and manage your booked appointments."
    let dailyViewHelp = "You can view your day in full, and change your availability for that day here."
    
}

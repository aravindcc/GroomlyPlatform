//
//  SubProcessVC.swift
//  Groomly
//
//  Created by clar3 on 12/05/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import Jelly

extension UIImage {
    func withOrg(pad value: CGFloat) -> UIImage {
        withAlignmentRectInsets(UIEdgeInsets(top: value, left: -value, bottom: value, right: 0))
    }
}

class JellyControl {
    static let shared = JellyControl()
    var jellyAnimator: Animator?
    var size: PresentationSize?

    func toBottom() {
        try? jellyAnimator?.updateVerticalAlignment(alignment: .bottom, duration: .reallyFast)
    }
    
    func centralise() {
        try? jellyAnimator?.updateVerticalAlignment(alignment: .center, duration: .reallyFast)
    }
    
}

class ProcessNVC: MainNVC {
    
    static var jellyLock: Bool = false
    
    weak var jellyAnimator: Animator? {
        get {
            JellyControl.shared.jellyAnimator
        } set {
            JellyControl.shared.jellyAnimator = newValue
        }
    }
    var completion: Completion?
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if !ProcessNVC.jellyLock { jellyAnimator = nil }
    }
    
    deinit {
        completion?()
    }
}

class SubProcessVC: APSubController {
    override func viewDidLoad() {
        super.viewDidLoad()
        //definesPresentationContext = true
    }
    
    private func getSize(for height: CGFloat) -> Size {
        if height == 0 {
            return .halfscreen
        } else if height == .infinity {
            return .fullscreen
        }
        return .custom(value: height)
    }
    
    func show(process rvc: UIViewController, dismissed: Completion? = nil) {
        let vc = ProcessNVC(rootViewController: rvc)
        rvc.view.accessibilityIdentifier = "rvcprocess"
        vc.completion = dismissed
        vc.view.colorScheme = self.view.colorScheme
        vc.navigationBar.backIndicatorImage = UIImage(systemName: "arrow.left")?.withOrg(pad: 10)
        vc.navigationBar.backIndicatorTransitionMaskImage = UIImage(systemName: "arrow.left")?.withOrg(pad: 10)
        
        var marginGuards = UIEdgeInsets(top: 20, left: 20, bottom: 50, right: 20)
        let uiConfiguration = PresentationUIConfiguration(cornerRadius: 10, backgroundStyle: .dimmed(alpha: 0.5), isTapBackgroundToDismissEnabled: true)
        let chosenHeight: Size = getSize(for: vc.preferredContentSize.height)
        let size = PresentationSize(width: .fullscreen, height: chosenHeight)
        if chosenHeight == .fullscreen, let insets = UIApplication.shared.windows.first?.safeAreaInsets {
            marginGuards.top = insets.top + 10
            marginGuards.bottom = insets.bottom + 10
            marginGuards.left = 10
            marginGuards.right = 10
        }
        let alignment = PresentationAlignment(vertical: .bottom, horizontal: .center)
        let interaction = InteractionConfiguration(presentingViewController: self, completionThreshold: 0.5, dragMode: .canvas, mode: .dismiss)
        let presentation = CoverPresentation(directionShow: .bottom, directionDismiss: .bottom, uiConfiguration: uiConfiguration, size: size, alignment: alignment, marginGuards: marginGuards, interactionConfiguration: interaction)
        vc.jellyAnimator = Animator(presentation: presentation)
        vc.jellyAnimator?.prepare(presentedViewController: vc)
        JellyControl.shared.size = size
        present(vc, animated: true, completion: nil)
    }
}

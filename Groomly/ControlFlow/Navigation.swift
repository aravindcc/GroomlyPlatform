//
//  Navigation.swift
//  Learning
//
//  Created by clar3 on 21/03/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import PinLayout
import Kingfisher

extension UIColor {

    // Check if the color is light or dark, as defined by the injected lightness threshold.
    // Some people report that 0.7 is best. I suggest to find out for yourself.
    // A nil value is returned if the lightness couldn't be determined.
    func isLight(threshold: Float = 0.7) -> Bool? {
        let originalCGColor = self.cgColor

        // Now we need to convert it to the RGB colorspace. UIColor.white / UIColor.black are greyscale and not RGB.
        // If you don't do this then you will crash when accessing components index 2 below when evaluating greyscale colors.
        let RGBCGColor = originalCGColor.converted(to: CGColorSpaceCreateDeviceRGB(), intent: .defaultIntent, options: nil)
        guard let components = RGBCGColor?.components else {
            return nil
        }
        guard components.count >= 3 else {
            return nil
        }

        let brightness = Float(((components[0] * 299) + (components[1] * 587) + (components[2] * 114)) / 1000)
        return (brightness > threshold)
    }
}

extension UIViewController {
    
    var safeAreaInsets: UIEdgeInsets {
        get {
            var insets = view.safeAreaInsets
            print("child \(view.safeAreaInsets.top)")
            print("parent \(navigationController?.view.safeAreaInsets.top ?? 0)")
            insets.top += navigationController?.navigationBar.height ?? 0
            print("added height \(navigationController?.navigationBar.height ?? 0)")
            return insets
        }
    }
    
    func push(to title: String, from previous: String, using vc: UIViewController) {
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)

        let containerView = UIView()
        containerView.colorScheme = .normal
        let size = CGSize(width: view.frame.width, height: 80)
        let textView = UILabel(right: 20, weight: .heavy)
        textView.text = title
        textView.frame = CGRect(origin: .zero, size: size)
        textView.textAlignment = .center
        containerView.schemedBG(.highlightColorA)
        containerView.addSubview(textView)
        vc.navigationItem.titleView = containerView
        vc.view.colorScheme = .normal
        textView.schemedTextColor(.lightShadow)
        textView.pin.vCenter().hCenter().marginTop(25)
        vc.view.backgroundColor = ColorScheme.normal.highlightColorA
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func push(vc: UIViewController) {
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func setStandardNav() {
        if let img = UIImage(named: "logo") {
            let containerView = UIView()
            containerView.frame.size = CGSize(width: view.frame.width, height: 80)
            
            let logoView = UIImageView()
            let logoh: CGFloat = 20
            let size = CGSize(width: img.size.width / img.size.height * logoh, height: logoh)
            logoView.frame = CGRect(origin: .zero, size: size)
            let processor = DownsamplingImageProcessorScaled(size: logoView.size * 2)
            let out = processor.process(item: ImageProcessItem.image(img), options: .init(nil))
            let logoAlpha: CGFloat = 0.8
            logoView.image = out?.withTintColor(ColorScheme.normal.lightShadow.withAlphaComponent(logoAlpha))
            logoView.contentMode = .scaleAspectFill
            containerView.addSubview(logoView)
            
            let textv = UILabel(central: 20, weight: .bold, wantsMonster: true)
            textv.schemedTextColor(.lightShadow)
            textv.text = "Groomly"
            textv.sizeToFit()
            containerView.addSubview(textv)
            
            let subtext = UILabel(central: 17, weight: .semibold, wantsMonster: true)
            subtext.schemedTextColor(.lightShadow, alpha: 0.9)
            subtext.text = "Keeping you fresh."
            subtext.sizeToFit()
            containerView.addSubview(subtext)
             
            navigationItem.titleView = containerView
            logoView.pin.top(3).left(10)
            let spacing = textv.intrinsicContentSize.height
            let offset: CGFloat = 15
            textv.pin.vCenter(offset).horizontally().marginTop(-spacing)
            subtext.pin.vCenter(offset).horizontally().margin(spacing)
            
            ColorConfigurator.shared.register(interestFor: .normal, andProperty: .lightShadow) {
                [weak logoView] color in
                guard let logoView = logoView else { return false }
                logoView.image = logoView.image?.withTintColor(color.withAlphaComponent(logoAlpha))
                return true
            }
        }
    }
    
}

class MainNVC: UINavigationController {
    override init(rootViewController: UIViewController) {
        super.init(navigationBarClass: MainNavBar.self, toolbarClass: nil)
        setViewControllers([rootViewController], animated: false)
        
        ColorConfigurator.shared.register(interestFor: .normal, andProperty: .bg) {
            [weak self] color in
            guard let self = self else { return false }
            self.setNeedsStatusBarAppearanceUpdate()
            return true
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return (ColorScheme.normal.bg.isLight() ?? true) ? .lightContent : .darkContent
    }
}

class MainNavBar: UINavigationBar {
    
    override init(frame: CGRect) {
        super.init(frame: frame)

        tintColor = ColorScheme.normal.lightShadow
        backgroundColor = .clear
        barTintColor = ColorScheme.normal.highlightColorA
        shadowImage = UIImage()
        
        ColorConfigurator.shared.register(interestFor: .normal, andProperties: [.lightShadow, .highlightColorA]) {
            [weak self] _ in
            guard let self = self else { return false }
            self.tintColor = ColorScheme.normal.lightShadow
            self.barTintColor = ColorScheme.normal.highlightColorA
            self.setNeedsDisplay()
            return true
        }
    }
    
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: 80)
    }
    
    override func draw(_ rect: CGRect) {
        func recurse(vv: UIView) {
            vv.clipsToBounds = false
            vv.layer.masksToBounds = false
            for v in vv.subviews {
                recurse(vv: v)
            }
        }
        recurse(vv: self)
        super.draw(rect)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


//
//  Network.swift
//  Learning
//
//  Created by clar3 on 30/04/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import Foundation
import Async
import JWTDecode
import Alamofire

enum NetworkReqError: Error {
    case InvalidParams
    case InvalidURLError
    case ResponseDecodeError
    case Authorization
    case Detail(data: Data)
    case Misc
}

enum EncodingMethod {
    case query, json, multipart
}

enum NetworkMethod: String {
    case GET = "GET"
    case POST = "POST"
    case DELETE = "DELETE"
    case PATCH = "PATCH"
    case PUT = "PUT"
    
    var encodeMethod: EncodingMethod {
        if .GET == self {
            return .query
        } else if .PUT == self {
            return .multipart
        }
        return .json
    }
}

extension Worker {
    func at<T>(once: T) -> Future<T> {
        let promi = self.eventLoop.newPromise(of: T.self)
        DispatchQueue.global(qos: .background).asyncAfter(deadline: .now() + 0.01) {
            promi.succeed(result: once)
        }
        return promi.futureResult
    }
}

extension Encodable {
    func asNetDictionary() throws -> [String: String] {
        let data = try NetEncoder().encode(self)
        guard let dictionary = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any] else {
            throw NSError()
        }
        return dictionary.mapValues { "\($0)" }
    }
}

class Networker: NSObject {
    
    static let shared = Networker()
    private(set) var hostURL: String!
    internal var worker: Worker!
    internal var loginReq: LoginRequest?
    internal var cachedLogin: LoginResponse?
    internal var decodedJWT: JWT?
    private let networkQueue = DispatchQueue(label: "networkQueue", qos: .background)
    let blank = BlankResponse(state: "ok")
    var apiKey: String?
    var session: StripePKResponse?
    init(_ hostURL: String? = nil) {
        super.init()
        
        #if DEBUG
        #if targetEnvironment(simulator)
          // your simulator code
            self.hostURL = hostURL ?? "http://localhost:8000"
        #else
          // your real device code
            self.hostURL = hostURL ?? "http://10.3.6.66:8000"
        #endif
        if AppDelegate.isUITestingEnabled {
            self.hostURL = ProcessInfo.processInfo.environment["TestServerURL"] ?? self.hostURL
            Log("USING TEST SERVER URL \(self.hostURL ?? "NONE!")")
        }
        #else
        self.hostURL = hostURL ?? "https://groomlyapp.com"
        #endif
    
        let eventLoopGroup = MultiThreadedEventLoopGroup(numberOfThreads: 1)
        let eventLoop = eventLoopGroup.next()
        self.worker = eventLoop
    }
    
    func path(_ route: String) -> URL? {
        if let hostURL = Networker.shared.hostURL, let baseURL = URL(string: hostURL) {
            return URL(string: route, relativeTo: baseURL)
        }
        return nil
    }
    func path<Param: Codable>(_ route: String,_ params: Param? = nil) -> URLRequest? {
        var endpoint = route
        if let params = try? params?.asNetDictionary(), params.count > 0 {
            endpoint += "?" + params.map({ "\($0.key)=\($0.value.replacingOccurrences(of: "#", with: ""))" }).joined(separator: "&")
        }
        if let url: URL = path(endpoint) {
            return URLRequest(url: url)
        }
        return nil
    }
    
    func authorize(user: LoginRequest) -> Future<Void> {
        Log("AUTHORIZED CALLED")
        return logout().flatMap { () in
            self.send("/auth/token/obtain/", .post, user, attemptRefresh: false).map { (resp: LoginResponse) in
                self.cachedLogin = resp
                self.loginReq = user
                self.decodedJWT = try? decode(jwt: resp.access)
                Log("LOGGED IN SUCCESSFULY!")
            }
        }
    }
    
    func logout() -> Future<Void> {
        guard let cachedLogin = cachedLogin else { return worker.at(once: ()) }
        return send("/auth/token/blacklist/", .post, cachedLogin).map { (resp: BlankResponse) in
            self.loginReq = nil
            self.cachedLogin = nil
        }
    }
    
    func clientList() -> Future<[ClientStubResponse]> {
        send("/data/nearby/", .get, blank)
    }
    
    func create(customer: CustomerModel) -> Future<CustomerRespModel> {
        if let key = apiKey {
            return send("/auth/client/", .post, customer, overrideAuthKey: "Api-Key \(key)")
        }
        return send("/auth/user/", .post, customer)
    }
    
    func get(booking: GetModelRequest) -> Future<BookingModel> {
        send("/booking/", .get, booking)
    }
    
    func create(booking: BookingCreateRequest) -> Future<BookingModel> {
        send("/booking/", .post, booking)
    }
    
    func update(booking: BookingUpdateRequestClient) -> Future<BookingModel> {
        send("/booking/", .post, booking)
    }
    
    func update(booking: BookingUpdateRequestCustomer) -> Future<BookingModel> {
        send("/booking/", .post, booking)
    }
    
    func validate(_ order: ServiceOrderModel) -> Bool {
        order.client != nil &&
        order.location != nil &&
        order.items.count > 0 &&
        order.totalValue() > 0 &&
        order.time != nil &&
        order.endTime != nil
    }
    
    func create(booking order: ServiceOrderModel) throws -> Future<BookingModel> {
        guard
            validate(order),
            let payment = PaymentBackend.shared.selected?.stripeId,
            let startTime = order.time,
            let endTime = order.endTime
        else { throw NetworkReqError.InvalidParams  }
        let request = BookingCreateRequest(
            clientID: order.client!.id,
            date: startTime,
            start_time: startTime,
            end_time: endTime,
            location: order.location!,
            services: order.items.map({
                ServicePriceModel(
                    id: $0.service.key,
                    price: $0.value!,
                    duration: $0.approxTime ?? .distantPast
                )
            }),
            payment_method: payment
        )
        return create(booking: request)
    }
    
    func delete(booking: GetModelRequest) -> Future<BlankResponse> {
        send("/booking/", .delete, booking)
    }
    
    func getClient(profile: GetClientProfileRequest) -> Future<ClientProfileModel> {
        send("/data/profile/", .get, profile)
    }
    
    func getProfile() -> Future<ClientProfileModel> {
        send("/data/profile/", .get, blank)
    }
    
    func saveClient(profile: ClientProfileUpdate) -> Future<ClientProfileModel> {
        send("/data/profile/", .post, profile)
    }
    
    func portfolio(post: PortfolioPostRequest, progress: (Request.ProgressHandler)? = nil) -> Future<ClientProfileModel> {
        send("/data/profile/", .put, post, progress: progress)
    }
    
    func deletePorfolio(image: GetModelRequest) -> Future<ClientProfileModel> {
        send("/data/profile/", .delete, image)
    }
    
    func getUserDiary(for monthYear: GetUserDiary) -> Future<[BookingModel]> {
        send("/diary/", .get, monthYear)
    }
    
    func getClient(availability: GetClientAvailability) -> Future<AvailableModel> {
        send("/diary/availability/", .get, availability)
    }
    
    func saveClient(availability: UpdateClientAvailability) -> Future<AvailableModel> {
        send("/diary/availability/", .post, availability)
    }
    
    func getWorkWeek() -> Future<DefaultAvailabilityResponse> {
        send("/diary/default/", .get, blank)
    }
    
    func saveClientDefault(availability: DefaultAvailability) -> Future<DefaultAvailabilityResponse> {
        send("/diary/default/", .post, availability)
    }
    
    func getGroups() -> Future<[ServiceGroup]> {
        return send("/data/groups/", .get, blank)
    }
    
    func getServices(for clientID: Int? = nil) -> Future<ClientServiceList> {
        return send("/data/list/", .get, GetClientProfileRequest(clientID: clientID))
    }
    
    func save(services: ClientServiceList) -> Future<ClientServiceList> {
        return send("/data/list/", .post, services)
    }
    
    func servicePortfolio(post: ServicePortfolioPostRequest, progress: (Request.ProgressHandler)? = nil) -> Future<ClientServiceList> {
        send("/data/list/", .put, post, progress: progress)
    }
    
    func deleteServicePorfolio(image: GetModelRequest) -> Future<ClientServiceList> {
        send("/data/list/", .delete, image)
    }
    
    func detail(service key: String) -> Future<ServiceDataResponse> {
        return send("/data/service/", .get, ["key": key])
    }
    
    func getAddresses() -> Future<[AddressModel]> {
        return send("/auth/address/", .get, blank).map {
            (resp: [AddressRespModel]) in
            return resp.map { $0.toAddressModel() }
        }
    }
    
    func send(review: ReviewRequest) -> Future<BlankResponse> {
        send("/review/", .post, review)
    }
    
    func select(address: AddressModel) throws -> Future<[AddressModel]> {
        guard let address_id = address.id else { throw NetworkReqError.InvalidParams }
        return send("/auth/address/", .patch, GetModelRequest(id: address_id)).map {
            (resp: [AddressRespModel]) in
            resp.map { $0.toAddressModel() }
        }
    }
    
    func create(address: AddressModel) -> Future<[AddressModel]> {
        return send("/auth/address/", .post, AddressRespModel.create(from: address)).map {
            (resp: [AddressRespModel]) in
            resp.map { $0.toAddressModel() }
        }
    }
    
    func delete(address: AddressModel) throws -> Future<[AddressModel]> {
        guard let address_id = address.id else { throw NetworkReqError.InvalidParams }
        return send("/auth/address/", .delete, GetModelRequest(id: address_id)).map {
            (resp: [AddressRespModel]) in
            resp.map { $0.toAddressModel() }
        }
    }
    
    func getFee(for booking: GetModelRequest) -> Future<FeeModel> {
        send("/payment/fee", .get, booking)
    }
    
    func getStripeSetup() -> Future<StripePKResponse> {
        return send("/payment/oauth", .get, blank)
    }
    
    func confirmStripeSetup(code: String) -> Future<Data> {
        return send("/payment/oauth", .get, ["code": code])
    }
    
    func confirmClientToken() -> Future<BlankResponse> {
        return send("/auth/client", .get, blank)
    }
    
    func getStripeAuth() throws -> URLRequest {
        let path = "/payment/authorize"
        let url = "\(self.hostURL!)\(path)"
        guard let token = cachedLogin?.token else { throw NetworkReqError.Authorization }
        let headers = ["Authorization": token]
        return try URLRequest(url: url, method: .get, headers: HTTPHeaders(headers))
    }
    
    func getStripeDashboard() throws -> URLRequest {
        let path = "/payment/dashboard"
        let url = "\(self.hostURL!)\(path)"
        guard APIClient.isClient, let token = cachedLogin?.token else { throw NetworkReqError.Authorization }
        let headers = ["Authorization": token]
        return try URLRequest(url: url, method: .get, headers: HTTPHeaders(headers))
    }
    
    func getClientHistory(range: HistoryRequest) -> Future<HistoryResponse> {
        send("/payment/history", .get, range)
    }
    
    func resetPasswordRequest(email: String) -> Future<Data> {
        send("/ios/password_reset/", .post, ["email": email], forceQueryCode: true)
    }

    func send<Param: Codable, Resp: Codable>(
        _ path: String,
        _ method: HTTPMethod,
        _ params: Param,
        attemptRefresh: Bool = true,
        chaining promise: EventLoopPromise<Resp>? = nil,
        progress: (Request.ProgressHandler)? = nil,
        overrideAuthKey: String? = nil,
        forceQueryCode: Bool = false
    ) -> Future<Resp> {
        let promi = promise ?? worker.eventLoop.newPromise(Resp.self)
        let url = "\(self.hostURL!)\(path)"
        var headers: HTTPHeaders?

        if let token = overrideAuthKey ?? cachedLogin?.token {
            headers = HTTPHeaders([HTTPHeader(name: "Authorization", value: token)])
        }

        let respond = { (error: AFError?, data: Data?, response: HTTPURLResponse) in
            if let data = data, (response.statusCode == 200 || response.statusCode == 201 || response.statusCode == 205) {
                if Resp.self == Data.self { promi.succeed(result: data as! Resp); return }
                do {
                    let response = try NetDecoder().decode(Resp.self, from: data)
                    promi.succeed(result: response)
                } catch {
                    Log(String(data: data, encoding: .utf8) ?? "NO DATA")
                    Log(error)
                    promi.fail(error: NetworkReqError.ResponseDecodeError)
                }
            } else if   response.statusCode == 401 || response.statusCode == 403 {
                if let cachedLogin = self.cachedLogin,
                    attemptRefresh {
                    self.send(
                        "/auth/token/refresh/",
                        .post,
                        RefreshTokenRequest(refresh: cachedLogin.refresh),
                        attemptRefresh: false,
                        progress: progress
                    ).do({ (tokenResp: LoginResponse) in
                        self.cachedLogin = tokenResp
                        let _ = self.send(path, method, params, attemptRefresh: false, chaining: promi, progress: progress)
                    }).catch { _ in
                        if let loginReq = self.loginReq {
                            let reauth = self.authorize(user: loginReq)
                            reauth.do({ _ in
                                let _ = self.send(path, method, params, attemptRefresh: false, chaining: promi, progress: progress)
                            }).catch { _ in
                                promi.fail(error: NetworkReqError.Authorization)
                            }
                        } else {
                            promi.fail(error: NetworkReqError.Authorization)
                        }
                    }
                } else {
                    promi.fail(error: NetworkReqError.Authorization)
                }
            } else if let data = data {
                promi.fail(error: NetworkReqError.Detail(data: data))
            } else if let error = error, let out = error.localizedDescription.data(using: .utf8) {
                promi.fail(error: NetworkReqError.Detail(data: out))
            } else {
                promi.fail(error: NetworkReqError.Misc)
            }
        }

        var request: DataRequest?
        if isBlank(params) {
            request = AF.request(url, method: method, headers: headers)
        } else if method == .get {
            let trans = (try? params.asNetDictionary()) ?? [:]
            request = AF.request(url, method: .get, parameters: trans, headers: headers)
        } else if method == .put, let multi = params as? MultipartRequest {
            let semi = AF.upload(multipartFormData: { (data) in
                for file in multi.files {
                    data.append(file.data, withName: file.key, fileName: file.filename, mimeType: file.mimeType)
                }
                for (key, value) in multi.data {
                    data.append(value.data(using: .utf8)!, withName: key)
                }
            }, to: url, method: .put, headers: headers)
            if let progress = progress {
                semi.uploadProgress(queue: .main, closure: progress)
            }
            request = semi
        } else {
            let encoder: ParameterEncoder = forceQueryCode ? URLEncodedFormParameterEncoder.default : JSONParameterEncoder(encoder: NetEncoder())
            request = AF.request(url, method: method, parameters: params, encoder: encoder, headers: headers)
        }
        request?.responseData(queue: networkQueue) { (response) in
            guard let urlResponse = response.response else {
                Log("EARLYYYYYYY \(response.error!.errorDescription!)")
                promi.fail(error: NetworkReqError.Misc)
                return
            }
            respond(response.error, response.data, urlResponse)
        }
        return promi.futureResult
    }
    
    private func isBlank(_ params: Codable) -> Bool {
        if let x = params as? BlankResponse {
            return x.state == blank.state
        }
        return false
    }
}

//
//  NetworkStructs.swift
//  Learning
//
//  Created by clar3 on 30/04/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import Foundation
import IGListKit

struct LoginRequest: Codable {
    init(email: String, password: String) {
        self.email = email
        self.password = password
        if Thread.isMainThread {
            self.apns_token = (UIApplication.shared.delegate as? AppDelegate)?.deviceToken
        } else {
            DispatchQueue.main.sync {
                self.apns_token = (UIApplication.shared.delegate as? AppDelegate)?.deviceToken
            }
        }
    }
    
    let email: String
    let password: String
    var apns_token: String?
}

struct LoginResponse: Codable {
    init(refresh: String, access: String) {
        self.refresh = refresh
        self.access = access
        self.apns_token = (UIApplication.shared.delegate as? AppDelegate)?.deviceToken
    }
    
    let refresh: String
    let access: String
    let apns_token: String?
    
    var token: String {
        "JWT \(access)"
    }
}

struct RefreshTokenRequest: Codable {
    let refresh: String
}

struct BlacklistTokenRequest: Codable {
    let refresh_token: String
}

struct BlankResponse: Codable {
    let state: String
}

struct StripePKResponse: Codable {
    let key: String
    let score: ReviewScore?
    let email: String
    let deadline: Int
}

struct ServicePriceModel: Codable {
    let id: String
    let price: Float
    let duration: Date
    
    var approxTime: Int {
        return duration.hour * 3600 + duration.minute * 60 + duration.second
    }
}

struct FrozenServicePriceModel: Codable {
    init(id: String, price: Float, duration: Date, service: ServiceModel? = nil) {
        self.service = service
        self.id = id
        self.price = price
        self.duration = duration
    }
    
    let service: ServiceModel?
    let id: String
    let price: Float
    let duration: Date
    
    var approxTime: Int {
        return duration.hour * 3600 + duration.minute * 60 + duration.second
    }
}

struct GetClientProfileRequest: Codable {
    let clientID: Int?
}

struct ClientServiceList: Codable {
    let services: [ServiceModel]
}

struct ClientProfileUpdate: Codable {
    internal init(
        description: String? = nil,
        matrix: [ServicePriceModel]? = nil,
        first_name: String? = nil,
        last_name: String? = nil,
        company: String? = nil,
        group: String? = nil,
        mileage: Float? = nil
    ) {
        self.description = description
        self.matrix = matrix
        self.first_name = first_name
        self.last_name = last_name
        self.company = company
        self.group = group
        self.mileage = mileage
    }
    
        
    let description: String?
    var matrix: [ServicePriceModel]?
    let first_name: String?
    let last_name: String?
    let company: String?
    let group: String?
    let mileage: Float?
}

struct ReviewScore: Codable {
    let score: Float
    let num: Int
}

struct ClientProfileModel: Codable, DisplayImageModel {
    internal init(clientID: Int, description: String, mileage: Float, matrix: [ServicePriceModel], first_name: String? = nil, last_name: String? = nil, company: String? = nil, portfolio: [PortfolioImage]? = nil, score: ReviewScore? = nil, group: String? = nil, widthInput: Float? = nil, heightInput: Float? = nil, imageString: String? = nil) {
        self.clientID = clientID
        self.description = description
        self.mileage = mileage
        self.matrix = matrix
        self.first_name = first_name
        self.last_name = last_name
        self.company = company
        self.portfolio = portfolio
        self.review_score = score
        self.group = group
        self.imageString = imageString
        self.widthInput = width
        self.heightInput = height
    }
    
    let group: String?
    let clientID: Int
    let description: String
    let mileage: Float
    let matrix: [ServicePriceModel]
    let first_name: String?
    let last_name: String?
    let company: String?
    let portfolio: [PortfolioImage]?
    let review_score: ReviewScore?
    var widthInput: Float?
    var heightInput: Float?
    var imageString: String?
    var image: URL? {
        guard let imageString = imageString else { return nil }
        #if DEBUG
        return URL(string: "\(Networker.shared.hostURL!)\(imageString)")
        #else
        return URL(string: imageString)
        #endif
    }
    var width: Float { widthInput ?? 0 }
    var height: Float { heightInput ?? 0 }
    
    enum CodingKeys: String, CodingKey {
        case imageString = "image"
        case group
        case clientID
        case description
        case mileage
        case matrix
        case first_name
        case last_name
        case company
        case portfolio
        case review_score
        case widthInput = "width"
        case heightInput = "height"
    }
    
    func price(for service: ServiceModel) -> Float? {
        matrix.first(where: { $0.id == service.key })?.price
    }
    
    func duration(for service: ServiceModel) -> Int? {
        matrix.first(where: { $0.id == service.key })?.approxTime
    }
}

struct ClientStubResponse: Codable, Equatable, Hashable {
    let id: Int
    let distance: String
    let first_name: String
    let last_name: String
    let formatted_address: String
    let company: String?
    let profile: ClientProfileModel?
    var image: URL?
    
    var name: String {
        return "\(first_name) \(last_name)".capitalized
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
        hasher.combine(distance)
    }
    
    static func ==(lhs: ClientStubResponse, rhs: ClientStubResponse) -> Bool {
        return lhs.id == rhs.id
    }
}

class ListableClientStubResponse: ListDiffable {
    
    let stub: ClientStubResponse
    init(stub: ClientStubResponse) {
        self.stub = stub
    }
    
    public func diffIdentifier() -> NSObjectProtocol {
        return stub.id as NSObjectProtocol
    }

    public func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        if self === object { return true }
        guard let object = object as? ListableClientStubResponse else { return false }
        return stub.id == object.stub.id
    }
}

struct PortfolioImage: Codable, DisplayImageModel {
    var imageString: String
    var caption: String
    var id: Int
    var width: Float
    var height: Float
    var description: String { caption }
    var image: URL? {
        #if DEBUG
        return URL(string: "\(Networker.shared.hostURL!)\(imageString)")
        #else
        return URL(string: imageString)
        #endif
    }
    
    enum CodingKeys: String, CodingKey {
        case imageString = "image"
        case caption
        case id
        case width
        case height
    }
}

struct FileUpload {
    var data: Data
    var key: String
    var type: String
    var filename: String {
        "0.\(type)"
    }
    var mimeType: String {
        "image/\(type)"
    }
}

protocol MultipartRequest {
    var files: [FileUpload] {get}
    var data: [String: String] {get}
}

struct PortfolioPostRequest: Codable, MultipartRequest {
    var caption: String
    var image: Data?
    var type: String?
    var is_profile: Bool = false
    var id: Int? = nil
    
    var data: [String: String] {
        var out = ["caption": caption, "is_profile": String(is_profile)]
        if let id = id {
            out["id"] = "\(id)"
        }
        return out
    }
    var files: [FileUpload] {
        if let image = image, let type = type {
            return [FileUpload(data: image, key: "image", type: type)]
        }
        return []
    }
}

struct ServicePortfolioPostRequest: Codable, MultipartRequest {
    var key: String
    var caption: String?
    var image: Data?
    var type: String?
    var id: Int? = nil
    
    var data: [String: String] {
        var out = ["key": key]
        if let id = id {
            out["id"] = "\(id)"
        }
        return out
    }
    var files: [FileUpload] {
        if let image = image, let type = type {
            return [FileUpload(data: image, key: "image", type: type)]
        }
        return []
    }
}

struct AddressRespModel: Codable {
    let id: Int?
    let formatted_address: String?
    let address: String
    let city: String
    let postal_code: String
    let primary: Bool
    
    static func create(from model: AddressModel) -> AddressRespModel {
        let address = "\(model.addressOne), \(model.addressTwo)"
        return AddressRespModel(
            id: nil,
            formatted_address: nil,
            address: address,
            city: model.city,
            postal_code: model.postcode,
            primary: model.primary
        )
    }
    
    func toAddressModel() -> AddressModel {
        let getAddress = { () -> (String, String) in
            guard self.address.count > 20 else {
                return (self.address, "")
            }
            let cleaned = self.address.replacingOccurrences(of: ", ", with: ",")
            let (left, right) = cleaned.split(separator: ",").split()
            return (left.joined(separator: ", "), right.joined(separator: ", "))
        }
        let (one, two) = getAddress()
        return AddressModel(
            addressOne: one,
            addressTwo: two,
            city: city,
            postcode: postal_code,
            id: id,
            primary: primary,
            formatted_address: formatted_address
        )
    }
}

struct AddressNetModel: Codable {
    let address: String
    let city: String
    let postal_code: String
}

struct CustomerModel: Codable {
    internal init(
        email: String,
        password: String?,
        primary_address: AddressNetModel,
        first_name: String,
        last_name: String,
        phone_number: String,
        company: String? = nil
    ) {
        self.email = email
        self.password = password
        self.primary_address = primary_address
        self.first_name = first_name
        self.last_name = last_name
        self.phone_number = phone_number
        self.company = company
    }
    
    let email: String
    let password: String?
    let primary_address: AddressNetModel
    let first_name: String
    let last_name: String
    let phone_number: String
    let company: String?
}

struct CustomerRespModel: Codable {
    let first_name: String
    let last_name: String
    let phone_number: String
}

struct ClientRespModel: Codable {
    let first_name: String
    let last_name: String
    let company: String?
    let phone_number: String
}

enum BookingState: Int, Codable, CustomStringConvertible, CaseIterable {
    case REQUESTED = 1
    case CONFIRMED = 2
    case COMPLETED = 3
    case RESOLVING = 4
    case CANCELLED = 5
    case PAID = 6
    case REFUNDED = 7
    
    var description: String {
        switch  self {
        case .CANCELLED: return "cancelled"
        case .COMPLETED: return "completed"
        case .CONFIRMED: return "confirmed"
        case .REQUESTED: return "requested"
        case .RESOLVING: return "resolving"
        case .PAID: return "paid"
        case .REFUNDED: return "refunded"
        }
    }
}


struct FeeModel: Codable {
    let current: [String: Float]
    let possible: [String: Float]
    let pending: [String: Float]
}

enum BookingRejectReason: String, Codable {
    case TIME = "time"
    case PAYMENT = "paym"
}

class BookingModel: Codable, Comparable, ListDiffable {
    static func < (lhs: BookingModel, rhs: BookingModel) -> Bool {
        guard let l_s = lhs.startTime, let r_s = rhs.startTime else { return true }
        return l_s < r_s
    }
    
    static func == (lhs: BookingModel, rhs: BookingModel) -> Bool {
        lhs.id == rhs.id
    }
    
    public func diffIdentifier() -> NSObjectProtocol {
        return id as NSObjectProtocol
    }
    
    public func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        if self === object { return true }
        guard let object = object as? BookingModel else { return false }
        return status == object.status &&
            reject_reason == object.reject_reason &&
            start_time == object.start_time &&
            endTime == object.endTime &&
            date == object.date
    }
    
    let id: Int
    let status: BookingState
    let clientID: Int
    let customerID: Int
    let clientStub: ClientRespModel
    let customerStub: CustomerRespModel
    let date: Date
    let start_time: Date
    let end_time: Date
    let location: String
    let services: [FrozenServicePriceModel]
    let reject_reason: BookingRejectReason?
    let reference: String
    let client_score: Float?
    let customer_score: Float?
    
    var startTime: Date? {
        return date.setTo(time: start_time)
    }
    var endTime: Date? {
        return date.setTo(time: end_time)
    }
    var displayName: String {
        if APIClient.isClient {
            return "\(customerStub.first_name) \(customerStub.last_name)"
        } else {
            var txt = "\(clientStub.first_name) \(clientStub.last_name)"
            if let company = clientStub.company {
                txt = "\(company): \(txt)"
            }
            return txt
        }
    }
    var displayPhone: String {
        return APIClient.isClient ? customerStub.phone_number : clientStub.phone_number
    }
    var order: ServiceOrderModel {
        let model = ServiceOrderModel()
        model.client = ClientStubResponse(id: clientID, distance: "", first_name: clientStub.first_name, last_name: clientStub.last_name, formatted_address: "", company: clientStub.company, profile: nil, image: nil)
        model.time = startTime
        model.items = services.map({
            ServiceItemOrderModel(service: $0.service!, value: $0.price, duration: $0.approxTime)
        })
        return model
    }
}

struct BookingCreateRequest: Codable {
    let clientID: Int
    let date: Date
    let start_time: Date
    let end_time: Date
    let location: String
    let services: [ServicePriceModel]
    let payment_method: String
}

struct BookingUpdateRequestCustomer: Codable {
    let id: Int
    let date: Date
    let start_time: Date
    let end_time: Date
}

struct BookingUpdateRequestClient: Codable {
    let id: Int
    let status: BookingState
    let reject_reason: BookingRejectReason?
}

struct GetModelRequest: Codable {
    let id: Int
    
    static let blank = GetModelRequest(id: -1)
}

struct GetUserDiary: Codable {
    let month: Int
    let year: Int
}

struct GetClientAvailability: Codable {
    let clientID: Int
    let date: Date
}

struct DefaultAvailability: Codable {
    let day: Int
    let blocks: [AvailableBlockModel]
}

struct DefaultAvailabilityResponse: Codable {
    let blocks: [DefaultAvailability]
}


struct AvailableModel: Codable {
    let blocks: [AvailableBlockModel]
}

struct AvailableBlockModel: Codable, TimeBlock {
    let start: Date
    let end: Date
}

struct UpdateClientAvailability: Codable {
    let date: Date
    var blocks: [AvailableBlockModel]
}

enum ServiceCategory: String, Codable, CaseIterable {
    case mensHair = "Men's Hair"
    case womensHair = "Women's Hair"
    case makeup = "Makeup"
    case beard = "Beard"
    case nails = "Nails"
    case eyes = "Eyes"
    
    var priority: Int {
        switch self {
        case .womensHair: return 1
        case .mensHair: return 2
        case .makeup: return 3
        case .nails: return 4
        case .eyes: return 5
        case .beard: return 6
        }
    }
}

struct ServiceCategoryStub: Codable, CustomStringConvertible {
    var description: String { label }
    let name: String
    let label: String
}

class ServiceGroup: Codable {
    var name: String
    var width: Float?
    var height: Float?
    var image: String?
}

class ServiceModel: Codable, Equatable {
    internal init(name: String, description: String? = nil, key: String = "", category: String) {
        self.name = name
        self.description = description
        self.key = key
        self.category = category
    }
    
    static func == (lhs: ServiceModel, rhs: ServiceModel) -> Bool {
        return lhs.key == rhs.key
    }
    
    var name: String
    var description: String?
    var key: String
    var category: String
    var order: Int = 0
}

struct ServiceDataResponse: Codable {
    let category: String
    let name: String?
    let description: String?
    let portfolio: [PortfolioImage]
}

enum PaymentState: Int, Codable {
    case REQUESTED = 1
    case AUTHORISED = 2
    case PAID = 3
    case REFUNDED = 4
    case CANCELLED = 5
    case ATTENTION = 6
    
    var isFinal: Bool {
        self == .PAID || self == .REFUNDED || self == .CANCELLED
    }
    
    var icon: String {
        switch self {
        case .ATTENTION, .AUTHORISED, .REQUESTED: return "hourglass"
        case .CANCELLED: return "xmark"
        case .PAID: return "checkmark"
        case .REFUNDED: return "arrow.uturn.left"
        }
    }
}

class PaymentHistoryModel: Codable, ListDiffable {
    public func diffIdentifier() -> NSObjectProtocol {
        return id as NSObjectProtocol
    }

    public func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        if self === object { return true }
        guard let object = object as? PaymentHistoryModel else { return false }
        return status == object.status &&
            start_time == object.start_time &&
            endTime == object.endTime &&
            date == object.date
    }
    
    let id: Int
    let status: PaymentState
    let name: String
    let date: Date
    let start_time: Date
    let end_time: Date
    let amount: Float
    
    var startTime: Date? {
        return date.setTo(time: start_time)
    }
    var endTime: Date? {
        return date.setTo(time: end_time)
    }
    
    var timeString: String? {
        let tf = DateFormatter.appTime
        let df = DateFormatter.appDate
        let d1 = df.string(from: date)
        let t1 = tf.string(from: start_time).lowercased()
        let t2 = tf.string(from: end_time).lowercased()
        return "\(t1) - \(t2) \(d1)"
    }
}

struct HistoryRequest: Codable {
    let days: Int?
}

struct HistoryResponse: Codable {
    let payments: [PaymentHistoryModel]
    let total: Float
    let pending: Float
}

struct TermsRequest: Codable {
    enum TermTypes: String, Codable {
        case privacy, client, customer
    }
    let type: TermTypes
    let bg_color: String
    let text_color: String
    let bold_color: String
    let link_color: String
}

struct ReviewRequest: Codable {
    let booking: Int
    let score: Double
}



//
//  APIClient.swift
//  Groomly
//
//  Created by Ravi on 17/07/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import Foundation
import Async

/**
 Login Screen required to make sure networker is authenticated and the customer or client version is chosen!
 Do this ! The UI and whole things - might as well do it know
 
 Buttons for choosing client and adding to order as customer!
 Update in basket - keep in HomeVC
 Create booking and then show in calendar - that is where you manage it; basket becomes cleared
 (Save basket if unfinished ?)
 
 Then fixing models in server to include name don't worry about extra images for client!
 And double check information bar information!
 
 Then doing calendar and getting all operations working don't worry about notifications.
 Changing availability etc for client. This is not available for customer!
 
 Home  / Calendar / Profile all seperate so can control state internally, but home can request update of booking info.
 We are going to have an APIClient that will wrap networker and keep internal caches e.g, session etc
 */

class APIClient {
    
    static let api = Networker.shared
    static var isLoggedIn: Bool {
        api.cachedLogin != nil
    }
    static var isClient: Bool {
        (api.decodedJWT?.body["is_client"] as? Bool) ?? false
    }
    static var signupClient: Bool {
        api.apiKey != nil
    }
    
    // CRUD booking
    // CRUD availablilty
}

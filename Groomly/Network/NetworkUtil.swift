//
//  NetworkUtil.swift
//  Groomly
//
//  Created by Ravi on 22/08/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import Foundation


extension DateFormatter {
    static let netDate: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        return formatter
    }()
    static let netTime: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        return formatter
    }()
    static let appTime: DateFormatter = {
        let tf = DateFormatter()
        tf.dateFormat = "hh:mma"
        return tf
    }()
    static let appShortTime: DateFormatter = {
        let tf = DateFormatter()
        tf.dateFormat = "HH:mm"
        return tf
    }()
    static let appFullDate: DateFormatter = {
        let df = DateFormatter()
        df.dateFormat = "EEEE, MMM d"
        return df
    }()
    static let appDate: DateFormatter = {
        let df = DateFormatter()
        df.dateFormat = "dd/MM/YY"
        return df
    }()
}

class NetDecoder: JSONDecoder {
    override init() {
        super.init()
        dateDecodingStrategy = .custom({ (decoder) -> Date in
            let container = try decoder.singleValueContainer()
            let dateStr = try container.decode(String.self)
            var date: Date? = nil
            if dateStr.contains("-") {
                date = DateFormatter.netDate.date(from: dateStr)
            } else {
                date = DateFormatter.netTime.date(from: dateStr)
            }
            guard let date_ = date else {
                throw DecodingError.dataCorruptedError(in: container, debugDescription: "Cannot decode date string \(dateStr)")
            }
            return date_
        })
    }
}

class NetEncoder: JSONEncoder {
    override init() {
        super.init()
        dateEncodingStrategy = .custom({ (date, encoder) in
            var container = encoder.singleValueContainer()
            let timeVals = ["start", "end", "start_time", "end_time", "duration"]
            var isTime = false
            if let lst = container.codingPath.last, timeVals.contains(lst.stringValue) {
                isTime = true
            }
            let stringData = (isTime ? DateFormatter.netTime : DateFormatter.netDate).string(from: date)
            try container.encode(stringData)
        })
    }
}

extension Array {
    func split() -> (left: [Element], right: [Element]) {
        let ct = self.count
        let half = Int(ceil(Float(ct) / 2))
        let leftSplit = self[0 ..< half]
        let rightSplit = self[half ..< ct]
        return (left: Array(leftSplit), right: Array(rightSplit))
    }
}

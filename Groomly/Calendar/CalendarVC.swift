//
//  CalendarVC.swift
//  Learning
//
//  Created by clar3 on 21/03/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import PinLayout
import Kingfisher
import Instructions

extension BookingModel: Keyable {
    static var keys: [KeyColorCode] = BookingState.allCases.map({ KeyColorCode($0.color, $0.description) })
}

class CalendarVC: UIViewController, APCalendarSource {
    private var calendar: APCalendar<BookingModel>!
    private let calendarWrap = UIView()
    var currentDate = Date().startOfMonth()
    private var cache: [Date: Month<BookingModel>] = [:]
    private weak var appointment: AppointmentVC?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.schemedBG()
        
        let openBooking: (BookingModel) -> Void = { [weak self] in
            self?.open(booking: $0)
        }
        
        var button: FlexButton?
        if APIClient.isClient {
            button = FlexButton()
            button?.tapped = { [weak self] _ in
                guard let self = self else { return }
                let dailyVC = DailyVC()
                let selected = self.calendar.selectedDate ?? Date()
                dailyVC.bookings = self.calendar.selectedEntries.filter({ $0.status != .CANCELLED })
                dailyVC.dayState.move(to: selected)
                dailyVC.didSelectEvent = { openBooking($0) }
                let ord = selected.ordinalString.split(separator: ",").map { String($0) }
                self.push(to: ord[0], from: ord[1], using: dailyVC)
            }
            button?.addTargetPinClosure { (pin) in
                pin.right()
            }
            button?.title = "Show Daily View"
        }
        
        calendarWrap.schemedBG()
        calendarWrap.layer.cornerRadius = 20
        calendarWrap.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        view.addSchemedView(calendarWrap)
        
        calendar = APCalendar<BookingModel>(entryClass: APCalendarEntryCell.self, button: button)
        calendar.delegate = self
        calendar.openEntry = { openBooking($0) }
        add(self.calendar!, anchor: calendarWrap)
        loadData(for: currentDate, inForeground: false)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if self.calendar.months.count == 0 {
            loadData(for: currentDate, inForeground: true)
        }
        Onboarder.check.showOnboarding(for: .calendar, atVC: self)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        calendarWrap.pin.top(safeAreaInsets).horizontally().bottom(view.pin.safeArea)
        calendar.view.pin.vertically().horizontally(10).marginTop(20)
    }
    
    func refresh(_ control: UIRefreshControl) {
        loadData(for: currentDate, useCache: false) { [weak control] in
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
                control?.endRefreshing()
            }
        }
    }
    
    func refresh(selecting: BookingModel) {
        refresh(selecting: selecting.date.day)
    }
    
    func refresh(selecting day: Int) {
        loadData(for: currentDate, useCache: false) { [weak self] in
            self?.calendar.select(day: day)
        }
    }
    
    func open(booking object: BookingModel, action: BookingActions? = nil) {
        let selected = self.calendar.selectedDate ?? Date()
        let ord = selected.ordinalString.split(separator: ",").map { String($0) }
        
        if let vc = self.appointment {
            vc.load(booking: object, action: action)
        } else {
            let vc = AppointmentVC()
            vc.load(booking: object, action: action)
            vc.calendar = self
            self.appointment = vc
            self.push(to: "Appointment", from: ord[1], using: vc)
        }
    }
    
    func open(booking: Int, action: BookingActions? = nil) {
        Message.notify(
            process: APIClient.api.get(booking: GetModelRequest(id: booking)),
            loadingMessage: "loading...",
            successMessage: action == nil ? "Loaded appointment" : nil,
            errorMessage: "Error loading appointment"
        ) { [weak self] (res) in
            guard let self = self else { return }
            if case .success(let model) = res {

                // otherwise update the calendar
                self.currentDate = model.date.startOfMonth()
                self.loadData(for: self.currentDate, useCache: false, inForeground: action == nil) { [weak self] in
                    self?.open(booking: model, action: action)
                }
            }
            #if DEBUG
            if case .failure(_) = res, AppDelegate.uiTestingBooking {
                self.loadData(for: self.currentDate, useCache: false, inForeground: action == nil) { [weak self] in
                    guard
                        let self = self,
                        let model = self.cache[self.currentDate]?.days.first?.appointments.first
                    else { return }
                    self.open(booking: model, action: action)
                }
            }
            #endif
        }
    }
    
    func nextMonth() {
        currentDate = currentDate.adding(.month, value: 1)
        loadData(for: currentDate)
    }
    
    func previousMonth() {
        currentDate = currentDate.adding(.month, value: -1)
        loadData(for: currentDate)
    }
    
    func reload(_ booking: BookingModel) {
        Message.notify(
            process: APIClient.api.get(booking: GetModelRequest(id: booking.id)),
            loadingMessage: "syncing...",
            successMessage: nil,
            errorMessage: "failed to sync."
        ) { [weak self] (res) in
            if case .success(let model) = res {
                self?.appointment?.load(booking: model)
            }
        }
    }
    
    func loadData(for date: Date, useCache: Bool = true, inForeground: Bool = true, _ completion: Completion? = nil) {
        let currentMonth = Calendar.autoupdatingCurrent.component(.month, from: date)
        let currentYear = Calendar.autoupdatingCurrent.component(.year, from: date)
        let onModel = { [weak self] (model: [BookingModel]) in
            guard let self = self else { return }
            var days = [Int: [BookingModel]]()
            for booking in model {
                let day = Calendar.autoupdatingCurrent.component(.day, from: booking.date)
                if days[day] == nil {
                    days[day] = []
                }
                days[day]?.append(booking)
            }
            let month = Month<BookingModel>(
                month: currentMonth,
                year: currentYear,
                appointments: days.map({
                    Day<BookingModel>(day: $0.key, appointments: $0.value)
                })
            )
            self.cache[date] = month
            self.calendar.months = [month]
        }
        
        if useCache, let cached = self.cache[date] {
            self.calendar.months = [cached]
            return
        }

        Message.notify(
            process: APIClient.api.getUserDiary(for: GetUserDiary(month: currentMonth, year: currentYear)),
            loadingMessage: .loadingAppointments,
            successMessage: "Loaded appointments.",
            errorMessage: "Error loading appointments",
            inForeground: inForeground) { (result) in
                switch result {
                case .failure(let error): Log("encountered \(error)")
                case .success(let model): onModel(model)
                }
                completion?()
        }
    }
}


extension CalendarVC: OnboardingExperience {
    func setup() {
        calendar.set(override: Date().day)
    }
    
    var messages: [(String, UIView, CoachMarkArrowOrientation)] {
        let v = calendar.collectionView.visibleCells.min(by: {
            $0.frame.minX < $1.frame.minX &&
            $0.frame.minY < $1.frame.minY
        }) ?? calendar.collectionView
        let b = calendar.buttonView ?? calendar.collectionView
        return [
            (Talk.it.calendarHelp, v, .top),
            (Talk.it.dailyViewHelp, b, .top),
        ]
    }
    
    func willTransition(to index: Int) {}
}

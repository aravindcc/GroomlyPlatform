//
//  DayDiary.swift
//  Learning
//
//  Created by clar3 on 25/03/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import CalendarKit
import PinLayout
import DateToolsSwift

protocol TimeBlock {
    var start: Date {get}
    var end: Date {get}
    init(start: Date, end: Date)
}

struct DiaryBlock: Equatable, TimeBlock {
    var start: Date
    var end: Date
}

struct DiaryEntry {
    var start: Date
    var end: Date
    var person: String
    var address: String
    var order: String
}

class AppointmentView: EventView {
    private let separator: UIView = {
        let layer = UIView()
        return layer
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
//        layer.cornerRadius = 5
//        layer.masksToBounds = true
//        clipsToBounds = true
        addSchemedView(separator)
        bringSubviewToFront(deleteEventHandle)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateWithDescriptor(event: EventDescriptor) {
        super.updateWithDescriptor(event: event)
        separator.backgroundColor = event.color
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        separator.pin.vertically().left().width(5)
    }
}

class DayDiary {
    var unavailable: [DiaryBlock] = [] {
        didSet {
            unavailable = unavailable.merged()
        }
    }
    var entries: [DiaryEntry] = []
    var adding: DiaryEntry?
    
    func dailyFullBlockTransform(_ block: DiaryBlock) -> EventDescriptor {
        let event = Event()
        event.startDate = block.start
        event.endDate   = block.end
        event.color = ColorScheme.normal.highlightColorA
        event.backgroundColor = ColorScheme.normal.highlightColorA.withAlphaComponent(0.3)
        event.zPosition = 1
        event.selectable = false
        return event
    }
    
    func dailyAvailEditTransform(_ block: DiaryBlock) -> EventDescriptor {
        let event = dailyFullBlockTransform(block)
        event.selectable = true
        return event
    }
    
    func dailyFullBlockTransform(_ block: DiaryEntry) -> EventDescriptor {
        let event = Event()
        event.startDate = block.start
        event.endDate   = block.end
        event.color = ColorScheme.normal.highlightColorB
        event.textColor = ColorScheme.normal.primaryTextColor
        event.backgroundColor = ColorScheme.normal.darkShadow
        event.zPosition = 2
        event.renderClass = AppointmentView.self
        event.renderInit = { AppointmentView.init(frame: .zero) }
        let info = [block.person, block.address, block.order]
        event.text = info.reduce("", {$0 + $1 + "\n"})
        event.selectable = false
        return event
    }
    
    func timeFullyBlockTransform(_ block: DiaryEntry) -> EventDescriptor {
        let event = dailyFullBlockTransform(block)
        event.selectable = true
        event.resizeable = false
        return event
    }
    
    func events() -> [EventDescriptor] {
        var output = [EventDescriptor]()
        output += unavailable.map(dailyFullBlockTransform)
        output += entries.map(dailyFullBlockTransform)
        if let adding = adding {
            output.append(timeFullyBlockTransform(adding))
        }
        return output
    }
    
    func availability() -> [EventDescriptor] {
        return unavailable.map(dailyAvailEditTransform)
    }
    
    func freeSpace(for block: DiaryBlock) -> Bool {
        for event in unavailable {
            if block.datePeriod.overlaps(with: event.datePeriod) {
                return false
            }
        }
        return true
    }
    
}

extension Array where Element: TimeBlock {
    var timeString: String {
        count == 0 ? "Off day" : compactMap({ $0.timeString }).joined(separator: ", ")
    }
    var timeDisplayString: String {
        count == 0 ? "Off day" : compactMap({ $0.timeString }).joined(separator: ",\n")
    }
    var attributedTimeDisplayString: NSAttributedString {
        count == 0 ?
            NSAttributedString(string: "Off day", attributes: [.font: UIFont.monsterFont(size: 15, weight: .bold) ]) :
        map({ $0.attritbTimeString }).reduce(NSMutableAttributedString(), { (x: NSMutableAttributedString, y: NSAttributedString)
            -> NSMutableAttributedString in
            if (x.length != 0) {
                x.append(NSAttributedString(string: "\n"))
            }
            x.append(y)
            return x
        })
    }
}
 

extension TimeBlock {
    var timeString: String? {
        let tf = DateFormatter.appTime
        let t1 = tf.string(from: start).lowercased()
        let t2 = tf.string(from: end).lowercased()
        return "\(t1) - \(t2)"
    }
    var attritbTimeString: NSAttributedString {
        let tf = DateFormatter.appTime
        let t1 = tf.string(from: start).lowercased()
        let t2 = tf.string(from: end).lowercased()
        let result = NSMutableAttributedString()
        
        result.append(
            NSAttributedString(string: t1, attributes: [
                NSAttributedString.Key.font: UIFont.monsterFont(size: 15, weight: .bold),
                NSAttributedString.Key.foregroundColor: ColorScheme.normal.primaryTextColor,
            ])
        )
        result.append(
            NSAttributedString(string: "   to   ", attributes: [
                NSAttributedString.Key.font: UIFont.monsterFont(size: 15, weight: .regular),
                NSAttributedString.Key.foregroundColor: ColorScheme.normal.secondaryTextColor,
            ])
        )
        result.append(
            NSAttributedString(string: t2, attributes: [
                NSAttributedString.Key.font: UIFont.monsterFont(size: 15, weight: .bold),
                NSAttributedString.Key.foregroundColor: ColorScheme.normal.primaryTextColor,
            ])
        )
        return result
    }
    
    func intersection(with other: DiaryBlock) -> DiaryBlock {
        let s = max(self.start, other.start)
        let e = min(self.end, other.end)
        return DiaryBlock(start: s, end: e)
    }
    
    var datePeriod: TimePeriod {
        return TimePeriod(beginning: start, end: end)
    }
    
    func overlaps(with event: TimeBlock) -> Bool {
        return datePeriod.overlaps(with: event.datePeriod)
    }
}


extension Array where Element: TimeBlock {
    private func getOverlaps() -> [[Element]]{
        // only non allDay events need their frames to be set
        let sortedEvents = sorted { (attr1, attr2) -> Bool in
            let start1 = attr1.start
            let start2 = attr2.start
            return start1.isEarlier(than: start2)
        }
        
        var groupsOfEvents = [[Element]]()
        var overlappingEvents = [Element]()
        
        for event in sortedEvents {
            if overlappingEvents.isEmpty {
                overlappingEvents.append(event)
                continue
            }
            
            let longestEvent = overlappingEvents.sorted { (attr1, attr2) -> Bool in
                let period1 = attr1.datePeriod.seconds
                let period2 = attr2.datePeriod.seconds
                return period1 > period2
            }
            .first!
            
            let lastEvent = overlappingEvents.last!
            if longestEvent.overlaps(with: event) ||
                lastEvent.overlaps(with: event) {
                overlappingEvents.append(event)
                continue
            }
            
            groupsOfEvents.append(overlappingEvents)
            overlappingEvents = [event]
        }
        
        groupsOfEvents.append(overlappingEvents)
        overlappingEvents.removeAll()
        return groupsOfEvents
    }
    
    func merged() -> [Element] {
        guard count > 0 else { return [] }
        let groupsOfEvents = getOverlaps()
        var output = [Element]()
        for overlappingEvents in groupsOfEvents {
            let start = overlappingEvents.reduce(Date.distantFuture, { (date, event) -> Date in
                return date.isEarlier(than: event.start) ? date : event.start
            })
            let end = overlappingEvents.reduce(Date.distantPast, { (date, event) -> Date in
                return date.isLater(than: event.end) ? date : event.end
            })
            output.append(Element(start: start, end: end))
        }
        return output
    }
}

//
//  DailyVC.swift
//  Learning
//
//  Created by clar3 on 24/03/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import PinLayout
import CalendarKit
import DateToolsSwift
import Async

protocol DefaultAvailabilityHandler {
    var defaultResponse: DefaultAvailability { get }
    func onSync(response: DefaultAvailabilityResponse) -> Void
}

class DailyVC: UIViewController {
    private let day = DayDiary()
    private let calendar = Calendar.autoupdatingCurrent
    private var timelinePagerView: TimelinePagerView!
    private var editButton: FlexButton!
    private var helpText: UILabel?
    var dayState: DayViewState!
    private var selectedEvent: EventDescriptor?
    private var selectingSpawned = false
    var didSelectEvent: ((BookingModel) -> Void)?
    var bookings: [BookingModel] = [] {
        didSet {
            self.day.entries = bookings.map {
                DiaryEntry(
                    start: $0.startTime!,
                    end: $0.endTime!,
                    person: $0.displayName,
                    address: $0.location,
                    order: $0.services.compactMap({ $0.service?.name ?? nil }).joined(separator: ", ")
                )
            }
        }
    }
    var helpTextStr: String? {
        didSet {
            if helpText == nil {
                helpText = UILabel(central: 15, weight: .bold)
                helpText?.singleCentralLine()
                helpText?.schemedTextColor(.primaryTextColor)
                view.addSubview(helpText!)
            }
            helpText?.text = helpTextStr
        }
    }
    
    var editingAvailability: Bool = false {
        didSet {
            editButton.title = editingAvailability ? "done" : "edit availability"
            helpTextStr = editingAvailability ? "Tap and drag to add when you are available" : ""
            timelinePagerView.reloadData()
        }
    }
    
    private var defaultHandle: DefaultAvailabilityHandler?
    init(forDefault defaultHandle: DefaultAvailabilityHandler? = nil) {
        super.init(nibName: nil, bundle: nil)
        self.defaultHandle = defaultHandle
        dayState = DayViewState(date: Date(), calendar: calendar)
        dayState.subscribe(client: self)
        timelinePagerView = TimelinePagerView(calendar: calendar, withSingle: true)
        timelinePagerView.state = dayState
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    private var containerWrap: UIView?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        helpTextStr = "Tap and drag to add when you are available"
        
        if self.defaultHandle == nil {
            containerWrap = UIView()
            containerWrap!.schemedBG()
            containerWrap!.layer.cornerRadius = 20
            containerWrap!.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
            containerWrap!.layer.masksToBounds = true
            view.addSchemedView(containerWrap!)
        }
        let container: UIView = self.defaultHandle == nil ? containerWrap! : view
        
        editButton = FlexButton()
        editButton.tapped = { [weak self] _ in
            guard let self = self else { return }
            if self.editingAvailability {
                self.timelinePagerDidTapToEndEdit(event: nil)
                self.triggerSync()
            }
            
            if self.defaultHandle == nil {
                self.editingAvailability = !self.editingAvailability
            }
        }
        editButton.addTargetPinClosure { (pin) in
            pin.hCenter()
        }
        container.addSchemedView(editButton)
        
        let getCalendarStyle = { (chosenScheme: ColorScheme) -> TimelineStyle in
            let schemeCase = chosenScheme.getCase() ?? .normal
            return schemeCase == .normal ? TimelineStyle.normal() : TimelineStyle.alt()
        }
        timelinePagerView.updateStyle(getCalendarStyle(view.colorScheme!))
        timelinePagerView.updateStyle(TimelineStyle.normal())

        view.schemedBG()
        container.addSchemedView(timelinePagerView)
        
        timelinePagerView.dataSource = self
        timelinePagerView.delegate = self
        editingAvailability = defaultHandle != nil
        
        if let schemeCase = view.colorScheme?.getCase() {
            let closure = { [weak self] (color: UIColor) -> Bool in
                guard let self = self else { return false }
                self.timelinePagerView.updateStyle(getCalendarStyle(self.view.colorScheme!))
                self.timelinePagerView.reloadData()
                return true
            }
            view.reschemer.add(rule: closure)
            ColorConfigurator.shared.register(interestFor: schemeCase, andProperties: [
                .bg, .primaryTextColor, .secondaryTextColor,
                .lightShadow, .darkShadow
            ], withUpdator: closure)
        }
    }
    
    func loadData(for date: Date) {
        let onBlocks = { [weak self] (blocks: [AvailableBlockModel]) in
            guard let self = self else { return }
            self.day.unavailable = WorkWeekView.getComplementPeriods(from: blocks).map {
                DiaryBlock(
                    start: date.setTo(time: $0.start)!,
                    end: date.setTo(time: $0.end)!
                )
            }
            self.timelinePagerView.reloadData()
        }
        
        if let defaultResponse = defaultHandle?.defaultResponse {
            onBlocks(defaultResponse.blocks)
        } else {
            let getAvail = GetClientAvailability(
                clientID: 0, date: date
            )
            Message.notify(
                process: APIClient.api.getClient(availability: getAvail),
                loadingMessage: "loading...",
                successMessage: nil,
                errorMessage: "failed to load"
            ) { (result) in
                if case .success(let (availability)) = result {
                    onBlocks(availability.blocks)
                }
            }
        }
    }
    
    func triggerSync() {
        if let defaultHandle = defaultHandle {
            let update = DefaultAvailability(
                day: defaultHandle.defaultResponse.day,
                blocks: WorkWeekView.getComplementPeriods(from: day.unavailable.map {
                    AvailableBlockModel(start: $0.start, end: $0.end)
                })
            )
            Message.notify(
                process: APIClient.api.saveClientDefault(availability: update),
                loadingMessage: "saving...",
                successMessage: nil,
                errorMessage: "failed to save"
            ) { (result) in
                if case .success(let model) = result {
                    defaultHandle.onSync(response: model)
                }
            }
            return
        }
        
        let date = dayState.selectedDate
        let update = UpdateClientAvailability(
            date: date,
            blocks: WorkWeekView.getComplementPeriods(from: day.unavailable.map {
                AvailableBlockModel(start: $0.start, end: $0.end)
            })
        )
        Message.notify(
            process: APIClient.api.saveClient(availability: update),
            loadingMessage: "saving...",
            successMessage: nil,
            errorMessage: "failed to save"
        ) { [weak self] (result) in
            guard let self = self else { return }
            if case .success(let model) = result {
                self.day.unavailable = WorkWeekView.getComplementPeriods(from: model.blocks).map {
                    DiaryBlock(
                        start: date.setTo(time: $0.start)!,
                        end: date.setTo(time: $0.end)!
                    )
                }
                self.timelinePagerView.reloadData()
            }
        }
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        containerWrap?.pin.top(safeAreaInsets).horizontally().bottom(view.pin.safeArea)
        let padding: CGFloat = 20
        editButton.pin.bottom((containerWrap == nil ? view.pin.safeArea.bottom : 15) + padding).height(35)
        if let helpText = helpText {
            helpText.pin
                .above(of: editButton)
                .marginBottom(padding)
                .horizontally()
                .height(helpTextStr == "" ? 0 : helpText.intrinsicContentSize.height * 1.1)
        }
        timelinePagerView.pin.left(-10).right().top(containerWrap == nil ? view.pin.safeArea.top : 5).above(of: helpText ?? editButton).marginBottom(padding)
    }
}

extension DailyVC: EventDataSource, DayViewStateUpdating {
    func eventsForDate(_ date: Date) -> [EventDescriptor] {
        return editingAvailability ? day.availability() : day.events()
    }
    func move(from oldDate: Date, to newDate: Date) {
        loadData(for: newDate)
    }
}

extension DailyVC: TimelinePagerViewDelegate {
    
    func timelinePagerDidSelectEventView(_ eventView: EventView) {
        guard let descriptor = eventView.descriptor as? Event, !selectingSpawned else {
            return
        }
        if editingAvailability {
            edit(eventView)
            return
        }
        
        if let event = day.entries.filter({ $0.start == descriptor.startDate }).first {
            Log("Event has been selected: \(descriptor) \(String(describing: event))")
            if let model = bookings.first(where: { $0.startTime! == event.start }) {
                didSelectEvent?(model)
            }
        }
    }
    
    private func edit(_ eventView: EventView) {
        guard let descriptor = eventView.descriptor as? Event else {
            return
        }
        
        timelinePagerView.endEventEditing()
        Log("Event has been longPressed: \(descriptor) \(String(describing: descriptor.userInfo))")
        selectedEvent = eventView.descriptor
        eventView.removeFromSuperview()
        timelinePagerView.beginEditing(event: descriptor, animated: true)
        Log(Date())
    }
    
    static func generateEventNearDate(_ input: Date, duration: Int = 60) -> DiaryBlock {
        let startDate = input.nearestFifteenMinutes
        let duration = TimeChunk.dateComponents(minutes: duration)
        let datePeriod = TimePeriod(beginning: startDate, chunk: duration)
        if input.endOfDay < datePeriod.end! {
            let endPeriod = TimePeriod(end: input.endOfDay, chunk: duration).beginning!
            return DiaryBlock(start: endPeriod, end: input.endOfDay)
        } else {
            return DiaryBlock(start: datePeriod.beginning!, end: datePeriod.end!)
        }
    }
    
    func timelinePager(timelinePager: TimelinePagerView, didTapTimelineAt date: Date) {
        guard editingAvailability, dayState.selectedDate.isSameDay(date: date), !selectingSpawned else { return }
        // Cancel editing current event and start creating a new one
        timelinePager.endEventEditing()
        let event = DailyVC.generateEventNearDate(date)
        Log("SPAWNING \(event.start) \(event.end)")
        day.unavailable.append(event)
        timelinePager.reloadData()
        selectingSpawned = true
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [weak self] in
            guard let self = self else { return }
            self.selectingSpawned = false
            if let eventView = self.timelinePagerView.findEventView(for: self.day.dailyAvailEditTransform(event)) {
                self.edit(eventView)
            }
        }
    }

    func timelinePagerDidTapToEndEdit(event: EventDescriptor?) {
        timelinePagerView.endEventEditing()
    }
    
    func timelinePager(timelinePager: TimelinePagerView, didUpdate event: EventDescriptor) {
        if let original = event.editedEvent {
            guard
                original.startDate.isSameDay(date: event.startDate) &&
                original.endDate.isSameDay(date: event.endDate)
            else {
                timelinePager.reloadData()
                return
            }
            day.unavailable = day.unavailable.filter { !($0.start == original.startDate && $0.end == original.endDate)  }
            let block = DiaryBlock(
                start: event.startDate,
                end: min(event.endDate, event.startDate.endOfDay)
            )
            day.unavailable.append(block)
            timelinePager.reloadData()
        }
    }
    
    func timelinePager(timelinePager: TimelinePagerView, didEditDelete desc: EventDescriptor) {
        guard let selectedEvent = selectedEvent else { return }
        day.unavailable = day.unavailable.filter { !($0.start == selectedEvent.startDate && $0.end == selectedEvent.endDate)  }
        timelinePager.reloadData()
        self.selectedEvent = nil
    }
    
    func timelinePagerDidLongPressEventView(_ eventView: EventView) {}
    func timelinePager(timelinePager: TimelinePagerView, didLongPressTimelineAt date: Date) {}
    func timelinePagerDidBeginDragging(timelinePager: TimelinePagerView) {}
    func timelinePager(timelinePager: TimelinePagerView, willMoveTo date: Date) {}
    func timelinePager(timelinePager: TimelinePagerView, didMoveTo date: Date) {}
}

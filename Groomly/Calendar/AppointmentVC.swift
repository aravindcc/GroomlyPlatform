//
//  AppointmentVC.swift
//  Groomly
//
//  Created by clar3 on 02/05/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import PinLayout
import FlexLayout
import Cosmos

extension Float {
    var sterling: String {
        String(format: "\(sign == .plus ? "" : "-")£%.2f", abs(self))
    }
}

class AppointmentVC: SubProcessVC {
    
    private let contentWrap = UIView()
    private let contentView = UIScrollView()
    private let containerView = UIView()
    private let descContainer = UIView()
    private let statusButton = FlatButton()
    private var statusButtonFlex: Flex?
    private let dateLabel = MaskedLabel(left: 23, weight: .semibold, color: .black)
    private let timeLabel = UILabel(left: 18, weight: .bold)
    private let nameLabel = UILabel(left: 18, weight: .regular)
    private let phoneLabel = UILabel(left: 18, weight: .regular)
    private let locationLabel = UILabel(left: 16, weight: .regular)
    private var descriptionFlex: Flex?
    private let orderContainer = UIView()
    private var orderFlex: Flex?
    private let optionView = UIView()
    private var options = [ButtonConfig]()
    private var basket: APBasket?
    private var fees: FeeModel?
    private var booking: BookingModel?
    private let stars: CosmosView = CosmosView.normal()
    weak var calendar: CalendarVC?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.colorScheme = .normal
        view.schemedBG()
        
        let descriptionLabel = UILabel(left: 19, weight: .regular)
        descriptionLabel.numberOfLines = 0
        descriptionLabel.accessibilityIdentifier = "stateExplanation"
        
        timeLabel.schemedTextColor(.highlightColorB)
        nameLabel.schemedTextColor(.primaryTextColor)
        phoneLabel.schemedTextColor(.primaryTextColor, alpha: 0.75)
        descriptionLabel.schemedTextColor(.primaryTextColor)
        locationLabel.schemedTextColor(.primaryTextColor, alpha: 0.6)
        dateLabel.addACGradient()
        
        basket = APBasket(nil, type: .receipt)
        containerView.flex
            .alignItems(.stretch)
            .marginHorizontal(5%)
            .define { (flex) in
            flex.addItem(descContainer)
            flex.addItem(stars)
            orderFlex = flex.addItem(orderContainer).height(100).marginVertical(15)
            descriptionFlex = flex.addItem(descriptionLabel)
            flex.addItem(optionView).height(35).marginTop(15).marginBottom(40)
        }
        
        add(basket!, anchor: orderContainer)
        
        statusButton.enabled = false
        statusButton.alpha = 0.85
        statusButton.defaultPx = 10
        statusButton.accessibilityIdentifier = "bookingState"
        
        optionView.flex.direction(.row).alignItems(.stretch).justifyContent(.spaceAround)
        
        stars.flex.height(0)
        stars.isHidden = true
        stars.settings.updateOnTouch = false
        
        contentView.layer.masksToBounds = false
        contentWrap.layer.masksToBounds = true
        contentWrap.addSchemedView(contentView)
        contentView.addSchemedView(containerView)
        
        contentWrap.schemedBG()
        contentWrap.layer.cornerRadius = 20
        contentWrap.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        view.addSubview(contentWrap)
    }

    func load(booking: BookingModel, action: BookingActions? = nil) {
        let loadBooking = { [weak self] (inputFee: FeeModel) in
            guard let self = self else { return }
            self.load(booking: booking, withFee: inputFee, withAction: action)
        }
        Message.notify(
            process: APIClient.api.getFee(for: GetModelRequest(id: booking.id)),
            loadingMessage: "loading...",
            successMessage: nil,
            errorMessage: "Error loading appointment.") {
                if case .success(let model) = $0 {
                    DispatchQueue.main.async {
                        loadBooking(model)
                    }
                }
        }
    }
    
    func load(booking: BookingModel, withFee inputFee: FeeModel, withAction action: BookingActions? = nil) {
        self.booking = booking
        
        if let score = APIClient.isClient ? booking.customer_score : booking.client_score {
            stars.flex.height(35)
            stars.isHidden = false
            stars.rating = Double(score)
        } else {
            stars.flex.height(0)
            stars.isHidden = true
        }
        
        let tf = DateFormatter.appShortTime
        nameLabel.text = booking.displayName
        phoneLabel.text = booking.displayPhone
        locationLabel.text = booking.location
        dateLabel.text = DateFormatter.appFullDate.string(from: booking.date)
        if let start = booking.startTime, let end = booking.endTime {
            timeLabel.text = "\(tf.string(from: start)) - \(tf.string(from: end))"
        } else {
            timeLabel.text = "n/a"
        }
        statusButton.title = "\(booking.status)"
        fees = inputFee
        
        descContainer.clear()
        descContainer.flex.alignItems(.stretch).define { (flex) in
            flex.addItem().direction(.row).define { (flex) in
                flex.addItem(dateLabel).grow(1)
                statusButtonFlex = flex.addItem(statusButton).width(100).alignSelf(.end).height(100%)
            }
            flex.addItem(timeLabel)
            flex.addItem(nameLabel).marginTop(10)
            flex.addItem(phoneLabel).marginTop(10)
            flex.addItem(locationLabel).marginBottom(10)
        }
        
        var feeWarning: String?
        if inputFee.current.count != 0, let (key, value) = inputFee.possible.first {
            feeWarning = "You will be charged a \(key) of \(value.sterling)."
        }

        options = BookingActions.actions(for: booking).map { state in
            ButtonConfig(title: state.rawValue) { [weak self] (_) in
                guard let self = self else { return }
                state.process(booking: booking, atVC: self, feeWarning: feeWarning).do { [weak self] in
                    // ask calendar to refresh and load in the booking into here.
                    self?.calendar?.reload(booking)
                }.catch { [weak self] error in
                    if case ActionError.detail = error {
                        DispatchQueue.main.async {
                        // hmmmm user notified so just dismiss
                            self?.navigationController?.popViewController(animated: true)
                        }
                    }
                }
            }
        }
        
        if let descriptionLabel = descriptionFlex?.view as? UILabel {
            let txt = booking.status.explanation(inputFee, rejectReason: booking.reject_reason)
            var rct = descriptionLabel.frame
            rct.origin = .zero
            rct.size.height = UIScreen.main.bounds.height
            let size = txt.calculateSize(ofFontSize: descriptionLabel.font.pointSize, weight: .light, inFrame: rct, andInsets: .zero)
            descriptionLabel.text = txt
            descriptionFlex?.height(size.height)
        }
        
        optionView.clear()
        optionView.flex.define { (flex) in
            for option in options {
                let but = FlexButton()
                but.title = option.title
                but.tapped = option.action
                but.preset = true
                flex.addItem(but).width(but.wrappedWidth)
            }
        }

        statusButtonFlex?.width(statusButton.wrappedWidth)
        statusButtonFlex?.view?.setNeedsDisplay()

        let order = booking.order
        if let fees = self.fees {
            if booking.status == .CANCELLED {
                order.extraFees = [:]
            } else {
                order.extraFees = fees.current.count == 0 ? fees.possible : fees.current
            }
        }
        self.basket?.model = order
        self.orderContainer.isHidden = booking.status == .CANCELLED
        view.setNeedsLayout()
        
        guard let action = action else { return }
        if BookingActions.actions(for: booking).contains(action) {
            // absorb do
            let _ = action.process(booking: booking, atVC: self).do { [weak self] in
                // ask calendar to refresh and load in the booking into here.
                self?.calendar?.reload(booking)
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        contentWrap.pin.horizontally().top(safeAreaInsets).bottom(view.pin.safeArea)
        contentView.pin.all()
            .marginTop(20)
            .marginBottom(view.pin.safeArea.bottom + 10)
        containerView.pin.all()
        if let basket = basket {
            if booking?.status == .CANCELLED {
                orderFlex?.height(0)
            } else {
                var fr = containerView.frame.size
                fr.height = .infinity
                orderFlex?.height(basket.maxHeight(inMaxFrame: fr))
            }
        }
        containerView.flex.layout(mode: .adjustHeight)
        descContainer.flex.layout()
        optionView.flex.layout()
        contentView.contentSize = containerView.frame.size
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let booking = booking {
            calendar?.refresh(selecting: booking)
        }
    }
}

//
//  APCalendarEntrySectionController.swift
//  Learning
//
//  Created by clar3 on 21/03/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import IGListKit

final class APCalendarEntrySectionController: ListSectionController {

    weak var delegate: APCalendarDelegate?
    private var object: AnyObject?
    private var entryClass: UICollectionViewCell.Type?
    
    init(_ delegate: APCalendarDelegate?,_ entryClass: UICollectionViewCell.Type?) {
        self.delegate = delegate
        self.entryClass = entryClass
    }
    
    override func numberOfItems() -> Int {
        return 1
    }
    
    override func sizeForItem(at index: Int) -> CGSize {
        guard let width = collectionContext?.containerSize.width else { return .zero }
        return CGSize(width: width, height: (object as? KeyColorCode) != nil ? 40.0 : 80.0)
    }

    override func cellForItem(at index: Int) -> UICollectionViewCell {
        var classType = entryClass ?? APCalendarEntryCell.self
        if (object as? KeyColorCode) != nil {
            classType = APCalendarKeyCell.self
        }
        guard   let cell = schemedReusableCell(of: classType, for: self, at: index) as? UICollectionViewCell & ListBindable,
                let object = object
        else { fatalError() }
        cell.bindViewModel(object)
        return cell
    }

    override func didUpdate(to object: Any) {
        self.object = object as AnyObject
    }

    override func didSelectItem(at index: Int) {
        guard let object = object else { return }
        delegate?.selectedEntry(object)
    }
}


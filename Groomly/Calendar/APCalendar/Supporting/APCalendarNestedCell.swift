//
//  APCalendarNestedCell.swift
//  Learning
//
//  Created by clar3 on 21/03/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import IGListKit
import PinLayout

final class APCalendarNestedCell: APCalendarCell {
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
        view.backgroundColor = .clear
        view.alwaysBounceHorizontal = false
        self.contentView.addSchemedView(view)
        return view
    }()

    override func layoutSubviews() {
        super.layoutSubviews()
        //contentView.superview?.pin.bottom()
        contentView.pin.all()
        collectionView.pin.all()
    }
}

extension APCalendarNestedCell: ListBindable {
    func bindViewModel(_ viewModel: Any) {
        return
    }
}

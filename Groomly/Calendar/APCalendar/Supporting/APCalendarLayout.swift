//
//  APCalendarLayout.swift
//  Learning
//
//  Created by clar3 on 21/03/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit

class APCalendarLayout: UICollectionViewFlowLayout {

    private var cache: [IndexPath: UICollectionViewLayoutAttributes] = [:]
    
    private var contentWidth: CGFloat {
        guard let collectionView = collectionView else {
            return 0
        }
        let insets = collectionView.contentInset
        return collectionView.bounds.width - (insets.left + insets.right)
    }
    
    private var contentHeight: CGFloat {
        guard let collectionView = collectionView else {
            return 0
        }
        let insets = collectionView.contentInset
        return collectionView.bounds.height - (insets.top + insets.bottom)
    }

    override var collectionViewContentSize: CGSize {
        return CGSize(width: contentWidth, height: contentHeight)
    }
  
    override func prepare() {
        super.prepare()
        guard
            let collectionView = collectionView,
            collectionView.numberOfSections > 0
        else { return }

        cache.removeAll()
        
        let num_items = collectionView.numberOfItems(inSection: 0)
        let indexPath = IndexPath(item: num_items - 1, section: 0)
        if let lastAttrib = super.layoutAttributesForItem(at: indexPath) {
            var frame = lastAttrib.frame
            frame.size.height = contentHeight - frame.minY
            let attrib = UICollectionViewLayoutAttributes(forCellWith: indexPath)
            attrib.frame = frame
            cache[indexPath] = attrib
        }
    }
  
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var visibleLayoutAttributes: [UICollectionViewLayoutAttributes] = super.layoutAttributesForElements(in: rect) ?? []
        
        for (_, myAttrib) in cache {
            // Loop through the cache and replace mine
            var replaced = false
            for (index, attrib) in visibleLayoutAttributes.enumerated() {
                if  attrib.frame.minY == myAttrib.frame.minY,
                    attrib.frame.minX == myAttrib.frame.minX,
                    attrib.frame.width == myAttrib.frame.width {
                    visibleLayoutAttributes[index] = myAttrib
                    replaced = true
                }
            }
            if !replaced, myAttrib.frame.intersects(rect) {
                visibleLayoutAttributes.append(myAttrib)
            }
        }
        return visibleLayoutAttributes
    }
  
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return cache[indexPath] ?? super.layoutAttributesForItem(at: indexPath)
    }
}


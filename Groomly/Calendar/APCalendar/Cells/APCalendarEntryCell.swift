//
//  APCalendarEntryCell.swift
//  Learning
//
//  Created by clar3 on 21/03/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import PinLayout
import IGListKit

final class APCalendarEntryCell: APCalendarCell {

    fileprivate let containerView = UIView()
    
    fileprivate let label: UILabel = {
        return UILabel(left: 13, weight: .regular)
    }()
    
    fileprivate let startLabel: UILabel = {
        return UILabel(central: 12, weight: .bold)
    }()
    
    fileprivate let endLabel: UILabel = {
        return UILabel(central: 12, weight: .semibold)
    }()

    let separator: UIView = UIView()
    let separator2: UIView = UIView()

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        label.schemedTextColor(.primaryTextColor)
        startLabel.schemedTextColor(.secondaryTextColor, alpha: 0.8)
        endLabel.schemedTextColor(.secondaryTextColor, alpha: 0.5)
        
        containerView.layer.cornerRadius = 20
        containerView.layer.masksToBounds = true
        containerView.schemedBG(.lightShadow)
    
        containerView.addSchemedView(separator)
        containerView.addSchemedView(separator2)
        containerView.addSchemedView(startLabel)
        containerView.addSchemedView(endLabel)
        containerView.addSchemedView(label)
        contentView.addSchemedView(containerView)
        
        backgroundView = nil
        backgroundColor = UIColor.clear
        containerView.backgroundColor = UIColor.clear
        contentView.backgroundColor = UIColor.clear
        
        separator.schemedBG()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
    
        containerView.pin.all().marginVertical(10)
        startLabel.singleCentralLine()
        endLabel.singleCentralLine()

        let maxWidth = "00:00".calculateSize(
            ofFontSize: startLabel.font.pointSize,
            weight: .heavy,
            inFrame: .infinite,
            andInsets: .zero
        ).width + 10
        
        startLabel.pin.left(10).top(2).width(maxWidth).height(50%)
        endLabel.pin.left(10).bottom(8).width(maxWidth).height(50%)
        
        separator.pin.vertically().right(of: startLabel).width(1)
        label.pin.vertically().right().right(of: separator).marginLeft(15)
        separator2.pin.horizontally().bottom().height(8)
        
        
    }

    override var isHighlighted: Bool {
        didSet {
            contentView.backgroundColor = UIColor.clear
        }
    }

}

extension APCalendarEntryCell: ListBindable {

    func bindViewModel(_ viewModel: Any) {
        guard
            let viewModel = viewModel as? BookingModel,
            let startTime = viewModel.startTime,
            let endTime = viewModel.endTime
        else { return }
        let tf = DateFormatter.appShortTime
        label.text = viewModel.displayName
        startLabel.text = tf.string(from: startTime)
        endLabel.text = tf.string(from: endTime)
        separator2.backgroundColor = viewModel.status.color
        layoutSubviews()
    }

}

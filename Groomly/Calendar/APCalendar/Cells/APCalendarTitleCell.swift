//
//  APCalendarTitleCell.swift
//  Learning
//
//  Created by clar3 on 21/03/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import PinLayout
import IGListKit

final class APCalendarTitleCell: APCalendarCell {
    
    fileprivate lazy var label: UILabel = {
        let view = UILabel()
        view.backgroundColor = .clear
        view.textAlignment = .left
        view.font = .boldSystemFont(ofSize: 18)
        self.contentView.addSchemedView(view)
        return view
    }()
    
    private let sized = { (img: String, height: CGFloat) -> UIView  in
        if let image = UIImage(systemName: img) {
            let aspect = image.size.width / image.size.height
            let width = aspect * height
            let v = UIImageView()
            v.size = CGSize(width: width, height: height)
            v.image = image
            return v
        }
        return UIView()
    }
    
    fileprivate lazy var previousButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(previousMonth), for: .touchUpInside)
        button.addSubview(self.sized("arrow.left", 20))
        self.contentView.addSchemedView(button)
        return button
    }()
    
    fileprivate lazy var nextButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(nextMonth), for: .touchUpInside)
        button.addSubview(self.sized("arrow.right", 20))
        self.contentView.addSchemedView(button)
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        label.schemedTextColor(.highlightColorA)
        previousButton.schemedTintColor(.highlightColorA)
        nextButton.schemedTintColor(.highlightColorA)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc private func previousMonth() {
        delegate?.previousMonth()
    }
    
    @objc private func nextMonth() {
        delegate?.nextMonth()
    }

    var text: String? {
        get {
            return label.text
        }
        set {
            label.text = newValue
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        label.pin.sizeToFit(.content).left().vCenter().marginLeft(10)
        nextButton.pin.wrapContent(
            padding: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 0)
        ).right().vCenter().marginRight(15)
        previousButton.pin.wrapContent(
            padding: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 0)
        ).left(of: nextButton).vCenter().marginRight(10)
    }

}

extension APCalendarTitleCell: ListBindable {

    func bindViewModel(_ viewModel: Any) {
        guard let viewModel = viewModel as? LabelViewModel else { return }
        label.text = viewModel.name.capitalized
        layoutSubviews()
    }

}

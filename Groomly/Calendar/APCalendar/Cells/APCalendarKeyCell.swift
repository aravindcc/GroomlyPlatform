//
//  APCalendarEntryCell.swift
//  Learning
//
//  Created by clar3 on 21/03/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import PinLayout
import IGListKit

final class APCalendarKeyCell: APCalendarCell {

    fileprivate let containerView = UIView()
    
    fileprivate let label: UILabel = {
        return UILabel(left: 15, weight: .regular)
    }()

    let separator: UIView = {
        let layer = UIView()
        return layer
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        label.schemedTextColor(.primaryTextColor)
        label.singleCentralLine()
        label.textAlignment = .center
        
        containerView.addSchemedView(separator)
        containerView.addSchemedView(label)
        contentView.addSchemedView(containerView)
        
        backgroundView = nil
        backgroundColor = UIColor.clear
        containerView.backgroundColor = UIColor.clear
        contentView.backgroundColor = UIColor.clear
        
        label.text = "0000000000"
        let w = label.intrinsicContentSize.width
        label.text = ""
        
        let d: CGFloat = 15
        containerView.flex
            .direction(.row)
            .alignItems(.center)
            .justifyContent(.center)
            .define { (flex) in
                flex.addItem(separator).width(d).height(d).marginRight(5)
                separator.layer.cornerRadius = d / 2
                flex.addItem(label).height(50).width(w)
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        containerView.pin.all().marginVertical(2)
        containerView.flex.layout()
    }

    override var isHighlighted: Bool {
        didSet {
            contentView.backgroundColor = UIColor.clear
        }
    }

}

extension APCalendarKeyCell: ListBindable {

    func bindViewModel(_ viewModel: Any) {
        guard
            let viewModel = viewModel as? KeyColorCode
        else { return }
        label.text = viewModel.desc
        separator.backgroundColor = viewModel.color
        layoutSubviews()
    }

}

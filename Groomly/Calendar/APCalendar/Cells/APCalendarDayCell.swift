//
//  APCalendarCells.swift
//  Learning
//
//  Created by clar3 on 21/03/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import IGListKit
import PinLayout

final class APCalendarDayCell: APCalendarCell {

    lazy fileprivate var label: UILabel = {
        let view = UILabel()
        view.backgroundColor = .clear
        view.textAlignment = .center
        view.font = .boldSystemFont(ofSize: 16)
        view.layer.borderWidth = 1
        view.clipsToBounds = true
        self.contentView.addSchemedView(view)
        return view
    }()

    lazy fileprivate var dotsLabel: UILabel = {
        let view = UILabel()
        view.backgroundColor = .clear
        view.textAlignment = .center
        self.contentView.addSchemedView(view)
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        label.schemedTextColor(.primaryTextColor)
        dotsLabel.schemedTextColor(.highlightColorA)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var text: String? {
        get {
            return label.text
        }
        set {
            label.text = newValue
        }
    }

    var dots: String? {
        get {
            return dotsLabel.text
        }
        set {
            dotsLabel.text = newValue
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        let bounds = contentView.bounds
        let half = bounds.height / 2
        label.pin.all()
        label.layer.cornerRadius = half
        dotsLabel.pin.sizeToFit(.content).hCenter().bottom(-5)
    }

}

extension BookingState: Comparable {
    static func < (lhs: BookingState, rhs: BookingState) -> Bool {
        return lhs.orderValue < rhs.orderValue
    }
    
    var orderValue: Int {
        switch self {
        case .CANCELLED: return 1
        case .PAID: return 1
        case .CONFIRMED: return 3
        case .REQUESTED: return 4
        case .RESOLVING: return 5
        case .COMPLETED: return 6
        case .REFUNDED: return 7
        }
    }
    
    
    var color: UIColor {
        switch self {
        case .CANCELLED: return ColorScheme.normal.secondaryTextColor
        case .COMPLETED: return ColorScheme.normal.highlightColorB
        case .CONFIRMED: return AppColors.purple
        case .REQUESTED: return ColorScheme.normal.highlightColorA
        case .RESOLVING: return AppColors.red
        case .PAID: return AppColors.green
        case .REFUNDED: return AppColors.red.withAlphaComponent(0.3)
        }
    }
}
extension APCalendarDayCell: ListBindable {

    func bindViewModel(_ viewModel: Any) {
        guard let viewModel = viewModel as? DayViewModel<BookingModel> else { return }
        label.text = viewModel.day().description
        
        if viewModel.canSelect {
            let highlight = colorScheme!.highlightColorA
            label.layer.borderColor = viewModel.today ? highlight.cgColor : UIColor.clear.cgColor
            label.backgroundColor = viewModel.selected ? highlight.withAlphaComponent(0.3) : UIColor.clear

            let appointments = viewModel.appointments.sorted(by: { $0.status < $1.status })
            let dots = NSMutableAttributedString()
            let hideAfter = 3
            for i in 0..<min(appointments.count, hideAfter) {
                let str = NSAttributedString(string: ".", attributes: [
                    .foregroundColor: appointments[i].status.color
                ])
                dots.append(str)
            }
            if appointments.count > hideAfter {
                let maximus = appointments[hideAfter...].max(by: { $0.status < $1.status })?.status ?? .REQUESTED
                dots.append(NSAttributedString(string: "+", attributes: [
                    .font: UIFont.boldSystemFont(ofSize: 15),
                    .foregroundColor: maximus.color
                ]))
            }
            dotsLabel.font = .boldSystemFont(ofSize: 30)
            dotsLabel.attributedText = dots
            label.alpha = 1
        } else {
            label.alpha = 0.5
            label.layer.borderColor = UIColor.clear.cgColor
            label.backgroundColor = .clear
            dotsLabel.text = ""
        }
        layoutSubviews()
    }

}


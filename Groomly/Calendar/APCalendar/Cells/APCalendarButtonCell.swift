//
//  APCalendarButtonCell.swift
//  Learning
//
//  Created by clar3 on 22/03/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import IGListKit
import PinLayout

class APCalendarButtonCell: APCalendarCell, ListBindable {
    var buttonView: FlexButton? {
        didSet {
            guard let buttonView = buttonView, buttonView.superview == nil else { return }
            contentView.addSchemedView(buttonView)
            layoutSubviews()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        buttonView?.pin.height(35).top()
    }
    
    func bindViewModel(_ viewModel: Any) {
        layoutSubviews()
    }
}

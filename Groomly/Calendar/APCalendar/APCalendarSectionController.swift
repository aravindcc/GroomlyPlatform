//
//  APCalendarSectionController.swift
//  Learning
//
//  Created by clar3 on 21/03/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import PinLayout
import IGListKit

class KeyColorCode: ListDiffable {
    func diffIdentifier() -> NSObjectProtocol {
        color.hexString as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        if self === object { return true }
        guard let object = object as? KeyColorCode else { return false }
        return color == object.color &&
            desc == object.desc
    }
    
    init(_ color: UIColor,_ desc: String) {
        self.color = color
        self.desc = desc
    }
    
    var color: UIColor
    var desc: String
}

protocol Keyable {
    static var keys: [KeyColorCode] { get }
}

typealias APCalEntry = Keyable & ListDiffable & Comparable
class SeparatorCell: UICollectionViewCell, ListBindable {
    static let HEIGHT: CGFloat = 40
    private let v = UIView()
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        v.schemedBG(.secondaryTextColor, alpha: 0.3)
        addSchemedView(v)
    }
    
    override func layoutSubviews() {
        v.pin.height(2).horizontally(-10).vCenter()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func bindViewModel(_ viewModel: Any) {}
}

final class APCalendarSectionController<Entry: APCalEntry>: ListBindingSectionController<ListDiffable>, ListBindingSectionControllerDataSource, ListBindingSectionControllerSelectionDelegate, ListAdapterDataSource {

    private let BUTTON_ID: Float = -10.0
    
    var selectedDay: Int = -1
    weak var delegate: APCalendarDelegate?
    var update: ((Int) -> Void)?
    private var model: Month<Entry>?
    private var entryClass: UICollectionViewCell.Type?
    private var buttonView: FlexButton?

    lazy var adapter: ListAdapter = {
        let adapter = ListAdapter(updater: ListAdapterUpdater(),
                                  viewController: self.viewController)
        adapter.dataSource = self
        return adapter
    }()
    
    init(_ delegate: APCalendarDelegate?,_ entryClass: UICollectionViewCell.Type?, button: FlexButton?) {
        super.init()
        dataSource = self
        selectionDelegate = self
        self.delegate = delegate
        self.entryClass = entryClass
        self.buttonView = button
    }

    // MARK: ListBindingSectionControllerDataSource

    func sectionController(_ sectionController: ListBindingSectionController<ListDiffable>, viewModelsFor object: Any) -> [ListDiffable] {
        guard   let month = object as? Month<Entry>,
                let startOfMonth = Date.dateOf(month: month.name, in: month.year)
        else { return [] }

        model = month
        let monthVal = Calendar.autoupdatingCurrent.component(.month, from: startOfMonth)
        let endOfMonth = startOfMonth.endOfMonth()
        let start = startOfMonth.isWeekday(.monday) ? startOfMonth : startOfMonth.previous(.monday)
        let end = endOfMonth.isWeekday(.sunday) ? endOfMonth : endOfMonth.next(.sunday)
        let dates = Date.dates(from: start, to: end)
        
        var viewModels = [ListDiffable]()

        let now_year = Date().year
        let year = now_year != month.year ? " \(month.year)" : ""
        viewModels.append(LabelViewModel(name: "\(month.name)\(year)", isMonth: true))

        viewModels += ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"].map { LabelViewModel(name: $0, isMonth: false)}
        
        for day in dates {
            let dayVal  = Calendar.autoupdatingCurrent.component(.day, from: day)
            let cellMonth = Calendar.autoupdatingCurrent.component(.month, from: day)
            let correctMonth = cellMonth == monthVal
            let appointments = Day.appointments(in: month.days, for: dayVal)
            let viewModel = DayViewModel<Entry>(
                date: day,
                today: day.sameDay(as: Date.today()),
                selected: dayVal == selectedDay,
                canSelect: correctMonth,
                appointments: appointments
            )
            viewModels.append(viewModel)
        }
        
        viewModels.append("separator" as ListDiffable)
        if selectedDay != -1 {
            viewModels.append(BUTTON_ID as ListDiffable)
        }
        
        viewModels.append(selectedDay as ListDiffable)
        return viewModels
    }

    func sectionController(_ sectionController: ListBindingSectionController<ListDiffable>,
                           cellForViewModel viewModel: Any,
                           at index: Int) -> UICollectionViewCell & ListBindable {
        let cellClass: UICollectionViewCell.Type
        if viewModel is DayViewModel<Entry> {
            cellClass = APCalendarDayCell.self
        } else if let cast = viewModel as? LabelViewModel {
            cellClass = cast.isMonth ? APCalendarTitleCell.self : APCalendarSubtitleCell.self
        } else if let cast = viewModel as? Float, cast == BUTTON_ID {
            cellClass = APCalendarButtonCell.self
        } else if let cast = viewModel as? String, cast == "separator" {
            return schemedReusableCell(of: SeparatorCell.self, for: self, at: index) as! UICollectionViewCell & ListBindable
        } else {
            cellClass = APCalendarNestedCell.self
        }
        guard let cell = schemedReusableCell(of: cellClass, for: self, at: index) as? APCalendarCell & ListBindable else {
            fatalError()
        }
        
        if let cell = cell as? APCalendarNestedCell {
            adapter.collectionView = cell.collectionView
            adapter.reloadData()
        } else if let cell = cell as? APCalendarButtonCell {
            cell.buttonView = buttonView
        }
        cell.delegate = delegate
        return cell
    }

    func sectionController(_ sectionController: ListBindingSectionController<ListDiffable>,
                           sizeForViewModel viewModel: Any,
                           at index: Int) -> CGSize {
        guard let width = collectionContext?.containerSize.width else { return .zero }
        let grid = { (_ height: CGFloat?) -> CGSize in
            let square = floor(width / 7.0)
            return CGSize(width: square, height: height ?? (square / 1.5))
        }
        if viewModel is DayViewModel<Entry> {
            return grid(nil)
        } else if let cast = viewModel as? LabelViewModel {
            return cast.isMonth ? CGSize(width: width, height: 30.0) : grid(30)
        } else if let cast = viewModel as? Float, cast == BUTTON_ID {
            return CGSize(width: width, height: APIClient.isClient ? 55.0 : 15)
        }  else if let cast = viewModel as? String, cast == "separator" {
            return CGSize(width: width, height: SeparatorCell.HEIGHT)
        } else {
            // the vc
            // the height will be pushed to the end of the view by layout so don't worry about it.
            // but make it non-zero
           return CGSize(width: width, height: width)
        }
    }

    // MARK: ListBindingSectionControllerSelectionDelegate

    func sectionController(_ sectionController: ListBindingSectionController<ListDiffable>, didSelectItemAt index: Int, viewModel: Any) {
        guard let dayViewModel = viewModel as? DayViewModel<Entry>, dayViewModel.canSelect else { return }
        if dayViewModel.day() == selectedDay {
            selectedDay = -1
        } else {
            selectedDay = dayViewModel.day()
        }
        delegate?.selectedDay(selectedDay)
        update(animated: true)
    }

    func sectionController(_ sectionController: ListBindingSectionController<ListDiffable>, didDeselectItemAt index: Int, viewModel: Any) {}

    func sectionController(_ sectionController: ListBindingSectionController<ListDiffable>, didHighlightItemAt index: Int, viewModel: Any) {}

    func sectionController(_ sectionController: ListBindingSectionController<ListDiffable>, didUnhighlightItemAt index: Int, viewModel: Any) {}
    
    // MARK: Nested Adapter Source
    
    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        let days = model?.days ?? []
        let list = Day.appointments(in: days, for: selectedDay)
        return list + Entry.keys
    }

    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        return APCalendarEntrySectionController(delegate, entryClass)
    }

    func emptyView(for listAdapter: ListAdapter) -> UIView? {
        return UIView()
    }

}

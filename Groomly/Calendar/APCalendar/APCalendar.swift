//
//  APCalendar.swift
//  Learning
//
//  Created by clar3 on 21/03/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import PinLayout
import IGListKit

protocol APCalendarSource: AnyObject {
    func refresh(_ control : UIRefreshControl)
    func previousMonth()
    func nextMonth()
    func reload(_ booking: BookingModel)
}

final class APCalendar<Entry: APCalEntry>: UIViewController, ListAdapterDataSource, APCalendarDelegate {

    var openEntry : ((Entry) -> Void)?

    lazy var adapter: ListAdapter = {
        return ListAdapter(updater: ListAdapterUpdater(), viewController: self)
    }()
    let collectionView = UICollectionView(
        frame: .zero,
        collectionViewLayout: APCalendarLayout()
    )

    var months = [Month<Entry>]() {
        didSet {
            selectedDay = nil
            selectedMonth = 0
            adapter.reloadData()
        }
    }
    var selectedMonth = 0
    private(set) var selectedDay: Day<Entry>?
    private var selectedDayInt: Int?
    private var entryClass: UICollectionViewCell.Type?
    private(set) var buttonView: FlexButton?
    weak var delegate: APCalendarSource?
    var selectedDate: Date? {
        guard
            let selectedDay = selectedDayInt,
            selectedMonth < months.count
        else { return nil }
        let month = months[selectedMonth]
        return Date(year: month.year, month: month.month, day: selectedDay)
    }
    var selectedEntries: [Entry] {
        return selectedDay?.appointments ?? []
    }
    
    init(entryClass: UICollectionViewCell.Type, button: FlexButton? = nil) {
        super.init(nibName: nil, bundle: nil)
        self.entryClass = entryClass
        self.buttonView = button
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let control = UIRefreshControl()
        control.schemedTintColor(.secondaryTextColor)
        control.addTarget(self, action: #selector(triggeredRefresh(_:)), for: .valueChanged)
        
        collectionView.clipsToBounds = false
        collectionView.refreshControl = control
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.schemedBG()
        view.addSchemedView(collectionView)
        adapter.collectionView = collectionView
        adapter.dataSource = self
    }
    
    @objc private func triggeredRefresh(_ control: UIRefreshControl) {
        delegate?.refresh(control)
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        collectionView.pin.all()
    }

    // MARK: ListAdapterDataSource

    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        return selectedMonth < months.count ? [months[selectedMonth]] : []
    }

    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        let v = APCalendarSectionController<Entry>(self, entryClass, button: buttonView)
        v.selectedDay = selectedDayInt ?? -1
        return v
    }

    func emptyView(for listAdapter: ListAdapter) -> UIView? { return nil }
    
    // MARK: APCalenderDelegate
    
    func nextMonth() {
        delegate?.nextMonth()
    }
    
    func previousMonth() {
        delegate?.previousMonth()
    }
    
    func selectedEntry(_ object: AnyObject) {
        guard let object = object as? Entry else { return }
        openEntry?(object)
    }
    
    func selectedDay(_ day: Int) {
        selectedDayInt = day == -1 ? nil : day
    }
    
    func select(day: Int) {
        selectedDayInt = day == -1 ? nil : day
        adapter.reloadData()
    }
    
    func set(override: Int?) {
        selectedDayInt = override
        adapter.reloadData()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}


//
//  APCalendarDelegate.swift
//  Learning
//
//  Created by clar3 on 21/03/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import IGListKit

class APCalendarCell: UICollectionViewCell {
    weak var delegate: APCalendarDelegate?
}

protocol APCalendarDelegate: AnyObject {    
    func nextMonth() -> Void
    func previousMonth() -> Void
    func selectedDay(_ day: Int) -> Void
    func selectedEntry(_ object: AnyObject) -> Void
}

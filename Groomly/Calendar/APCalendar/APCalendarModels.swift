//
//  APCalendarModels.swift
//  Learning
//
//  Created by clar3 on 21/03/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import IGListKit

final class Day<Entry: ListDiffable & Comparable>: ListDiffable {
    
    static func get(in arr: [Day<Entry>], for day: Int) -> Day<Entry>? {
        for s in arr {
            if s.day == day {
                return s
            }
        }
        return nil
    }
    
    static func appointments(in arr: [Day<Entry>], for day: Int) -> [Entry] {
        (get(in: arr, for: day)?.appointments ?? []).sorted()
    }

    let day: Int
    var appointments: [Entry]

    init(day: Int, appointments: [Entry]) {
        self.day = day
        self.appointments = appointments
    }

    func diffIdentifier() -> NSObjectProtocol {
        return ("\(day)") as NSObjectProtocol
    }

    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        if self === object { return true }
        guard let object = object as? Day else { return false }
        return appointments.equals(object.appointments)
    }

}


final class Month<Entry: ListDiffable & Comparable>: ListDiffable {

    let month: Int
    let name: String
    let year: Int

    // day int mapped to an array of appointment names
    let days: [Day<Entry>]

    init(month: Int, year: Int, appointments: [Day<Entry>]) {
        self.month = month
        self.name = DateFormatter().monthSymbols[month - 1]
        self.year = year
        self.days = appointments
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        return ("\(month)\(year)") as NSObjectProtocol
    }

    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        if self === object { return true }
        guard   let object = object as? Month,
                days.count == object.days.count
        else { return false }
        return days.equals(object.days)
    }

}

final class DayViewModel<Entry: ListDiffable & Comparable>: ListDiffable {
    
    func diffIdentifier() -> NSObjectProtocol {
        return date as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        if self === object { return true }
        guard let object = object as? DayViewModel else { return false }
        return  canSelect == object.canSelect &&
            today == object.today &&
            selected == object.selected &&
            appointments.equals(object.appointments) 
    }

    let date: Date
    let today: Bool
    let selected: Bool
    let appointments: [Entry]
    let canSelect: Bool

    init(date: Date, today: Bool, selected: Bool, canSelect: Bool, appointments: [Entry]) {
        self.date = date
        self.today = today
        self.selected = selected
        self.canSelect = canSelect
        self.appointments = appointments
    }
    
    func day() -> Int {
        return Calendar.autoupdatingCurrent.component(.day, from: date)
    }

}

final class LabelViewModel {

    let name: String
    let isMonth: Bool

    init(name: String, isMonth: Bool) {
        self.name = name
        self.isMonth = isMonth
    }

}

extension LabelViewModel: ListDiffable {

    func diffIdentifier() -> NSObjectProtocol {
        return name as NSObjectProtocol
    }

    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        if self === object { return true }
        guard let object = object as? LabelViewModel else { return false }
        return isMonth == object.isMonth
    }

}

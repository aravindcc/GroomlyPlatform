//
//  BookingActions.swift
//  Groomly
//
//  Created by Ravi on 06/08/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import Async
import MessageUI

enum ActionError: Error {
    case invalid
    case cancelled
    case detail(message: Error)
}

class Mailer: NSObject, MFMailComposeViewControllerDelegate {
    static let shared = Mailer()
    var promise: EventLoopPromise<Void>?
    weak var vc: MFMailComposeViewController?
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        vc?.dismiss(animated: true, completion: nil)
        if result == .sent {
            Feedback.showNotification(message: NotificationDetails(title: "Sent email.", imageName: "checkmark"))
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.8) {
                self.promise?.succeed()
                self.promise = nil
            }
        } else {
            promise?.succeed()
            promise = nil
        }
    }
}

enum BookingActions {
    case customerCancel
    case clientCancel
    case accept
    case resolveRequest
    case changeTime
    case resolveTime
    case resolvePayment
    case review
    case report
    case confirm
    case clientReport
    
    var rawValue: String {
        switch self {
        case .customerCancel: return "cancel"
        case .clientCancel: return "cancel"
        case .accept: return "accept"
        case .resolveRequest: return "unavailable"
        case .changeTime: return "change time"
        case .resolveTime: return "submit time"
        case .review: return "review"
        case .report: return "report"
        case .confirm: return "confirm"
        case .clientReport: return "report"
        case .resolvePayment: return "finish payment"
        }
    }
    
    static func actions(for booking: BookingModel) -> [BookingActions] {
        let isClient = APIClient.isClient
        switch booking.status {
        case .CANCELLED: return []
        case .COMPLETED: return isClient ? [.confirm, clientReport]                     : [.report]
        case .CONFIRMED: return isClient ? [.resolveRequest, .clientCancel]             : [.changeTime, .customerCancel]
        case .REQUESTED: return isClient ? [.accept, .resolveRequest, .clientCancel]    : [.changeTime, .customerCancel]
        case .RESOLVING:
            if isClient {
                return []
            }
            switch booking.reject_reason {
            case .none: return []
            case .PAYMENT: return [.resolvePayment, .customerCancel]
            case .TIME: return [.resolveTime, .customerCancel]
            }
        case .PAID:
            if isClient {
                return booking.customer_score == nil ? [.review] : []
            } else {
                return (booking.client_score == nil ? [.review] : []) + [.report]
            }
        case .REFUNDED: return []
        }
    }
    
    var isClientAction: Bool {
        let actions: [BookingActions] = [
            .clientCancel,
            .accept,
            .resolveRequest,
            .confirm,
            .clientReport,
            .review
        ]
        return actions.contains(self)
    }
    var dualAction: Bool {
        let actions: [BookingActions] = [
            .review
        ]
        return actions.contains(self)
    }
    
    func process(booking: BookingModel, atVC: SubProcessVC?, feeWarning: String? = nil) -> Future<Void> {
        let promise = APIClient.api.worker.eventLoop.newPromise(Void.self)
        guard dualAction || APIClient.isClient == isClientAction else {
            promise.fail(error: ActionError.invalid);
            return promise.futureResult
        }
        
        let notifierModel = Message.curriedNotify(
            loadingMessage: "Updating ...",
            successMessage: nil,
            errorMessage: "Error updating appointment."
        ) { (res: Result<BookingModel, Error>) in
            switch res {
            case .success(_):
                promise.succeed()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    Feedback.showNotification(message: NotificationDetails(title: "Updated.", imageName: "checkmark"))
                }
            case .failure(let error): promise.fail(error: ActionError.detail(message: error))
            }
        }
        
        let notifierBlank = Message.curriedNotify(
            loadingMessage: "Requesting cancellation ...",
            successMessage: nil,
            errorMessage: "Error cancelling appointment."
        ) { (res: Result<BlankResponse, Error>) in
            switch res {
            case .success(_):
                promise.succeed()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    Feedback.showNotification(message: NotificationDetails(title: "Cancelled appointment.", imageName: "checkmark"))
                }
            case .failure(let error): promise.fail(error: ActionError.detail(message: error))
            }
        }
        
        let processTimeChange = {
            let onDates = { (start: Date, end: Date) in
                let param = BookingUpdateRequestCustomer(
                    id: booking.id,
                    date: start,
                    start_time: start,
                    end_time: end
                )
                notifierModel(APIClient.api.update(booking: param))
            }
            #if DEBUG
            if AppDelegate.isUITestingEnabled {
                let tbd = booking.date.adding(.day, value: 1)
                onDates(
                    tbd.setTo(time: booking.start_time)!,
                    tbd.setTo(time: booking.end_time)!
                )
                return
            }
            #endif
            let vc = TimeVC()
            vc.initalDate = booking.date
            vc.client = booking.clientID
            vc.completion = { entry in
                defer {
                    vc.dismiss(animated: true)
                }
                guard let entry = entry else { return }
                onDates(entry.start, entry.end)
            }
            atVC?.present(vc, animated: true)
        }
        
        switch self {
        case .accept:
            let param = BookingUpdateRequestClient(
                id: booking.id,
                status: .CONFIRMED,
                reject_reason: nil
            )
            notifierModel(APIClient.api.update(booking: param))
        case .clientCancel, .customerCancel:
            Message.showConfirmation(
                title: "Cancel",
                description: "Please confirm you want to cancel the appointment. \(feeWarning ?? "")"
            ) {
                if $0 {
                    let param = GetModelRequest(id: booking.id)
                    notifierBlank(APIClient.api.delete(booking: param))
                } else {
                    promise.fail(error: ActionError.cancelled)
                }
            }
        case .changeTime, .resolveTime:
            if let warning = feeWarning {
                Message.showConfirmation(
                    title: "Request different time",
                    description: "If the appointment cannot be rescheduled \(warning.lowercased())"
                ) {
                    if $0 {
                        processTimeChange()
                    } else {
                        promise.fail(error: ActionError.cancelled)
                    }
                }
            } else {
                processTimeChange()
            }
        case .report:
            if MFMailComposeViewController.canSendMail() {
                let mvc = MFMailComposeViewController()
                Mailer.shared.promise = promise
                Mailer.shared.vc = mvc
                mvc.mailComposeDelegate = Mailer.shared
                mvc.setToRecipients(["support@groomlyapp.com"])
                mvc.setSubject("Report Appointment: Reference - \(booking.reference)")
                atVC?.view.window?.backgroundColor = AppColors.orange
                atVC?.present(mvc, animated: true, completion: nil)
            } else {
                Message.showConfirmation(
                    title: "Report",
                    description: "Please send an email to support@groomlyapp.com, and quote the booking reference \(booking.reference)",
                    cancelExists: false) { (_) in
                    promise.succeed()
                }
            }
        case .resolveRequest:
            Message.showConfirmation(
                title: "Request different time",
                description: "Please make sure you have updated your availability, and confirm you want to ask the customer to choose a different time. \(feeWarning ?? "")"
            ) {
                if $0 {
                    let param = BookingUpdateRequestClient(
                        id: booking.id,
                        status: .RESOLVING,
                        reject_reason: .TIME
                    )
                    notifierModel(APIClient.api.update(booking: param))
                } else {
                    promise.fail(error: ActionError.cancelled)
                }
            }
        case .review:
            Feedback.showRating(
                title: "Review",
                desc: "Please rate your experience from 1 to 5 stars."
            ) { (rating) in
                if let rating = rating {
                    Message.notify(
                        process: APIClient.api.send(review: ReviewRequest(booking: booking.id, score: rating)),
                        loadingMessage: "Sending...",
                        successMessage: nil,
                        errorMessage: "Error updating appointment."
                    ) { (res: Result<BlankResponse, Error>) in
                        switch res {
                        case .success(_): promise.succeed()
                        case .failure(let error): promise.fail(error: ActionError.detail(message: error))
                        }
                    }
                }
            }
        case .confirm:
            Message.showConfirmation(
                title: "Confirm",
                description: "Please confirm that the appointment has been completed."
            ) {
                if $0 {
                    let param = BookingUpdateRequestClient(
                        id: booking.id,
                        status: .PAID,
                        reject_reason: nil
                    )
                    notifierModel(APIClient.api.update(booking: param))
                } else {
                    promise.fail(error: ActionError.cancelled)
                }
            }
        case .clientReport:
            promise.succeed()
        case .resolvePayment:
            let blank: String? = nil
            Message.notify(
                process: PaymentBackend.shared.authenticate(booking: booking.id, showingRemedyOn: atVC),
                loadingMessage: "Getting details...",
                successMessage: "Appointment confirmed.",
                errorMessage: blank,
                silentSuccess: false
            ) {
                if case .success(_) = $0 {
                    promise.succeed()
                } else {
                    promise.fail(error: ActionError.cancelled)
                }
            }
            break
        }
        return promise.futureResult
    }
}

extension Dictionary where Key == String, Value == Float {
    var sterling: String {
        map({
            "\($0.key) \(abs($0.value).sterling)"
        }).joined(separator: ", and a ")
    }
}


extension BookingState {
    func explanation(_ fees: FeeModel, rejectReason: BookingRejectReason? = nil) -> String {
        var pending = ""
        if fees.pending.count > 0 {
            pending = "\nIf the appointment can't be rescheduled you will be charged a "
            pending += fees.pending.sterling
        }
        if APIClient.isClient {
            switch self {
            case .CANCELLED:
                if fees.current.count == 0 {
                    return "Appointment was cancelled. No payment will be received."
                } else if fees.current.count < 0 {
                    let feeLine: String = fees.current.sterling
                    return "Appointment was cancelled. You were paid a \(feeLine)."
                } else {
                    let feeLine: String = fees.current.sterling
                    return "Appointment was cancelled. You were charged a \(feeLine) for cancelling."
                }
            case .COMPLETED:
                return "Appointment should be completed please confirm to process payment."
            case .CONFIRMED:
                return "Appointment is confirmed, please make sure you're ready for it."
            case .REQUESTED:
                return "Customer has requested an appointment please confirm.\(pending)"
            case .RESOLVING:
                var desc = "Waiting for customer action."
                if let rejectReason = rejectReason {
                    switch rejectReason {
                    case .TIME:
                        desc = "Waiting for the customer to request an alternate time."
                    case .PAYMENT:
                        desc = "Waiting for the customer to authorise payment."
                    }
                }
                return "\(desc)\(pending)"
            case .PAID:
                return "Appointment is completed, payment has been processed."
            case .REFUNDED:
                return "Appointment was refunded."
            }
        } else {
            switch self {
            case .CANCELLED:
                if fees.current.count == 0 {
                    return "Appointment was cancelled. No payment was taken."
                } else {
                    let feeLine: String = fees.current.sterling
                    return "Appointment was cancelled. You were charged a \(feeLine) for cancelling."
                }
            case .COMPLETED:
                return "Appointment should be completed waiting for stylist confirmation."
            case .CONFIRMED:
                return "Appointment is confirmed, please make sure you're ready for it."
            case .REQUESTED:
                return "Waiting for confirmation from stylist.\(pending)"
            case .RESOLVING:
                var desc = "Please perform action."
                if let rejectReason = rejectReason {
                    switch rejectReason {
                    case .TIME:
                        desc = "The time you chose was unavailable please request an alternate time."
                    case .PAYMENT:
                        desc = "We had a problem authorising your payment. Please confirm payment soon otherwise booking will be cancelled."
                    }
                }
                return "\(desc)\(pending)"
            case .PAID:
                return "Appointment is completed, payment has been processed."
            case .REFUNDED:
                return "Appointment was refunded. Refunds may take upto 5-10 business days to show up on your balance."
            }
        }
    }
}

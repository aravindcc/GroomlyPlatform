//
//  ColorScheme.swift
//  Groomly
//
//  Created by clar3 on 13/05/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import PinLayout
import IGListKit

extension ListSectionController {
    func schemedReusableCell(of cellClass: UICollectionViewCell.Type, for controller: ListSectionController, at index: Int) -> UICollectionViewCell? {
        let cell = collectionContext?.dequeueReusableCell(of: cellClass, for: self, at: index)
        cell?.rescheme(disregardOwn: false)
        return cell
    }
}

extension UIView {
    
    typealias PinExecute = (PinLayout<UIView>) -> Void

    private class PinClosureWrapper: NSObject {
        let closure: PinExecute
        init(_ closure: @escaping PinExecute) {
            self.closure = closure
        }
    }
    
    class ReschemeWrapper: NSObject {
        private var schemers: [ColorClosure] = []
        
        func execute() {
            schemers.forEach { let _ = $0(.clear) }
        }
        
        func add(rule: @escaping ColorClosure) {
            schemers.append(rule)
        }
        
        func removeAll() {
            schemers.removeAll()
        }
    }
    
    func important(gesture recogniser: UIGestureRecognizer) {
        func recurse(_ v: UIView) {
            guard (v as? UIScrollView) == nil else { return }
            for r in (v.gestureRecognizers ?? []) {
                if r != recogniser {
                    r.require(toFail: recogniser)
                }
            }
            for vv in v.subviews {
                recurse(vv)
            }
        }
        recurse(self)
    }
    
    private struct AssociatedKeys {
        static var targetPinClosure = "targetPinClosure"
        static var colorScheme = "colorScheme"
        static var reschemer = "reschemer"
    }
    
    private var targetPinClosure: PinExecute? {
        get {
            guard let closureWrapper = objc_getAssociatedObject(self, &AssociatedKeys.targetPinClosure) as? PinClosureWrapper else { return nil }
            return closureWrapper.closure
        }
        set(newValue) {
            guard let newValue = newValue else { return }
            objc_setAssociatedObject(self, &AssociatedKeys.targetPinClosure, PinClosureWrapper(newValue), objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }

    func addTargetPinClosure(closure: @escaping PinExecute) {
        targetPinClosure = closure
    }
    
    @objc func executePin(recursive: Bool = false) {
        targetPinClosure?(pin)
        if recursive {
            subviews.forEach { $0.executePin(recursive: true) }
        }
    }
    
    var reschemer: ReschemeWrapper {
        get {
            if let closureWrapper = objc_getAssociatedObject(self, &AssociatedKeys.reschemer) as? ReschemeWrapper {
                return closureWrapper
            } else {
                let newValue = ReschemeWrapper()
                objc_setAssociatedObject(self, &AssociatedKeys.reschemer, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
                return newValue
            }
        }
    }
    
    var colorScheme: ColorScheme? {
        get {
            guard let wrapper = objc_getAssociatedObject(self, &AssociatedKeys.colorScheme) as? ColorScheme else {
                func lookHigher(_ view: UIView) -> ColorScheme? {
                    if let up = view.superview {
                        return up.colorScheme ?? lookHigher(up)
                    }
                    return .normal
                }
                return lookHigher(self)
            }
            return wrapper
        }
        set(newValue) {
            guard let newValue = newValue else { return }
            objc_setAssociatedObject(self, &AssociatedKeys.colorScheme, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            self.rescheme(disregardOwn: false)
            if let schemeCase = newValue.getCase() {
                ColorConfigurator.shared.registerRefresh(schemeCase) { [weak self] schemeCase in
                    if let self = self {
                        objc_setAssociatedObject(self, &AssociatedKeys.colorScheme, schemeCase.scheme, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
                        self.rescheme(disregardOwn: false)
                        return true
                    }
                    return false
                }
            }
        }
    }
    
    func addSchemedView(_ view: UIView) {
        addSubview(view)
        view.rescheme()
    }
 
    var rawScheme: ColorScheme? {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys.colorScheme) as? ColorScheme
        }
    }
    
    func schemedBG(_ property: ColorKey = .bg, alpha: CGFloat = 1.0, register: Bool = true) {
        let scheme: ColorScheme = colorScheme ?? ColorScheme.normal
        backgroundColor = scheme.get(property: property)?.withAlphaComponent(alpha)
       
        if register {
            let closure: ColorClosure = { [weak self] _ in
                if let view = self {
                    view.schemedBG(property, alpha: alpha, register: false)
                    return true
                }
                return false
            }
            reschemer.add(rule: closure)
            ColorConfigurator.shared.register(andProperties: [property], withUpdator: closure)
        }
    }
    
    func schemedBorderColor(_ property: ColorKey = .bg, alpha: CGFloat = 1.0, register: Bool = true) {
        let scheme: ColorScheme = colorScheme ?? ColorScheme.normal
        borderColor = scheme.get(property: property)?.withAlphaComponent(alpha)
       
        if register {
            let closure: ColorClosure = { [weak self] _ in
                if let view = self {
                    view.schemedBorderColor(property, alpha: alpha, register: false)
                    return true
                }
                return false
            }
            reschemer.add(rule: closure)
            ColorConfigurator.shared.register(andProperties: [property], withUpdator: closure)
        }
    }
    
    @objc func rescheme(disregardOwn: Bool = true) {
        if (objc_getAssociatedObject(self, &AssociatedKeys.reschemer) as? ReschemeWrapper) != nil {
            reschemer.execute()
            setNeedsDisplay()
        }
        for v in subviews {
            if disregardOwn || v.rawScheme == nil {
                v.rescheme(disregardOwn: disregardOwn)
            }
        }
    }
}

extension UITextView {
    func schemedTextColor(_ property: ColorKey, alpha: CGFloat = 1.0, register: Bool = true, refresh: (() -> Void)? = nil) {
        let scheme: ColorScheme = colorScheme ?? ColorScheme.normal
        textColor = scheme.get(property: property)?.withAlphaComponent(alpha)
        refresh?()
        if register {
            let closure: ColorClosure = { [weak self] _ in
                if let label = self {
                    label.schemedTextColor(property, alpha: alpha, register: false, refresh: refresh)
                    return true
                }
                return false
            }
            reschemer.add(rule: closure)
            ColorConfigurator.shared.register(andProperties: [property], withUpdator: closure)
        }
    }
}

extension UISegmentedControl {
    func schemedTextColor(_ property: ColorKey, size: CGFloat, register: Bool = true) {
        let scheme: ColorScheme = colorScheme ?? ColorScheme.normal
        if let textColor = scheme.get(property: property) {
            setTitleTextAttributes([
                .foregroundColor: textColor,
                .font: UIFont.systemFont(ofSize: size, weight: .semibold)
            ], for: .normal)
            setTitleTextAttributes([
                .foregroundColor: textColor,
                .font: UIFont.systemFont(ofSize: size, weight: .bold)
            ], for: .selected)
        }
        
        if register {
            let closure: ColorClosure = { [weak self] _ in
                if let seg = self {
                    seg.schemedTextColor(property, size: size, register: false)
                    return true
                }
                return false
            }
            reschemer.add(rule: closure)
            ColorConfigurator.shared.register(andProperties: [property], withUpdator: closure)
        }
    }
    
    func schemedTintColor(_ property: ColorKey, register: Bool = true) {
        let scheme: ColorScheme = colorScheme ?? ColorScheme.normal
        selectedSegmentTintColor = scheme.get(property: property)
        if register {
            let closure: ColorClosure = { [weak self] _ in
                if let seg = self {
                    seg.schemedTintColor(property, register: false)
                    return true
                }
                return false
            }
            reschemer.add(rule: closure)
            ColorConfigurator.shared.register(andProperties: [property], withUpdator: closure)
        }
    }
}

extension UIRefreshControl {
    func schemedTintColor(_ property: ColorKey, register: Bool = true) {
        let scheme: ColorScheme = colorScheme ?? ColorScheme.normal
        tintColor = scheme.get(property: property)
       
        if register {
            let closure: ColorClosure = { [weak self] _ in
                if let button = self {
                    button.schemedTintColor(property, register: false)
                    return true
                }
                return false
            }
            reschemer.add(rule: closure)
            ColorConfigurator.shared.register(andProperties: [property], withUpdator: closure)
        }
    }
}

extension UIButton {
    func schemedTintColor(_ property: ColorKey, register: Bool = true) {
        let scheme: ColorScheme = colorScheme ?? ColorScheme.normal
        tintColor = scheme.get(property: property)
       
        if register {
            let closure: ColorClosure = { [weak self] _ in
                if let button = self {
                    button.schemedTintColor(property, register: false)
                    return true
                }
                return false
            }
            reschemer.add(rule: closure)
            ColorConfigurator.shared.register(andProperties: [property], withUpdator: closure)
        }
    }
}

extension UIImageView {
    func schemedTintColor(_ property: ColorKey, alpha: CGFloat = 1.0, register: Bool = true) {
        let scheme: ColorScheme = colorScheme ?? ColorScheme.normal
        tintColor = scheme.get(property: property)?.withAlphaComponent(alpha)
       
        if register {
            let closure: ColorClosure = { [weak self] _ in
                if let button = self {
                    button.schemedTintColor(property, register: false)
                    return true
                }
                return false
            }
            reschemer.add(rule: closure)
            ColorConfigurator.shared.register(andProperties: [property], withUpdator: closure)
        }
    }
}

extension UILabel {
    func schemedTextColor(_ property: ColorKey, alpha: CGFloat = 1.0, register: Bool = true, refresh: (() -> Void)? = nil) {
        let scheme: ColorScheme = colorScheme ?? ColorScheme.normal
        textColor = scheme.get(property: property)?.withAlphaComponent(alpha)
        refresh?()
        if register {
            let closure: ColorClosure = { [weak self] _ in
                if let label = self {
                    label.schemedTextColor(property, alpha: alpha, register: false, refresh: refresh)
                    return true
                }
                return false
            }
            reschemer.add(rule: closure)
            ColorConfigurator.shared.register(andProperties: [property], withUpdator: closure)
        }
    }
}

extension UITextField {
    func schemedTextColor(_ property: ColorKey, alpha: CGFloat = 1.0, register: Bool = true) {
        let scheme: ColorScheme = colorScheme ?? ColorScheme.normal
        let base = scheme.get(property: property)
        textColor = base?.withAlphaComponent(alpha)
        if let place = base?.withAlphaComponent(0.3) {
            setPlaceHolderTextColor(place)
        }
        
        if register {
            let closure: ColorClosure = { [weak self] _ in
                if let label = self {
                    label.schemedTextColor(property, alpha: alpha, register: false)
                    return true
                }
                return false
            }
            reschemer.add(rule: closure)
            ColorConfigurator.shared.register(andProperties: [property], withUpdator: closure)
        }
    }
    
    func schemedTintColor(_ property: ColorKey, alpha: CGFloat = 1.0, register: Bool = true) {
        let scheme: ColorScheme = colorScheme ?? ColorScheme.normal
        tintColor = scheme.get(property: property)?.withAlphaComponent(alpha)
        
        if register {
            let closure: ColorClosure = { [weak self] _ in
                if let label = self {
                    label.schemedTintColor(property, alpha: alpha, register: false)
                    return true
                }
                return false
            }
            reschemer.add(rule: closure)
            ColorConfigurator.shared.register(andProperties: [property], withUpdator: closure)
        }
    }
}

extension UIVisualEffectView {
    override func rescheme(disregardOwn: Bool = true) {
        super.rescheme(disregardOwn: disregardOwn)
        let isLight = (colorScheme!.bg.isLight() ?? true)
        let blurEffect = UIBlurEffect(style: isLight ? .light : .dark)
        effect = blurEffect
    }
}

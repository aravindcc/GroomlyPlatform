//
//  FlatColors.swift
//  Groomly
//
//  Created by clar3 on 06/05/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import Foundation

enum FlatColors: String, CaseIterable {
    case turquoise_50 = "#e8f8f5"
    case turquoise_100 = "#d1f2eb"
    case turquoise_200 = "#a3e4d7"
    case turquoise_300 = "#76d7c4"
    case turquoise_400 = "#48c9b0"
    case turquoise_500 = "#1abc9c"
    case turquoise_600 = "#17a589"
    case turquoise_700 = "#148f77"
    case turquoise_800 = "#117864"
    case turquoise_900 = "#0e6251"
    
    /* Green Sea */
    case green_sea_50 = "#e8f6f3"
    case green_sea_100 = "#d0ece7"
    case green_sea_200 = "#a2d9ce"
    case green_sea_300 = "#73c6b6"
    case green_sea_400 = "#45b39d"
    case green_sea_500 = "#16a085"
    case green_sea_600 = "#138d75"
    case green_sea_700 = "#117a65"
    case green_sea_800 = "#0e6655"
    case green_sea_900 = "#0b5345"
    
    /* Emerald */
    case emerald_50 = "#eafaf1"
    case emerald_100 = "#d5f5e3"
    case emerald_200 = "#abebc6"
    case emerald_300 = "#82e0aa"
    case emerald_400 = "#58d68d"
    case emerald_500 = "#2ecc71"
    case emerald_600 = "#28b463"
    case emerald_700 = "#239b56"
    case emerald_800 = "#1d8348"
    case emerald_900 = "#186a3b"
    
    /* Nephritis */
    case nephritis_50 = "#e9f7ef"
    case nephritis_100 = "#d4efdf"
    case nephritis_200 = "#a9dfbf"
    case nephritis_300 = "#7dcea0"
    case nephritis_400 = "#52be80"
    case nephritis_500 = "#27ae60"
    case nephritis_600 = "#229954"
    case nephritis_700 = "#1e8449"
    case nephritis_800 = "#196f3d"
    case nephritis_900 = "#145a32"
    
    /* Peter River */
    case peter_river_50 = "#ebf5fb"
    case peter_river_100 = "#d6eaf8"
    case peter_river_200 = "#aed6f1"
    case peter_river_300 = "#85c1e9"
    case peter_river_400 = "#5dade2"
    case peter_river_500 = "#3498db"
    case peter_river_600 = "#2e86c1"
    case peter_river_700 = "#2874a6"
    case peter_river_800 = "#21618c"
    case peter_river_900 = "#1b4f72"
    
    /* Belize Hole */
    case belize_hole_50 = "#eaf2f8"
    case belize_hole_100 = "#d4e6f1"
    case belize_hole_200 = "#a9cce3"
    case belize_hole_300 = "#7fb3d5"
    case belize_hole_400 = "#5499c7"
    case belize_hole_500 = "#2980b9"
    case belize_hole_600 = "#2471a3"
    case belize_hole_700 = "#1f618d"
    case belize_hole_800 = "#1a5276"
    case belize_hole_900 = "#154360"
    
    /* Amethyst */
    case amethyst_50 = "#f5eef8"
    case amethyst_100 = "#ebdef0"
    case amethyst_200 = "#d7bde2"
    case amethyst_300 = "#c39bd3"
    case amethyst_400 = "#af7ac5"
    case amethyst_500 = "#9b59b6"
    case amethyst_600 = "#884ea0"
    case amethyst_700 = "#76448a"
    case amethyst_800 = "#633974"
    case amethyst_900 = "#512e5f"
    
    /* Wisteria */
    case wisteria_50 = "#f4ecf7"
    case wisteria_100 = "#e8daef"
    case wisteria_200 = "#d2b4de"
    case wisteria_300 = "#bb8fce"
    case wisteria_400 = "#a569bd"
    case wisteria_500 = "#8e44ad"
    case wisteria_600 = "#7d3c98"
    case wisteria_700 = "#6c3483"
    case wisteria_800 = "#5b2c6f"
    case wisteria_900 = "#4a235a"
    
    /* Wet Asphalt */
    case wet_asphalt_50 = "#ebedef"
    case wet_asphalt_100 = "#d6dbdf"
    case wet_asphalt_200 = "#aeb6bf"
    case wet_asphalt_300 = "#85929e"
    case wet_asphalt_400 = "#5d6d7e"
    case wet_asphalt_500 = "#34495e"
    case wet_asphalt_600 = "#2e4053"
    case wet_asphalt_700 = "#283747"
    case wet_asphalt_800 = "#212f3c"
    case wet_asphalt_900 = "#1b2631"
    
    /* Midnight Blue */
    case midnight_blue_50 = "#eaecee"
    case midnight_blue_100 = "#d5d8dc"
    case midnight_blue_200 = "#abb2b9"
    case midnight_blue_300 = "#808b96"
    case midnight_blue_400 = "#566573"
    case midnight_blue_500 = "#2c3e50"
    case midnight_blue_600 = "#273746"
    case midnight_blue_700 = "#212f3d"
    case midnight_blue_800 = "#1c2833"
    case midnight_blue_900 = "#17202a"
    
    /* Sunflower */
    case sunflower_50 = "#fef9e7"
    case sunflower_100 = "#fcf3cf"
    case sunflower_200 = "#f9e79f"
    case sunflower_300 = "#f7dc6f"
    case sunflower_400 = "#f4d03f"
    case sunflower_500 = "#f1c40f"
    case sunflower_600 = "#d4ac0d"
    case sunflower_700 = "#b7950b"
    case sunflower_800 = "#9a7d0a"
    case sunflower_900 = "#7d6608"
    
    /* Orange */
    case orange_50 = "#fef5e7"
    case orange_100 = "#fdebd0"
    case orange_200 = "#fad7a0"
    case orange_300 = "#f8c471"
    case orange_400 = "#f5b041"
    case orange_500 = "#f39c12"
    case orange_600 = "#d68910"
    case orange_700 = "#b9770e"
    case orange_800 = "#9c640c"
    case orange_900 = "#7e5109"
    
    /* Carrot */
    case carrot_50 = "#fdf2e9"
    case carrot_100 = "#fae5d3"
    case carrot_200 = "#f5cba7"
    case carrot_300 = "#f0b27a"
    case carrot_400 = "#eb984e"
    case carrot_500 = "#e67e22"
    case carrot_600 = "#ca6f1e"
    case carrot_700 = "#af601a"
    case carrot_800 = "#935116"
    case carrot_900 = "#784212"
    
    /* Pumpkin */
    case pumpkin_50 = "#fbeee6"
    case pumpkin_100 = "#f6ddcc"
    case pumpkin_200 = "#edbb99"
    case pumpkin_300 = "#e59866"
    case pumpkin_400 = "#dc7633"
    case pumpkin_500 = "#d35400"
    case pumpkin_600 = "#ba4a00"
    case pumpkin_700 = "#a04000"
    case pumpkin_800 = "#873600"
    case pumpkin_900 = "#6e2c00"
    
    /* Alizarin */
    case alizarin_50 = "#fdedec"
    case alizarin_100 = "#fadbd8"
    case alizarin_200 = "#f5b7b1"
    case alizarin_300 = "#f1948a"
    case alizarin_400 = "#ec7063"
    case alizarin_500 = "#e74c3c"
    case alizarin_600 = "#cb4335"
    case alizarin_700 = "#b03a2e"
    case alizarin_800 = "#943126"
    case alizarin_900 = "#78281f"
    
    /* Pomegranate */
    case pomegranate_50 = "#f9ebea"
    case pomegranate_100 = "#f2d7d5"
    case pomegranate_200 = "#e6b0aa"
    case pomegranate_300 = "#d98880"
    case pomegranate_400 = "#cd6155"
    case pomegranate_500 = "#c0392b"
    case pomegranate_600 = "#a93226"
    case pomegranate_700 = "#922b21"
    case pomegranate_800 = "#7b241c"
    case pomegranate_900 = "#641e16"
    
    /* Clouds */
    case clouds_50 = "#fdfefe"
    case clouds_100 = "#fbfcfc"
    case clouds_200 = "#f7f9f9"
    case clouds_300 = "#f4f6f7"
    case clouds_400 = "#f0f3f4"
    case clouds_500 = "#ecf0f1"
    case clouds_600 = "#d0d3d4"
    case clouds_700 = "#b3b6b7"
    case clouds_800 = "#979a9a"
    case clouds_900 = "#7b7d7d"
    
    /* Silver */
    case silver_50 = "#f8f9f9"
    case silver_100 = "#f2f3f4"
    case silver_200 = "#e5e7e9"
    case silver_300 = "#d7dbdd"
    case silver_400 = "#cacfd2"
    case silver_500 = "#bdc3c7"
    case silver_600 = "#a6acaf"
    case silver_700 = "#909497"
    case silver_800 = "#797d7f"
    case silver_900 = "#626567"
    
    /* Concrete */
    case concrete_50 = "#f4f6f6"
    case concrete_100 = "#eaeded"
    case concrete_200 = "#d5dbdb"
    case concrete_300 = "#bfc9ca"
    case concrete_400 = "#aab7b8"
    case concrete_500 = "#95a5a6"
    case concrete_600 = "#839192"
    case concrete_700 = "#717d7e"
    case concrete_800 = "#5f6a6a"
    case concrete_900 = "#4d5656"
    
    /* Asbestos */
    case asbestos_50 = "#f2f4f4"
    case asbestos_100 = "#e5e8e8"
    case asbestos_200 = "#ccd1d1"
    case asbestos_300 = "#b2babb"
    case asbestos_400 = "#99a3a4"
    case asbestos_500 = "#7f8c8d"
    case asbestos_600 = "#707b7c"
    case asbestos_700 = "#616a6b"
    case asbestos_800 = "#515a5a"
    case asbestos_900 = "#424949"
    
}

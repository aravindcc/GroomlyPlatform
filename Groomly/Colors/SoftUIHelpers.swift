//
//  Neumorphic.swift
//  Groomly
//
//  Created by clar3 on 12/05/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit

extension CGSize {
    static func *(lhs: CGSize, rhs: CGFloat) -> CGSize {
        return CGSize(width: lhs.width * rhs, height: lhs.height * rhs)
    }
    
    static func *(lhs: CGFloat, rhs: CGSize) -> CGSize {
        return rhs * lhs
    }
    
    func toPoint() -> CGPoint {
        return CGPoint(x: width, y: height)
    }
}

extension CGPoint {
    static func *(lhs: CGPoint, rhs: CGFloat) -> CGPoint {
        return CGPoint(x: lhs.x * rhs, y: lhs.y * rhs)
    }
    
    static func *(lhs: CGFloat, rhs: CGPoint) -> CGPoint {
        return rhs * lhs
    }
    
    func toSize() -> CGSize {
        return CGSize(width: x, height: y)
    }
}

extension UIView {
    
    enum NeumorphicCase {
        case inner
        case cusp
        case outer
    }
    
    private func createShadow(fillColor bg: UIColor, shadowColor color: UIColor,_ key: ColorKey) -> CALayer {
        let shadowLayer = CAShapeLayer()
        shadowLayer.frame = bounds
        shadowLayer.name = "shadow-\(key)"
        shadowLayer.backgroundColor = bg.cgColor
        shadowLayer.shadowColor = color.cgColor
        shadowLayer.shouldRasterize = true
        shadowLayer.rasterizationScale = UIScreen.main.scale
        shadowLayer.cornerRadius = cornerRadius
        shadowLayer.shadowOpacity = 1
        return shadowLayer
    }
    
    func addSoftUIEffectForView(cornerRadius: CGFloat = 15,
                                shadowRadius: CGFloat = 5,
                                invert: Bool = false,
                                type: NeumorphicCase = .outer) {
        
        self.backgroundColor = .clear
        self.layer.cornerRadius = cornerRadius
        
        var hadBefore = false
        for layer in self.layer.sublayers ?? [] {
            if layer.name?.contains("shadow") ?? false {
                layer.removeFromSuperlayer()
                hadBefore = true
            }
        }
        
        let scheme: ColorScheme = colorScheme ?? .normal
        if type != .outer {
            layer.masksToBounds = true
            let produceBView = { [weak self] () -> UIView in
                guard let self = self else { return UIView() }
                let rad = shadowRadius * 1.8
                let parent = UIView(frame: self.bounds)
                let scheme = self.colorScheme!
                parent.backgroundColor = scheme.bg
                parent.layer.cornerRadius = self.cornerRadius
                parent.layer.addInnerShadow(onSide: .topAndLeft, shadowColor: scheme.darkShadow, shadowSize: rad)
                parent.layer.addInnerShadow(onSide: .bottomAndRight, shadowColor: scheme.lightShadow, shadowSize: rad)
                return parent
            }
            let blur = BlurLayer(foreground: produceBView)
            blur.name = "shadow-image"
            self.layer.insertSublayer(blur, at: 0)
        }

        if type != .inner {
            let offsetBase = CGSize(width: shadowRadius, height: shadowRadius) * 1.5
            layer.masksToBounds = false
            let layerone = createShadow(fillColor: scheme.bg, shadowColor: scheme.darkShadow, .darkShadow)
            let layertwo = createShadow(fillColor: scheme.bg, shadowColor: scheme.lightShadow, .lightShadow)
            layerone.shadowOffset = (invert ? -1 : 1) * offsetBase
            layertwo.shadowOffset = (invert ? 1 : -1) * offsetBase
            layerone.shadowRadius = shadowRadius * 2
            layertwo.shadowRadius = shadowRadius * 2
            self.layer.insertSublayer(layertwo, at: 0)
            self.layer.insertSublayer(layerone, at: 0)
        }
        
        if !hadBefore {
            registerSoftUI()
        }
    }
    
    private func registerSoftUI() {
        if let schemeCase = colorScheme?.getCase() {
            let closure: ColorClosure = { [weak self] color in
                if let view = self {
                    let read: ColorScheme = view.rawScheme ?? schemeCase.scheme
                    view.layer.shadowColor = read.darkShadow.cgColor
                    for lay in (view.layer.sublayers ?? []) {
                        if lay.name == "shadow-image" {
                            (lay as? BlurLayer)?.updateImage()
                        } else if lay.name == "shadow-\(ColorKey.darkShadow)" {
                            lay.backgroundColor = read.bg.cgColor
                            lay.shadowColor = read.darkShadow.cgColor
                        } else if lay.name == "shadow-\(ColorKey.lightShadow)" {
                            lay.backgroundColor = read.bg.cgColor
                            lay.shadowColor = read.lightShadow.cgColor
                        }
                    }
                    view.setNeedsDisplay()
                    view.setNeedsLayout()
                    return true
                }
                return false
            }
            reschemer.add(rule: closure)
            ColorConfigurator.shared.register(interestFor: schemeCase, andProperties: [
                .darkShadow, .lightShadow, .bg
            ], withUpdator: closure)
        }
    }
    
    func invertSoftShadows() {
        var one: CALayer!
        var two: CALayer!
        for layer in (layer.sublayers ?? []) {
            if layer.name == "shadow-\(ColorKey.lightShadow)" || layer.name == "shadow-\(ColorKey.darkShadow)" {
                if one == nil { one = layer }
                else { two = layer; break }
            }
        }
        guard one != nil && two != nil else { return }
        let tmpShadowColor = one.shadowColor
        one.shadowColor = two.shadowColor
        two.shadowColor = tmpShadowColor
    }
    
    func updateSoftUI() {
        layer.cornerRadius = min(layer.cornerRadius, layer.frame.height / 2)
        
        func updateShadows(_ layer: CALayer) {
            for layer in (layer.sublayers ?? []) {
                if layer.name?.contains("shadow") ?? false {
                    layer.frame = bounds
                    layer.cornerRadius = self.layer.cornerRadius
                    if let layer = layer as? BlurLayer {
                        layer.updateImage()
                        layer.displayIfNeeded()
                    }
                } else {
                    updateShadows(layer)
                }
            }
        }
        updateShadows(layer)
    }
}

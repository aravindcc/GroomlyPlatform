//
//  ColorConfigurator.swift
//  Groomly
//
//  Created by Ravi on 23/08/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import PinLayout
import FlexLayout

enum ColorKey: String, CaseIterable {
    case bg = "Background Color"
    case lightShadow = "Neumorphism Light Shadow Color"
    case darkShadow = "Neumorphism Dark Shadow Color"
    case primaryTextColor = "Primary Text Color"
    case highlightColorA = "Accent 1A (Main)"
    case highlightColorC = "Accent 1B"
    case highlightColorB = "Accent 2 (Darker)"
    case secondaryTextColor = "Secondary Text Color"
}

enum SchemeCase: String, Codable, CaseIterable {
    case normal = "Normal"
    case alt = "Alternative"
    
    var scheme: ColorScheme {
        self == .normal ? .normal : .alt
    }
}

protocol PropertyReflectable { }

extension PropertyReflectable {
    subscript(key: String) -> Any? {
        let m = Mirror(reflecting: self)
        for child in m.children {
            if child.label == key { return child.value }
        }
        return nil
    }
}

extension ColorScheme: PropertyReflectable {
    func asDictionary() -> [String: String] {
        return Dictionary(uniqueKeysWithValues:
            Mirror(reflecting: self).children.compactMap({ prop in
                guard let l = prop.label, let c = prop.value as? UIColor else { return nil }
                return (l, c.hexString)
            })
        )
    }
}

typealias ColorClosure = (UIColor) -> Bool
typealias RefreshClosure = (SchemeCase) -> Bool

class ColorConfigurator {
    static var shared = ColorConfigurator()
    var reactors = [String: [ColorClosure]]()
    var refreshers = [String: [RefreshClosure]]()
    var references = [UIView: ColorClosure]()
    var registerQueue = DispatchQueue(label: "colorConfigReg")
    
    func register(interestFor scheme: SchemeCase, andProperty property: ColorKey, withUpdator updator: @escaping ColorClosure) {
        registerQueue.sync {
            let key = scheme.rawValue + property.rawValue
            if reactors[key] != nil {
                reactors[key]!.append(updator)
            }
            else {
                reactors[key] = [updator]
            }
        }
    }
    
    func register(interestFor scheme: SchemeCase, andProperties properties: [ColorKey], withUpdator updator: @escaping ColorClosure) {
        for prop in properties {
            register(interestFor: scheme, andProperty: prop, withUpdator: updator)
        }
    }
    
    func register(interestFor schemes: [SchemeCase] = SchemeCase.allCases, andProperties properties: [ColorKey], withUpdator updator: @escaping ColorClosure) {
        for scheme in schemes {
            for prop in properties {
                register(interestFor: scheme, andProperty: prop, withUpdator: updator)
            }
        }
    }
    
    func registerRefresh(_ schemeCase: SchemeCase, listener: @escaping RefreshClosure) {
        registerQueue.sync {
            let key = schemeCase.rawValue
            if refreshers[key] != nil {
                refreshers[key]!.append(listener)
            }
            else {
                refreshers[key] = [listener]
            }
        }
    }
    
    func notifyRefresh() {
        registerQueue.sync {
            for scheme in SchemeCase.allCases {
                let key = scheme.rawValue
                guard var observers = refreshers[key] else { return }
                var remove: [Int] = []
                observers.enumerated().forEach({ (i, fun) in
                    if !fun(scheme) {
                        remove.append(i)
                    }
                })
                remove.sort()
                for (i, r) in remove.enumerated() {
                    let _ = observers.remove(at: r - i)
                }
            }
        }
    }
    
    func notify(changeOfproperty property: ColorKey, forScheme scheme: SchemeCase, withColor color: UIColor) {
        registerQueue.sync {
            let key = scheme.rawValue + property.rawValue
            guard var observers = reactors[key] else { return }
            var remove: [Int] = []
            observers.enumerated().forEach({ (i, fun) in
                if !fun(color) {
                    remove.append(i)
                }
            })
            remove.sort()
            for (i, r) in remove.enumerated() {
                let _ = observers.remove(at: r - i)
            }
        }
    }

    static func serialiseCurrent(copyToClipboard save: Bool = true) -> String {
        var output = [String: [String: String]]()
        SchemeCase.allCases.forEach {
            output[$0.rawValue] = $0.scheme.asDictionary()
        }
        if  let data = try? JSONEncoder().encode(output),
            let str = String(data: data, encoding: .utf8) {
            if save {
                UIPasteboard.general.string = str
            }
            return str
        }
        return ""
    }
    
    static func set(color: UIColor, forScheme schemeCase: SchemeCase, andProperty property: ColorKey) {
        let scheme = schemeCase.scheme
        switch property {
        case .bg: scheme.bg = color
        case .darkShadow: scheme.darkShadow = color
        case .highlightColorA: scheme.highlightColorA = color
        case .highlightColorB: scheme.highlightColorB = color
        case .highlightColorC: scheme.highlightColorC = color
        case .lightShadow: scheme.lightShadow = color
        case .primaryTextColor: scheme.primaryTextColor = color
        case .secondaryTextColor: scheme.secondaryTextColor = color
        }
    }
    
    static func getColor(scheme: SchemeCase?, property: ColorKey) -> UIColor? {
        var color: UIColor?
        if scheme == .normal {
            color = ColorScheme.normal[String(describing: property)] as? UIColor
        } else {
            color = ColorScheme.alt[String(describing: property)] as? UIColor
        }
        return color
    }
    
    
    func load(savedColors str: String) {
        guard   let data = str.data(using: .utf8),
                let output = try? JSONDecoder().decode([String: [String: String]].self, from: data)
        else { return }
        output.forEach { (schemeStr, serial) in
            guard let scheme = SchemeCase(rawValue: schemeStr) else { return }
            serial.forEach { (prop, value) in
                guard let property = ColorKey(rawValue: prop) else { return }
                let color = UIColor(value)
                ColorConfigurator.set(color: color, forScheme: scheme, andProperty: property)
                notify(changeOfproperty: property, forScheme: scheme, withColor: color)
            }
        }
    }
    
    func refresh() {
        // technically the reactors shouldn't need to be refreshed
        // but we have some exceptions that directly use the color configurator that we know are .normal
        for scheme in SchemeCase.allCases {
            for property in ColorKey.allCases {
                if let color = ColorConfigurator.getColor(scheme: scheme, property: property) {
                    notify(changeOfproperty: property, forScheme: scheme, withColor: color)
                }
            }
        }
        notifyRefresh()
    }
    
}

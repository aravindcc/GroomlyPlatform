//
//  Colors.swift
//  Learning
//
//  Created by clar3 on 18/03/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import CalendarKit

extension UIColor {
    convenience init(_ r: CGFloat,_ g: CGFloat,_ b: CGFloat) {
        self.init(red: r / 255.0, green: g / 255.0, blue: b / 255.0, alpha: 1)
    }
    convenience init(_ hex: String) {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }

        if ((cString.count) != 6) {
            self.init(0, 0, 0)
            return
        }

        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)

        self.init(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}

extension TimelineStyle {
    static func normal() -> TimelineStyle {
        var style = TimelineStyle()
        style.font = .systemFont(ofSize: 17, weight: .bold)
        style.leftInset = 35
        style.backgroundColor = ColorScheme.normal.bg
        style.lineColor = ColorScheme.normal.secondaryTextColor.withAlphaComponent(0.7)
        style.timeColor = ColorScheme.normal.secondaryTextColor.withAlphaComponent(0.7)
        return style
    }
    static func alt() -> TimelineStyle {
        var style = TimelineStyle()
        style.leftInset = 35
        style.font = .systemFont(ofSize: 17, weight: .bold)
        style.backgroundColor = ColorScheme.alt.bg
        style.lineColor = ColorScheme.alt.lightShadow
        style.timeColor = ColorScheme.alt.lightShadow
        return style
    }
}


class ColorScheme: Equatable {
    static func == (lhs: ColorScheme, rhs: ColorScheme) -> Bool {
        for key in ColorKey.allCases {
            if let a = lhs.get(property: key), let b = rhs.get(property: key), a != b {
                return false
            }
        }
        return true
    }
    
    
    init(lightShadow: UIColor, darkShadow: UIColor,
         primaryTextColor: UIColor,
         secondaryTextColor: UIColor,
         bg: UIColor,
         highlightColorA: UIColor = AppColors.purple,
         highlightColorB: UIColor = AppColors.darkBlue,
         highlightColorC: UIColor = AppColors.pink) {
        self.lightShadow = lightShadow
        self.darkShadow = darkShadow
        self.bg = bg
        self.primaryTextColor = primaryTextColor
        self.secondaryTextColor = secondaryTextColor
        self.highlightColorA = highlightColorA
        self.highlightColorB = highlightColorB
        self.highlightColorC = highlightColorC
    }
    
    var lightShadow: UIColor
    var darkShadow: UIColor
    var bg: UIColor
    var primaryTextColor: UIColor
    var secondaryTextColor: UIColor
    var highlightColorA: UIColor
    var highlightColorB: UIColor
    var highlightColorC: UIColor
    
    var isLight: Bool {
        bg.isLight() ?? true
    }
    
    static var normal = ColorScheme.grey.copy()
    static var alt = ColorScheme.white.copy()
    
    static let grey = ColorScheme(lightShadow: AppColors.lightShadow,
                                       darkShadow: AppColors.darkShadow,
                                       primaryTextColor: .black,
                                       secondaryTextColor: AppColors.grey,
                                       bg: AppColors.lightBG,
                                       highlightColorA: AppColors.orange,
                                       highlightColorB: AppColors.mellowBlue,
                                       highlightColorC: AppColors.lightOrange)
    
    static let white = ColorScheme(lightShadow: AppColors.whiteLightShadow,
                                        darkShadow: AppColors.whiteDarkShadow,
                                        primaryTextColor: .black,
                                        secondaryTextColor: AppColors.grey,
                                        bg: AppColors.white,
                                        highlightColorA: AppColors.orange,
                                        highlightColorB: AppColors.mellowBlue,
                                        highlightColorC: AppColors.lightOrange)
    
    static let dark = ColorScheme(lightShadow: AppColors.altlightShadow,
                                       darkShadow: AppColors.altDarkShadow,
                                       primaryTextColor: AppColors.lightGrey,
                                       secondaryTextColor: AppColors.grey,
                                       bg: AppColors.altBG,
                                       highlightColorA: AppColors.orange,
                                       highlightColorB: AppColors.brightBlue,
                                       highlightColorC: AppColors.lightOrange)
    static let black = ColorScheme(lightShadow: AppColors.altlightShadow,
                                       darkShadow: AppColors.altDarkShadow,
                                       primaryTextColor: AppColors.lightGrey,
                                       secondaryTextColor: AppColors.grey,
                                       bg: .black,
                                       highlightColorA: AppColors.orange,
                                       highlightColorB: AppColors.brightBlue,
                                       highlightColorC: AppColors.lightOrange)
    
    func copy() -> ColorScheme {
        return ColorScheme(lightShadow: lightShadow,
                                darkShadow: darkShadow,
                                primaryTextColor: primaryTextColor,
                                secondaryTextColor: secondaryTextColor,
                                bg: bg,
                                highlightColorA: highlightColorA,
                                highlightColorB: highlightColorB,
                                highlightColorC: highlightColorC)
    }
    func getCase() -> SchemeCase? {
        if self == ColorScheme.normal {
            return .normal
        }
        if self == ColorScheme.alt {
            return .alt
        }
        return nil
    }
    
    func get(property: ColorKey) -> UIColor? {
        return self[String(describing: property)] as? UIColor
    }
    
}

class AppColors {
    static let pink = UIColor(196, 91, 170)
    
    static let lightShadow = UIColor(250, 251, 255)
    static let darkShadow = UIColor(166, 171, 189)
    static let lightBG = UIColor(235, 236, 240)
    
    static let whiteLightShadow = UIColor(251, 251, 251)
    static let whiteDarkShadow = UIColor(189, 189, 189)
    
    static let altlightShadow = UIColor(36, 36, 36)
    static let altDarkShadow = UIColor(0, 0, 0)
    static let altBG = UIColor("1C1C1E")

    static let lightOrange = UIColor(255, 154, 71)
    static let darkOrange = UIColor(184, 83, 0)
    static let orange = UIColor(253, 115, 0)
    static let green = UIColor(6, 214, 160)
    static let purple = UIColor(170, 58, 159)
    static let grey = UIColor(182, 182, 182)
    static let lightGrey = UIColor(216, 216, 216)
    static let white = UIColor(255, 255, 255)
    static let mellowBlue = UIColor(30, 88, 113)
    static let darkBlue = UIColor(1, 22, 39)
    static let lightBlue = UIColor(49, 69, 82)
    static let brightBlue = UIColor(74, 78, 105)
    static let red = UIColor(255, 0, 34)
    static let darkGrey = UIColor(129, 129, 129)
    static let darkenedLightBG = UIColor(61, 65, 81)
}

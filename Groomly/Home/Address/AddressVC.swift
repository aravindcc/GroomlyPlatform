//
//  AddressVC.swift
//  Groomly
//
//  Created by clar3 on 13/05/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import PinLayout
import FlexLayout
import IGListKit

final class AddressModel: ListDiffable {
    
    func diffIdentifier() -> NSObjectProtocol {
        return (addressOne + addressTwo + city + postcode) as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        if self === object { return true }
        guard   let object = object as? AddressModel
            else { return false }
        return  addressOne == object.addressOne &&
                addressTwo == object.addressTwo &&
                city == object.city &&
                postcode == object.postcode
    }
    
    
    init(
        addressOne: String,
        addressTwo: String,
        city: String,
        postcode: String,
        id: Int? = nil,
        primary: Bool = false,
        formatted_address: String? = nil
    ) {
        self.addressOne = addressOne
        self.addressTwo = addressTwo
        self.city = city
        self.postcode = postcode
        self.id = id
        self.primary = primary
        self.formatted_address = formatted_address
    }
    
    var id: Int?
    var addressOne: String
    var addressTwo: String
    var city: String
    var postcode: String
    var primary: Bool
    var formatted_address: String?
}


class AddressVC: APListController<AddressModel, AddressListEntryCell> {

    init(viewType: APListModeReduced) {
        //super.init(viewType: .embedded)
        switch viewType {
        case .edit:
            super.init(viewType: .edit(onNew: NewAddressVC()))
        case .embedded:
            super.init(viewType: .embedded)
        case .full(let onSelect):
            super.init(viewType: .full(doneTitle: "ok", onSelect: onSelect, onNew: NewAddressVC()))
        case .selection(let onSelect):
            super.init(viewType: .selection(doneTitle: "ok", onSelect: onSelect))
        }
        
        apiDataSource = AddressBackend.shared
        preferredContentSize = CGSize(width: .zero, height: 400)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
    

//
//  NewAddress.swift
//  Groomly
//
//  Created by clar3 on 12/05/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import PinLayout
import FlexLayout

class NewAddressVC: APListNewController<AddressModel> {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        IQKeyboardManager.shared.previousNextDisplayMode = .alwaysShow
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        IQKeyboardManager.shared.previousNextDisplayMode = .alwaysHide
    }
    
    let containerView = IQPreviousNextView()
    let fields = ["Address Line 1", "Address Line 2", "City", "Postcode" ]
    var addButton: FlexButton!
    var textFields: [SoftTextField] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        preferredContentSize = CGSize(width: .zero, height: 400)
        view.colorScheme = .normal
        view.schemedBG()
        view.addSchemedView(containerView)
        
        addButton = FlexButton()
        addButton.title = "Add"
        addButton.preset = true
        addButton.tapped = {
            [weak self] _ in
            guard
                let addrOne = self?.textFields[0].field.text,
                let addrTwo = self?.textFields[1].field.text,
                let city = self?.textFields[2].field.text,
                let postcode = self?.textFields[3].field.text
            else { return }
            let model = AddressModel(
                addressOne: addrOne,
                addressTwo: addrTwo,
                city: city,
                postcode: postcode
            )
            self?.apiDataSource?.add(model: model)
            self?.navigationController?.popViewController(animated: true)
        }
        containerView.flex.marginHorizontal(20).define { (flex) in
            for f in fields {
                let field = SoftTextField(title: f)
                flex.addItem(field).height(45).marginVertical(15)
                textFields.append(field)
            }
            flex.addItem(addButton).width(addButton.wrappedWidth).alignSelf(.center).height(40).marginTop(35)
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        containerView.pin.all(view.pin.safeArea)
        containerView.flex.layout()
    }
}

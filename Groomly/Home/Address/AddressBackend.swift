//
//  AddressBackend.swift
//  Groomly
//
//  Created by Ravi on 01/08/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import Foundation
import Async

class AddressBackend: ListBackend<AddressModel> {
    static let shared = AddressBackend()
    
    override func add(model: AddressModel) {
        run(
            process: APIClient.api.create(address: model),
            loadingMessage: "Adding address...",
            successMessage: "Added address",
            errorMessage: "Error adding address"
        )
    }
    
    override func remove(model: AddressModel) {
        run(
            process: try? APIClient.api.delete(address: model),
            loadingMessage: "Removing address...",
            successMessage: "Removed address",
            errorMessage: "Error removing address"
        )
    }
    
    override func select(model: AddressModel) {
        guard selected?.id != model.id else { return }
        run(
            process: try? APIClient.api.select(address: model),
            loadingMessage: "Selecting address as primary...",
            successMessage: "Selected address",
            errorMessage: "Error selecting address"
        )
    }
    
    override func refreshList() {
        run(
            process: APIClient.api.getAddresses(),
            loadingMessage: nil,
            successMessage: nil,
            errorMessage: "Error loading your information."
        )
    }
    
    override func getSelected() -> Int {
        for (i, addr) in cachedList.enumerated() {
            if addr.primary {
                return i
            }
        }
        return 0
    }
}

//
//  AddressListSectionController.swift
//  Groomly
//
//  Created by clar3 on 13/05/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import IGListKit
import PinLayout
import FlexLayout

extension UILabel {
    func singleCentralLine() {
        adjustsFontSizeToFitWidth = true
        baselineAdjustment = .alignCenters
        numberOfLines = 1
    }
}

extension String {
    func add(phrase string: String) -> String {
        if string == "" && self == "" {
            return ""
        } else if string == "" {
            return self
        } else if self == "" {
            return "\(string)"
        } else {
            return "\(self), \(string)"
        }
    }
}

class AddressListEntryCell: APListEntryCell {
    private let mainLabel = MaskedLabel(left: 16, weight: .semibold, color: .black)
    private let secondaryLabel = UILabel(left: 14, weight: .bold)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        infoView.flex.define { (flex) in
            flex.addItem(mainLabel).grow(1)
            flex.addItem(secondaryLabel).grow(1)
        }
        
        mainLabel.addACGradient()
        secondaryLabel.schemedTextColor(.highlightColorB)

    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func bindViewModel(_ viewModel: Any) {
        super.bindViewModel(viewModel)
        
        guard let info = viewModel as? ListInfoModel, let model = info.model as? AddressModel  else { return }

        if info.editing {
            mainLabel.alpha = 0.5
            secondaryLabel.alpha = 0.5
        } else {
            mainLabel.alpha = 1
            secondaryLabel.alpha = 1
        }
        mainLabel.text = model.addressOne.add(phrase: model.addressTwo)
        secondaryLabel.text = model.city.add(phrase: model.postcode)
    }
    
}

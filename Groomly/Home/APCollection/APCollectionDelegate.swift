//
//  APCollectionDelegate.swift
//  Learning
//
//  Created by clar3 on 16/03/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import FlexLayout

protocol APCollectionDelegate: AnyObject {
    func put(cache model: ImageModel, index: Int, with image : UIImage) -> Void
    func get(cache model: ImageModel, index: Int) -> UIImage?
    func component(for model: ImageModel, index: Int, in parent: Flex) -> Void
    func tapped(model: ImageModel, at point : CGRect, in cell: UIView, transition: UIImage?) -> Void
    func didSelect(index: Int?) -> Void
}

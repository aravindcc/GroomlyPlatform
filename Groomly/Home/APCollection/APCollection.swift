//
//  CustomCollection.swift
//  player
//
//  Created by clar3 on 24/12/2018.
//  Copyright © 2018 Ashla. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

class APCollection : UIView {
    weak var delegate : APCollectionDelegate? {
        didSet {
            for v in views {
                v.delegate = delegate
            }
        }
    }

    private let verticalMiddleOffset : CGFloat = 0.05
    private var WIDTH : CGFloat = 180
    private let TAPER_FROM : CGFloat = 5
    private let USE_FULL_TO_TAPER : Bool = false
    private let SPACING : CGFloat = -0.3
    private let SCALE_CAP : CGFloat = 1.8
    private let END_MOMENTUM : Int = 8
    private var views : [APCollectionCell] = []
    private var selected : Int = 0 {
        didSet {
            notifiedSelected = selected
        }
    }
    private var notifiedSelected: Int? {
        didSet {
            if oldValue != notifiedSelected {
                delegate?.didSelect(index: notifiedSelected)
            }
        }
    }
    private let original_urgency : TimeInterval = 0.6
    private var prevTranslation : CGPoint = .zero
    private var count : Int!
    private var direction : Int = 0
    private var touchStartTime : CFTimeInterval?
    private var previousLocation : CGPoint?
    private var displayLinkHandle : CADisplayLink?
    private var holdingMutiSelected : Int?
    private var animateMomentumStart : Date!
    
    private var dropScale : CGFloat = 0
    
    
    // API ------------------------------------------------------------
    func deselectCell() {
        updateViews(deselect: true)
    }
    
    var currentPosition : Int {
        get {
            return selected
        }
        set (val) {
            selected = val%%count
            updateViews(deselect: true)
        }
    }
    var currentCount : Int {
        get {
            return count
        }
    }
    var lastSelected : UIView?
    var items : [ImageModel] = [] {
        didSet {
            count = items.count
            prev_selected_i = max(0, min(prev_selected_i, count - 1))
            selected = max(0, min(selected, count - 1))
            self.updateViews(0)
        }
    }
    
    
    // INITIALISATION --------------------------------------------------
    init(frame: CGRect, delegate: APCollectionDelegate, dropScale: CGFloat = 0) {
        super.init(frame: frame)
        self.delegate = delegate
        self.dropScale = dropScale
        count = items.count
        
        let pangesture = UIPanGestureRecognizer(target: self, action: #selector(handlePan(_:)))
        addGestureRecognizer(pangesture)
        
        setUpDraw()
        
        accessibilityIdentifier = "apcollection"
    }
    
    func setUpDraw() {
        WIDTH = frame.height - dropScale
        self.updateViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func weightOf(_ pos : Int, expand : Float = 1) -> CGFloat {
        let i = Float(reZero(pos)) * expand
        func fx(_ x : Float) -> Float {
            let f = 1 + pow(x/2,2)
            return 1/f
        }
        return CGFloat(fx(i))
    }
    
    func reZero(_ row : Int) -> Int{
        return abs(row - selected)
    }
    
    func calculateSize(_ pos : Int, ext: Float = 1) -> CGFloat {
        var expand : Float = 1.3
        if selected < 2  {
            expand = 1.1
        }
        let dec =  max(0.01, self.weightOf(pos, expand: expand * ext))
        return WIDTH * dec
    }
    
    
    private var prev_selected_i : Int = -1
    private var reusableCells : [APCollectionCell] = []
    private var previousSkipBoundary : Int = 0
    func updateViews(_ override_urgency : TimeInterval? = nil, deselect: Bool = false){
    
        // break out if nothing noteworthy
        if (count == 0) {
            for v in views {
                v.isHidden = true
            }
            return
        }
        
        
        let urgency = override_urgency ?? original_urgency // speed up the animation
        let shadow = 3 // add extra view at top to have smooth conveyor belt
        
        
        /**
         Attempts to find the correct cell inside the held views.
         If there is none then will attempt to use a reusuable cells.
         If no reusable cell a new cell will be created with default dimensions and appended to held views
         */
        func getSubview(_ i : Int, smooth: Bool = false) -> (Bool, APCollectionCell) {
            guard i >= views.count else { let v = views[i]; return (v.isHidden, v) }
            if let reusable_queue = reusableCells.popLast() {
                views.append(reusable_queue)
                if i == 0 { return (true, reusable_queue) }
                else {
                    reusable_queue.frame = getSubview(i - 1).1.frame
                    return (false, reusable_queue)
                }
            }
            let baseLabelRect = i >= 1 ? getSubview(i - 1).1.frame : CGRect(x: 0, y: 0, width: WIDTH, height: frame.height)
            let v = APCollectionCell(frame: baseLabelRect)
            v.delegate = delegate
            views.append(v)
            addSchemedView(v)
            return (false, v)
        }
        
        /**
         Improvement could possibly use double buffering to calculate this on the background thread and have a potentially expensive preUpdate already performed (like ComponentKit)
         Will get the view to correct dimensions before being populated with data (preUpdate). The view will then have its update method trigerred to animate the change.
         The view is returned for further computation.
         - Parameter dataPos: This is the position of the element in the data
         - Parameter viewPos: This is the position of the reusable view in views.
         - Parameter y:This is the running total of how far across the screen everything is.
         - Parameter shadow:This is whether the view being created is a shadow.
         - Parameter preUpdate: Perform data updates before initialising animation
         */
        func getViewReady(dataPos: Int, viewPos: Int, y: inout CGFloat, shadow: Bool, preUpdate: ((APCollectionCell) -> Void)? = nil) -> APCollectionCell {
            let i = dataPos
            let zi = CGFloat(reZero(i))
            let weightCoeff: Float = direction == 0 ? 1.5 : 1.2
            let scaleCoeff: Float = 1
            
            var offset: CGFloat = 0
            offset =  max(20 - pow(zi, 2.5), 0)
            if zi == 0 { offset = 23 }
            
            let weight = weightOf(i, expand: weightCoeff)
            let scale = calculateSize(i, ext: scaleCoeff)
            let (refresh, view) = getSubview(viewPos)
            view.isHidden = shadow
            view.layer.zPosition = CGFloat(count - reZero(i))
            preUpdate?(view)
            view.update(with: weight, and: scale, to: y, with: refresh ? 0 : urgency, offset: offset)
            view.accessibilityIdentifier = zi == 0 ? "apselected" : "apnormal"
            if !shadow { y += scale * (1.0 + SPACING) }
            return view
        }
        
        
        /**
         This will create the required number of shadow views giving a start location in terms of y/x, dataPos, viewPos either before or after.
         - Parameter origin:Where to start the physical position of the elements
         - Parameter dataPos: This is the position of the element in the data
         - Parameter viewPos: This is the position of the reusable view in views.
         - Parameter prepending:Whether shadows are before or after main set.
         */
        func addShadowViews(_ origin: CGFloat, dataPos: Int, viewPos: Int, prepending: Bool) {
            guard shadow != 0 else { return }
            let startUnit = prepending ? -1 : 1
            var shadowy: CGFloat = origin
            var dontSkipfirst = prepending
            
            for j in 1...shadow {
                let i = dataPos + j * startUnit
                if dontSkipfirst { shadowy += calculateSize(i) * (1.0 + SPACING) * CGFloat(startUnit) }
                dontSkipfirst = true
                let _ = getViewReady(dataPos: i, viewPos: viewPos + j - 1, y: &shadowy, shadow: true)
            }
        }
        

        // main loop state
        var skipped : Int = 0
        var reached : Int = 0
        var goneThroughAll = false
        var d = 0
        var y: CGFloat = 0
        
        // allow for flow by getting conveyor belt effect of views.
        // this is done by calculating the currently selected view is the same and the direction is forward.
        var pre_skipped = 0
        var current_selected_i = 0
        for k in 0..<count {
            let scale = calculateSize(k)
            if scale < SCALE_CAP {
                pre_skipped += 1;
                continue
            }
            let i = k - pre_skipped + shadow
            if k == selected {
                current_selected_i = i
                if i == prev_selected_i {
                    if direction == -1, let tp = views.popLast() {
                        views.insert(tp, at: 0)
                    }
                    break
                }
            }
        }
        prev_selected_i = current_selected_i
        
        /**
         This for the fact that the sizes are perfectly calibrated so there needs to a shift from going top down, to bottom up.
         */
        // different tappering to set inital y
        let taperPos: CGFloat = CGFloat(min(count - selected - 1, Int(TAPER_FROM)))
        if count > Int(TAPER_FROM) {
            let desiredcenter: CGFloat! = (frame.width - WIDTH) / 2
            let scales = (0..<selected).map { calculateSize($0) * (1.0 + SPACING) }
            let selectedY = scales.reduce(0, +)
            let diff = desiredcenter + (WIDTH * verticalMiddleOffset) - selectedY
            let dontMoveDown = taperPos == TAPER_FROM
            y = dontMoveDown ? min(0, diff) : diff
        }
        
        
        var prependedShadows = false
        while !goneThroughAll {
            let scale = calculateSize(d)
            if scale < SCALE_CAP {
                y += scale * (1.0 + SPACING)
                skipped += 1;
                d += 1;
                if d >= count {
                    goneThroughAll = true
                }
                continue
            }
            
            if !prependedShadows {
                if previousSkipBoundary != skipped {
                    let diffVal = abs(skipped - previousSkipBoundary)
                    if skipped > previousSkipBoundary {
                        reusableCells += views.popFirst(diffVal)
                        reusableCells.forEach {cell in cell.isHidden = true; cell.isSelected = false}
                    } else {
                        views.append(contentsOf: reusableCells.popFirst(diffVal))
                    }
                }
                previousSkipBoundary = skipped
                addShadowViews(y, dataPos: d, viewPos: 0, prepending: true)
                prependedShadows = true
            }
            let i = d - skipped + shadow
            reached = i
            
            let view = getViewReady(dataPos: d, viewPos: i, y: &y, shadow: false) { [weak self] cell in
                guard   let WIDTH = self?.WIDTH,
                        d < self?.count ?? -1,
                        let item = self?.items[d]
                else { return }
                cell.apply(model: item, index: d)
                if let url = item.image {
                    cell.setImageURL(url, maxDim: WIDTH)
                }
            }
            
            if d >= 0 && d < items.count {
                view.isSelected = d == selected
            } else {
                view.isSelected = false
                view.alpha = 0
            }

            d += 1
            if d >= count {
                goneThroughAll = true
            }
        }
        
        addShadowViews(y, dataPos: d - 1, viewPos: reached + 1, prepending: false)
        
        let bottom = reached + 1 + shadow
        if bottom < views.count {
            reusableCells.append(contentsOf: views.pop(from: bottom))
            reusableCells.forEach {cell in cell.isHidden = true; cell.isSelected = false}
        }
    }
    
    override func removeFromSuperview() {
        views = []
        reusableCells = []
        super.removeFromSuperview()
    }
    
}


// CUSTOM SCROLLING ------------------------------------------------------------------------
extension APCollection {
    
    @objc private func handlePan(_ gestureRecognizer : UIPanGestureRecognizer) {
        let touchLocation = gestureRecognizer.location(in: self)
        switch gestureRecognizer.state {
        case .began:
            displayLinkHandle?.invalidate() //remove previous momentum scrolling
            prevTranslation = .zero
            touchStartTime = CACurrentMediaTime()
            previousLocation = touchLocation
        case .changed:
            guard let previousLocation = previousLocation else { self.previousLocation = touchLocation; return }
            let p = touchLocation
            let o = previousLocation
            prevTranslation += (p - o)
            var units = (prevTranslation.x * -1) / 45
            units.clamp(from: -1, to: 1)
            if abs(units) == 1 {
                prevTranslation = .zero
                self.previousLocation = nil
                touchStartTime = CACurrentMediaTime()
                let prevSelected = selected
                direction = units > 0 ? 1 : -1
                selected = (selected + direction).clamped(from: 0, to: count - 1)
                if prevSelected == selected { return }
                updateViews()
            }
        case .cancelled:
            clearTouchesHist()
        case .ended:
            guard let previousLocation = previousLocation else { clearTouchesHist(); return }
            let endTime = CACurrentMediaTime()
            let p = touchLocation
            let o = previousLocation
            let delta = p - o
            let time_delta = endTime - (touchStartTime ?? 0)
            var speed = delta.x / CGFloat(time_delta * 0.8)
            let maxSpeed : CGFloat = 80;
            
            if abs(speed) < 5 || time_delta > 0.35 { clearTouchesHist(); return }
            speed.clamp(from: -maxSpeed, to: maxSpeed)
            
            let units = round(speed * -1 / 20)
            direction = units > 0 ? 1 : -1
            
            guard   (direction == 1 && (selected + END_MOMENTUM) < count) ||
                    (direction == -1 && selected > END_MOMENTUM)
            else { clearTouchesHist(); return }
            
            holdingMutiSelected = selected + Int(units)
            holdingMutiSelected?.clamp(from: 0, to: count - 1)
            if holdingMutiSelected == selected { clearTouchesHist(); return }
            
            displayLinkHandle?.invalidate()
            animateMomentumStart = Date()
            if displayLinkHandle == nil {
                displayLinkHandle = CADisplayLink(target: self, selector: #selector(updateSelected))
                displayLinkHandle?.preferredFramesPerSecond = 10
            }
            displayLinkHandle?.add(to: .main, forMode: .default)
        default:
            return
        }
    }
    
    private func clearTouchesHist() {
        prevTranslation = .zero
        direction = 0
        previousLocation = nil
        displayLinkHandle?.invalidate()
        displayLinkHandle = nil
        holdingMutiSelected = nil
    }
    
    @objc func updateSelected(){
        guard   let holdingMutiSelected = holdingMutiSelected,
                direction != 0,
                (direction > 0 && holdingMutiSelected > selected) ||
                (direction < 0 && holdingMutiSelected < selected)
            else { clearTouchesHist(); return }
        direction = holdingMutiSelected - selected > 0 ? 1 : -1
        selected += direction
        updateViews()
    }
}


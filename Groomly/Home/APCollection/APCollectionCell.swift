//
//  customCollectionCell.swift
//  player
//
//  Created by clar3 on 11/04/2019.
//  Copyright © 2019 Ashla. All rights reserved.
//

import Foundation
import UIKit
import FlexLayout
import PinLayout
import Kingfisher

class APCollectionCell : UIView {
    
    static let DEF_BORDER_RADIUS : CGFloat = 10
    static let transitionID : String = "APCollectionCellTransition"
    static func configureBG(image: UIImage? = nil) -> UIImageView {
        let imagev = UIImageView()
        imagev.contentMode = .scaleAspectFill
        imagev.layer.cornerRadius = APCollectionCell.DEF_BORDER_RADIUS
        imagev.layer.masksToBounds = true
        imagev.image = image
        return imagev
    }
    
    weak var delegate : APCollectionDelegate?
    
    private weak var imagev : UIImageView?
    private weak var title : UIImageView?
    private var model : ImageModel?
    private var image: URL?
    private var setImage: URL?
    private let imageQueue = DispatchQueue(label: "cellImageQueue")
    private func setImage(_ data: UIImage?, at url : URL) {
        guard (image == nil || url == image) && (url != setImage) else { return }
        setImage = url
        DispatchQueue.main.sync {
            imagev?.image = data
        }
    }
    
    var isSelected : Bool = false
    
    func apply(model : ImageModel, index: Int) {
        self.model = model
        title?.image = delegate?.get(cache: model, index: index) ?? captureComponent(model, index: index)
    }
    
    func setImageURL(_ url : URL?, maxDim WIDTH: CGFloat) {
        guard let url = url else {
            imageQueue.async {
                self.setImage = nil
                DispatchQueue.main.sync {
                    self.imagev?.image = nil
                }
            }
            return
        }
        imageQueue.async {
            self.image = url
            let processor = DownsamplingImageProcessorScaled(size: CGSize(width: WIDTH, height: WIDTH))
            KingfisherManager.shared.retrieveImage(with: url, options: [.processor(processor)]) { result in
                switch result {
                case .success(let value):
                    self.imageQueue.async {
                        self.setImage(value.image, at: url)
                    }
                case .failure(let error):
                    Log(error) // The error happens
                }
            }
        }
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleCellTap(_:)))
        addGestureRecognizer(tapGesture)
        superview?.important(gesture: tapGesture)
        setUpView()
    }
    
    private func captureComponent(_ model: ImageModel, index: Int) -> UIImage? {
        let max = UIScreen.main.bounds.width * 0.4
        let hiddenparent = UIView()
        hiddenparent.frame.size = CGSize(width: max, height: max)
        hiddenparent.backgroundColor = .clear
        hiddenparent.layer.cornerRadius = APCollectionCell.DEF_BORDER_RADIUS
        hiddenparent.layer.masksToBounds = true
        hiddenparent.clipsToBounds = true

        let dark = ColorScheme.normal.darkShadow
        let gradient = CAGradientLayer(start: .topCenter, end: .bottomCenter, colors: [UIColor.clear.cgColor, dark.withAlphaComponent(0.7).cgColor], type: .axial)
        hiddenparent.layer.insertSublayer(gradient, at: 0)
        gradient.pin.horizontally().bottom().top(40%)
        
        let flexContainer = UIView()
        hiddenparent.addSchemedView(flexContainer)
        flexContainer.pin.horizontally(5%).bottom().top(50%)
        
        flexContainer.flex.justifyContent(.center).define { flex in
            delegate?.component(for: model, index: index, in: flex)
        }
        
        flexContainer.flex.layout()
        
        hiddenparent.addSchemedView(flexContainer)
        
        if let img = UIView.image(with: hiddenparent) {
            delegate?.put(cache: model, index: index, with: img)
            return img
        }
        return nil
    }
    
    func setUpView() {
        layer.cornerRadius = APCollectionCell.DEF_BORDER_RADIUS
        layer.masksToBounds = true
        
        let imagev = APCollectionCell.configureBG(image: nil)
        addSchemedView(imagev)
        self.imagev = imagev
        
        let titleImg = UIImageView()
        addSchemedView(titleImg)
        self.title = titleImg
        
        self.imagev?.isUserInteractionEnabled = false
        self.title?.isUserInteractionEnabled = false
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        imagev?.pin.all()
        title?.pin.all()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc private func handleCellTap(_ gestureRecogniser: UITapGestureRecognizer) {
        guard let model = model, let superview = superview, isSelected else { return }
        delegate?.tapped(model: model, at: frame, in: superview, transition: self.imagev?.image)
    }
    
    func update(with weight : CGFloat,
                and scale : CGFloat,
                to : CGFloat,
                with urgency : TimeInterval,
                offset: CGFloat? = nil){
        
        let newFrame = CGRect(x: to, y: offset ?? frame.origin.y, width: scale, height: scale)
        UIView.animate(withDuration: urgency, delay: 0, options: [.allowUserInteraction], animations: {
            self.imagev?.alpha = weight
            self.title?.alpha = weight
            self.frame = newFrame
            let radius = min(APCollectionCell.DEF_BORDER_RADIUS, (weight + 0.2) * APCollectionCell.DEF_BORDER_RADIUS)
            self.layer.cornerRadius = radius
            self.imagev?.layer.cornerRadius = radius
            self.backgroundColor = ColorScheme.normal.primaryTextColor.linear(with: ColorScheme.normal.bg, weight: weight * 0.7 + 0.3)
            self.layoutIfNeeded()
        }, completion: nil)
    }
    
    override func removeFromSuperview() {
        super.removeFromSuperview()
    }
}

fileprivate extension UIColor {
    func linear(with other: UIColor, weight: CGFloat) -> UIColor {
        let (red, blue, green) = rgbComponents
        let (otherRed, otherGreen, otherBlue) = other.rgbComponents
        let resultRed = CGFloat(red) + weight * CGFloat(otherRed - red)
        let resultGreen = CGFloat(green) + weight * CGFloat(otherGreen - green)
        let resultBlue = CGFloat(blue) + weight * CGFloat(otherBlue - blue)
        return UIColor(resultRed, resultBlue, resultGreen)
    }
}

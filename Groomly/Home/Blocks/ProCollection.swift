//
//  ProCollection.swift
//  Groomly
//
//  Created by Ravi on 23/09/2021.
//  Copyright © 2021 Ashla. All rights reserved.
//


import UIKit
import IGListKit
import PinLayout
import FlexLayout
import Cosmos
import Kingfisher
import Jelly

fileprivate let innerBlockPadding: CGFloat = 10

fileprivate class ProCell: UICollectionViewCell, ListBindable {
    static let HEIGHT: CGFloat = 100
    static let PADDING: CGFloat = 20
    static let FULLHEIGHT: CGFloat = 2 * PADDING + HEIGHT
    
    private let container = UIView()
    private var addedSoft = false
    
    private let photo = UIImageView()
    
    private let textContainer = UIView()
    private let name = UILabel(left: 16, weight: .bold)
    private let company = UILabel(left: 15, weight: .regular)
    private let stars = CosmosView.normal(sized: 20)
    private let numReviews = UILabel(left: 13, weight: .regular)
    
    private let distanceContainer = UIView()
    private let distanceIcon = UIImageView()
    private let distanceText = UILabel(central: 13, weight: .bold)
    
    func load(_ url: URL?) {
        guard let url = url else { return }
        let s = UIScreen.main.bounds.width * 0.5
        let processor = DownsamplingImageProcessorScaled(size: CGSize(width: s, height: s))
        KingfisherManager.shared.retrieveImage(with: url, options: [.processor(processor)]) { result in
            switch result {
            case .success(let value):
                DispatchQueue.main.async {
                    self.photo.image = value.image
                    self.setNeedsLayout()
                }
            case .failure(let error):
                Log(error) // The error happens
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        accessibilityIdentifier = "ProCellID"
//        container.schemedBG()
//        container.layer.shadowColor = UIColor.black.cgColor
//        container.layer.shadowOpacity = 0.2
//        container.layer.shadowOffset = .zero
//        container.layer.shadowRadius = 8
//        container.backgroundColor = .clear
//        container.borderColor = colorScheme?.secondaryTextColor.withAlphaComponent(0.5)
//        container.borderWidth = 2
        
        name.schemedTextColor(.primaryTextColor)
        company.schemedTextColor(.primaryTextColor)
        numReviews.schemedTextColor(.secondaryTextColor)
        distanceText.schemedTextColor(.primaryTextColor)
        distanceIcon.schemedTintColor(.highlightColorB)
        let config = UIImage.SymbolConfiguration(pointSize: 20, weight: .semibold)
        let icon = UIImage(systemName: "location", withConfiguration: config)
        distanceIcon.image = icon
        
        photo.schemedBG(.secondaryTextColor, alpha: 0.3)
        photo.maskToBounds = true
        photo.contentMode = .scaleAspectFill
        photo.clipsToBounds = true
        stars.rating = 0

        let containerPadding: CGFloat = 10
        let imgMargin: CGFloat = 5
        let imgHeight = ProCell.HEIGHT - 2 * containerPadding
        photo.layer.cornerRadius = imgHeight / 2
        container.layer.cornerRadius = ProCell.HEIGHT / 2
        container.flex.direction(.row).alignItems(.stretch).padding(containerPadding).define { (flex) in
            flex.addItem(photo).alignSelf(.center).width(imgHeight).height(imgHeight).shrink(0).marginHorizontal(imgMargin)
            flex.addItem(textContainer)
                .grow(1)
                .shrink(1)
                .marginLeft(10)
                .marginRight(15)
                .direction(.column)
                .justifyContent(.spaceAround)
                .alignItems(.stretch)
                .marginVertical(5)
                .define { nest in
                nest.addItem(name)
                nest.addItem(company).marginTop(4)
                nest.addItem(stars).marginTop(8)
                nest.addItem(numReviews).marginTop(4)
            }
            flex.addItem().width(2).view?.schemedBG(.darkShadow, alpha: 0.2)
            flex.addItem(distanceContainer).width(75).shrink(0).marginRight(5).justifyContent(.center).alignItems(.center).define { nest in
                nest.addItem(distanceIcon).marginBottom(10)
                nest.addItem(distanceText).alignSelf(.stretch)
            }
        }
        photo.yoga.overflow = .hidden
        textContainer.yoga.overflow = .hidden

        addSubview(container)
    }
    
    required init?(coder: NSCoder) {
        super.init(frame: .zero)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        container.pin.vertically(ProCell.PADDING).horizontally()
        
        if !addedSoft {
//            container.layer.shadowPath = UIBezierPath(
//                roundedRect: container.bounds,
//                byRoundingCorners:.allCorners,
//                cornerRadii: CGSize(width: ProCell.HEIGHT / 2, height: ProCell.HEIGHT / 2)
//            ).cgPath
            container.addSoftUIEffectForView(cornerRadius: ProCell.HEIGHT / 2)
            self.setNeedsDisplay()
            addedSoft = true
        }
        container.flex.layout()
    }
    
    func bindViewModel(_ viewModel: Any) {
        guard let m = (viewModel as? ListableClientStubResponse)?.stub, let model = m.profile else { return }
        load(m.profile?.image)
        name.text = (model.first_name ?? "") + " " + (model.last_name ?? "")
        company.text = model.company ?? "company..."
        if let num = model.review_score {
            numReviews.text = "\(num.num) review\(num.num == 1 ? "" : "s")"
            stars.rating = Double(num.score)
        } else {
            numReviews.text = "no reviews"
            stars.rating = 0
        }
        distanceText.text = m.distance
        layoutSubviews()
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        UIView.animate(withDuration: 0.2, animations: { self.container.alpha = 0.99; self.container.invertSoftShadows() }) { _ in
            UIView.animate(withDuration: 0.2, animations: { self.container.alpha = 1; self.container.invertSoftShadows() })
        }
    }
}

final class ProCollection: UIViewController, ListAdapterDataSource {
    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        return proItems
    }
    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        TableSectionController<ListableClientStubResponse, ProCell>(cellHeight: ProCell.FULLHEIGHT) { [weak self] in
            self?.showStylistview(model: $0.stub)
        }
    }
    func emptyView(for listAdapter: ListAdapter) -> UIView? { nil }

    lazy var cv : UICollectionView = {
        let cv = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        cv.accessibilityIdentifier = "proCollectionVC"
        cv.showsHorizontalScrollIndicator = false
        cv.showsVerticalScrollIndicator = false
        return cv
    }()
    lazy var adapter: ListAdapter = {
        return ListAdapter(updater: ListAdapterUpdater(), viewController: nil)
    }()
    
    var proItems: [ListableClientStubResponse] = [] {
        didSet {
            adapter.reloadData()
        }
    }
    
    func set(items: [ClientStubResponse]) {
        proItems = items.map { ListableClientStubResponse(stub: $0) }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(cv)
        cv.schemedBG()
        adapter.collectionView = cv
        adapter.dataSource = self
        view.maskToBounds = false
        cv.maskToBounds = false
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        view.pin.all()
        cv.pin.all()
    }
    
    var filterAnimator: Jelly.Animator?
    private func showStylistview(model: ClientStubResponse) {
        let uiConfiguration = PresentationUIConfiguration(
            cornerRadius: 10,
            backgroundStyle: .dimmed(alpha: 0.5),
            isTapBackgroundToDismissEnabled: true,
            corners: []
        )
        let chosenHeight: Size = .fullscreen
        let size = PresentationSize(width: .fullscreen, height: chosenHeight)
        let alignment = PresentationAlignment(vertical: .bottom, horizontal: .center)
        let interaction = InteractionConfiguration(presentingViewController: self, completionThreshold: 0.5, dragMode: .canvas, mode: .dismiss)
        let presentation = CoverPresentation(directionShow: .bottom, directionDismiss: .bottom, uiConfiguration: uiConfiguration, size: size, alignment: alignment, marginGuards: .zero, interactionConfiguration: interaction)
        
        let vc = StylistVC(model: model)
        filterAnimator = Animator(presentation: presentation)
        filterAnimator?.prepare(presentedViewController: vc)
        present(vc, animated: true, completion: nil)
    }
    
}

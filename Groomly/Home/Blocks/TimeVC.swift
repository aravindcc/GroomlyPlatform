//
//  TimeVC.swift
//  Learning
//
//  Created by clar3 on 25/03/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//


import UIKit
import PinLayout
import CalendarKit
import DateToolsSwift

class TimeVC: DayViewController, DayViewStateUpdating {
    
    enum ProcessState {
        case invalid, editing, ok
    }
    private let titleLabel = UILabel(central: 20, weight: .bold)
    private var doneButton: FlexButton!
    private let day = DayDiary()
    private var selectedEvent: EventDescriptor?
    
    private let givenText = "New Appointment"
    private let appointmentDuration = 90
    var completion : ((DiaryEntry?) -> Void)?
    var initalDate: Date?
    private var processState: ProcessState = .invalid {
        didSet {
            switch processState {
            case .editing: doneButton.title = "ok"
            case .invalid: doneButton.title = "cancel"
            case .ok: doneButton.title = "done"
            }
        }
    }
    
    var setDurationOfEvent: Int = 60
    private var notifyMistake = false
    private var hasNotified = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.text = "Select Appointment Time"
        view.addSchemedView(titleLabel)
        view.addSchemedView(dayView)
        
        doneButton = FlexButton()
        doneButton.title = "done"
        doneButton.tapped = { [weak self] _ in
            guard let self = self else { return }
            if self.processState == .editing {
                self.notifyMistake = true
                self.dayView.timelinePagerView.endEventEditing()
            } else {
                self.hasNotified = true
                self.completion?(self.day.adding)
            }
        }
        doneButton.addTargetPinClosure { (pin) in
            pin.hCenter()
        }
        view.addSchemedView(doneButton)
        dayView.updateStyle(TimeVC.getCalendarStyle(view.colorScheme!))
        dayView.state?.subscribe(client: self)
        
        processState = .invalid
        
        view.schemedBG()
        titleLabel.schemedBG()
        titleLabel.schemedTextColor(.highlightColorB)
        
        if let schemeCase = view.colorScheme?.getCase() {
            let closure = { [weak self] (color: UIColor) -> Bool in
                guard let self = self else { return false }
                self.dayView.updateStyle(TimeVC.getCalendarStyle(self.view.colorScheme!))
                return true
            }
            view.reschemer.add(rule: closure)
            ColorConfigurator.shared.register(interestFor: schemeCase, andProperties: [.bg, .primaryTextColor], withUpdator: closure)
        }
        
        let inQ = initalDate ?? Date()
        dayView.state?.move(to: inQ)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.dayView.timelinePagerView.scrollTo(hour24: Float(inQ.hour))
        }
    }
    
    static func getCalendarStyle(_ chosenScheme: ColorScheme) -> CalendarStyle {
        var s = CalendarStyle()
        s.header.backgroundColor = chosenScheme.bg
        s.header.daySelector.selectedBackgroundColor = chosenScheme.highlightColorA
        s.header.daySelector.weekendTextColor = chosenScheme.primaryTextColor.withAlphaComponent(0.8)
        s.header.daySelector.activeTextColor = chosenScheme.primaryTextColor
        s.header.daySelector.inactiveTextColor = chosenScheme.primaryTextColor
        s.header.swipeLabel.textColor = chosenScheme.primaryTextColor
        s.header.daySymbols.weekDayColor = chosenScheme.primaryTextColor
        s.header.daySymbols.weekendColor = chosenScheme.primaryTextColor.withAlphaComponent(0.8)
        if let schemeCase = chosenScheme.getCase() {
            s.timeline = schemeCase == .normal ? TimelineStyle.normal() : TimelineStyle.alt()
        }
        return s
    }
    
    var client: Int?
    func loadData(for date: Date) {
        guard let client = client else { return }
        let getAvail = GetClientAvailability(
            clientID: client, date: date
        )
        Message.notify(
            process: APIClient.api.getClient(availability: getAvail),
            loadingMessage: "loading...",
            successMessage: nil,
            errorMessage: "failed to load"
        ) { [weak self] (result) in
            guard let self = self else { return }
            if case .success(let (availability)) = result {
                self.day.unavailable = availability.blocks.map {
                    DiaryBlock(
                        start: date.setTo(time: $0.start)!,
                        end: date.setTo(time: $0.end)!
                    )
                }
                let shift = APIClient.api.session?.deadline ?? 0
                self.day.unavailable.append(
                    DiaryBlock(start: .distantPast, end: Date().adding(.hour, value: shift))
                )
                self.dayView.timelinePagerView.reloadData()
            }
        }
    }
    
    override func loadView() {
        view = UIView()
    }
    
    override func eventsForDate(_ date: Date) -> [EventDescriptor] {
        return day.events()
    }
    
    func move(from oldDate: Date, to newDate: Date) {
        loadData(for: newDate)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let safe = view.pin.safeArea
        titleLabel.pin.horizontally().top().height(40)
        
        let padding: CGFloat = 15
        doneButton.pin.bottom(view.pin.safeArea.bottom + padding).height(35)
        
        dayView.pin.below(of: titleLabel).horizontally(safe).above(of: doneButton).marginBottom(padding)
    }

    override func dayViewDidSelectEventView(_ eventView: EventView) {
        guard let descriptor = eventView.descriptor as? Event else {
            return
        }
        if descriptor.selectable {
            eventView.removeFromSuperview()
            edit(descriptor: descriptor)
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if !hasNotified {
            completion?(nil)
        }
    }
    
    private func edit(descriptor: EventDescriptor) {
        processState = .editing
        day.adding = nil
        dayView.timelinePagerView.endEventEditing()
        selectedEvent = descriptor
        dayView.timelinePagerView.beginEditing(event: descriptor, animated: true)
    }
    
    public override func dayView(dayView: DayView, didTapTimelineAt date: Date) {
        Log("Did tap timeline at date \(date)")
        day.adding = nil
        dayView.timelinePagerView.reloadData()
        dayView.timelinePagerView.endEventEditing()
        let block = DailyVC.generateEventNearDate(date, duration: setDurationOfEvent)
        let event = DiaryEntry(
            start: block.start,
            end: block.end,
            person: givenText,
            address: "",
            order: ""
        )
        edit(descriptor: day.timeFullyBlockTransform(event))
    }
    
    override func dayViewDidTapToEndEdit(event: EventDescriptor?) {
        if let event = event {
            let block = DiaryBlock(start: event.startDate, end: event.endDate)
            if day.freeSpace(for: block) {
                dayView.timelinePagerView.endEventEditing()
            }
        }
    }
    
    override func dayView(dayView: DayView, didUpdate event: EventDescriptor) {
        let block = DiaryBlock(start: event.startDate, end: event.endDate)
        if day.freeSpace(for: block) {
            let entry = DiaryEntry(start: event.startDate, end: event.endDate, person: givenText, address: "", order: "")
            day.adding = entry
            processState = .ok
        } else {
            if notifyMistake {
                Feedback.showNotification(message: NotificationDetails(title: "Time is not available"))
            }
            processState = .invalid
        }
        notifyMistake = false
        dayView.timelinePagerView.reloadData()
    }
    
    override func dayViewDidEditDelete() {
        processState = .invalid
    }
}

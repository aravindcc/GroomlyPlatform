//
//  CheckoutVC.swift
//  Learning
//
//  Created by clar3 on 23/03/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import PinLayout
import IGListKit

enum CheckoutVisibleState {
    case Handle
    case Small
    case Large
}

class StickyBasketVC: SubProcessVC {
    let SMALL_VIEW_HEIGHT: CGFloat = 40
    let HANDLE_PADDING: CGFloat = 10
    let HANDLE_SIZE = CGSize(width: 50, height: 6)
    
    var handleHasPanned : ((UIPanGestureRecognizer) -> Void)?
    
    func minimumHeight() -> CGFloat {
        return height(for: .Handle)
    }
    func maximumHeight() -> CGFloat {
        return  height(for: .Handle) + HANDLE_PADDING +
                max(largeBounds, height(for: .Large))
    }
    func currentRange() -> (CGFloat, CGFloat) {
        let (_, lower, upper) = nextVisibleState()
        return (lower, upper)
    }
    private func nextVisibleState() -> (CheckoutVisibleState, CGFloat, CGFloat) {
        let h = view.frame.height
        if h >= height(for: .Large) {
            return (.Large, height(for: .Small) , maximumHeight())
        } else if h >= height(for: .Small) {
            return (.Small, height(for: .Small), maximumHeight())
        } else {
            return (.Handle, height(for: .Handle), height(for: .Small))
        }
    }
    private func height(for state: CheckoutVisibleState) -> CGFloat {
        switch state {
        case .Handle:
            return 2*HANDLE_PADDING + HANDLE_SIZE.height
        case .Small:
            return 3*HANDLE_PADDING + HANDLE_SIZE.height + SMALL_VIEW_HEIGHT
        default:
            return height(for: .Small) * 1.5
        }
    }
    
    private var showingState : CheckoutVisibleState = .Handle {
        didSet {
            let speed = 0.15
            switch showingState {
            case .Handle:
            UIView.animate(withDuration: speed) {
                self.smallView.alpha = 0
                self.largeView.alpha = 0
            }
            case .Small:
            UIView.animate(withDuration: speed) {
                self.smallView.alpha = 1
                self.largeView.alpha = 0
            }
            case .Large:
            basket.reload()
            UIView.animate(withDuration: speed) {
                self.smallView.alpha = 0
                self.largeView.alpha = 1
            }
            }
        }
    }
    private let handleView = UIView()
    private let smallView = CheckoutSmallView()
    private let largeView = UIView()
    private var largeBounds: CGFloat = 0
//    private var blurView: UIVisualEffectView = {
//        let blurEffect = UIBlurEffect(style: .systemChromeMaterialLight)
//        let blurredEffectView = UIVisualEffectView(effect: blurEffect)
//        return blurredEffectView
//    }()
    private var basket : APBasket!
    
    func reload(_ model: ServiceOrderModel?) {
        smallView.model = model
        basket.model = model
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.colorScheme = .alt
        basket = APBasket(self, type: .services)
        
        view.schemedBG()
        view.roundCorners(corners: [.topLeft, .topRight], radius: 10)
        view.clipsToBounds = true
        
        smallView.backgroundColor = .clear
        largeView.backgroundColor = .clear
        
        handleView.layer.cornerRadius = HANDLE_SIZE.height / 2
        handleView.schemedBG(.primaryTextColor, alpha: 0.2)
        handleView.frame.size = HANDLE_SIZE
        
       // view.addSchemedView(blurView)
        view.addSchemedView(handleView)
        view.addSchemedView(smallView)
        view.addSchemedView(largeView)
        
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(handlePan(_:)))
        view.addGestureRecognizer(panGesture)
        
        showingState = .Handle
        add(basket, anchor: largeView)
    }
    
    func calculateBounds() {
        largeBounds = basket.maxHeight(inMaxFrame: CGSize(width: view.frame.width, height: UIScreen.main.bounds.height * 0.6))
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        showingState = nextVisibleState().0
        
        //blurView.pin.all()
        handleView.pin.hCenter().top(HANDLE_PADDING).height(HANDLE_SIZE.height)
        
        if showingState != .Handle {
            calculateBounds()
            smallView.pin.horizontally().below(of: handleView).marginTop(HANDLE_PADDING).height(SMALL_VIEW_HEIGHT)
            largeView.pin.horizontally().below(of: handleView).marginTop(HANDLE_PADDING).height(largeBounds)
        }
    }
    
    @objc private func handlePan(_ gestureRecogniser: UIPanGestureRecognizer) {
        handleHasPanned?(gestureRecogniser)
    }
    
}

extension StickyBasketVC: APBasketDelegate {
    func delete(item: ServiceItemOrderModel) {
        Coordinater.shared.remove(service: item.service)
    }
    
    func changeLocation() {
        show(process: AddressVC())
    }
    
    func changeTime() {
        let vc = TimeVC()
        vc.completion = { entry in
            defer {
                vc.dismiss(animated: true)
            }
            guard let entry = entry else { return }
            print("chosen appointment from \(entry.start) till \(entry.end)")
        }
        present(vc, animated: true)
    }
}


//
//  ProView.swift
//  Groomly
//
//  Created by Ravi on 30/07/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import PinLayout
import FlexLayout
import Cosmos
import CalendarKit

extension UIView {
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder?.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
}

class WrapperView: UIView {
    let wrap: CGFloat
    init(wrap: CGFloat) {
        self.wrap = wrap
        super.init(frame: .zero)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        guard let v = subviews.first else { return }
        v.pin.all(wrap)
    }
}

extension DayHeaderStyle {
    func transparent() -> DayHeaderStyle {
        var out = DayHeaderStyle()
        out.daySelector = self.daySelector
        out.daySymbols = self.daySymbols
        out.swipeLabel = self.swipeLabel
        out.backgroundColor = .clear
        return out
    }
}

class ProView: UIView {
    typealias ChosenImageType = PortfolioImage
    
    private var modelCacheSync = DispatchQueue(label: "cacheQueue", qos: .background)
    private var profileModelCache : [ClientStubResponse : UIImage] = [:]
    private var collectionView: ProCollection!
    private var selectedIndex: Int?
    private var emptyLabel: UILabel!
    private var storedItems: [ClientStubResponse] = []
    
    func set(items: [ClientStubResponse]) {
        profileModelCache = [:]
        storedItems = items
        collectionView.set(items: items)
        emptyLabel.isHidden = !(items.count == 0)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(frame: .zero)
        commonInit()
    }
    
    func commonInit() {
        collectionView = ProCollection()
        emptyLabel = UILabel(central: 18, weight: .medium)
        emptyLabel.text = "No stylists found at this location for your order! Try changing your location."
        emptyLabel.schemedTextColor(.primaryTextColor)
        emptyLabel.isHidden = true
        addSubview(emptyLabel)
    }
    
    var lastBlockDraw: CGFloat?
    override func layoutSubviews() {
        super.layoutSubviews()
        let block_height: CGFloat = min(165, UIScreen.main.bounds.height * 0.18)
        collectionView.view.pin.horizontally().vertically()
        if lastBlockDraw != block_height {
            lastBlockDraw = block_height
        }
        emptyLabel.pin.horizontally(5%).sizeToFit(.width).vCenter()
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        parentViewController?.add(collectionView, anchor: self)
    }
}

extension ProView: DayViewStateUpdating {
    func move(from oldDate: Date, to newDate: Date) {}
}

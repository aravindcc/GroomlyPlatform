//
//  PopoverVC.swift
//  Learning
//
//  Created by clar3 on 19/03/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import PinLayout
import FlexLayout
import Hero
import Kingfisher

protocol TransitionController: UIViewController {
    var heroID: String? { get set }
}

typealias TransitionVCIndClosure<T> = (T, Int, UIImage?) -> UIViewController?

// ASSUMPTION THE CONTEXT ISN'T GOING TO BE GREATER THAN 1 - CONTENT_MAX

class PopoverVC<T: ImageModel>: FullScreenVC, TransitionController {
    private lazy var CONTENT_MAX : CGFloat = {
        var one: CGFloat?
        if self.contentView.frame.maxY > (self.view.frame.maxY * 0.97) {
            one = self.contentView.frame.minY - self.contentView.frame.maxY + (self.view.frame.maxY * 0.97)
        }
        let two = 0.4 * self.view.frame.height
        return one == nil ? two : min(one!, two)
    }()
    private let CONTENT_V_PADDING : CGFloat = 10
    private var model : T
    private var profileImageV : UIImageView!
    private let containerView = UIView()
    private var hasCrisped : Bool = false
    private var originalImageY: CGFloat?
    
    var heroID: String? {
        get {
            return profileImageV.hero.id
        }
        set (val) {
            profileImageV.hero.id = val
            if let val = val {
                containerView.hero.modifiers = [.source(heroID: val), .spring(stiffness: 250, damping: 25)]
            }
        }
    }
    
    let contentView = UIView()
    
    init(_ model: T, transition: UIImage?) {
        self.model = model
        super.init(nibName: nil, bundle: nil)
        
        view.schemedBG()

        // apply content view with model
        containerView.backgroundColor = .clear
        contentView.backgroundColor = .clear
        createContent(model: model)
        
        // use lower resolution transistion image.
        profileImageV = APCollectionCell.configureBG()
        profileImageV.image = transition
        profileImageV.isUserInteractionEnabled = true
        profileImageV.layer.cornerRadius = 0
        profileImageV.accessibilityIdentifier = "popoverImage"
        
        containerView.addSchemedView(profileImageV)
        containerView.addSchemedView(contentView)
        view.addSchemedView(containerView)
        
        containerView.clipsToBounds = true

        containerView.hero.modifiers = [.spring(stiffness: 250, damping: 25)]
        profileImageV.hero.modifiers = [.forceNonFade, .useOptimizedSnapshot, .spring(stiffness: 250, damping: 25)]
        contentView.hero.modifiers = [.forceNonFade, .useNoSnapshot, .forceAnimate, .spring(stiffness: 250, damping: 25)]
        
        let panGest = UIPanGestureRecognizer(target: self, action: #selector(handlePan(gr:)))
        view.addGestureRecognizer(panGest)
        view.isUserInteractionEnabled = true
    }
    
    func createContent(model: T) {
        // create content override in subclasses ...
    }
    
    func onDismiss() {
        // special dismiss handler
    }

    @objc func handleDrag(_ currentTrans: CGPoint) {
        guard Hero.shared.progress == 0.0, let originalImageY = originalImageY, originalImageY > CONTENT_MAX else { return }
        let prev = self.prevTranslation ?? .zero
        let translation = currentTrans - prev
        let imageHeight = profileImageV.frame.height
        var newVal = containerView.frame.origin.y + translation.y
        newVal.clamp(from: CONTENT_MAX - imageHeight, to: 0)
        containerView.frame.origin.y = newVal
        self.prevTranslation = currentTrans
    }
    
    private var startedHero = false
    private var prevTranslation: CGPoint?
    @objc func handlePan(gr: UIPanGestureRecognizer) {
        let translation = gr.translation(in: view)
        
        if gr.state == .began {
            prevTranslation = nil
            startedHero = false
        }
        
        if containerView.frame.origin.y >= 0, translation.y > 0 {
            switch gr.state {
            case .began, .changed:
                // make sure the hero has begun this is it was originally a scroll up
                if Hero.shared.progress == 0 && !startedHero {
                    dismiss(animated: true, completion: nil)
                    startedHero = true
                }
                Hero.shared.update(translation.y / view.bounds.height)
            default:
                let velocity = gr.velocity(in: view)
                if ((translation.y + velocity.y) / view.bounds.height) > 0.5 {
                    Hero.shared.finish()
                    viewDidLayoutSubviews()
                } else {
                    Hero.shared.cancel()
                    viewDidLayoutSubviews()
                }
            }
        } else {
            // cancel hero cause they are now scrolling
            if startedHero {
                Hero.shared.cancel(animate: false)
                viewDidLayoutSubviews()
                startedHero = false
            }
            handleDrag(translation)
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        containerView.frame = view.bounds
        
        if profileImageV.image != nil {
            profileImageV.pin.horizontally().top().aspectRatio()
        } else {
            profileImageV.pin.horizontally().top().aspectRatio(1)
        }
        contentView.pin.horizontally().top(profileImageV.frame.maxY + CONTENT_V_PADDING)
        contentView.flex.layout(mode: .adjustHeight)
        contentView.executePin()
        containerView.frame.size.height = contentView.frame.maxY
         
        
        if originalImageY == nil || originalImageY! == 0 {
            originalImageY = profileImageV.frame.height
        }
        
        
        if !hasCrisped {
            hasCrisped = true
            // attempt to high res image
            if let image = model.image {
                let processor = DownsamplingImageProcessorScaled(size: profileImageV.frame.size)
                KingfisherManager.shared.retrieveImage(with: image, options: [.processor(processor)]) { result in
                    switch result {
                    case .success(let value):
                        DispatchQueue.main.async {
                            self.profileImageV.image = value.image
                            self.view.layoutSubviews()
                        }
                    case .failure(let error):
                        Log(error) // The error happens
                    }
                }
            }
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


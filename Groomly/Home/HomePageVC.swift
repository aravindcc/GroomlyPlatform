//
//  HomePageVC.swift
//  Groomly
//
//  Created by Ravi on 22/10/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import PinLayout

class HomePageVC: UIViewController {
    private let homePage = HomePage()
    private lazy var loadingIndicator: UIActivityIndicatorView = {
        let progressView = UIActivityIndicatorView(style: .medium)
        progressView.color = self.view.colorScheme!.highlightColorA
        progressView.tag = 88
        progressView.startAnimating()
        self.view.addSubview(progressView)
        progressView.pin.center()
        return progressView
    }()
    private var hasLoaded: Bool = true {
        didSet {
            let hasLoaded = self.hasLoaded
            UIView.animate(withDuration: 0.1) {
                self.loadingIndicator.isHidden = hasLoaded
                self.homePage.isHidden = !hasLoaded
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        homePage.didSelect = {
            // send that to home coordinater.
            if $0 == "__calendar__" {
                if let mainvc = self.tabBarController as? MainTabVC {
                    mainvc.switchTo(view: .calendar)
                }
            } else {
                HomeCoordinater.shared.select(group: $0)
            }
        }
        view.addSchemedView(homePage)
        HomeCoordinater.shared.registerEntry(self)
        
        loadGroups()
        view.schemedBG()
        hasLoaded = false
    }
    
    private func loadGroups() {
        Message.notify(
            process: APIClient.api.getGroups(),
            loadingMessage: nil,
            successMessage: nil,
            errorMessage: "Server error getting data., please try again later."
        ) { [weak self] (result) in
            guard let self = self else { return }
            self.hasLoaded = true
            switch result {
            case .success(let model):
                self.homePage.load(groups: model)
            case .failure(let error):
                Log("ERROR \(error)")
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        homePage.pin.top(safeAreaInsets).horizontally().bottom(view.pin.safeArea)
    }
}

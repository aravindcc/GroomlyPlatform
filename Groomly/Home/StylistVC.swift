//
//  StylistVC.swift
//  Groomly
//
//  Created by Ravi on 24/09/2021.
//  Copyright © 2021 Ashla. All rights reserved.
//

import UIKit
import PinLayout
import Kingfisher
import Cosmos
import FlexLayout
import CalendarKit

class StylistVC: UIViewController {
    
    private let imgF: CGFloat = 0.65
    private let photo = UIImageView()
    private let container = UIView()
    private let infoView = UIView()
    private let nameLabel = UILabel(central: 16, weight: .bold)
    private let descriptionLabel = UILabel(central: 14, weight: .regular)
    private let locationLabel = UILabel(central: 14, weight: .regular)
    private let stars = CosmosView.normal()
    private let cutoutView = UIView()
    private let bookButton = FLTButton()
    private var daySelector: DayHeaderView!
    private var dayWrapper: UIView!
    private var imageGrid: APImageGrid<PortfolioImage>!
    private var imageGridFlex: Flex!
    let dayState = DayViewState(date: Date(), calendar: Calendar.autoupdatingCurrent)
    
    private let model: ClientStubResponse
    var gallery: [PortfolioImage] = []
    
    func load(_ url: URL?) {
        guard let url = url else { return }
        let s = UIScreen.main.bounds.width * imgF
        let processor = DownsamplingImageProcessorScaled(size: CGSize(width: s, height: s))
        KingfisherManager.shared.retrieveImage(with: url, options: [.processor(processor)]) { result in
            switch result {
            case .success(let value):
                DispatchQueue.main.async {
                    self.photo.layer.masksToBounds = true
                    self.photo.image = value.image
                    self.view.setNeedsLayout()
                }
            case .failure(let error):
                Log(error) // The error happens
            }
        }
    }
    
    init(model: ClientStubResponse) {
        self.model = model
        super.init(nibName: nil, bundle: nil)
        
        gallery = model.profile?.portfolio ?? []
    
        if let num = model.profile?.review_score {
            stars.rating = Double(num.score)
        } else {
            stars.rating = 0
        }
        nameLabel.text = "\(model.first_name) \(model.last_name)"
        descriptionLabel.text = model.profile?.description
        locationLabel.text = model.formatted_address
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.colorScheme = .normal
        
        container.schemedBG(.lightShadow)
        container.layer.cornerRadius = 20
        container.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        view.addSubview(container)
        view.isUserInteractionEnabled = true
        container.addSubview(photo)
        
        let w = UIScreen.main.bounds.width * imgF
        self.photo.frame = CGRect(origin: .zero, size: CGSize(width: w, height: w))
        self.photo.layer.cornerRadius = w / 2
        
        photo.schemedBG(.lightShadow)
        photo.schemedBG(alpha: 0.8)
        photo.maskToBounds = true
        photo.contentMode = .scaleAspectFill
        photo.clipsToBounds = true
        photo.accessibilityIdentifier = "proviewimage"
        
        load(model.profile?.image)
        
        nameLabel.schemedTextColor(.primaryTextColor)
        descriptionLabel.schemedTextColor(.primaryTextColor)
        descriptionLabel.accessibilityIdentifier = "proviewdesc"
        descriptionLabel.numberOfLines = 0
        locationLabel.schemedTextColor(.primaryTextColor)
        
        infoView.flex.alignItems(.stretch).define { flex in
            flex.addItem(nameLabel)
            flex.addItem(descriptionLabel).marginTop(5)
            flex.addItem(locationLabel).marginTop(5)
            flex.addItem(stars).alignSelf(.center).marginVertical(10)
        }
        view.addSchemedView(infoView)
        
        cutoutView.schemedBG()
        cutoutView.layer.cornerRadius = 20
        cutoutView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        view.addSchemedView(cutoutView)
        
        bookButton.title = "book"
        bookButton.tapped = { [weak self] _ in
            guard let model = self?.model else { return }
            self?.dismiss(animated: true) {
                HomeCoordinater.shared.select(client: model)
            }
        }
        bookButton.layer.zPosition = 10
        bookButton.accessibilityIdentifier = "BookButton"
        
        imageGrid = APImageGrid<PortfolioImage>()
        imageGrid?.assignHeroes = false
        imageGrid?.transitionClosure = { [weak self] (model, ind, transition) in
            let carousel = ImageCarousel<PortfolioImage>()
            carousel.selected = ind
            carousel.items = self?.gallery ?? []
            return carousel
        }
        imageGrid?.set(items: gallery)
        
        dayWrapper = WrapperView(wrap: 15)
        daySelector = DayHeaderView(calendar: Calendar.autoupdatingCurrent)
        daySelector.updateStyle(TimeVC.getCalendarStyle(view.colorScheme!).header.transparent())
        daySelector.state = dayState
        daySelector.showsSwipeLabelView = false
        dayState.subscribe(client: self)
        dayWrapper.addSubview(daySelector)
        
        cutoutView.flex.alignItems(.stretch).define { flex in
            flex.addItem(dayWrapper).height(100)
            flex.addItem(bookButton).height(30).width(bookButton.wrappedWidth).alignSelf(.center).marginTop(-15).marginBottom(-15)
            imageGridFlex = flex.addItem().grow(1)
        }
        
        add(imageGrid, anchor: imageGridFlex.view)
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let ext = view.pin.safeArea.top
        container.pin.vertically(UIEdgeInsets(top: photo.frame.height / 2 + 20 + ext, left: 0, bottom: 0, right: 0)).horizontally()
        photo.pin.top(-photo.frame.height / 2).hCenter()
        infoView.pin.below(of: photo).marginTop(10).horizontally()
        infoView.flex.marginHorizontal(5%).layout(mode: .adjustHeight)
        cutoutView.pin.horizontally().below(of: infoView).marginTop(5).bottom(-view.pin.safeArea.bottom)
        cutoutView.flex.layout()
        imageGrid?.view.pin.all()
    }
}

extension StylistVC: DayViewStateUpdating {
    func move(from oldDate: Date, to newDate: Date) {}
}

//
//  HomePageVC.swift
//  Groomly
//
//  Created by Ravi on 05/10/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import PinLayout
import FlexLayout
import Kingfisher

extension CAGradientLayer
{
    func animateChanges(to colors: [UIColor],
                        duration: TimeInterval)
    {
        CATransaction.begin()
        CATransaction.setCompletionBlock({
            // Set to final colors when animation ends
            self.colors = colors.map{ $0.cgColor }
        })
        let animation = CABasicAnimation(keyPath: "colors")
        animation.duration = duration
        animation.toValue = colors.map{ $0.cgColor }
        animation.fillMode = .forwards
        animation.isRemovedOnCompletion = false
        add(animation, forKey: "changeColors")
        CATransaction.commit()
    }
}


class DescriptionCell: UIView {
    private let imageWrapper = UIView()
    private let imageView = UIImageView()
    private let title = UILabel(left: 18, weight: .bold)
    private let label = UILabel(left: 16, weight: .regular)
    private var cachedGroup: ServiceGroup?
    private var aspectRatio: CGFloat = 0.5
    private var labelWrapper = UIView()
    private var actionButton: FlatButton!
    
    var didSelect: (() -> Void)?
    
    func lblHeight(in width: CGFloat) -> CGFloat {
        return min(self.aspectRatio, 1) * width
    }
    
    lazy var gradient: CAGradientLayer = {
        let gradient = CAGradientLayer()
        gradient.type = .axial
        gradient.locations = [0, 0.4, 1]
        gradient.startPoint = CGPoint(x: 0, y: 0)
        gradient.endPoint = CGPoint(x: 1, y: 1)
        return gradient
    }()
    
    func load(_ path: String) {
        let url = URL(string: "\(Networker.shared.hostURL!)\(path)")!
        let s = UIScreen.main.bounds.width * 0.5
        let processor = DownsamplingImageProcessorScaled(size: CGSize(width: s, height: s))
        KingfisherManager.shared.retrieveImage(with: url, options: [.processor(processor)]) { result in
            switch result {
            case .success(let value):
                DispatchQueue.main.async {
                    self.aspectRatio = value.image.size.height / value.image.size.width
                    self.imageView.image = value.image
                    self.superview?.superview?.superview?.setNeedsLayout()
                }
            case .failure(let error):
                Log(error) // The error happens
            }
        }
    }
    
    init(title: String, buttonTitle: String, description: String, imagePath: String? = nil, _ didSelect: (() -> Void)?) {
        super.init(frame: .zero)
        
        self.didSelect = didSelect
        
        self.title.text = title
        label.text = description
        label.schemedTextColor(.lightShadow)
        self.title.schemedTextColor(.lightShadow)
        
        imageView.schemedBG(.lightShadow, alpha: 0.8)
        imageView.maskToBounds = true
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.isUserInteractionEnabled = true
        imageWrapper.clipsToBounds = true
        
        addSchemedView(imageWrapper)
        imageWrapper.addSchemedView(imageView)

        self.gradient.colors = [
            ColorScheme.normal.primaryTextColor.withAlphaComponent(0.5),
            ColorScheme.normal.primaryTextColor.withAlphaComponent(0.3),
            ColorScheme.normal.primaryTextColor.withAlphaComponent(0.1),
        ]
        
        let closure: ColorClosure = { [weak self] color in
            if let self = self {
                self.gradient.animateChanges(to: [
                    ColorScheme.normal.primaryTextColor.withAlphaComponent(0.5),
                    ColorScheme.normal.primaryTextColor.withAlphaComponent(0.3),
                    ColorScheme.normal.primaryTextColor.withAlphaComponent(0.1),
                ], duration: 0.2)
                return true
            }
            return false
        }
        reschemer.add(rule: closure)
        ColorConfigurator.shared.register(andProperties: [.primaryTextColor], withUpdator: closure)
        
        self.layer.addSublayer(gradient)
        addSchemedView(labelWrapper)
        
        actionButton = FLTButton()
        actionButton.title = buttonTitle
        actionButton.icon = "arrow.right.circle"
        actionButton.preset = true
        actionButton.tapped = { [unowned self] _ in
            self.didSelect?()
        }
        labelWrapper.flex.direction(.column).alignItems(.stretch).define { flex in
            flex.addItem(self.title)
            flex.addItem(label)
            flex.addItem(actionButton).height(30).width(actionButton.wrappedWidth).marginTop(10)
        }
        label.numberOfLines = description.components(separatedBy: .newlines).count
        
        if let imagePath = imagePath {
            load(imagePath)
        }
    }
    
    @objc private func handleTap() {
        self.didSelect?()
    }
    
    override func draw(_ rect: CGRect) {
        gradient.frame = self.bounds
        super.draw(rect)
    }
    
    override func layoutSubviews() {
        imageWrapper.pin.all()
        imageView.pin.all()
        labelWrapper.pin.horizontally(5%).top(20%)
        labelWrapper.flex.layout()
        gradient.frame = self.bounds
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

fileprivate class HomeCell: UIView {
    private let imageWrapper = UIView()
    private let imageView = UIImageView()
    private let label = UILabel(central: 16, weight: .bold)
    private var cachedGroup: ServiceGroup?
    private var aspectRatio: CGFloat = 1
    private var labelWrapper = UIView()
    
    var didSelect: ((String) -> Void)?
    
    func lblHeight(in width: CGFloat) -> CGFloat {
        return self.aspectRatio * width
    }
    
    lazy var gradient: CAGradientLayer = {
        let gradient = CAGradientLayer()
        gradient.type = .axial
        gradient.locations = [0, 1]
        return gradient
    }()
    
    func load(group: ServiceGroup) {
        self.cachedGroup = group
        self.label.text = group.name
        if let image = group.asset {
            let s = UIScreen.main.bounds.width * 0.5
            let processor = DownsamplingImageProcessorScaled(size: CGSize(width: s, height: s))
            KingfisherManager.shared.retrieveImage(with: image, options: [.processor(processor)]) { result in
                switch result {
                case .success(let value):
                    DispatchQueue.main.async {
                        if case let .network(resource) = value.source, resource.downloadURL == self.cachedGroup?.asset {
                            self.aspectRatio = value.image.size.height / value.image.size.width
                            self.imageView.image = value.image
                        }
                    }
                case .failure(let error):
                    Log(error) // The error happens
                }
            }
        }
    }
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        let out = super.hitTest(point, with: event)
        if out == nil && labelWrapper.frame.contains(point) {
            return self
        }
        return out
    }
    
    init(group: ServiceGroup? = nil,_ didSelect: ((String) -> Void)?, overlayColor: UIColor? = nil) {
        super.init(frame: .zero)
        
        self.didSelect = didSelect
        label.schemedTextColor(.primaryTextColor)
        labelWrapper.schemedBG(.lightShadow)
        labelWrapper.maskToBounds = true
        labelWrapper.contentMode = .scaleAspectFill
        labelWrapper.clipsToBounds = true
        labelWrapper.isUserInteractionEnabled = true
        
        imageView.schemedBG(.lightShadow, alpha: 0.8)
        imageView.maskToBounds = true
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.isUserInteractionEnabled = true
        imageWrapper.clipsToBounds = true
        
        addSchemedView(imageWrapper)
        imageWrapper.addSchemedView(imageView)
        
        if let group = group {
            load(group: group)
        }
        
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTap)))
        labelWrapper.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTap)))
        
        let accOverlayColor = overlayColor ?? ColorScheme.normal.highlightColorA
        gradient.colors = [
            UIColor.clear.cgColor,
            accOverlayColor.withAlphaComponent(0.4).cgColor,
        ]
        self.layer.addSublayer(gradient)
        addSchemedView(labelWrapper)
        labelWrapper.addSchemedView(label)
    }
    
    @objc private func handleTap(_ gesture: UITapGestureRecognizer) {
        let touchPoint = gesture.location(in: self)
        if touchPoint.y > labelWrapper.frame.height / 2 {
            self.didSelect?(cachedGroup?.name ?? "")
        }
    }
    
    override func draw(_ rect: CGRect) {
        gradient.frame = self.bounds
        super.draw(rect)
    }
    
    override func layoutSubviews() {
        imageWrapper.pin.all()
        imageView.pin.horizontally().aspectRatio(1).vCenter().hCenter()
        label.pin.sizeToFit()
        labelWrapper.pin.height(label.frame.height + 16).horizontally(15%).maxWidth(150)
        label.pin.vCenter().hCenter()
        labelWrapper.pin.bottom(-labelWrapper.frame.height / 2).hCenter()
        labelWrapper.layer.cornerRadius = labelWrapper.frame.height / 2
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class HomePage: UIView {
    let content =  UIView()
    let decorative = UIView()
    let scroll = UIScrollView()
    var didSelect: ((String) -> Void)?
    init() {
        super.init(frame: .zero)
        decorative.schemedBG(.lightShadow)
        content.flex.wrap(.wrap).alignContent(.center).direction(.row)
        scroll.addSchemedView(content)
        addSchemedView(scroll)
        addSchemedView(decorative)
        scroll.isUserInteractionEnabled = true
        content.isUserInteractionEnabled = true
        accessibilityIdentifier = "homePageView"
    }
    
    func load(groups: [ServiceGroup]) {
        content.clear()
       
        groups.forEach {
            content.flex.addItem(HomeCell(group: $0, didSelect)).grow(0)
        }
        
        content.flex.addItem(DescriptionCell(
            title: "What do I have booked",
            buttonTitle: "My Calendar",
            description: "Find your recent bookings\nand future bookings all\nin one",
            imagePath: "/static/images/calendar.jpg",
            { [unowned self] in
                self.didSelect?("__calendar__")
            }
        )).grow(1)
        
        layoutSubviews()
    }
    
    override func layoutSubviews() {
        decorative.pin.top(-6).width(70).height(6).hCenter()
        scroll.pin.all()
        content.pin.all()
        
        if content.subviews.count == 0 {
            return
        }
            
        let w = content.frame.width * 0.5
        let inQSubvs = content.subviews[0..<content.subviews.count - 1]
        let isOdd = inQSubvs.count%2 == 1
        let total = inQSubvs.count
        for v in inQSubvs.enumerated() {
            let ind = v.offset
            let vw = ind == inQSubvs.count - 1 && isOdd ? w * 2 : w
            if let v = v.element as? HomeCell {
                v.layer.zPosition = CGFloat(total - ind)
                v.flex.width(vw).height(w).shrink(0).grow(0)
            }
        }
        
        if let v = content.subviews.last as? DescriptionCell {
            let totalHeight: CGFloat = w * round(CGFloat(total) / 2)
            let contentdesc = v.lblHeight(in: content.frame.width)
            let tbh = max(frame.height - totalHeight, contentdesc)
            v.flex.width(content.frame.width).height(tbh)
        }
        
        content.flex.layout(mode: .adjustHeight)
        let reachedH = content.bounds.size.height
        scroll.contentSize.height = max(frame.height, reachedH)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

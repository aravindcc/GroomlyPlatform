//
//  FilterVC.swift
//  Groomly
//
//  Created by Ravi on 24/09/2021.
//  Copyright © 2021 Ashla. All rights reserved.
//

import UIKit
import PinLayout
import FlexLayout

fileprivate let padding: CGFloat = 40
fileprivate let textSize: CGFloat = 20

fileprivate class FilterBlock: UIView {
    private let text: UILabel = UILabel(left: textSize, weight: .regular, wantsMonster: true)
    private let container = UIView()
    
    init(heading: String, factory: (Flex) -> Void) {
        super.init(frame: .zero)
        text.text = heading
        text.schemedTextColor(.primaryTextColor)
        
        flex.alignItems(.stretch)
        flex.addItem().height(2).shrink(0).marginBottom(10).view?.schemedBG(.secondaryTextColor, alpha: 0.4)
        flex.addItem(text).shrink(0).marginHorizontal(padding)
        flex.addItem(container).direction(.row).wrap(.wrap).marginHorizontal(padding * 0.5).marginBottom(15).shrink(0).define(factory)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

enum DistanceFilterOption: String, CaseIterable {
    case upto3 = "0-3 miles"
    case upto6 = "3-6 miles"
    case upto9 = "6-9 miles"
    case upto12 = "9-12 miles"
    case upto15 = "12-15 miles"
    case all = "15+ miles"
}

enum RatingFilterOption: String, CaseIterable {
    case one = "★"
    case two = "★★"
    case three = "★★★"
    case four = "★★★★"
    case five = "★★★★★"
}

class FilterVC: UIViewController {
    
    private let container = UIView()

    var toggleDistance: ((DistanceFilterOption) -> Void)?
    var toggleRating: ((RatingFilterOption) -> Void)?
    var refresh: (() -> Void)?
    var clear: (() -> Void)?
    
    private var distanceUpdateStore = [DistanceFilterOption: FlatButton]()
    private var ratingUpdateStore = [RatingFilterOption: FlatButton]()
    private func bt<T: RawRepresentable>(_ option: T) -> FlatButton where T.RawValue == String {
        let b = FlatButton()
        b.title = option.rawValue
        b.defaultPx = 8
        b.layer.cornerRadius = 15
        b.flex.width(b.wrappedWidth).height(30).shrink(0)
        b.tapped = { [unowned self] _ in
            if T.self == DistanceFilterOption.self {
                self.toggleDistance?(option as! DistanceFilterOption)
            } else if T.self == RatingFilterOption.self {
                self.toggleRating?(option as! RatingFilterOption)
            }
        }
        if T.self == DistanceFilterOption.self {
            distanceUpdateStore[option as! DistanceFilterOption] = b
        } else if T.self == RatingFilterOption.self {
            ratingUpdateStore[option as! RatingFilterOption] = b
        }
        return b
    }
    
    func renderOptions(distances: Set<DistanceFilterOption>, ratings: Set<RatingFilterOption>) {
        for distance in DistanceFilterOption.allCases {
            let selected = distances.contains(distance)
            if let v = distanceUpdateStore[distance] {
                v.reschemer.removeAll()
                v.setTitleTextColor(key: selected ? .lightShadow : .highlightColorA)
                v.schemedBG(.highlightColorA, alpha: selected ? 1 : 0)
            }
        }
        
        for rating in RatingFilterOption.allCases {
            let selected = ratings.contains(rating)
            if let v = ratingUpdateStore[rating] {
                v.reschemer.removeAll()
                v.setTitleTextColor(key: selected ? .lightShadow : .highlightColorA)
                v.schemedBG(.highlightColorA, alpha: selected ? 1 : 0)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.schemedBG()
        
        modalPresentationStyle = .custom
        
        let text: UILabel = UILabel(left: 25, weight: .semibold, wantsMonster: true)
        text.text = "Filters"
        text.schemedTextColor(.primaryTextColor)
        
        let clear = IconButton()
        clear.titleLabel.font = UILabel(central: textSize, weight: .regular, wantsMonster: true).font
        clear.title = "Clear"
        clear.fontSize = textSize
        clear.defaultPx = 0
        clear.setTitleTextColor(key: .primaryTextColor)
        clear.tapped = { [unowned self] _ in
            self.clear?()
        }
        
        container.flex.direction(.column).alignItems(.stretch).paddingTop(padding).define { flex in
            flex.addItem().height(text.intrinsicContentSize.height).marginHorizontal(padding).marginBottom(padding * 0.5).direction(.row).define { flex in
                flex.addItem(text).shrink(0)
                flex.addItem().grow(1).shrink(1)
                flex.addItem(clear).width(clear.wrappedWidth).shrink(0)
            }
            flex.addItem(FilterBlock(heading: "Distance") { flex in
                DistanceFilterOption.allCases.forEach {
                    flex.addItem(bt($0)).marginRight(10).marginTop(10)
                }
            })
            flex.addItem(FilterBlock(heading: "Rating") { flex in
                RatingFilterOption.allCases.forEach {
                    flex.addItem(bt($0)).marginRight(10).marginTop(10)
                }
            }).marginTop(20)
        }
        
        view.addSubview(container)
        
        refresh?()
    }
    
    override func viewDidLayoutSubviews() {
        container.pin.all()
        container.flex.layout()
    }
}

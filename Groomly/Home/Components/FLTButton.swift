//
//  FLTButton.swift
//  Groomly
//
//  Created by Ravi on 23/09/2021.
//  Copyright © 2021 Ashla. All rights reserved.
//

import UIKit
import PinLayout

class FLTButton: FlatButton {
    override func refreshText() {
        
        // calculate icon
        var balanceAttr: [NSAttributedString.Key: Any]?
        var attachmentString: NSAttributedString?
        if let icon = icon {
            let imageAttachment = NSTextAttachment()
            let config = UIImage.SymbolConfiguration(pointSize: fontSize)
            let img = UIImage(systemName: icon, withConfiguration: config)?.withTintColor(titleLabel.textColor)
            imageAttachment.image = img
            attachmentString = NSAttributedString(attachment: imageAttachment)
            
            let font = titleLabel.font!
            let imgHeight = img?.size.height ?? 10
            let imageDiff = (imgHeight - fontSize) / 2
            let descender = shouldDescend == nil ? 0: shouldDescend! * font.descender / 4
            balanceAttr = [
                .font: font,
                .baselineOffset: imageDiff - descender
            ]
        }
        
        // Initialize mutable string
        let space = icon == nil ? "" : " "
        let completeText = NSMutableAttributedString(string: "\(title ?? "")\(space)", attributes: balanceAttr)
        if let attachmentString = attachmentString {
            completeText.append(attachmentString)
        }
        
        // Assign
        titleLabel.attributedText = completeText
        setNeedsDisplay()
    }
    
    var shouldDescend: CGFloat?
    override func draw(_ rect: CGRect) {
        layer.cornerRadius = rect.height / 2
        super.normal(draw: rect)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        schemedBG(.lightShadow)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


//
//  BlurLayer.swift
//  Groomly
//
//  Created by clar3 on 12/05/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit

extension UIImage {
    func blurred() -> CGImage? {
        if let ciImg = CIImage(image: self) {
            ciImg.applyingFilter("CIGaussianBlur", parameters: [kCIInputRadiusKey: 2.0])
            return ciImg.cgImage
        }
        return nil
    }
}

class BlurLayer: CALayer {
    let producer: () -> UIView
    init(foreground: @escaping () -> UIView) {
        self.producer = foreground
        super.init()
    }
    
    override init(layer: Any) {
        producer = { UIView() }
        super.init(layer: layer)
    }
    
    func updateImage() {
        contents = UIView.image(with: producer())?.blurred()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

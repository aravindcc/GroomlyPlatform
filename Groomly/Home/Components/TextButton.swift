//
//  TextButton.swift
//  Groomly
//
//  Created by Ravi on 08/09/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import PinLayout
import FlexLayout

class TextButton: UILabel {
    var tapped: Completion?
    init(color: ColorKey, text: String) {
        super.init(frame: .zero)
        font = .systemFont(ofSize: 14, weight: .bold)
        schemedTextColor(color)
        attributedText = NSAttributedString(
            string: text,
            attributes: [.underlineStyle: NSUnderlineStyle.single.rawValue]
        )
        let tap = UITapGestureRecognizer(target: self, action: #selector(trigger))
        addGestureRecognizer(tap)
        isUserInteractionEnabled = true
    }
    
    required init?(coder: NSCoder) {
        super.init(frame: .zero)
    }
    
    @objc private func trigger() {
        tapped?()
    }
}

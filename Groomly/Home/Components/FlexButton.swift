//
//  FlexButton.swift
//  Learning
//
//  Created by clar3 on 25/03/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//


import UIKit
import PinLayout

class FlexButton: FlatButton {
    
    override func commonInit() {
        super.commonInit()
        addSoftUIEffectForView()
    }
    
    override func draw(_ rect: CGRect) {
        updateSoft()
        super.normal(draw: rect)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateSoft()
    }
    
    override func handleClick() {
        UIView.animate(withDuration: 0.2, animations: { self.containerView.alpha = 0.99; self.invertSoftShadows() }) { _ in
            UIView.animate(withDuration: 0.2, animations: { self.containerView.alpha = 1; self.invertSoftShadows() }) { _ in
                self.makeAction()
            }
        }
    }
    
    func updateSoft() {
        updateSoftUI()
        layer.cornerRadius = frame.height / 2
    }
}

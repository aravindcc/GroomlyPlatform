//
//  ImageCarousel.swift
//  Groomly
//
//  Created by Ravi on 19/07/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import PinLayout
import FlexLayout
import Kingfisher

fileprivate let captionPadding: UIEdgeInsets = UIEdgeInsets(top: 15, left: 5, bottom: 10, right: 5)
fileprivate let pagerWidthPadding: Percent = 5%
fileprivate let pagerHeight: Percent = 95%
fileprivate let cellCornerRadius: CGFloat = APCollectionCell.DEF_BORDER_RADIUS

extension PinLayout {
    @discardableResult
    func aspectRatio(_ ratio: CGFloat?) -> PinLayout {
        return ratio == nil ? aspectRatio() : aspectRatio(ratio!)
    }
}

class ImageCarouselCell: KFBasedCell {
    
    private let content = UIView()
    private let caption = UILabel(left: 15, weight: .medium)
    private var imageFlex: Flex!
    private var labelFlex: Flex!
    private var currentRatio: CGFloat = 1
    var text: String {
        get { caption.text ?? "" }
        set {
            caption.text = newValue
            caption.pin.sizeToFit(.width)
            labelFlex.height(caption.frame.height)
        }
    }
    
    @discardableResult
    func positionViews(_ ratio: CGFloat? = nil) -> CGFloat {
        if let ratio = ratio {
            imageFlex.height(ceil(ratio * frame.width))
            currentRatio = ratio
        } else {
            imageFlex.height(ceil(currentRatio * frame.width))
        }
        content.pin.horizontally()
        content.flex.layout(mode: .adjustHeight)
        return content.frame.height
    }
    
    func commonInit() {
        addSubview(content)
        
        caption.schemedTextColor(.primaryTextColor)
        imagev.layer.cornerRadius = cellCornerRadius
        imagev.layer.masksToBounds = true
        imagev.contentMode = .scaleAspectFit
        imagev.clipsToBounds = true
        imagev.schemedBG(.darkShadow, alpha: 0.5)
        
        content.flex.alignItems(.stretch).define { (flex) in
            imageFlex = flex.addItem(imagev).width(100%)
            labelFlex = flex.addItem(caption).margin(captionPadding)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(frame: .zero)
        commonInit()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        positionViews()
    }
}

class ImageCarousel<ImageType: DisplayImageModel>: UIViewController, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, ImageCacher {
    
    var items: [ImageType] = [] {
        didSet {
            cacheImages(for: items)
            DispatchQueue.main.async {
                UIView.animate(withDuration: 0, animations: {
                    self.pagerView.alpha = 0
                    self.pagerView.reloadData()
                }) { _ in
                    let path = IndexPath(item: self.selected, section: 0)
                    self.pagerView.scrollToItem(at: path, at: .centeredVertically, animated: false)
                    self.pagerView.alpha = 1
                }
                
            }
        }
    }
    var cache: [URL : UIImage] = [:]
    var selected: Int = 0
    
    // visual effect view
    private let visualEffect: UIVisualEffectView
    // pager view
    private let pagerView: UICollectionView
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    
    // set selected page with appropriate hero
    let primaryCell = UIView()
    let sizingCell = ImageCarouselCell()
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let index = indexPath.row
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ImageCarouselCell
        let aspect = CGFloat(items[index].height / items[index].width)
        cell.text = items[index].description
        cell.positionViews(aspect)
        if let url = items[index].image {
            cell.setImageFrom(url: url, maxDim: cell.frame.width)
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let index = indexPath.row
        let aspect = CGFloat(items[index].height / items[index].width)
        let width = collectionView.frame.width * (1 - pagerWidthPadding.of(1))
        sizingCell.frame.size.width = width
        sizingCell.text = items[index].description
        let height = sizingCell.positionViews(aspect)
        let size = CGSize(width: width, height: ceil(height))
        return size
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        pagerView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        pagerView.register(ImageCarouselCell.self, forCellWithReuseIdentifier: "cell")
        
        visualEffect = UIVisualEffectView()
        visualEffect.alpha = 0
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        modalPresentationStyle = .overCurrentContext
        pagerView.dataSource = self
        pagerView.delegate = self
        pagerView.hero.modifiers = [.forceNonFade, .useNoSnapshot, .spring(stiffness: 250, damping: 25)]
        visualEffect.hero.modifiers = [.fade, .useNoSnapshot, .spring(stiffness: 250, damping: 25)]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UIView.animate(withDuration: 0.2, delay: 0.3, options: [], animations: {
            self.visualEffect.alpha = 1
        }, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.clear
        pagerView.backgroundView = nil
        pagerView.backgroundColor = nil

        view.addSchemedView(visualEffect)
        view.addSubview(primaryCell)
        view.addSubview(pagerView)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleClick(_:)))
        view.addGestureRecognizer(tapGesture)
        
        view.window?.backgroundColor = .clear
    }
    
    @objc private func handleClick(_ recogniser: UITapGestureRecognizer) {
        hero.isEnabled = false
        UIView.animate(withDuration: 0.2, animations: {
            self.pagerView.alpha = 0
        }) { _ in
            UIView.animate(withDuration: 0.25, animations: {
                self.visualEffect.alpha = 0
            }) { _ in
                self.dismiss(animated: false, completion: nil)
            }
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        visualEffect.pin.all()
        pagerView.pin.horizontally().height(pagerHeight).vCenter()
        
        let aspect = CGFloat(items[selected].width / items[selected].height)
        primaryCell.pin.horizontally(pagerWidthPadding).aspectRatio(aspect).vCenter()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//
//  OptionButton.swift
//  Groomly
//
//  Created by Ravi on 19/07/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import PinLayout

extension CGSize {
    static func +(lhs: CGSize, rhs: CGSize) -> CGSize {
        return CGSize(
            width: lhs.width + rhs.width,
            height: lhs.height + rhs.height
        )
    }
    static func -(lhs: CGSize, rhs: CGSize) -> CGSize {
        return CGSize(
            width: lhs.width - rhs.width,
            height: lhs.height - rhs.height
        )
    }
}

class OptionButton: FlexButton {
    
    class RoundSegmentedControl: UISegmentedControl {
        override func layoutSubviews() {
            super.layoutSubviews()
            layer.cornerRadius = frame.height / 2
        }
        
        private var current: Int = UISegmentedControl.noSegment
        override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
            current = self.selectedSegmentIndex
            super.touchesBegan(touches, with: event)
        }
        override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
            super.touchesEnded(touches, with: event)
            if current == self.selectedSegmentIndex {
                self.sendActions(for: .valueChanged)
            }
        }
    }
    
    var didSelectOption: ((String) -> Void)?
    var selectedOption: String {
        return items.count > 0 ? items[segmented.selectedSegmentIndex] : ""
    }
    var items: [String] = [] {
        didSet {
            publishSegments()
        }
    }
    private func publishSegments() {
        if segmented != nil {
            segmented.removeFromSuperview()
        }
        segmented = RoundSegmentedControl(items: items)
        segmented.alpha = 0
        segmented.addTarget(self, action: #selector(didChooseOption(_:)), for: .touchUpInside)
        segmented.addTarget(self, action: #selector(didChooseOption(_:)), for: .valueChanged)
        segmented.selectedSegmentIndex = 0
        segmented.schemedBG()
        segmented.schemedTextColor(.highlightColorA, size: 14)
        segmented.schemedTintColor(.bg)
        initalSegmentWidth = segmented.frame.width
        containerView.addSubview(segmented)
        title = items.first
        super.layoutSubviews()
    }
    private var segmented: RoundSegmentedControl!
    private var showingSegmented = false {
        didSet {
            titleLabel.alpha = showingSegmented ? 0 : 1
            segmented.alpha = showingSegmented ? 1 : 0
        }
    }
    
    func forceHideSegmented() {
        UIView.animate(withDuration: 0.05) {
            self.showingSegmented = false
            self.executePin()
        }
    }
    
    func forceShowSegmented() {
        UIView.animate(withDuration: 0.05) {
            self.showingSegmented = true
            self.executePin()
        }
    }
    
    var segmentPadding: CGFloat = 0
    private var initalSegmentWidth: CGFloat = 0
    private var segmentWidth: CGFloat {
        initalSegmentWidth + segmentPadding
    }
    
    convenience init<Option: RawRepresentable & CaseIterable>(cases: Option.Type) where Option.RawValue == String {
        self.init(items: cases.allCases.map { $0.rawValue })
    }
    
    init(items: [String]) {
        super.init(frame: .zero)
        self.items = items
        publishSegments()
    }
    
    required init?(coder: NSCoder) {
        super.init(frame: .zero)
    }
    
    override func commonInit() {
        super.commonInit()
        initalSegmentWidth = 0
        containerView.layer.masksToBounds = true
        containerView.clipsToBounds = true
    }
    
    @objc func didChooseOption(_ control: UISegmentedControl) {
        let option = items[control.selectedSegmentIndex]
        didSelectOption?(option)
        title = option
        usleep(useconds_t(350 * 1000))
        UIView.animate(withDuration: 0.4) {
            self.showingSegmented = false
            self.executePin()
        }
    }
    
    func select(option: String) {
        guard let index = items.firstIndex(of: option) else { return }
        UIView.animate(withDuration: 0.4, animations: {
            if !self.showingSegmented {
                self.showingSegmented = true
            }
            self.segmented.selectedSegmentIndex = index
            self.didChooseOption(self.segmented)
        }) { _ in
            UIView.animate(withDuration: 0.4) {
               self.showingSegmented = false
            }
        }
    }
    
    override func handleClick() {
        UIView.animate(withDuration: 0.4) {
            self.showingSegmented = true
            self.superview?.layoutSubviews()
        }
    }
    
    override func updateSoft() {
        super.updateSoft()
        containerView.layer.cornerRadius = frame.height / 2
    }
    
    override var wrappedWidth: CGFloat {
        return showingSegmented ? segmentWidth : wrappedSize.width
    }

    override func layoutSubviews() {
        let width = showingSegmented ? segmentWidth : wrappedWidth
        containerView.pin.width(width).vertically()
        if showingSegmented {
            segmented.pin.horizontally(-3).vertically(-5)
        } else {
            titleLabel.pin.all()
        }
        containerView.pin.center()
        updateSoft()
    }
}

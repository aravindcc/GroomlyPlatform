//
//  TabView.swift
//  Learning
//
//  Created by clar3 on 20/03/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import PinLayout
import FlexLayout

class TabElementView: UIView {
    private let titleLabel = UILabel(central: 14, weight: .light, color: AppColors.darkShadow)
    private let selectedView = UIView()
    private let SELECTED_CIRCLE_SIZE: CGFloat = 5
    var index : Int?
    
    var title: String? {
        get { return titleLabel.text }
        set (val) { titleLabel.text = val }
    }
    
    var wrappedWidth: CGFloat {
        titleLabel.intrinsicContentSize.width + 50
    }
    
    var selected: Bool = false {
        didSet {
            UIView.animate(withDuration: 0.1) { [weak self] in
                guard let self = self else { return }
                self.selectedView.alpha = self.selected ? 1 : 0
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    
        addSchemedView(titleLabel)
        
        selectedView.backgroundColor = AppColors.darkShadow
        selectedView.layer.cornerRadius = SELECTED_CIRCLE_SIZE / 2
        selectedView.clipsToBounds = true
        addSchemedView(selectedView)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        titleLabel.pin.sizeToFit(.content).hCenter().top()
        selectedView.pin.height(SELECTED_CIRCLE_SIZE)
                        .width(SELECTED_CIRCLE_SIZE)
                        .hCenter().below(of: titleLabel).marginTop(SELECTED_CIRCLE_SIZE)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class TabView<Option: CustomStringConvertible>: UIView {
    
    typealias TabSelectClosure = (Option) -> Void
    
    private var items: [Option]
    private var elems: [Flex]
    private var scrollView: UIScrollView
    private var tapped : TabSelectClosure?
    private var totalWidth: CGFloat!
    
    var selected: Int = 0 {
        didSet {
            for (index, view) in scrollView.subviews.enumerated() {
                if let element = view as? TabElementView {
                    element.selected = index == selected
                    if index == selected {
                        scrollView.scrollRectToVisible(element.frame, animated: true)
                    }
                }
            }
            tapped?(selectedItem)
        }
    }
    var selectedItem: Option {
        items[selected]
    }
    
    init(frame: CGRect, items: [Option] = [], tapped: TabSelectClosure? = nil) {
        self.items = items
        self.tapped = tapped
        self.scrollView = UIScrollView()
        self.elems = []
        super.init(frame: frame)
        
        scrollView.scrollIndicatorInsets = UIEdgeInsets(top: 0, left: 0, bottom: -3, right: 0)
        addSubview(scrollView)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleClick(_:)))
        addGestureRecognizer(tapGesture)
        set()
    }
    
    @objc private func handleClick(_ recogniser: UITapGestureRecognizer) {
        let locx = recogniser.location(in: scrollView).x
        for s in scrollView.subviews {
            guard let s = s as? TabElementView else { continue }
            if (s.frame.minX...s.frame.maxX).contains(locx) {
                selected = s.index ?? 0
            } else if locx < s.frame.minX { break }
        }
    }
    
    func selectNext() {
        let n = min(elems.count - 1, selected + 1)
        if n != selected {
            selected = n
        }
    }
    
    func selectPrevious() {
        let n = max(selected - 1, 0)
        if n != selected {
            selected = n
        }
    }
    
    
    func set(items: [Option]? = nil) {
        if let items = items { self.items = items }
        if self.items.count > 0 {
            selected = 0
        }
        self.elems = []
        scrollView.clear()
        let elems = self.items.enumerated().map { (index, item) -> TabElementView in
            let view = TabElementView()
            view.index = index
            view.title = item.description
            view.selected = index == selected
            return view
        }
        totalWidth = elems.map({ $0.wrappedWidth }).reduce(0, +)
        
        scrollView.flex.direction(.row).alignItems(.stretch).define { (flex) in
            for view in elems {
                self.elems.append(flex.addItem(view))
            }
        }
        if items != nil {
            layoutSubviews()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        scrollView.pin.all()
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = totalWidth > scrollView.frame.width
        if totalWidth > scrollView.frame.width {
            elems.forEach { (flex) in
                guard let width = (flex.view as? TabElementView)?.wrappedWidth else { return }
                flex.width(width).grow(0)
            }
            scrollView.flex.layout(mode: .adjustWidth)
            scrollView.contentSize = CGSize(width: totalWidth, height: frame.height)
            scrollView.pin.all()
        } else {
            elems.forEach { (flex) in
                flex.width(nil).grow(1)
            }
            scrollView.flex.layout()
            scrollView.contentSize = scrollView.frame.size
        }
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

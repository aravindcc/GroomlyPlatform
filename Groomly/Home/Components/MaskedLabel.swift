//
//  MaskedLabel.swift
//  Groomly
//
//  Created by clar3 on 04/05/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit

public enum FillOption {
    case background, text
}

extension CGRect {
    func range(point: CGPoint) -> CGPoint {
        let rX = maxX - minX
        let rY = maxY - minY
        return CGPoint(x: rX * point.x + minX, y: rY * point.y + minY)
    }
}

extension CGContext {
    func mask(with option: FillOption) -> CGImage? {
        guard let image = makeImage() else {
            Log("******** WARNING (MaskedLabel): Unable to create image from graphics context")
            return nil
        }
        
        switch option {
        case .background:
            if let imageMask = imageMask(from: image) {
                return imageMask
            }
            else {
                return image
            }
        case .text:
            return image
        }
    }
    
    func drawGradient(withColorComponents components: [CGFloat], locations: [CGFloat]?, startPoint: CGPoint, endPoint: CGPoint) {
        guard let gradient = CGGradient(colorSpace: CGColorSpaceCreateDeviceRGB(),
                                        colorComponents: components,
                                        locations: locations,
                                        count: components.count / 4) else {
                                            Log("******** WARNING (MaskedLabel): Unable to create gradient from gradient colors")
                                            return
        }
        
        drawLinearGradient(gradient, start: startPoint, end: endPoint, options: CGGradientDrawingOptions(rawValue: 0))
    }
    
    func fill(_ rect: CGRect, with color: UIColor) {
        setFillColor(color.cgColor)
        fill(CGRect(x: 0.0, y: 0.0, width: rect.width, height: rect.height))
    }
    
    // MARK: Auxiliary methods
    
    func imageMask(from image:CGImage) -> CGImage? {
        guard
            let dataProvider = image.dataProvider,
            let imageMask = CGImage(maskWidth: image.width,
                                    height: image.height,
                                    bitsPerComponent: image.bitsPerComponent,
                                    bitsPerPixel: image.bitsPerPixel,
                                    bytesPerRow: image.bytesPerRow,
                                    provider: dataProvider,
                                    decode: image.decode,
                                    shouldInterpolate: true) else {
                                        Log("******** WARNING (MaskedLabel): Unable to create image mask from graphics context")
                                        return nil
        }
        
        return imageMask
    }
}

class MaskedLabel: UILabel {

    var gradientColors: [UIColor]?
    var gradientLocations: [CGFloat]?
    var startPoint: CGPoint?
    var endPoint: CGPoint?
    var angle: CGFloat?
    var fillColor: UIColor = UIColor.black
    var fillOption = FillOption.text
    
    private var colorComponents: [CGFloat]? {
        get {
            guard let gradientColors = gradientColors, gradientColors.count > 0 else {
                return nil
            }
            
            var colorComponents = [CGFloat]()
            
            for color in gradientColors {
                var red = CGFloat(0.0)
                var green = CGFloat(0.0)
                var blue = CGFloat(0.0)
                var alpha = CGFloat(0.0)
                
                color.getRed(&red, green: &green, blue: &blue, alpha: &alpha)
                colorComponents += [red, green, blue, alpha]
            }
            
            if colorComponents.count <= 4 {
                colorComponents += colorComponents
            }
            
            return colorComponents
        }
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        textColor = UIColor.white
    }
    
    override open func drawText(in rect: CGRect) {
        super.drawText(in: rect)
        
        let context = UIGraphicsGetCurrentContext()
        
        defer {
            context?.restoreGState()
        }
        
        guard let mask = context?.mask(with: fillOption) else {
            return
        }
        
        context?.saveGState()
        context?.concatenate(CGAffineTransform(a: 1, b: 0, c: 0, d: -1, tx: 0, ty: rect.height))
        
        context?.clear(rect)
        context?.clip(to: rect, mask: mask)
        
        if let colorComponents = colorComponents {
            context?.concatenate(CGAffineTransform(a: 1, b: 0, c: 0, d: -1, tx: 0, ty: rect.height))
            var startPoint: CGPoint!
            var endPoint: CGPoint!
            if let angle = angle {
                (startPoint, endPoint) = calculatePoints(for: angle)
                startPoint = rect.range(point: startPoint)
                endPoint = rect.range(point: endPoint)
            } else {
                startPoint = self.startPoint ?? CGPoint(x: rect.minX, y: rect.minY)
                endPoint = self.endPoint ?? CGPoint(x: rect.maxX, y: rect.maxY)
            }
            context?.drawGradient(withColorComponents: colorComponents, locations: gradientLocations, startPoint: startPoint, endPoint: endPoint)
        }
        else {
            context?.fill(rect, with: fillColor)
        }
    }
    
    func calculatePoints(for angle: CGFloat) -> (CGPoint, CGPoint) {

        var startPoint: CGPoint!
        var endPoint: CGPoint!
        var ang = (-angle).truncatingRemainder(dividingBy: 360)

        if ang < 0 { ang = 360 + ang }

        let n: CGFloat = 0.5

        switch ang {

        case 0...45, 315...360:
            let a = CGPoint(x: 0, y: n * tanx(ang) + n)
            let b = CGPoint(x: 1, y: n * tanx(-ang) + n)
            startPoint = a
            endPoint = b

        case 45...135:
            let a = CGPoint(x: n * tanx(ang - 90) + n, y: 1)
            let b = CGPoint(x: n * tanx(-ang - 90) + n, y: 0)
            startPoint = a
            endPoint = b

        case 135...225:
            let a = CGPoint(x: 1, y: n * tanx(-ang) + n)
            let b = CGPoint(x: 0, y: n * tanx(ang) + n)
           startPoint = a
            endPoint = b

        case 225...315:
            let a = CGPoint(x: n * tanx(-ang - 90) + n, y: 0)
            let b = CGPoint(x: n * tanx(ang - 90) + n, y: 1)
            startPoint = a
            endPoint = b

        default:
            let a = CGPoint(x: 0, y: n)
            let b = CGPoint(x: 1, y: n)
            startPoint = a
            endPoint = b

        }
        
        return (startPoint, endPoint)
    }

    /// Private function to aid with the math when calculating the gradient angle
    private func tanx(_ 𝜽: CGFloat) -> CGFloat {
        return tan(𝜽 * CGFloat.pi / 180)
    }
}

extension MaskedLabel {
    func addACGradient() {
        addGradient {
            [$0.highlightColorA, $0.highlightColorC]
        }
        
    }
    func addBGradient() {
        addGradient {
            [$0.highlightColorB, $0.highlightColorB.withAlphaComponent(0.6)]
        }
    }
    private func addGradient(_ inputScheme: ColorScheme? = nil, colors: @escaping (ColorScheme) -> [UIColor]) {
        let scheme = inputScheme ?? colorScheme!
        fillOption = .text
        gradientColors = colors(scheme)
        angle = 315
        
        if let schemeCase = scheme.getCase() {
            ColorConfigurator.shared.register(interestFor: schemeCase, andProperties: [.highlightColorA, .highlightColorB, .highlightColorC]) {
                [weak self] _ in
                guard let self = self else { return false }
                self.gradientColors = colors(schemeCase.scheme)
                self.setNeedsDisplay()
                return true
            }
        }
    }
}

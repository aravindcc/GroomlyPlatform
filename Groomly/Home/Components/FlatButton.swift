//
//  FlexButton.swift
//  Learning
//
//  Created by clar3 on 25/03/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//


import UIKit
import PinLayout

extension UIFont {
    var weight: UIFont.Weight {
        guard let weightNumber = traits[.weight] as? NSNumber else { return .regular }
        let weightRawValue = CGFloat(weightNumber.doubleValue)
        let weight = UIFont.Weight(rawValue: weightRawValue)
        return weight
    }

    private var traits: [UIFontDescriptor.TraitKey: Any] {
        return fontDescriptor.object(forKey: .traits) as? [UIFontDescriptor.TraitKey: Any]
            ?? [:]
    }
}

extension String {
    func calculateSize(ofFontSize fontSize: CGFloat,
                        weight: UIFont.Weight,
                        inFrame frame: CGRect,
                        andInsets inset: UIEdgeInsets,
                        repeating: Int = 1,
                        numberOfLines: Int = 0) -> CGSize {
        let titleLabel = UILabel(central: fontSize, weight: .semibold)
        titleLabel.text = String(repeating: self, count: repeating)
        titleLabel.numberOfLines = numberOfLines
        let dummy = UIView(frame: frame)
        dummy.addSchemedView(titleLabel)
        titleLabel.pin.horizontally().sizeToFit(.width)
        dummy.pin.wrapContent(padding: inset)
        return dummy.frame.size
    }
    
    func calculateSize(withFont font: UIFont,
                        inFrame frame: CGRect,
                        andInsets inset: UIEdgeInsets) -> CGSize {
        let titleLabel = UILabel()
        titleLabel.font = font
        titleLabel.text = self
        titleLabel.pin.sizeToFit(.content)
        let dummy = UIView(frame: frame)
        dummy.addSchemedView(titleLabel)
        dummy.pin.wrapContent(padding: inset)
        return dummy.frame.size
    }
}

extension NSAttributedString {
    func calculateSize(ofFontSize fontSize: CGFloat,
                        weight: UIFont.Weight,
                        inFrame frame: CGRect,
                        andInsets inset: UIEdgeInsets) -> CGSize {
        let titleLabel = UILabel(central: fontSize, weight: .semibold)
        titleLabel.attributedText = self
        titleLabel.pin.sizeToFit(.content)
        let dummy = UIView(frame: frame)
        dummy.addSchemedView(titleLabel)
        dummy.pin.wrapContent(padding: inset)
        return dummy.frame.size
    }
    
    func calculateSize(withFont font: UIFont,
                        inFrame frame: CGRect,
                        andInsets inset: UIEdgeInsets) -> CGSize {
        let titleLabel = UILabel()
        titleLabel.font = font
        titleLabel.attributedText = self
        titleLabel.pin.sizeToFit(.content)
        let dummy = UIView(frame: frame)
        dummy.addSchemedView(titleLabel)
        dummy.pin.wrapContent(padding: inset)
        return dummy.frame.size
    }
}

class FlatButton: UIView {
    let containerView = UIView()
    let titleLabel = UILabel(central: 15, weight: .semibold)
    var tapped : ButtonCompletion?
    var action: ButtonAction?
    var preset : Bool = false
    var defaultPx: CGFloat = 20
    var enabled: Bool = true {
        didSet {
            alpha = enabled ? 1 : 0.5
        }
    }
    
    var fontSize: CGFloat = 15 {
        didSet {
            titleLabel.set(pointSize: fontSize)
            refreshText()
        }
    }
    

    var icon: String? {
        didSet {
            refreshText()
            layoutSubviews()
        }
    }
    
    var title: String? {
        didSet {
            refreshText()
            layoutSubviews()
        }
    }
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        enabled ? super.hitTest(point, with: event) : nil
    }
    
    func refreshText() {
        
        // calculate icon
        var balanceAttr: [NSAttributedString.Key: Any]?
        var attachmentString: NSAttributedString?
        if let icon = icon {
            let imageAttachment = NSTextAttachment()
            let config = UIImage.SymbolConfiguration(pointSize: fontSize)
            let img = UIImage(systemName: icon, withConfiguration: config)?.withTintColor(titleLabel.textColor)
            imageAttachment.image = img
            attachmentString = NSAttributedString(attachment: imageAttachment)
            
            let font = titleLabel.font!
            let imgHeight = img?.size.height ?? 10
            let imageDiff = (imgHeight - fontSize) / 2
            let descender = font.descender / 4
            balanceAttr = [
                .font: font,
                .baselineOffset: imageDiff - descender
            ]
        }
        
        // Initialize mutable string
        let space = icon == nil || title == nil ? "" : " "
        let completeText = NSMutableAttributedString(string: "\(space)\(title ?? "")", attributes: balanceAttr)
        if let attachmentString = attachmentString {
            completeText.insert(attachmentString, at: 0)
        }
        
        // Assign
        titleLabel.attributedText = completeText
    }
    
    var wrappedSize: CGSize {
        return wrappedSize(withPadding: defaultPx)
    }
    
    func wrappedSize(withPadding px: CGFloat) -> CGSize {
        let text = self.titleLabel.attributedText ?? NSAttributedString()
        let py = (frame.height - titleLabel.frame.height) / 2
        let inset = UIEdgeInsets(top: py, left: px, bottom: py, right: px)
        return text.calculateSize(withFont: titleLabel.font, inFrame: frame, andInsets: inset)
    }
    
    var innerSize: CGSize {
        let text = self.titleLabel.attributedText ?? NSAttributedString()
        return text.calculateSize(withFont: titleLabel.font, inFrame: frame, andInsets: .zero)
    }
    
    var wrappedWidth: CGFloat {
        return wrappedSize.width
    }
    
    func setTitleTextColor(key: ColorKey) {
        titleLabel.reschemer.removeAll()
        titleLabel.schemedTextColor(key) { [weak self] in
            self?.refreshText()
        }
    }

    func commonInit() {
        setTitleTextColor(key: .highlightColorA)
        //titleLabel.singleCentralLine()
        titleLabel.adjustsFontSizeToFitWidth = false
        
        backgroundColor = .clear
        layer.masksToBounds = true
        clipsToBounds = true
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(_handleClick(_:)))
        addGestureRecognizer(tapGesture)
        
        addSubview(containerView)
        containerView.addSchemedView(titleLabel)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    @objc private func _handleClick(_ recogniser: UITapGestureRecognizer) {
        guard enabled else { return }
        self.handleClick()
    }
    
    func makeAction() {
        if let action = self.action, let promi = action(self) {
            self.becomeFirstResponder()
            self.showProgressView()
            promi.always(self.clearProgressView)
        } else {
            self.tapped?(self)
        }
    }
    
    func handleClick() {
        blink()
        makeAction()
    }
    
    func showProgressView() {
        let progressView = UIActivityIndicatorView(style: .medium)
        progressView.color = colorScheme!.highlightColorA
        progressView.tag = 88
        progressView.startAnimating()
        addSubview(progressView)
        progressView.pin.center()
        titleLabel.alpha = 0
    }
    
    func clearProgressView() {
        DispatchQueue.main.async {
            self.viewWithTag(88)?.removeFromSuperview()
            UIView.animate(withDuration: 0.1) { [weak self] in
                self?.titleLabel.alpha = 1
            }
        }
    }
    
    override func draw(_ rect: CGRect) {
        layer.borderWidth = 1
        layer.borderColor = titleLabel.textColor.cgColor
        super.draw(rect)
    }
    
    func normal(draw rect: CGRect) {
        super.draw(rect)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        containerView.pin.size(innerSize)
        titleLabel.pin.all()
        if preset {
            containerView.pin.center()
        } else {
            let padding = (frame.height - titleLabel.frame.height) / 2
            pin.wrapContent(padding:
                UIEdgeInsets(top: padding, left: defaultPx, bottom: padding, right: defaultPx))
            executePin()
        }
    }

}

class IconButton: FlatButton {
    override func draw(_ rect: CGRect) {
        super.normal(draw: rect)
    }
}

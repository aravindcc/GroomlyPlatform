//
//  BlockView.swift
//  Learning
//
//  Created by clar3 on 20/03/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import PinLayout

extension UIViewController {
    func add(_ child: UIViewController, block: BlockView) {
        addChild(child)
        block.set(content: child.view)
        child.didMove(toParent: self)
    }
}

class BlockView: UIView {
    private let containerView = UIView()
    private let titleLabel = MaskedLabel(left: 22, weight: .semibold, color: .black)
    let buttonView: FlatButton
    let barView = UIView()
    let contentView = UIScrollView()
    private var swipedown : UISwipeGestureRecognizer!
    private var swipeup : UISwipeGestureRecognizer!
    private let defaultBarHeight : CGFloat = 40
    private var barHeight : CGFloat!
    var mainButton: FlatButton? {
        didSet {
            if let mainButton = mainButton {
                addSubview(mainButton)
            }
        }
    }
    var showsMainButtonOnLeft = false {
        didSet {
            layoutSubviews()
        }
    }
    
    func set(content: UIView) {
        for v in contentView.subviews {
            v.removeFromSuperview()
        }
        contentView.addSchemedView(content)
        contentView.clipsToBounds = false
        contentView.contentSize = contentView.bounds.size
        content.pin.all()
    }
    
    func set(bar: UIView) {
        for v in barView.subviews {
            v.removeFromSuperview()
        }
        barView.addSchemedView(bar)
        bar.executePin()
        bar.pin.vCenter()
        barHeight = max(bar.frame.height, defaultBarHeight)
    }
    
    private(set) var headerHeight: CGFloat?
    var title: String? {
        get { return titleLabel.text }
        set (val) { titleLabel.text = val; layoutSubviews() }
    }
    
    var buttonTitle: String? {
        get { return buttonView.title }
        set (val) { buttonView.title = val; layoutSubviews() }
    }
    var buttonIcon: String? {
        get { return buttonView.icon }
        set (val) { buttonView.icon = val; layoutSubviews() }
    }
    var showingBar : Bool = false {
        didSet {
            UIView.animate(withDuration: 0.3) {
               self.contentView.contentOffset = .zero
               self.setNeedsLayout()
               self.layoutIfNeeded()
            }
        }
    }
    
    init(button: FlatButton) {
        self.buttonView = button
        super.init(frame: .zero)
        commonInit()
    }
    
    override init(frame: CGRect) {
        self.buttonView = FlexButton()
        super.init(frame: frame)
        commonInit()
    }
    
    func commonInit() {
        buttonView.layer.zPosition = 100000
        barHeight = defaultBarHeight
        titleLabel.addACGradient()
        titleLabel.singleCentralLine()
        
        addSchemedView(titleLabel)
        addSchemedView(buttonView)
        addSchemedView(barView)
        
        containerView.clipsToBounds = true
        addSubview(containerView)
        containerView.addSchemedView(contentView)
        
        swipedown = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe(_:)))
        swipedown.direction = .down
        swipeup = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe(_:)))
        swipeup.direction = .up
    }
    
    func addToggleBarToButton(_ completion: ((Bool) -> Void)? = nil) {
        buttonView.tapped = { [weak self] _ in
            guard let self = self else { return }
            self.showingBar = !self.showingBar
            completion?(self.showingBar)
        }
    }
    
    func tapped(closure: @escaping ButtonCompletion) {
        buttonView.tapped = closure
    }
    
    func enableSwipeOnBar() {
        contentView.addGestureRecognizer(swipedown)
        contentView.addGestureRecognizer(swipeup)
    }
    
    func disableSwipeOnBar() {
        contentView.removeGestureRecognizer(swipedown)
        contentView.removeGestureRecognizer(swipeup)
    }
    
    @objc private func handleSwipe(_ gestureRecogniser: UISwipeGestureRecognizer) {
        if !showingBar, gestureRecogniser.direction == .down {
            showingBar = true
        } else if showingBar, gestureRecogniser.direction == .up {
            showingBar = false
        }
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if let mainButton = mainButton {
            if showsMainButtonOnLeft {
                mainButton.pin.top().left()
                    .height(titleLabel.frame.height)
                    .width(mainButton.wrappedWidth)
                mainButton.cornerRadius = titleLabel.frame.height / 2
                titleLabel.pin.top().right(of: mainButton).sizeToFit(.content).marginLeft(15)
            } else {
                titleLabel.pin.top().left().sizeToFit(.content)
                mainButton.pin
                    .top()
                    .right(of: titleLabel)
                    .marginLeft(15)
                    .height(titleLabel.frame.height)
                    .width(mainButton.wrappedWidth)
            }
        } else {
            titleLabel.pin.top().left().sizeToFit(.content)
        }
        buttonView.addTargetPinClosure { [weak self] (pin) in
            guard let self = self else { return }
            pin.top().right()
                .height(of: self.titleLabel)
                .width(self.buttonTitle == nil && self.buttonIcon == nil ? 0 : self.buttonView.wrappedWidth)
        }
        buttonView.executePin()
        
        let topSpace: CGFloat = 8
        if showingBar {
            barView.alpha = 1
            barView.pin.below(of: titleLabel).height(barHeight).horizontally().top().marginTop(topSpace)
            headerHeight = barView.frame.maxY
        } else {
            barView.alpha = 0
            barView.pin.below(of: titleLabel).height(barHeight).horizontally().top().marginTop(-5)
            headerHeight = titleLabel.frame.maxY
        }
        
        // resize
        executePin()
        containerView.pin.below(of: showingBar ? barView : titleLabel).marginTop(topSpace).horizontally(-frame.origin.x).bottom()
        contentView.pin.vertically().horizontally(frame.origin.x)
    
        if let v = contentView.subviews.first {
            v.pin.all()
        }
        if let v = barView.subviews.first {
            v.executePin()
            v.pin.vCenter()
        }
        pin.sizeToFit(.content)
        
        // promote gesture recogniser
        contentView.important(gesture: swipedown)
        contentView.important(gesture: swipeup)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

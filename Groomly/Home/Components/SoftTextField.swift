//
//  SoftTextField.swift
//  Groomly
//
//  Created by clar3 on 13/05/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import PinLayout
import FlexLayout
import PhoneNumberKit

extension UITextField {
    func setHorizontal(padding amount:CGFloat){
        let size = CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height)
        let paddingView = UIView(frame: size)
        self.leftView = paddingView
        self.leftViewMode = .always
        
        let paddingViewTwo = UIView(frame: size)
        self.rightView = paddingViewTwo
        self.rightViewMode = .always
    }
}

fileprivate class MyGBTextField: PhoneNumberTextField {
    override var defaultRegion: String {
        get {
            return "GB"
        }
        set {} // exists for backward compatibility
    }
    
    override func updatePlaceholder() {
        super.updatePlaceholder()
        self.rescheme()
    }
}

class SimplestTextField: UIView {
    var field: UITextField!
    
    override var isHidden: Bool {
        didSet {
            field.isHidden = isHidden
        }
    }
    
    var onChangeCB: ((String) -> Void)? {
        didSet {
            field.addTarget(self, action: #selector(onChange(_:)), for: .editingChanged)
        }
    }
    init(title: String) {
        super.init(frame: .zero)
        
        if title.lowercased().contains("phone") {
            let phone = MyGBTextField()
            phone.withPrefix = true
            phone.withExamplePlaceholder = true
            #if DEBUG
            if AppDelegate.isUITestingEnabled {
                phone.withExamplePlaceholder = false
                phone.withPrefix = true
                phone.placeholder = title
            }
            #endif
            field = phone
        } else {
            field = UITextField()
            field.placeholder = title
        }
        field.schemedTextColor(.primaryTextColor)
        field.font = UIFont.systemFont(ofSize: 13, weight: .bold)
        field.setHorizontal(padding: 15)
        addSchemedView(field)
    }
    
    @objc private func onChange(_ field: UITextField) {
        onChangeCB?(field.text ?? "")
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        field.pin.all()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class SoftTextField: SimplestTextField {
    
    override init(title: String) {
        super.init(title: title)
        field.addSoftUIEffectForView(type: .cusp)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        field.pin.all()
        field.updateSoftUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class SimpleTextField: SimplestTextField {
    override init(title: String) {
        super.init(title: title)
        backgroundColor = .clear
        borderColor = colorScheme?.lightShadow.withAlphaComponent(1)
        borderWidth = 2
        layer.masksToBounds = true
        layer.cornerRadius = 10
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


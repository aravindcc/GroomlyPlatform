//
//  CheckoutVC.swift
//  Groomly
//
//  Created by Ravi on 30/07/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import PinLayout
import FlexLayout

class CheckoutVC: SubProcessVC {
    lazy var basket: APBasket = {
        APBasket(self, type: .checkout)
    }()
    let basketAnchor = UIView()
    let contentWrap = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        contentWrap.schemedBG(.lightShadow)
        contentWrap.layer.cornerRadius = 20
        contentWrap.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        add(basket, anchor: basketAnchor)
        view.addSubview(contentWrap)
        contentWrap.addSchemedView(basketAnchor)
        
        #if DEBUG
        if AppDelegate.isUITestingEnabled {
            if let str = ProcessInfo.processInfo.environment["ClampDate"] {
                let date = Date(dateString: str, format: "yyyy-MM-dd'T'HH:mm:ss")
                HomeCoordinater.shared.select(time: date)
            } else {
                let new = Date().startOfDay.adding(.day, value: 2).adding(.hour, value: 12)
                HomeCoordinater.shared.select(time: new)
            }
        }
        #endif
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        contentWrap.pin.horizontally().top(safeAreaInsets).bottom(view.pin.safeArea)
        basketAnchor.pin.horizontally().top(20).bottom()
        basket.view.pin.all()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if self.navigationController?.viewControllers.firstIndex(of: self) == nil {
            HomeCoordinater.shared.orderFSM.process(event: .cancelledCheckout)
        }

        super.viewWillDisappear(animated)
    }
}

extension CheckoutVC: APBasketDelegate {
    func selectedNext() {
        HomeCoordinater.shared.orderFSM.process(event: .confirmedCheckout)
        self.show(process: CardViewer(viewType:
            .full(onSelect: {
                // the coordinate will send of the request and display processing on checkoutvc
                HomeCoordinater.shared.orderFSM.process(event: .confirmedPayment)
            })
        )) {
            HomeCoordinater.shared.orderFSM.process(event: .paymentCancelled)
        }
    }
    
    func delete(item: ServiceItemOrderModel) {
        HomeCoordinater.shared.remove(service: item.service)
    }
    
    func changeTime() {
        let vc = TimeVC()
        if HomeCoordinater.shared.load(timeVC: vc) {
            vc.completion = { entry in
                defer {
                    vc.dismiss(animated: true)
                }
                guard let entry = entry else { return }
                HomeCoordinater.shared.select(time: entry.start)
                Log("chosen appointment from \(entry.start) till \(entry.end)")
            }
            view.window?.backgroundColor = AppColors.orange
            present(vc, animated: true)
        }
    }
}

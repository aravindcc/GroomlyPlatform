//
//  APBasketSectionController.swift
//  Learning
//
//  Created by clar3 on 24/03/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import IGListKit
import PinLayout

final class ServiceOrderItemSetModel: ListDiffable {
    func diffIdentifier() -> NSObjectProtocol {
        return created as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        if self === object { return true }
        guard   let object = object as? ServiceOrderItemSetModel
            else { return false }
        return items.equals(object.items)
    }
    
    var created: Date!
    var items: [ServiceItemOrderModel] = []
    
    init() {
        created = Date.today()
    }
}


class APBasketSectionController: ListBindingSectionController<ListDiffable>, ListBindingSectionControllerDataSource, ListBindingSectionControllerSelectionDelegate, ListAdapterDataSource {
    
    private weak var delegate: APBasketDelegate?
    private let type: APBasketType
    private var selectedObject : ServiceOrderModel?
    private lazy var adapter: ListAdapter = {
        let adapter = ListAdapter(updater: ListAdapterUpdater(),
                                  viewController: self.viewController)
        adapter.dataSource = self
        return adapter
    }()
    
    init(_ delegate: APBasketDelegate?, type: APBasketType) {
        self.type = type
        super.init()
        self.delegate = delegate
        dataSource = self
        selectionDelegate = self
    }
    
    func sectionController(_ sectionController: ListBindingSectionController<ListDiffable>, viewModelsFor object: Any) -> [ListDiffable] {
        guard let object = object as? ServiceOrderModel else { return [] }
        selectedObject = object
        return ["placeholder" as ListDiffable, object]
    }
    
    func sectionController(_ sectionController: ListBindingSectionController<ListDiffable>, cellForViewModel viewModel: Any, at index: Int) -> UICollectionViewCell & ListBindable {
        let cellClass: UICollectionViewCell.Type
        if viewModel is String {
            cellClass = APBasketNestedCell.self
        } else {
            cellClass = APBasketFooter.self
        }
        guard let cell = schemedReusableCell(of: cellClass, for: self, at: index) as? UICollectionViewCell & ListBindable else {
            fatalError()
        }
        if let cell = cell as? APBasketNestedCell {
            adapter.collectionView = cell.collectionView
            adapter.reloadData()
        } else if let cell = cell as? APBasketFooter {
            cell.delegate = delegate
            cell.type = type
        }
        return cell
    }
    
    func size(forViewModel viewModel: Any, inFrame frame: CGSize?) -> CGSize {
        guard   let width = frame?.width,
                let height = frame?.height
        else { return .zero }
        let footer_height: CGFloat = type.footerHeight(selectedObject)
        if viewModel is String, let items = selectedObject?.items {
            let contentHeight = max(1, CGFloat(items.count)) * APBasketItemsSectionController.cellHeight
            return CGSize(width: width, height: min(height - footer_height, contentHeight))
        } else {
            return CGSize(width: width, height: footer_height)
        }
    }
    
    func sectionController(_ sectionController: ListBindingSectionController<ListDiffable>, sizeForViewModel viewModel: Any, at index: Int) -> CGSize {
        return size(forViewModel: viewModel, inFrame: collectionContext?.containerSize)
    }
    
    // unused
    func sectionController(_ sectionController: ListBindingSectionController<ListDiffable>, didSelectItemAt index: Int, viewModel: Any) {}
    func sectionController(_ sectionController: ListBindingSectionController<ListDiffable>, didDeselectItemAt index: Int, viewModel: Any) {}
    func sectionController(_ sectionController: ListBindingSectionController<ListDiffable>, didHighlightItemAt index: Int, viewModel: Any) {}
    func sectionController(_ sectionController: ListBindingSectionController<ListDiffable>, didUnhighlightItemAt index: Int, viewModel: Any) {}
    
    
    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        let out = selectedObject == nil ? [] : selectedObject!.items
        return out
    }
    
    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        return APBasketItemsSectionController(delegate, type: type)
    }
    
    func emptyView(for listAdapter: ListAdapter) -> UIView? {
        let v = UILabel()
        v.text = "No services selected."
        v.schemedTextColor(.primaryTextColor)
        v.textAlignment = .center
        Log("ASKED TO RENDER EMPTY")
        return v
    }
}

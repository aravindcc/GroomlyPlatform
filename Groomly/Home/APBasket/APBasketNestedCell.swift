//
//  APBasketNestedCell.swift
//  Learning
//
//  Created by clar3 on 24/03/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import PinLayout
import IGListKit

final class APBasketNestedCell: UICollectionViewCell {
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
        view.backgroundColor = .clear
        view.showsVerticalScrollIndicator = true
        view.alwaysBounceHorizontal = false
        self.contentView.addSchemedView(view)
        return view
    }()

    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.pin.all()
        collectionView.pin.all()
    }
}

extension APBasketNestedCell: ListBindable {
    func bindViewModel(_ viewModel: Any) {
        collectionView.flashScrollIndicators()
        return
    }
}


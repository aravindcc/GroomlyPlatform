//
//  CheckoutSmallView.swift
//  Learning
//
//  Created by clar3 on 24/03/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import PinLayout

class CheckoutSmallView: UIView {
    weak var model: ServiceOrderModel? {
        didSet {
            guard let model = model else { return }
            let n = model.items.count
            if n == 0 {
                nameLabel.text = "Add services to order above."
                valueLabel.text = ""
            } else {
                nameLabel.text = "Swipe up to view order."
                valueLabel.text = "\(n) \(n == 1 ? "item" : "items")"
            }
            layoutSubviews()
        }
    }
    
    private let nameLabel = MaskedLabel(left: 17, weight: .bold)
    private let valueLabel = UILabel(central: 17, weight: .regular)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        nameLabel.addBGradient()
        valueLabel.numberOfLines = 1
        valueLabel.schemedTextColor(.secondaryTextColor)
        
        addSchemedView(nameLabel)
        addSchemedView(valueLabel)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let padding : CGFloat = 20
        valueLabel.pin.sizeToFit(.content).vCenter().right(padding)
        nameLabel.pin.vertically(2).left(padding).left(of: valueLabel).marginRight(padding / 2)
    }
}

//
//  APBasket.swift
//  Learning
//
//  Created by clar3 on 24/03/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import IGListKit
import PinLayout

protocol APBasketDelegate: AnyObject {
    func delete(item: ServiceItemOrderModel) -> Void
    func changeTime()
    func selectedNext()
}

class APBasket: APSubController {
    
    private weak var delegate: APBasketDelegate?
    
    var model: ServiceOrderModel? {
        didSet {
            guard model != nil else { return }
            adapter.performUpdates(animated: true)
        }
    }
    let type: APBasketType
    private let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    private lazy var adapter: ListAdapter = {
        return ListAdapter(updater: ListAdapterUpdater(), viewController: self)
    }()
    
    func reload() {
        adapter.reloadData()
    }
    
    init(_ delegate: APBasketDelegate?, type: APBasketType = .checkout) {
        self.type = type
        super.init(nibName: nil, bundle: nil)
        self.delegate = delegate
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.backgroundColor = .clear
        view.addSchemedView(collectionView)
        adapter.collectionView = collectionView
        adapter.dataSource = self
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        view.pin.all()
        collectionView.pin.all()
    }
    
    func maxHeight(inMaxFrame frame: CGSize) -> CGFloat {
        guard let model = model else { return 0 }
        let controller = APBasketSectionController(nil, type: self.type)
        let vms = controller.sectionController(controller, viewModelsFor: model)
        let heights = vms.map { controller.size(forViewModel: $0, inFrame: frame).height }
        return heights.reduce(0, +)
    }
}

extension APBasket: ListAdapterDataSource {
    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        guard let model = model else { return [] }
        return [model]
    }
    
    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        return APBasketSectionController(delegate, type: self.type)
    }
    
    func emptyView(for listAdapter: ListAdapter) -> UIView? {
        // use this to show instructions !
        return nil
    }
}

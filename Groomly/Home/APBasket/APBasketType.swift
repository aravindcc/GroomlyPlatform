//
//  APBasketType.swift
//  Groomly
//
//  Created by Ravi on 30/07/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit

enum APBasketType {
    case services
    case checkout
    case receipt
    
    var totalFontSize: CGFloat {
        switch self {
        case .services: return 0
        case .checkout: return 17
        case .receipt: return 16
        }
    }
    
    func footerHeight(_ model: ServiceOrderModel?) -> CGFloat {
        let extra = CGFloat(model?.extraFees.count ?? 0)
        switch self {
        case .services: return 60
        case .checkout: return APBasketFooter.cellHeight * (5 + extra) + 70
        case .receipt: return APBasketFooter.cellHeight * (1 + extra) + 30
        }
    }
    
    var itemPadding: CGFloat {
        switch self {
        case .services: return 20
        case .checkout: return 20
        case .receipt: return 0
        }
    }
    
    var separatorPadding: CGFloat {
        switch self {
        case .services: return 50
        case .checkout: return 0
        case .receipt: return 0
        }
    }
    
    var nextButtonMTop: CGFloat {
        switch self {
        case .services: return 0
        case .checkout: return 20
        case .receipt: return 0
        }
    }
    
    var nextButtonHeight: CGFloat? {
        if self == .services {
            return 30
        }
        return nil
    }
    
    var showsSeparator: Bool { self != .services }
    var isEditable: Bool { self != .receipt }
    var showsPrice: Bool { true }
    var showsExtraFees: Bool { self != .services }
    var showsAppointmentDetails: Bool { self != .services }
    
    var nextButtonIcon: String? {
        switch self {
        case .services: return nil
        case .checkout: return "creditcard"
        case .receipt: return nil
        }
    }
    
    var nextButtonTitle: String? {
        switch self {
        case .services: return "choose time"
        case .checkout: return "pay"
        case .receipt: return nil
        }
    }
}

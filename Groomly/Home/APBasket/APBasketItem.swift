//
//  APBasketItem.swift
//  Learning
//
//  Created by clar3 on 24/03/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import IGListKit
import PinLayout

extension UILabel {
    func set(pointSize: CGFloat) {
        self.font = font.withSize(pointSize)
    }
    
    func maxWidth(_ placeholder: String) -> CGFloat {
        guard let font = font else { return 0 }
        let fontAttributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: font]
        return (placeholder as NSString).size(withAttributes: fontAttributes).width
    }
}

class APBasketItem: UICollectionViewCell, ListBindable {
    
    var tapped: (() -> Void)?
    
    var type: APBasketType = .checkout {
        didSet {
            setFlex()
            valueLabel.isHidden = !type.showsPrice
        }
    }
    private let container = UIView()
    private let button = UIButton()
    private let nameLabel = UILabel(left: 15, weight: .semibold)
    private let valueLabel = UILabel(right: 15, weight: .semibold)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        button.addTarget(self, action: #selector(handleDeleteTap), for: .touchUpInside)
        button.setImage(UIImage(systemName: "xmark.circle"), for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 6, left: 6, bottom: 6, right: 6)
        
        nameLabel.schemedTextColor(.primaryTextColor)
        valueLabel.schemedTextColor(.primaryTextColor, alpha: 0.65)
        button.schemedTintColor(.highlightColorC)
        
        valueLabel.singleCentralLine()
        nameLabel.singleCentralLine()
        addSchemedView(container)
        setFlex()
    }
         
    func setFlex() {
        container.clear()
        let padding: CGFloat = 20
        container.flex
            .marginHorizontal(self.type.itemPadding)
            .direction(.row)
            .alignItems(.center)
            .define { (flex) in
                if self.type.isEditable {
                    flex.addItem(button).width(40).aspectRatio(1)
                    flex.addItem(nameLabel).grow(1).shrink(1).marginHorizontal(padding / 2)
                } else {
                    flex.addItem(nameLabel).grow(1).shrink(1).marginRight(padding / 2).marginLeft(0)
                }
                flex.addItem(nameLabel).grow(1).shrink(1)
                flex.addItem(valueLabel).width(valueLabel.maxWidth("£WWW.WW"))
        }
    }
    
    @objc private func handleDeleteTap() {
        tapped?()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        container.pin.all()
        container.flex.layout()
        button.updateSoftUI()
    }
    
    func bindViewModel(_ viewModel: Any) {
        guard let object = viewModel as? ServiceItemOrderModel else { return }
        if let value = object.value {
            valueLabel.text = value.sterling
            valueLabel.rescheme()
        } else {
            valueLabel.text = "N/A"
            valueLabel.textColor = AppColors.red
        }
        button.isHidden = !self.type.isEditable
        nameLabel.text = object.service.name
        layoutSubviews()
    }
}

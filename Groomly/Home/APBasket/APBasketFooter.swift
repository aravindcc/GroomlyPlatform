//
//  APBasketFooter.swift
//  Learning
//
//  Created by clar3 on 24/03/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import IGListKit
import PinLayout
import FlexLayout

fileprivate let mainFontSize: CGFloat = 15

fileprivate class APBasketFooterLine: UIView {
    let container = UIView()
    let mainLabel = UILabel(left: mainFontSize, weight: .semibold)
    let secondaryLabel = UILabel(left: mainFontSize, weight: .regular)
    var edgeView: UIView?

    func setup() {
        
        mainLabel.schemedTextColor(.primaryTextColor)
        secondaryLabel.schemedTextColor(.primaryTextColor, alpha: 0.8)
        
        secondaryLabel.adjustsFontSizeToFitWidth = true
        secondaryLabel.numberOfLines = 1
        container.flex.direction(.row).alignItems(.center).define { (flex) in
            flex.addItem(mainLabel).marginRight(5)
            flex.addItem(secondaryLabel).grow(1).shrink(1)
            if let edgeView = edgeView {
                let cap = flex.addItem(edgeView).marginLeft(10)
                if let edgeView = edgeView as? FlatButton {
                    cap.width(edgeView.wrappedWidth * 0.8).height(25)
                }
            }
        }
        
        addSchemedView(container)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    init(edgeView: UIView) {
        self.edgeView = edgeView
        super.init(frame: .zero)
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        container.pin.all()
        container.flex.layout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

fileprivate class APBasketButtonFooterLine: APBasketFooterLine {
    var tapped: Completion?
    init() {
        let button = FlatButton()
        button.setTitleTextColor(key: .secondaryTextColor)
        button.fontSize = 12
        button.preset = true
        button.title = "change"
        super.init(edgeView: button)
        
        button.tapped = { [weak self] _ in
            self?.tapped?()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

fileprivate class RoundButton: FlatButton {
    override func draw(_ rect: CGRect) {
        layer.cornerRadius = frame.height / 2
        super.normal(draw: rect)
    }
}

fileprivate class RoundSecondaryButton: FlatButton {
    override func draw(_ rect: CGRect) {
        layer.cornerRadius = frame.height / 2
        super.draw(rect)
    }
}

class APBasketFooter: UICollectionViewCell, ListBindable {
    static let cellHeight: CGFloat = 35
    private let cellHeight = APBasketFooter.cellHeight
    weak var delegate: APBasketDelegate?
    
    var type: APBasketType = .checkout {
        didSet {
            totalValueLabel.set(pointSize: type.totalFontSize)
            for flex in switchFlexes {
                flex.height(type.showsAppointmentDetails ? cellHeight : 0)
                flex.view?.isHidden = !type.showsAppointmentDetails
            }
            if !type.showsExtraFees {
                extraFees.height(0)
            }
            buttonFlex?.height(type.isEditable ? (type.nextButtonHeight ?? cellHeight) : 0)
            buttonFlex?.height(type.isEditable ? (type.nextButtonHeight ?? cellHeight) : 0)
            if type.showsAppointmentDetails {
                timeLine.isHidden = !type.isEditable
                locationLine.isHidden = !type.isEditable
            }
            if let button = buttonFlex?.view as? RoundButton {
                button.icon = self.type.nextButtonIcon
                button.title = self.type.nextButtonTitle
                buttonFlex?.width(button.wrappedWidth)
            }
        }
    }
    private let container = UIView()
    private let totalValueLabel = UILabel(right: mainFontSize, weight: .semibold)
    private var extraFees: Flex!
    private var totalLine: APBasketFooterLine
    private let clientLine = APBasketFooterLine()
    private let timeLine = APBasketButtonFooterLine()
    private let locationLine = APBasketFooterLine()
    private var switchFlexes: [Flex] = []
    private var buttonFlex: Flex?
    
    private let separator = UIView()
    
    override init(frame: CGRect) {
        
        totalValueLabel.schemedTextColor(.highlightColorB)
        separator.schemedBG(.highlightColorB)
        totalLine = APBasketFooterLine(edgeView: totalValueLabel)
        super.init(frame: frame)
        
        totalLine.mainLabel.text = "Total:"
        clientLine.mainLabel.text = "Shop:"
        timeLine.mainLabel.text = "Time:"
        locationLine.mainLabel.text = "Location:"
        timeLine.tapped = { [weak self] in
            self?.delegate?.changeTime()
        }
        
        container.flex.alignItems(.stretch).define { (flex) in
            extraFees = flex.addItem().alignItems(.stretch).height(0)
            let vs : [UIView] = [totalLine, clientLine, timeLine, locationLine]
            for v in vs {
                switchFlexes.append(flex.addItem(v).height(cellHeight))
            }
            
            let button = RoundButton()
            button.icon = "creditcard"
            button.title = "pay"
            button.schemedBG(.highlightColorA, alpha: 1)
            button.setTitleTextColor(key: .primaryTextColor)
            button.tapped = { [weak self] _ in
                self?.delegate?.selectedNext()
            }
            buttonFlex =
                flex.addItem(button)
                    .marginTop(self.type.nextButtonMTop)
                    .width(button.wrappedWidth)
                    .height(cellHeight)
                    .alignSelf(.center)
        }
        
        contentView.addSchemedView(container)
        contentView.addSchemedView(separator)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        container.pin.horizontally(self.type.itemPadding).top(self.type.showsSeparator ? 25 : 0).bottom()
        if self.type.showsSeparator {
            separator.pin.top(15).horizontally(self.type.separatorPadding).height(2)
        }
        separator.isHidden = !self.type.showsSeparator
        container.flex.layout()
    }
    
    func bindViewModel(_ viewModel: Any) {
        guard let order = viewModel as? ServiceOrderModel else { return }
        if let button = buttonFlex?.view as? RoundButton {
            button.enabled = type == .checkout ? APIClient.api.validate(order) : order.items.count > 0
        }
        update(extraFeesData: order.extraFees)
        clientLine.secondaryLabel.text = order.client?.name
        totalValueLabel.text = order.totalValue().sterling
        timeLine.secondaryLabel.text = order.timeString ?? "not chosen"
        locationLine.secondaryLabel.text = order.location ?? "not chosen"
        layoutSubviews()
        setNeedsDisplay()
    }
    
    private func update(extraFeesData: [String: Float]) {
        guard type.showsExtraFees, let container = extraFees.view else { return }
        let c = extraFeesData.count
        extraFees.height(cellHeight * CGFloat(c))
        let diff = container.subviews.count - c
        if diff > 0 {
            for i in (diff - 1)..<container.subviews.count {
                container.subviews[i].removeFromSuperview()
            }
        } else if diff < 0 {
            for _ in 0..<abs(diff) {
                let label = UILabel(right: mainFontSize, weight: .semibold)
                label.schemedTextColor(.highlightColorB)
                let line = APBasketFooterLine(edgeView: label)
                container.flex.addItem(line).height(cellHeight)
            }
        }
        
        guard container.subviews.count == c else { return }
        
        for (i, (key, val)) in extraFeesData.enumerated() {
            guard
                let line = container.subviews[i] as? APBasketFooterLine,
                let edge = line.edgeView as? UILabel
            else { continue }
            line.mainLabel.text = "\(key):"
            edge.text = val.sterling
        }
    }
}

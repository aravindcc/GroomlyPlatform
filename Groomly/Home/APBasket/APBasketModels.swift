//
//  APBasketModels.swift
//  Learning
//
//  Created by clar3 on 24/03/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import PinLayout
import IGListKit

extension Int {
    var approxTime: Date? {
        let (hs, ms, ss) = (self / 3600, (self % 3600) / 60, (self % 3600) % 60)
        return .init(year: 2000, month: 1, day: 1, hour: hs, minute: ms, second: ss)
    }
}

final class ServiceItemOrderModel: ListDiffable {
    func diffIdentifier() -> NSObjectProtocol {
        return uuid as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        if self === object { return true }
        guard   let object = object as? ServiceItemOrderModel
            else { return false }
        return
            service.key == object.service.key &&
            value == object.value &&
            duration == object.duration
    }
    
    let uuid: String
    var service: ServiceModel
    var value: Float?
    var duration: Int?
    
    init(service: ServiceModel, value: Float?, duration: Int?) {
        self.uuid = UUID().uuidString
        self.service = service
        self.value = value
        self.duration = duration
    }

    var approxTime: Date? {
        duration?.approxTime
    }
}

final class ServiceOrderModel: ListDiffable {
    
    func copy() -> ServiceOrderModel {
        let out = ServiceOrderModel(id: uuid)
        out.client = client
        out.items = items
        out.extraFees = extraFees
        out.time = time
        out.location = location
        return out
    }
    func diffIdentifier() -> NSObjectProtocol {
        return uuid as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        if self === object { return true }
        guard
            let object = object as? ServiceOrderModel
        else { return false }
        return  client?.id == object.client?.id &&
                items.equals(object.items) &&
                extraFees == extraFees &&
                time == object.time &&
                location == object.location
    }
    
    private var uuid: String!
    var client: ClientStubResponse?
    var items: [ServiceItemOrderModel] = []
    var time: Date?
    var location: String?
    var extraFees: [String: Float] = [
        "Booking Fee": 1,
        "Groomly Fee": 10,
        "Cancellation Fee": 2.50
    ]
    
    func totalValue() -> Float {
        let extra = extraFees.reduce(0, { $0 + $1.value })
        return items.reduce(0, { $0 + ($1.value ?? 0) }) + extra
    }
    
    var timeString: String? {
        guard let endTime = endTime else { return nil }
        let df = DateFormatter.appDate
        let tf = DateFormatter.appTime
        let date = df.string(from: time!)
        let t1 = tf.string(from: time!).lowercased()
        let t2 = tf.string(from: endTime).lowercased()
        return "\(t1) - \(t2) \(date)"
    }
    
    var endTime: Date? {
        let seconds = items.reduce(0, { $0 + ($1.duration ?? 0) })
        guard seconds > 0, let startTime = time else { return nil }
        return startTime.adding(.second, value: seconds)
    }
    
    init() {
        self.uuid = UUID().uuidString
    }
    
    init(id uuid: String) {
        self.uuid = uuid
    }
}

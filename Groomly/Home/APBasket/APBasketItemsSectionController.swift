//
//  APBasketItemsSectionController.swift
//  Learning
//
//  Created by clar3 on 24/03/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import IGListKit
import PinLayout

class APBasketItemsSectionController: ListSectionController {

    private weak var delegate: APBasketDelegate?
    private var object: AnyObject?
    private let type: APBasketType
    static let cellHeight: CGFloat = 40
    
    init(_ delegate: APBasketDelegate?, type: APBasketType) {
        self.type = type
        super.init()
        self.delegate = delegate
    }
    
    override func numberOfItems() -> Int {
        return 1
    }
    
    override func sizeForItem(at index: Int) -> CGSize {
        guard let width = collectionContext?.containerSize.width else { return .zero }
        return CGSize(width: width, height: APBasketItemsSectionController.cellHeight)
    }

    override func cellForItem(at index: Int) -> UICollectionViewCell {
        guard   let cell = schemedReusableCell(of: APBasketItem.self, for: self, at: index) as? APBasketItem,
                let object = object
        else { fatalError() }
        cell.type = type
        cell.bindViewModel(object)
        cell.tapped = { [weak self] in
            guard let object = self?.object as? ServiceItemOrderModel else { return }
            self?.delegate?.delete(item: object)
        }
        return cell
    }

    override func didUpdate(to object: Any) {
        self.object = object as AnyObject
    }

    override func didSelectItem(at index: Int) {
        // selected
    }
}


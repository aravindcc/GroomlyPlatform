//
//  ServiceList.swift
//  Groomly
//
//  Created by Ravi on 23/10/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import PinLayout
import FlexLayout
import IGListKit

class ServiceDetailView: UIViewController {
    
    private let contentView = UIView()
    private let titleLabel = UILabel(left: 18, weight: .bold)
    private let descriptionLabel = UILabel(left: 16, weight: .regular)
    private var imageGrid: APImageGrid<PortfolioImage>!
    private var imageGridFlex: Flex!
    private let model: ServiceModel
    private let gotData: ServiceDataResponse
    private lazy var loadingIndicator: UIActivityIndicatorView = {
        let progressView = UIActivityIndicatorView(style: .medium)
        progressView.color = self.view.colorScheme!.highlightColorA
        progressView.tag = 88
        progressView.startAnimating()
        self.view.addSubview(progressView)
        progressView.pin.center()
        return progressView
    }()
    private var hasLoaded: Bool = true {
        didSet {
            let hasLoaded = self.hasLoaded
            UIView.animate(withDuration: 0.1) {
                self.loadingIndicator.isHidden = hasLoaded
                self.contentView.isHidden = !hasLoaded
            }
        }
    }
    
    init(model: ServiceModel, data: ServiceDataResponse) {
        self.model = model
        self.gotData = data
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        preferredContentSize.height = .infinity
        view.colorScheme = .normal
        view.schemedBG()
        
        titleLabel.schemedTextColor(.primaryTextColor)
        descriptionLabel.schemedTextColor(.primaryTextColor, alpha: 0.9)
        
        imageGrid = APImageGrid<PortfolioImage>()
        imageGrid?.assignHeroes = false
        imageGrid?.transitionClosure = { [weak self] (model, ind, transition) in
            let carousel = ImageCarousel<PortfolioImage>()
            carousel.selected = ind
            carousel.view.cornerRadius = 10
            carousel.view.layer.masksToBounds = true
            carousel.items = self?.gotData.portfolio ?? []
            return carousel
        }
        
        let addButton = FlexButton()
        addButton.title = "order"
        addButton.icon = "plus"
        addButton.tapped = { [unowned self] _ in
            HomeCoordinater.shared.add(service: self.model)
            self.dismiss(animated: true)
        }
        
        contentView.flex.alignItems(.stretch).define { (flex) in
            flex.addItem(titleLabel).marginBottom(10)
            flex.addItem(descriptionLabel).marginVertical(25)
            imageGridFlex = flex.addItem()
            flex.addItem(addButton).width(addButton.wrappedWidth).height(35).alignSelf(.center).marginTop(25)
        }
        
        add(imageGrid, anchor: imageGridFlex.view)
        view.addSchemedView(contentView)
        
        hasLoaded = false
        displayData()
    }
    
    private func displayData() {
        titleLabel.text = gotData.name
        descriptionLabel.text = gotData.description
        titleLabel.frame.size.width = contentView.frame.width
        descriptionLabel.frame.size.width = contentView.frame.width
        titleLabel.flex.height(titleLabel.maxHeight)
        descriptionLabel.flex.height(descriptionLabel.maxHeight)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) { [weak self] in
            guard let self = self else { return }
            self.imageGrid.set(items: self.gotData.portfolio)
            if let height = self.imageGrid?.cv.collectionViewLayout.collectionViewContentSize.height {
                self.imageGridFlex?.height(height)
                self.imageGrid?.cv.isScrollEnabled = false
            }
            self.hasLoaded = true
            self.viewDidLayoutSubviews()
        }
    }
    
    private var adjusted = false
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        contentView.pin.horizontally().vCenter().marginHorizontal(20)
        contentView.flex.layout(mode: .adjustHeight)
        imageGrid?.view.pin.all()
        loadingIndicator.pin.center()
        
        if !adjusted {
            adjusted = true
            let padTop: CGFloat = 10
            let topM = UIScreen.main.bounds.height - contentView.frame.height - padTop * 2
            try? JellyControl.shared.jellyAnimator?.updateMarginGuards(
                marginGuards: UIEdgeInsets(top: topM, left: 10, bottom: topM, right: 10),
                duration: .medium
            )
        }
    }
}

class ServiceListSectionController: ListSectionController, ListSupplementaryViewSource {
    
    var didSelect: ((ServiceModel) -> Void)?
    
    func supportedElementKinds() -> [String] {
        [UICollectionView.elementKindSectionHeader]
    }
    
    func viewForSupplementaryElement(ofKind elementKind: String, at index: Int) -> UICollectionReusableView {
        guard let view = collectionContext?.dequeueReusableSupplementaryView(
            ofKind: UICollectionView.elementKindSectionHeader,
            for: self,
            class: PriceEditorCategoryHeader.self,
            at: index
        ) as? PriceEditorCategoryHeader
        else { return UICollectionViewCell() }
        view.title = (object as? ListableCategory)?.category
        view.clear = true
        return view
    }
    
    func sizeForSupplementaryView(ofKind elementKind: String, at index: Int) -> CGSize {
        return CGSize(width: collectionContext!.containerSize.width, height: 35)
    }
    

    private let rowHeight: CGFloat = 70
    private var object: Any? = nil
    
    override init() {
        super.init()
        supplementaryViewSource = self
    }
    
    override func numberOfItems() -> Int {
        (object as? ListableCategory)?.services.count ?? 0
    }
    
    override func sizeForItem(at index: Int) -> CGSize {
        guard let width = collectionContext?.containerSize.width else { return .zero }
        return CGSize(width: width, height: rowHeight)
    }

    override func cellForItem(at index: Int) -> UICollectionViewCell {
        if
            let topBoy = object as? ListableCategory,
            topBoy.services.count > 0,
            let cell = schemedReusableCell(of: PriceEditorCell.self, for: self, at: index) as? PriceEditorCell {
            cell.display(model: topBoy.services[index])
            cell.enabled = false
            return cell
        }
        return UICollectionViewCell()
    }
    
    override func didSelectItem(at index: Int) {
        if let object = object as? ListableCategory, index < object.services.count {
            didSelect?(object.services[index].service)
        }
    }

    override func didUpdate(to object: Any) {
        if let object = object as? ListableCategory {
            self.object = object
        }
    }
}

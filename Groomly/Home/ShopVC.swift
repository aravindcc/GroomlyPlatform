//
//  HomeVC.swift
//  Learning
//
//  Created by clar3 on 16/03/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import FlexLayout
import PinLayout
import Hero
import IGListKit
import Async
import Jelly

class ShopVC: SubProcessVC {
    
    private let container = UIView()
    private let contentWrap = UIView()
    private var professionalView: ProView!
    private var profileModelCache : [ClientStubResponse : UIImage] = [:]
    private lazy var loadingIndicator: UIActivityIndicatorView = {
        let progressView = UIActivityIndicatorView(style: .medium)
        progressView.color = self.view.colorScheme!.highlightColorA
        progressView.tag = 88
        self.view.addSubview(progressView)
        progressView.pin.center()
        return progressView
    }()
    var hasLoaded: Bool = true {
        didSet {
            let hasLoaded = self.hasLoaded
            if hasLoaded {
                loadingIndicator.stopAnimating()
            } else {
                loadingIndicator.startAnimating()
            }
            UIView.animate(withDuration: 0.1) {
                self.loadingIndicator.isHidden = hasLoaded
                self.container.isHidden = !hasLoaded
            }
        }
    }
    
    @ClearableProperty(defaultValue: true, clearTime: 0.2)
    private var refreshResults: Bool = true
    private let search = UISearchBar()
    private var response: [ClientStubResponse] = []
    private var filterOptionsDistance: Set<DistanceFilterOption> = []
    private var filterOptionsRating: Set<RatingFilterOption> = []
    
    func loadData(clients: [ClientStubResponse]) {
        self.response = clients
        DispatchQueue.main.async {
            self.hasLoaded = true
            self.professionalView.set(items: self.response)
            self.search.text = nil
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        HomeCoordinater.shared.refreshClients()
    }
    
    private func displayFiltered() {
        let filter_by = self.search.text?.lowercased() ?? ""
        var items = filter_by == "" ? self.response : self.response.filter({ $0.name.lowercased().contains(filter_by) })
        if filterOptionsDistance.count > 0 {
            var digis = CharacterSet.decimalDigits
            digis.insert(".")
            items = items.filter {
                let dist_val = $0.distance.contains("ft") ? 0 : Float($0.distance.components(separatedBy: digis.inverted).joined()) ?? 0
                for distance in filterOptionsDistance {
                    switch distance {
                    case .upto3:
                        if dist_val < 3 {
                            return true;
                        }
                    case .upto6:
                        if dist_val >= 3 && dist_val < 6 {
                            return true;
                        }
                    case .upto9:
                        if dist_val >= 6 && dist_val < 9 {
                            return true;
                        }
                    case .upto12:
                        if dist_val >= 9 && dist_val < 12 {
                            return true;
                        }
                    case .upto15:
                        if dist_val >= 12 && dist_val < 15 {
                            return true;
                        }
                    case .all:
                        if dist_val >= 15 {
                            return true;
                        }
                    }
                }
                return false;
            }
        }
        if filterOptionsRating.count > 0 {
            items = items.filter {
                let val = $0.profile?.review_score?.score ?? 0
                for rating in filterOptionsRating {
                    switch rating {
                    case .one:
                        if val == 1 {
                            return true;
                        }
                    case .two:
                        if val == 2 {
                            return true;
                        }
                    case .three:
                        if val == 3 {
                            return true;
                        }
                    case .four:
                        if val == 4 {
                            return true;
                        }
                    case .five:
                        if val == 5 {
                            return true;
                        }
                    }
                }
                return false;
            }
        }
        self.professionalView.set(items: items)
    }
     
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //definesPresentationContext = true
        
        _refreshResults.display = { [unowned self] in
            if self.refreshResults {
                self.displayFiltered()
            }
        }
        
        // blocks
        let changeLocationButton = FlatButton()
        changeLocationButton.title = "change"
        changeLocationButton.defaultPx = 10
        changeLocationButton.tapped = { [weak self] _ in
            AddressBackend.shared.sendsNotification = false
            self?.show(process: AddressVC(viewType:
                .full(onSelect: {
                    HomeCoordinater.shared.refreshClients()
                    AddressBackend.shared.sendsNotification = true
                })
            ))
        }
        
        contentWrap.schemedBG()
        contentWrap.layer.cornerRadius = 20
        contentWrap.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        view.addSchemedView(contentWrap)
        contentWrap.addSchemedView(container)
        
        
        // collection view
        professionalView = ProView()
        search.overrideUserInterfaceStyle = .light
        search.barTintColor = .clear
        search.backgroundColor = .clear
        search.backgroundImage = UIImage()
        search.isTranslucent = true
        search.showsCancelButton = false
        search.delegate = self
        search.addTargetPinClosure { pin in
            pin.horizontally().sizeToFit(.content).vCenter()
        }
        
        // color configuration
        view.backgroundColor = .clear
        search.searchTextField.schemedTextColor(.primaryTextColor)
        hasLoaded = false
        
        let c = UIView()
        c.layer.zPosition = 2
        c.schemedBG(.lightShadow)
        professionalView.layer.zPosition = 1
        
        let blt = IconButton()
        blt.icon = "line.horizontal.3.decrease.circle.fill"
        blt.fontSize = 20
        blt.defaultPx = 10
        blt.tapped = { _ in
            self.showFilterview()
        }
        
        container.flex.alignItems(.stretch).define { flex in
            flex.addItem(c).paddingVertical(contentPadding / 2)
                .paddingHorizontal(contentPadding).shrink(0).grow(0)
                .direction(.row)
                .alignItems(.center).define { flex in
                flex.addItem(search).shrink(1).grow(1)
                flex.addItem(blt).width(blt.wrappedWidth).height(30)
            }
            flex.addItem(professionalView).marginHorizontal(contentPadding).grow(1).shrink(1)
        }
        contentWrap.maskToBounds = true
    }
    
    var filterAnimator: Jelly.Animator?
    private func showFilterview() {
        let marginGuards = UIEdgeInsets(top: 50, left: 20, bottom: 50, right: 20)
        let uiConfiguration = PresentationUIConfiguration(
            cornerRadius: 10,
            backgroundStyle: .dimmed(alpha: 0.5),
            isTapBackgroundToDismissEnabled: true
        )
        let chosenHeight: Size = .custom(value: view.frame.height * 0.8)
        let size = PresentationSize(width: .fullscreen, height: chosenHeight)
        let alignment = PresentationAlignment(vertical: .center, horizontal: .center)
        let interaction = InteractionConfiguration(presentingViewController: self, completionThreshold: 0.5, dragMode: .canvas, mode: .dismiss)
        let presentation = CoverPresentation(directionShow: .top, directionDismiss: .top, uiConfiguration: uiConfiguration, size: size, alignment: alignment, marginGuards: marginGuards, interactionConfiguration: interaction)
        
        let vc = FilterVC()
        vc.refresh = { [unowned self] in
            self.displayFiltered()
            vc.renderOptions(distances: self.filterOptionsDistance, ratings: self.filterOptionsRating)
        }
        vc.toggleRating = { [unowned self] in
            if self.filterOptionsRating.contains($0) {
                self.filterOptionsRating.remove($0)
            } else {
                self.filterOptionsRating.insert($0)
            }
            vc.refresh?()
        }
        vc.toggleDistance = { [unowned self] in
            if self.filterOptionsDistance.contains($0) {
                self.filterOptionsDistance.remove($0)
            } else {
                self.filterOptionsDistance.insert($0)
            }
            vc.refresh?()
        }
        vc.clear = { [unowned self] in
            self.filterOptionsRating = []
            self.filterOptionsDistance = []
            vc.refresh?()
        }
        filterAnimator = Animator(presentation: presentation)
        filterAnimator?.prepare(presentedViewController: vc)
        present(vc, animated: true, completion: nil)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        layoutSubviews()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if self.navigationController?.viewControllers.firstIndex(of: self) == nil {
            HomeCoordinater.shared.orderFSM.process(event: .cancelledClients)
        }

        super.viewWillDisappear(animated)
    }
    
    private let contentPadding: CGFloat = 20
    private func layoutSubviews() {
        //let block_height: CGFloat = (professionalsBlock.headerHeight ?? 0) + min(165, view.frame.height * 0.21)
        contentWrap.pin.top(safeAreaInsets).horizontally().bottom(view.pin.safeArea)
        container.pin.vertically().horizontally()
        container.flex.layout()
    }

    override var prefersStatusBarHidden: Bool { return false }
}

extension ShopVC: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        refreshResults = false
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}

//
//  HomeVC.swift
//  Learning
//
//  Created by clar3 on 16/03/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import FlexLayout
import PinLayout
import Hero
import IGListKit
import Async

class RandomPhoto: DisplayImageModel, Codable, ListDiffable {
    let id: String
    let author: String
    let width: Float
    let height: Float
    let url: String
    let download_url: String
    var description: String {
        return author
    }
    
    var image : URL? {
        return getURL()
    }
    
    func getURL() -> URL? {
        return URL(string: download_url)
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        return id as NSString
    }

    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard let object = object as? RandomPhoto else { return false }
        return self.id == object.id
    }
}

//struct ClientProfile: TabItem, APItem, Equatable, Hashable {
//    var image: URL?
//    var name: String
//    var company: String = "City Hairdressers"
//    var distance: String = "1 mile"
//    var id: String
//    
//    init(id: String, name: String, image: URL?) {
//        self.name = name
//        self.image = image
//        self.id = id
//    }
//    
//    static func ==(lhs: ClientProfile, rhs: ClientProfile) -> Bool {
//        return lhs.id == rhs.id
//    }
//}

enum ServicesGroup: String, CaseIterable {
    case men = "mens"
    case women = "womens"
    case uni = "unisex"
    
    var categories: [ServiceCategory] {
        switch self {
        case .men:
            return [.mensHair, .beard]
        case .women:
            return [.womensHair, .makeup, .eyes]
        case .uni:
            return [.nails]
        }
    }
}

class ShopVC: UIViewController {
    
    let orderVC = StickyBasketVC()
    private var professionalsBlock: BlockView!
    private var serviceBlock: BlockView!
    private var professionalView: ProView!
    private var imageGrid: APImageGrid<ServiceModel>?
    private var tabView: TabView<ServiceCategory>!
    private var serviceGroupButton: OptionButton<ServicesGroup>!
    private var profileModelCache : [ClientStubResponse : UIImage] = [:]
    private var serviceModelStore: [ServiceCategory: [ServiceModel]] = [:]
    private var stockPhotos: [RandomPhoto] = []
    
    static func randomPhotos(completion: @escaping ([RandomPhoto]) -> Void) {
        guard let url = URL(string: "https://picsum.photos/v2/list?page=8&limit=30") else { completion([]); return }
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data else { completion([]); return }
            let jsonDecoder = JSONDecoder()
            do {
                let parsedJSON = try jsonDecoder.decode([RandomPhoto].self, from: data)
                completion(parsedJSON)
            }
            catch {
                print(error)
                completion([])
            }
        }.resume()
    }
    
    func display(serviceGroup: ServicesGroup) {
        let categories = serviceGroup.categories
        self.tabView.set(items: categories)
        self.imageGrid?.set(items: self.serviceModelStore[self.tabView.selectedItem] ?? [])
    }
    
    func loadData(stockPhotos: [RandomPhoto], clients: [ClientStubResponse], services: [ServiceModel]) {
        self.stockPhotos = stockPhotos
        DispatchQueue.main.async {
            guard stockPhotos.count >= 30 else { return }
            let items = Array(0...20).map { i in
                ClientStubResponse(id: i,
                                   distance: "\((1...10).randomElement() ?? 1) km",
                                   first_name: Lorem.firstName,
                                   last_name: Lorem.lastName,
                                   formatted_address: Lorem.emailAddress,
                                   company: Lorem.fullName,
                                   profile: ClientProfileModel(clientID: i,
                                                               description: Lorem.sentences(4),
                                                               matrix: []),
                                   image: stockPhotos[i].getURL())
            }
            self.professionalView.gallery = stockPhotos
            self.professionalView.set(items: items)
            for m in services {
                if self.serviceModelStore[m.category] == nil {
                    self.serviceModelStore[m.category] = [m]
                } else {
                    self.serviceModelStore[m.category]!.append(m)
                }
            }
            for (category, ms) in self.serviceModelStore {
                self.serviceModelStore[category] = APImageGrid.createNiceColumnOrder(items: ms)
            }
            self.display(serviceGroup: self.serviceGroupButton.selectedOption)
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        definesPresentationContext = true
        
        // blocks
        professionalsBlock = BlockView()
        professionalsBlock.title = "Professionals"
        professionalsBlock.buttonIcon = "magnifyingglass"
        professionalsBlock.addTargetPinClosure { [weak self] (_) in
            self?.layoutSubviews()
        }
        view.addSchemedView(professionalsBlock)
        
        serviceGroupButton = OptionButton<ServicesGroup>()
        serviceGroupButton.didSelectOption = {
            self.display(serviceGroup: $0)
        }
        
        serviceBlock = BlockView(button: serviceGroupButton)
        serviceBlock.showingBar = true
        serviceBlock.title = "Services"
        //view.addSchemedView(serviceBlock)
        
        tabView = TabView<ServiceCategory>(frame: .zero) { [weak self] (item) in
            guard let self = self else { return }
            self.imageGrid?.set(items: self.serviceModelStore[item] ?? [])
        }
        tabView.addTargetPinClosure { (pin) in
            pin.all(3)
            self.tabView.flex.layout()
        }
        serviceBlock.set(bar: tabView)
        
        // collection view
        professionalView = ProView()
        professionalsBlock.set(content: professionalView)
        let search = UISearchBar()
        search.overrideUserInterfaceStyle = .light
        search.barTintColor = .clear
        search.backgroundColor = .clear
        search.backgroundImage = UIImage()
        search.isTranslucent = true
        search.showsCancelButton = false
        search.addTargetPinClosure { pin in
            pin.horizontally().sizeToFit(.content).vCenter()
        }
        professionalsBlock.set(bar: search)
        professionalsBlock.addToggleBarToButton()

        // image grid
        imageGrid = APImageGrid()
        imageGrid?.transitionClosure = { (model, ind, transition) in
            let vc = ServiceVC(model, transition: transition)
            return vc
        }
        add(self.imageGrid!, block: serviceBlock)
        
      
        orderVC.handleHasPanned = { [weak self] recogniser in
            self?.popoverHandlePan(recogniser)
        }
        currentPopoverHeight = orderVC.minimumHeight()
        add(orderVC)
        
        Coordinater.shared.registerHome(self)
        
        // color configuration
        view.schemedBG()
        search.searchTextField.schemedTextColor(.primaryTextColor)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        layoutSubviews()
    }
    
    func resizeCart() {
        orderVC.calculateBounds()
        currentPopoverHeight = min(currentPopoverHeight, orderVC.maximumHeight())
        currentPopoverHeight = max(currentPopoverHeight, orderVC.minimumHeight())
        self.layoutSubviews()
    }
    
    private func layoutSubviews() {
        let padding: CGFloat = 15
        //let block_height: CGFloat = (professionalsBlock.headerHeight ?? 0) + min(165, view.frame.height * 0.21)
        professionalsBlock.pin.top(view.pin.safeArea).marginTop(5).horizontally(padding).bottom(view.pin.safeArea + orderVC.minimumHeight())
        orderVC.view.pin.horizontally().height(currentPopoverHeight).bottom(view.pin.safeArea)
        serviceBlock.pin.horizontally(padding).below(of: professionalsBlock).marginTop(50).bottom(view.pin.safeArea + orderVC.minimumHeight())
    }
    
    private var currentPopoverHeight : CGFloat!
    private var startedHeight: CGFloat!
    private func popoverHandlePan(_ sender: UIPanGestureRecognizer) {
        let translation = sender.translation(in: self.view)
        
        defer {
            layoutSubviews()
        }
        
        if sender.state == .began {
            startedHeight = currentPopoverHeight
        }
        
        if let viewToDrag = sender.view {
            currentPopoverHeight += translation.y * -1
            currentPopoverHeight = min(currentPopoverHeight, orderVC.maximumHeight())
            currentPopoverHeight = max(currentPopoverHeight, orderVC.minimumHeight())
            sender.setTranslation(CGPoint(x: 0, y: 0), in: viewToDrag)
        }
        
        if sender.state == .ended {
            let goingDown = startedHeight > currentPopoverHeight
            let (lower, upper) = orderVC.currentRange()
            let ratio = (currentPopoverHeight - lower) / (upper - lower)
            if goingDown {
                currentPopoverHeight = ratio < 0.7 ? lower : upper
            } else {
                currentPopoverHeight = ratio < 0.3 ? lower : upper
            }
        }
    }
    
    func showCart(silent: Bool) {
        if silent && orderVC.showingState != .Large {
            return
        }
        orderVC.calculateBounds()
        currentPopoverHeight = orderVC.maximumHeight()
        UIView.animate(withDuration: 0.1, animations: {
            self.layoutSubviews()
        })
    }
    
    override var prefersStatusBarHidden: Bool { return false }
}

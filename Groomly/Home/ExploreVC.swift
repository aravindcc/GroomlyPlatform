//
//  ExploreVC.swift
//  Groomly
//
//  Created by Ravi on 30/07/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import PinLayout
import FlexLayout
import IGListKit

class TabScrollView: UIScrollView {
    var didSelect: ((Int) -> Void)?
    
    @ClearableProperty(defaultValue: nil, clearTime: 1)
    var selectedIndex: Int?
    
    @ClearableProperty(defaultValue: true, clearTime: 0.05)
    private var scrollTo: Bool
    private var selectedView: UIView?
    private var refSelect: Int?

    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        _scrollTo.display = { [unowned self] in
            guard let s = selectedView, scrollTo else { return }
            selectedView = nil
            DispatchQueue.main.async { [unowned self] in
                self.setContentOffset(
                    CGPoint(
                        x: max(0, min(self.contentSize.width - self.bounds.width, s.frame.minX)),
                        y: 0
                    ),
                    animated: true
                )
            }
        }
        
        let closure: ColorClosure = { [weak self] _ in
            if let self = self, let refSelect = self.refSelect {
                DispatchQueue.main.async { [weak self] in
                    self?.select(refSelect)
                }
                return true
            }
            return false
        }
        reschemer.add(rule: closure)
        ColorConfigurator.shared.register(andProperties: [.lightShadow, .bg], withUpdator: closure)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        if let touch = touches.first {
            let touchpoint:CGPoint = touch.location(in: self)
            var matches = [(offset: Int, element: UIView)]()
            for e in self.subviews.enumerated() {
                guard let s = e.element as? UIImageView else { continue }
                if s.frame.contains(touchpoint) {
                    matches.append(e)
                }
            }
            if matches.count == 0 { return }
            if matches.count == 1 || matches[0].element.tag < matches[1].element.tag {
                // select this
                selectedIndex = matches[0].offset
                didSelect?(matches[0].offset)
            } else {
                selectedIndex = matches[0].offset
                didSelect?(matches[1].offset)
            }
           
        }
    }
    
    
    func set(items: [String]) {
        clear()
        if refSelect == nil {
            refSelect = 0
        }
        if items.count == 0 { return }
        if let tabIMG = UIImage(named: "TabBG")?.withRenderingMode(.alwaysTemplate) {
            flex.direction(.row).define { flex in
                for i in 0..<items.count {
                    let im = UIImageView(image: tabIMG)
                    let t = UILabel(central: 13, weight: .semibold, wantsMonster: true)
                    t.schemedTextColor(.primaryTextColor)
                    t.text = items[i]
                    let w = max(120, t.intrinsicContentSize.width)
                    let wx = max(120, w + 20)
                    im.flex.width(wx).height(43).alignItems(.center).justifyContent(.center).addItem(t).width(w)
                    im.tintColor = i == 0 ? colorScheme!.lightShadow : colorScheme!.bg
                    im.layer.zPosition = CGFloat(items.count - i)
                    im.tag = i
                    flex.addItem(im).marginRight(i == items.count - 1 ? 0 : -10)
                }
            }
        }
    }
    
    func select(_ index: Int?) {
        guard let index = selectedIndex ?? index else { return }
        refSelect = index
        let vs = subviews
        if index < vs.count {
            for e in self.subviews.enumerated() {
                guard let s = e.element as? UIImageView else { continue }
                if e.offset == index {
                    s.tintColor = colorScheme!.lightShadow
                    selectedView = s
                    scrollTo = false
                } else {
                    s.tintColor = colorScheme!.bg
                }
            }
        }
    }
}


class ExploreVC: SubProcessVC, ListAdapterDataSource, ListScrollDelegate {
    
    func listAdapter(_ listAdapter: ListAdapter, didScroll sectionController: ListSectionController) {
        categories.select(
            listAdapter.visibleSectionControllers().map { $0.section }.min()
        )
    }
    func listAdapter(_ listAdapter: ListAdapter, willBeginDragging sectionController: ListSectionController) {}
    func listAdapter(_ listAdapter: ListAdapter, didEndDragging sectionController: ListSectionController, willDecelerate decelerate: Bool) {}
    
    
    
    
    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        items
    }
    
    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        let controller = ServiceListSectionController()
        controller.scrollDelegate = self
        controller.didSelect = { [unowned self] in self.edit(service: $0)}
        return controller
    }
    
    func emptyView(for listAdapter: ListAdapter) -> UIView? {
        return nil
    }
    
    private func edit(service: ServiceModel) {
        Message.notify(
            process: APIClient.api.detail(service: service.key),
            loadingMessage: "Loading data...",
            successMessage: nil,
            errorMessage: "There was an error loading the service."
        ) { [weak self] (result) in
            switch result {
            case .success(let model):
                if !(model.description == nil || model.description == "") || model.portfolio.count != 0 {
                    self?.show(process: ServiceDetailView(model: service, data: model))
                } else {
                    Message.showConfirmation(title: "Add?", description: "Please confirm if you want to add \(service.name) to your order.") {
                        if $0 {
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                                HomeCoordinater.shared.add(service: service)
                            }
                        }
                    }
                } 
            case .failure(let error):
                Log("FAILED FOR REASON \(error)")
            }
        }
    }
    
    private var items: [ListableCategory] = []
    lazy var cv : UICollectionView = {
        let layout = ListCollectionViewLayout(stickyHeaders: false, topContentInset: 0, stretchToEdge: true)
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.accessibilityIdentifier = "priceListCV"
        cv.backgroundColor = .clear
        return cv
    }()
    lazy var adapter: ListAdapter = {
        return ListAdapter(updater: ListAdapterUpdater(), viewController: self)
    }()
    
    let orderVC = StickyBasketVC()
    private let categories = TabScrollView()
    private let contentWrap = UIView()
    private let contentView = UIView()
    private lazy var loadingIndicator: UIActivityIndicatorView = {
        let progressView = UIActivityIndicatorView(style: .medium)
        progressView.color = self.view.colorScheme!.highlightColorA
        progressView.tag = 88
        progressView.startAnimating()
        self.view.addSubview(progressView)
        progressView.pin.center()
        return progressView
    }()
    private var hasLoaded: Bool = false {
        didSet {
            let hasLoaded = self.hasLoaded
            UIView.animate(withDuration: 0.1) {
                self.loadingIndicator.isHidden = hasLoaded
                self.cv.isHidden = !hasLoaded
                self.orderVC.view.isHidden = !hasLoaded
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.colorScheme = .normal
        adapter.collectionView = cv
        adapter.dataSource = self
        
        // Do any additional setup after loading the view.
        //definesPresentationContext = true
        
        contentView.addSchemedView(cv)
        
        contentWrap.schemedBG(.lightShadow)
        contentWrap.layer.cornerRadius = 20
        contentWrap.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        contentWrap.addSchemedView(contentView)
        
        categories.showsHorizontalScrollIndicator = false
        view.addSchemedView(categories)
        view.addSchemedView(contentWrap)
        
        categories.didSelect = { [unowned self] index in
            self.adapter.scroll(
                to: self.items[index],
                supplementaryKinds: [UICollectionView.elementKindSectionHeader],
                scrollDirection: .vertical,
                scrollPosition: .top,
                animated: true
            )
        }
        
      
        orderVC.handleHasPanned = { [weak self] recogniser in
            self?.popoverHandlePan(recogniser)
        }
        currentPopoverHeight = orderVC.defaultHeight
        add(orderVC)
        
        // color configuration
        view.backgroundColor = .clear
        
        if !hasLoaded {
            hasLoaded = false
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            [weak self] in
            guard let self = self else { return }
            self.currentPopoverHeight = self.orderVC.height(for: .Small)
            self.layoutSubviews()
        }
    }
    
    func load(client: ClientStubResponse?, list: ClientServiceList?) {
        guard let profile = client?.profile, let list = list else { return }
        items = PriceEditor.transform(inputServices: list.services)
        var seenItems = Set<ListableCategory>()
        for item in self.items {
            var out = [EditableServicePrice]()
            for entry in profile.matrix {
                if let index = item.services.firstIndex(where: { $0.service.key == entry.id }) {
                    seenItems.insert(item)
                    let current = item.services[index]
                    out.append(EditableServicePrice(
                        service: current.service,
                        price: entry.price,
                        duration: entry.approxTime
                    ))
                }
            }
            item.services = out
        }
        items = items.filter({ seenItems.contains($0) })
        categories.set(items: items.map { $0.category })
        self.adapter.reloadData()
        DispatchQueue.main.async { [unowned self] in
            self.categories.didSelect?(0)
        }
        hasLoaded = true
        layoutSubviews()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        layoutSubviews()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if self.navigationController?.viewControllers.firstIndex(of: self) == nil {
            HomeCoordinater.shared.orderFSM.process(event: .cancelledServices)
        }

        super.viewWillDisappear(animated)
    }
    
    func resizeCart() {
        orderVC.calculateBounds()
        currentPopoverHeight = min(currentPopoverHeight, orderVC.maximumHeight())
        currentPopoverHeight = max(currentPopoverHeight, orderVC.minimumHeight())
        self.layoutSubviews()
    }
    
    private func layoutSubviews() {
        let padding: CGFloat = 15
        orderVC.view.pin.horizontally().height(currentPopoverHeight).bottom(view.pin.safeArea)
        categories.pin.horizontally(20).height(40).top(safeAreaInsets)
        categories.flex.layout()
        categories.contentSize.width = categories.subviews.last!.frame.maxX
        contentWrap.pin.horizontally().top(safeAreaInsets + 38).bottom(view.pin.safeArea)
        contentView.pin.horizontally(padding).top(20).bottom(orderVC.minimumHeight())
        cv.pin.all()
    }
    
    private var currentPopoverHeight : CGFloat!
    private var startedHeight: CGFloat!
    private func popoverHandlePan(_ sender: UIPanGestureRecognizer) {
        let translation = sender.translation(in: self.view)
        
        defer {
            layoutSubviews()
        }
        
        if sender.state == .began {
            startedHeight = currentPopoverHeight
        }
        
        if let viewToDrag = sender.view {
            currentPopoverHeight += translation.y * -1
            currentPopoverHeight = min(currentPopoverHeight, orderVC.maximumHeight())
            currentPopoverHeight = max(currentPopoverHeight, orderVC.minimumHeight())
            sender.setTranslation(CGPoint(x: 0, y: 0), in: viewToDrag)
        }
        
        if sender.state == .ended {
            let goingDown = startedHeight > currentPopoverHeight
            let (lower, upper) = orderVC.currentRange()
            let ratio = (currentPopoverHeight - lower) / (upper - lower)
            if goingDown {
                currentPopoverHeight = ratio < 0.7 ? lower : upper
            } else {
                currentPopoverHeight = ratio < 0.3 ? lower : upper
            }
        }
    }
    
    func showCart(silent: Bool) {
        if silent && orderVC.showingState != .Large {
            return
        }
        orderVC.calculateBounds()
        currentPopoverHeight = orderVC.maximumHeight()
        UIView.animate(withDuration: 0.1, animations: {
            self.layoutSubviews()
        })
    }
    
    deinit {
        Log("EXPLORE VC DEINITALISED")
    }
    
    override var prefersStatusBarHidden: Bool { return false }
}

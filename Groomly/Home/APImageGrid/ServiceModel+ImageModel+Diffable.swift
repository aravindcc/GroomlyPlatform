//
//  ServiceModel+ImageModel+Diffable.swift
//  Groomly
//
//  Created by Ravi on 16/07/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import Foundation
import IGListKit

extension ServiceModel: ListDiffable {
    func diffIdentifier() -> NSObjectProtocol {
        return key as NSString
    }

    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard let object = object as? ServiceModel else { return false }
        return self.key == object.key
    }
}

extension ServiceGroup {
    var asset: URL? {
        guard let image = image else { return nil }
        #if DEBUG
        return URL(string: "\(Networker.shared.hostURL!)\(image)")
        #else
        return URL(string: image)
        #endif
    }
}

//
//  APImageCell.swift
//  Learning
//
//  Created by clar3 on 19/03/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import PinLayout
import Kingfisher

class KFBasedCell: UICollectionViewCell {
    var imagev = UIImageView()
    private let imageQueue = DispatchQueue(label: "cellImageQueue")
    var url: URL?
    var setImage: URL?
    var image : UIImage? {
        get {
            return imagev.image
        }
    }
    
    func setImageFrom(url: URL, maxDim WIDTH: CGFloat) {
        imageQueue.async {
            guard (url != self.setImage) else { return }
            DispatchQueue.main.sync {
                self.imagev.image = nil
            }
            self.url = url
            let processor = DownsamplingImageProcessorScaled(size: CGSize(width: WIDTH, height: WIDTH))
            KingfisherManager.shared.retrieveImage(with: url, options: [.processor(processor)]) { result in
                switch result {
                case .success(let value):
                    self.imageQueue.async {
                        self.setImage(value.image, at: url)
                    }
                case .failure(let error):
                    Log(error) // The error happens
                }
            }
        }
    }
    
    private func setImage(_ data: UIImage?, at url : URL) {
        guard url == self.url && (url != setImage) else { return }
        setImage = url
        DispatchQueue.main.sync {
            imagev.image = data
        }
    }
}

final class APImageCell: KFBasedCell {
    static let ID = "APImageCellID"
    
    var isChosen: Bool = false {
        didSet {
            imagev.alpha = isChosen ? 0.2 : 1
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        schemedBG(.darkShadow, alpha: 0.6)
        imagev = APCollectionCell.configureBG(image: nil)
        imagev.layer.cornerRadius = 0
        addSchemedView(imagev)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        imagev.pin.all()
    }
    
    func setImage(_ image: UIImage?, backup: URL?, index: Int, assignHeroes: Bool) {
        if assignHeroes {
            hero.id = "\(APImageCell.ID)\(index)"
        }
        guard image == nil else { imagev.image = image; return }
        guard let backup = backup else { return }
        setImageFrom(url: backup, maxDim: 300)
    }
}

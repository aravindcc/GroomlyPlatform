//
//  APImageGridLayout.swift
//  Learning
//
//  Created by clar3 on 19/03/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit

protocol APImageGridLayoutDelegate: AnyObject {
    func collectionView(_ collectionView: UICollectionView, aspectForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat
    var isEmpty : Bool { get }
}

class APImageGridLayout: UICollectionViewLayout {

    weak var delegate: APImageGridLayoutDelegate?

    static let numberOfColumns = 2
    private let numberOfColumns = APImageGridLayout.numberOfColumns
    private let cellPadding: CGFloat = 1


    private var cache: [UICollectionViewLayoutAttributes] = []


    private var contentHeight: CGFloat = 0

    private var contentWidth: CGFloat {
        guard let collectionView = collectionView else {
            return 0
        }
        let insets = collectionView.contentInset
        return collectionView.bounds.width - (insets.left + insets.right)
    }


    override var collectionViewContentSize: CGSize {
        return CGSize(width: contentWidth, height: contentHeight)
    }
    
    func clearCache() {
        cache = []
        invalidateLayout()
    }
  
    override func prepare() {
        guard   cache.isEmpty == true,
                !(delegate?.isEmpty ?? true),
                let collectionView = collectionView,
                collectionView.numberOfSections > 0,
                contentWidth > 0
        else {
            return
        }

        let columnWidth = contentWidth / CGFloat(numberOfColumns)
        var xOffset: [CGFloat] = []
        for column in 0..<numberOfColumns {
            xOffset.append(CGFloat(column) * columnWidth)
        }
        var column = 0
        var yOffset: [CGFloat] = .init(repeating: 0, count: numberOfColumns)
          
        for item in 0..<collectionView.numberOfItems(inSection: 0) {
            let indexPath = IndexPath(item: item, section: 0)
            
            let aspect = delegate?.collectionView(collectionView, aspectForPhotoAtIndexPath: indexPath) ?? 1
            let height = cellPadding * 2 + columnWidth * aspect
            let frame = CGRect( x: xOffset[column],
                                y: yOffset[column],
                                width: columnWidth,
                                height: height)
            let insetFrame = frame.insetBy(dx: cellPadding, dy: cellPadding)

            let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
            attributes.frame = insetFrame
            cache.append(attributes)

            contentHeight = max(contentHeight, frame.maxY)
            yOffset[column] = yOffset[column] + height

            column = column < (numberOfColumns - 1) ? (column + 1) : 0
        }
    }
  
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var visibleLayoutAttributes: [UICollectionViewLayoutAttributes] = []

        // Loop through the cache and look for items in the rect
        for attributes in cache {
            if attributes.frame.intersects(rect) {
                visibleLayoutAttributes.append(attributes)
            }
        }
        return visibleLayoutAttributes
    }
  
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return indexPath.item < cache.count ? cache[indexPath.item] : nil
    }
}


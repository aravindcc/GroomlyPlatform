//
//  APImageGrid.swift
//  Learning
//
//  Created by clar3 on 18/03/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import PinLayout
import Kingfisher
import Hero

protocol ImageCacher: AnyObject {
    var cache: [URL: UIImage] { get set }
    func cacheImages(for items: [ImageModel], downsampleSize: CGSize?) -> Void
}

extension ImageCacher {
    func cacheImages(for items: [ImageModel], downsampleSize: CGSize? = nil) {
        for photo in items {
            // don't worry about caching twice cause Kingfisher will take care of it and only a little wasted time
            guard   let url = photo.image else { continue }
            var processors: [KingfisherOptionsInfoItem] = []
            if let size = downsampleSize {
                let processor = DownsamplingImageProcessorScaled(size: size)
                processors.append(.processor(processor))
            }
            KingfisherManager.shared.retrieveImage(with: url, options: processors) { result in
                switch result {
                case .success(let value):
                    objc_sync_enter(self.cache)
                    defer {
                        objc_sync_exit(self.cache)
                    }
                    self.cache[url] = value.image
                case .failure(let error):
                    Log(error) // The error happens
                }
            }
        }
    }
}

extension Thread {
    static func onMain(_ block: @escaping () -> Void) {
        if isMainThread {
            block()
        } else {
            DispatchQueue.main.async {
                block()
            }
        }
    }
}

protocol SwipeDelegate: AnyObject {
    func swipeLeft() -> Void
    func swipeRight() -> Void
}

class APImageGrid<ImageType: DisplayImageModel>: APSubController, ImageCacher, UICollectionViewDataSource, UICollectionViewDelegate {
    private weak var popover: UIViewController?
    lazy var cv : UICollectionView = {
        let layout = APImageGridLayout()
        layout.delegate = self
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.accessibilityIdentifier = "imageGridCV"
        cv.showsVerticalScrollIndicator = false
        cv.showsHorizontalScrollIndicator = false
        cv.backgroundColor = .clear
        cv.backgroundView = nil
        cv.contentInset = .zero
        cv.dataSource = self
        cv.delegate = self
        cv.register(APImageCell.self, forCellWithReuseIdentifier: "reuseImageCell")
        
        view.addSchemedView(cv)
        return cv
    }()
    var transitionClosure : TransitionVCIndClosure<ImageType>? = nil
    
    
     
    private(set) var selectedMap: [IndexPath: Bool] = [:]
    override var isEditing: Bool {
        didSet {
            if oldValue != isEditing {
                selectedMap = [:]
            }
            cv.reloadData()
        }
    }
    var items : [ImageType] = []
    var cache: [URL: UIImage] = [:]
    var assignHeroes = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .clear
        cv.dataSource = self
        cv.delegate = self
        
        for d in [UISwipeGestureRecognizer.Direction.left, .right] {
            let l = UISwipeGestureRecognizer(target: self, action: #selector(handleswipe(_:)))
            l.direction = d
            view.addGestureRecognizer(l)
        }
    }
    
    weak var swipeDel: SwipeDelegate?
    @objc private func handleswipe(_ recogniser: UISwipeGestureRecognizer) {
        if let swipeDel = swipeDel {
            recogniser.direction == .left ? swipeDel.swipeLeft() : swipeDel.swipeRight()
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        cv.pin.all(view.pin.safeArea)
    }
    
    func set(items: [ImageType]) {
        self.items = items
        
        DispatchQueue.global(qos: .background).async {
            self.cacheImages(for: items, downsampleSize: CGSize(width: 300, height: 300))
        }
        
        Thread.onMain {
            (self.cv.collectionViewLayout as? APImageGridLayout)?.clearCache()
            self.cv.reloadData()
            if self.items.count > 0 {
                self.cv.scrollToItem(at: IndexPath(item: 0, section: 0), at: .top, animated: false)
            }
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "reuseImageCell", for: indexPath) as? APImageCell {
            let index = indexPath.row
            if let url = items[index].image {
                cell.setImage(cache[url], backup: url, index: index, assignHeroes: assignHeroes)
            }
            cell.isChosen = selectedMap[indexPath] == true
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if isEditing {
            let prev = selectedMap[indexPath] ?? false
            (collectionView.cellForItem(at: indexPath) as? APImageCell)?.isChosen = !prev
            selectedMap[indexPath] = !prev
        } else {
            let cell = (collectionView.cellForItem(at: indexPath) as? APImageCell)?.image
            show(model: items[indexPath.row], index: indexPath.row, transition: cell)
        }
    }
    func show(model: ImageType, index: Int, transition: UIImage?) {
        guard popover == nil, let vc = transitionClosure?(model, index, transition) else {
            return
        }
        let presenter = tabBarController ?? navigationController ?? self
        if let vc = vc as? TransitionController {
            vc.heroID = "\(APImageCell.ID)\(index)"
            presenter.present(hero: vc)
            popover = vc
        } else {
            presenter.present(vc, animated: true, completion: nil)
            popover = vc
        }
    }
}

extension APImageGrid: APImageGridLayoutDelegate {
    static func createNiceColumnOrder(items: [ImageType]) -> [ImageType] {        
        let numberOfColumns = APImageGridLayout.numberOfColumns
        let totalNum = items.count
        var aspects: [(CGFloat, Int)] = items.enumerated().map {
            let aspect = aspectFor(model: $0.element)
            return (aspect, $0.offset)
        }
        aspects.sort { (a, b) -> Bool in
            b.0 - a.0 > 0
        }
        var columnIndicies: [[Int]] = Array(repeating: [], count: numberOfColumns)
        var totalAspect: [CGFloat] = Array(repeating: 0, count: numberOfColumns)
        for i in 0..<totalNum {
            let column = i%numberOfColumns
            columnIndicies[column].append(aspects[i].1)
            totalAspect[column] += aspects[i].0
        }
        
        var columnOrder = totalAspect.enumerated().sorted { (a, b) -> Bool in
            a.element - b.element > 0
        }.map { $0.offset }
        columnOrder.swapAt(1, 2)
        columnIndicies = columnOrder.map { columnIndicies[$0] }
        
        var out = [Int]()
        var track = Array(repeating: 0, count: numberOfColumns)
        columnIndicies = columnIndicies.map { $0.shuffled }
        for i in 0..<totalNum {
            let column = i%numberOfColumns
            out.append(columnIndicies[column][track[column]])
            track[column] += 1
        }
        return out.map { items[$0] }
    }

    static func aspectFor(model object: ImageType) -> CGFloat {
//        .clamped(from: 0.8, to: 1.5)
//        let aspect = (object.height / object.width)
//        return CGFloat(aspect)
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, aspectForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat {
        guard indexPath.row < items.count else { return 1 }
        return APImageGrid.aspectFor(model: items[indexPath.row])
    }
    var isEmpty: Bool { return items.count == 0 }
}

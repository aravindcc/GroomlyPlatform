//
//  ListBackend.swift
//  Groomly
//
//  Created by Ravi on 01/08/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import Foundation
import IGListKit
import Async

class ListBackend<Model: ListDiffable>: NSObject {
    
    typealias RefreshRequest = ([Model]) -> Bool
    typealias RefreshPassiveRequest = () -> Bool
    private var responders: [(RefreshRequest, Bool)] = []
    private var passiveResponders: [RefreshPassiveRequest] = []
    var cachedList: [Model] = [] {
        didSet {
            queue.async {
                DispatchQueue.main.sync {
                    self.notify()
                }
            }
        }
    }
    var selected: Model? {
        get {
            let ind = getSelected()
            if ind < cachedList.count {
                return cachedList[ind]
            }
            return nil
        }
    }
    var sendsNotification = true
    var silentRefresh: Bool {
        get {
            responders.first(where: { $0.1 == true }) == nil
        }
    }
    let queue = DispatchQueue(label: "ListBackend")
    
    func load() {
        queue.async {
            self.refreshList()
        }
    }
    
    func clear() {
        responders = []
        passiveResponders = []
        cachedList = []
    }
    
    func subscribe(onSelectChange: @escaping RefreshPassiveRequest) {
        let _ = onSelectChange()
        queue.async {
            self.passiveResponders.append(onSelectChange)
        }
    }
    func subscribe<Cell: UICollectionViewCell>(_ request: APListController<Model, Cell>, inForeground: Bool) {
        let requestClosure: RefreshRequest = { [weak request] list in
            guard let request = request else { return false }
            request.items = list
            request.refresh()
            return true
        }
        queue.async {
            DispatchQueue.main.sync {
                let _ = requestClosure(self.cachedList)
            }
            self.responders.append((requestClosure, inForeground))
        }
    }
    func notify() {
        responders = responders.filter { $0.0(cachedList) }
        if sendsNotification {
            passiveResponders = passiveResponders.filter { $0() }
        }
    }
    
    func run(
        process: Future<[Model]>?,
        loadingMessage: String?,
        successMessage: String?,
        errorMessage: String?,
        silentSuccess: Bool = true
    ) {
        run(
            process: process, loadingMessage: loadingMessage, successMessage: successMessage,
            errorMessage: errorMessage == nil ? nil : { _ in errorMessage! },
            silentSuccess: silentSuccess
        )
    }
    
    func run(
        process: Future<[Model]>?,
        loadingMessage: String?,
        successMessage: String?,
        errorMessage: ((Error) -> String)?,
        silentSuccess: Bool = true
    ) {
        Message.notify(
            process: process,
            loadingMessage: loadingMessage,
            successMessage: successMessage,
            errorMessage: errorMessage,
            silentSuccess: silentSuccess
        ) { [weak self] (result) in
            if case .success(let items) = result {
                self?.cachedList = items
            }
        }
    }
    
    // marker functions - they must set cached login
    func add(model: Model) {}
    func remove(model: Model) {}
    func select(model: Model) {}
    func refreshList() {}
    func getSelected() -> Int { 0 }
}

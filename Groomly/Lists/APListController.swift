//
//  APListController.swift
//  Groomly
//
//  Created by clar3 on 14/05/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import IGListKit

struct ButtonConfig {
    init(title: String, configAction: ButtonCompletion? = nil, action: @escaping ButtonCompletion) {
        self.title = title
        self.action = action
        self.configAction = configAction
    }
    
    let title: String
    let action: ButtonCompletion
    let configAction: ButtonCompletion?
}

class APListNewController<Model: ListDiffable>: APSubController {
    weak var apiDataSource: ListBackend<Model>?
}

enum APListModeReduced {
    case embedded, edit
    case selection(onSelect: Completion)
    case full(onSelect: Completion)
}

enum APListMode<Model: ListDiffable>: Equatable {
    static func == (lhs: APListMode, rhs: APListMode) -> Bool {
        switch (lhs, rhs) {
        case (.embedded, .embedded):
            return true
        case (.selection, .selection):
            return true
        case (.edit, .edit):
            return true
        case (.full, .full):
            return true
        default:
            return false
        }
    }
    
    case embedded
    case selection(doneTitle: String, onSelect: Completion)
    case edit(onNew: @autoclosure () -> APListNewController<Model>)
    case full(doneTitle: String, onSelect: Completion, onNew: @autoclosure () -> APListNewController<Model>)
    
    var embeds: Bool { self == .embedded }
}

class APListController<Model: ListDiffable, Cell: UICollectionViewCell>: APSubController, ListAdapterDataSource {
    let containerView = UIView()
    let optionView = UIView()
    var localSelectedIndex = 0
    var selectedIndex: Int {
        get {
            apiDataSource?.getSelected() ?? localSelectedIndex
        } set {
            if let source = apiDataSource {
                source.select(model: self.items[newValue])
            } else {
                localSelectedIndex = newValue
            }
        }
    }
    var didSetEditing: (Completion)?
    var editingList = false {
        didSet {
            didSetEditing?()
        }
    }
    var rowHeight: CGFloat { return 65.0 }
    var buttons: [ButtonConfig] = []
    var items: [Model] = []
    weak var apiDataSource: ListBackend<Model>? {
        didSet {
            apiDataSource?.subscribe(self, inForeground: self.viewType != .embedded)
        }
    }
    var selectionItems: [ListInfoModel] {
        get {
            items.enumerated().map { (i, obj) in
                ListInfoModel(obj, selected: i == selectedIndex, editing: editingList)
            }
        }
    }
    private lazy var cv : UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        view.addSchemedView(cv)
        return cv
    }()
    lazy var adapter: ListAdapter = {
        return ListAdapter(updater: ListAdapterUpdater(), viewController: self)
    }()
    
    var notSelectedString: String {
        "Please choose an option"
    }
    private var endButton: FlexButton?
    private let viewType: APListMode<Model>
    init(viewType: APListMode<Model>) {
        self.viewType = viewType
        super.init(nibName: nil, bundle: nil)
        
        let configureButtons = {
            (title: String?, onSelect: Completion?, onNew: (() -> APListNewController<Model>)?) in
            self.buttons = []
            if let onNew = onNew {
                let editPersist = { [weak self] (sender: FlatButton) in
                    guard let self = self else { return }
                    sender.title = self.editingList ? "ok" : "edit"
                    UIView.animate(withDuration: 0.3) {
                        if !self.editingList {
                            self.optionView.subviews.forEach {$0.alpha = 1}
                            self.endButton?.enabled = self.selectedIndex < self.items.count
                        } else {
                            self.optionView.subviews.forEach {$0.alpha = 0}
                            sender.alpha = 1
                        }
                    }
                }
                self.buttons = [
                    ButtonConfig(title: "new") {
                        [weak self] _ in
                        let vc = onNew()
                        vc.apiDataSource = self?.apiDataSource
                        self?.push(vc: vc)
                    },
                    ButtonConfig(title: "edit", configAction: { [weak self] sender in
                        self?.didSetEditing = { editPersist(sender) }
                    }) {
                        [weak self] sender in
                        guard let self = self else { return }
                        self.toggleEditing()
                        editPersist(sender)
                    },
                ]
            }
            if let title = title, let onSelect = onSelect {
                self.buttons += [
                    ButtonConfig(title: title) {
                        [weak self] _ in
                        guard let self = self else { return }
                        if self.selectedIndex >= self.selectionItems.count {
                            Feedback.showNotification(message: NotificationDetails(title: self.notSelectedString))
                            return
                        }
                        self.dismiss(animated: true)
                        onSelect()
                    }
                ]
            }
        }

        switch viewType {
        case let .edit(onNew):
            configureButtons(nil, nil, onNew)
        case let .full(title, onSelect, onNew):
            configureButtons(title, onSelect, onNew)
        case let .selection(title, onSelect):
            configureButtons(title, onSelect, nil)
        default:
            return
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if viewType.embeds {
            view.backgroundColor = .clear
        } else {
            view.colorScheme = .normal
            view.schemedBG()
        }
        view.addSchemedView(containerView)
        
        cv.showsVerticalScrollIndicator = false
        cv.showsHorizontalScrollIndicator = false
        cv.backgroundColor = .clear
        cv.contentInset = .zero
        adapter.collectionView = cv
        adapter.dataSource = self
    
        containerView.flex.marginHorizontal(viewType.embeds ? 0 : 20).alignItems(.stretch).define { (flex) in
            flex.addItem(cv).grow(1)
            if !viewType.embeds {
                flex.addItem(optionView)
                    .height(40)
                    .alignItems(.stretch)
                    .marginBottom(30)
                    .marginTop(20)
                    .direction(.row)
                    .justifyContent(.spaceAround)
                    .define { (flex) in
                    for (i, config) in buttons.enumerated() {
                        let but = FlexButton()
                        but.title = config.title
                        but.preset = true
                        but.tapped = config.action
                        config.configAction?(but)
                        flex.addItem(but).width(but.wrappedWidth)
                        if
                            i == buttons.count - 1,
                            case .selection = viewType,
                            case .full = viewType {
                            endButton = but
                            endButton?.enabled = selectedIndex < items.count
                        }
                    }
                }
            }
        }
    }
    
    func picked(_ model: ListInfoModel) {
        guard let (index, _) = selectionItems.enumerated().filter({ $0.1.isEqual(toDiffableObject: model) }).first else { return }
        if model.editing {
            if index == selectedIndex { return } // absorb
            self.apiDataSource?.remove(model: self.items[index])
            self.endButton?.enabled = self.selectedIndex < self.items.count
            self.toggleEditing(force: false)
        } else {
            selectedIndex = index
            adapter.performUpdates(animated: false)
        }
    }
    
    func refresh() {
        adapter.performUpdates(animated: true)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            let selectionItems = self.selectionItems
            let selectedIndex = self.selectedIndex
            if selectionItems.count > 0 && selectedIndex < selectionItems.count {
                self.adapter.scroll(
                    to: selectionItems[selectedIndex],
                    supplementaryKinds: nil,
                    scrollDirection: .vertical,
                    scrollPosition: .top,
                    animated: true
                )
            }
        }
    }
    
    func toggleEditing(force: Bool? = nil) {
        editingList = force ?? !editingList
        adapter.performUpdates(animated: true)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        containerView.pin.all(view.pin.safeArea)
        containerView.flex.layout()
    }
    
    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        return selectionItems
    }
    
    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        return APListSectionController<Cell>(object: object, rowHeight: rowHeight) { [weak self] obj in self?.picked(obj) }
    }
    
    func emptyView(for listAdapter: ListAdapter) -> UIView? {
        return nil
    }
}


class APListSectionController<Cell: UICollectionViewCell>: ListSectionController {

    private var picked: ((ListInfoModel) -> ())?
    private var object: Any
    private let rowHeight: CGFloat
    
    init(object: Any, rowHeight: CGFloat, onSelectAddress fun: ((ListInfoModel) -> ())? = nil) {
        self.rowHeight = rowHeight
        self.object = object
        self.picked = fun
        super.init()
    }
    override func numberOfItems() -> Int {
        return 1
    }
    
    override func sizeForItem(at index: Int) -> CGSize {
        guard let width = collectionContext?.containerSize.width else { return .zero }
        return CGSize(width: width, height: rowHeight)
    }

    override func cellForItem(at index: Int) -> UICollectionViewCell {
        guard   let cell = schemedReusableCell(of: Cell.self, for: self, at: index) as? UICollectionViewCell & ListBindable,
                let object = object as? ListInfoModel
        else { fatalError() }
        cell.bindViewModel(object)
        return cell
    }

    override func didUpdate(to object: Any) {
        if let object = object as? ListInfoModel {
            self.object = object
        }
    }

    override func didSelectItem(at index: Int) {
        if let object = object as? ListInfoModel {
            picked?(object)
        }
    }
}

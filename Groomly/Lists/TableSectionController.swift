//
//  TableSectionController.swift
//  Groomly
//
//  Created by Ravi on 22/08/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import UIKit
import IGListKit

final class TableSectionController<Entry: AnyObject, Cell: UICollectionViewCell & ListBindable>: ListSectionController {
    private var object: Entry?
    var didSelect: ((Entry) -> Void)?
    override func numberOfItems() -> Int {
        return 1
    }
    
    let cellHeight: CGFloat
    init(cellHeight: CGFloat = 55.0, onSelect: ((Entry) -> Void)? = nil) {
        self.cellHeight = cellHeight
        self.didSelect = onSelect
        super.init()
    }
    
    override func sizeForItem(at index: Int) -> CGSize {
        guard let width = collectionContext?.containerSize.width else { return .zero }
        return CGSize(width: width, height: cellHeight)
    }

    override func cellForItem(at index: Int) -> UICollectionViewCell {
        guard   let cell = schemedReusableCell(of: Cell.self, for: self, at: index) as? UICollectionViewCell & ListBindable,
                let object = object
        else { fatalError() }
        cell.bindViewModel(object)
        return cell
    }

    override func didUpdate(to object: Any) {
        self.object = object as? Entry
    }

    override func didSelectItem(at index: Int) {
        guard let object = object else { return }
        didSelect?(object)
    }
}

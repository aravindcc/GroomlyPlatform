//
//  APListEntryCell.swift
//  Groomly
//
//  Created by clar3 on 14/05/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//


import UIKit
import IGListKit
import PinLayout
import FlexLayout

extension UIView {
    func blink(fadeTime: Double = 0.1, brightTime: Double = 0.1) {
        let previousAlpha = alpha
        UIView.animate(withDuration: fadeTime, animations: {
            self.alpha = 0
        }) { _ in
            UIView.animate(withDuration: brightTime) {
                self.alpha = previousAlpha
            }
        }
    }
}

class ListInfoModel: ListDiffable {
    func diffIdentifier() -> NSObjectProtocol {
        return "\(model.diffIdentifier().description)\(selected)" as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        if self === object { return true }
        guard   let object = object as? ListInfoModel
            else { return false }
        return  model.isEqual(toDiffableObject: object.model) &&
                selected == object.selected &&
                editing == object.editing
    }
    
    var selected: Bool
    var editing: Bool
    var model: ListDiffable
    
    init(_ model: ListDiffable, selected: Bool = false, editing: Bool = false) {
        self.model = model
        self.selected = selected
        self.editing = editing
    }
}

class APListEntryCell: UICollectionViewCell, ListBindable {
    let infoView = UIView()
    private let container = UIView()
    private let checkedImageV = UIImageView()
    private var isEditing: Bool = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)

        checkedImageV.image = UIImage(systemName: "checkmark")
        contentView.addSchemedView(container)
        
        container.flex.direction(.row).alignItems(.center).marginVertical(10).define { (flex) in
            flex.addItem(checkedImageV).height(20).aspectRatio(1).marginHorizontal(10)
            flex.addItem(infoView).alignSelf(.stretch).grow(1).alignItems(.stretch)
        }
        checkedImageV.schemedTintColor(.primaryTextColor)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        container.pin.all()
        container.flex.layout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        if isEditing {
            if point.x < infoView.frame.minX {
                checkedImageV.blink()
                return super.hitTest(point, with: event)
            }
            return nil
        }
        return super.hitTest(point, with: event)
    }
    
    func bindViewModel(_ viewModel: Any) {
        guard let info = viewModel as? ListInfoModel  else { return }
        isEditing = !info.selected && info.editing
        let ifNotSelected = { () -> Bool in
            if info.selected {
                self.checkedImageV.schemedTintColor(.primaryTextColor, register: false)
                self.checkedImageV.image = UIImage(systemName: "checkmark")
                self.checkedImageV.alpha = info.editing ? 0.5 : 1
            }
            return !info.selected
        }
        
        if info.editing {
            if ifNotSelected() {
                checkedImageV.tintColor = AppColors.red
                checkedImageV.image = UIImage(systemName: "trash")
                checkedImageV.alpha = 1
            }
        } else {
            if ifNotSelected() {
                checkedImageV.alpha = 0
            }
        }
    }
}

